import os
import sys
import yaml

sys.path.append(os.path.join('src', 'python'))

from buildtools import log, os_utils
from buildtools.indentation import IndentWriter

from twinehack.ast import nodes
from twinehack.ast.parser import PassageParser
from twinehack.utils import CamelCasify
from slimit4twine.visitors import nodevisitor
from slimit4twine import ast

PROJ_BASE = os.path.join('projects', 'hacked')
PASSAGE_BASE = os.path.join('src', 'tiddler', 'passages')
DROP = frozenset([
    '$cantCapture', # Unused, only really used by Phoenix and Nefirian.
    '$noSnare', # Set, but unused.
    '$noStasis', # Set, but not used.
])
VarMap = {
    '$encounterRace':    'Race',
    '$encounterSubtype': 'Subtype',
    #'$creatureColor':    'Color',
    '$encounterElement': 'Element',
    '$encounterDesc':    'Desc',
    '$creatureDesc':     'ModDesc',
    '$creatureInfo':     'Info',
    '$creatureRarity':   'Rarity',
    '$creatureIntro':    'Intro',
    '$creatureCalories': 'Calories',
    '$creatureGirth':    'Girth',
    '$moveset':          'MoveSet',
    '$bribeAction':      'BribeAction',
    '$bribeID':          'BribeID',
    '$bribeName':        'BribeName',
    '$bribeType':        'BribeType',
    '$bribeQuantity':    'BribeQuantity',
    '$attackFed':        'FedVerbage',
    '$encounterWeapon':  'Weapon',
    '$lootRolls':        'LootRolls',
    '$encounterFinesse': 'Finesse',
    '$encounterMaxPain': 'MaxPain',
    '$encounterPower':   'Power',
    #'$encounterSpecial': 'Specials'
    '$lootTable':        'LootTable',
    '$venomType':        'VenomType',
    '$injectType':       'InjectType',
    '$submissionDesc':   'SubmissionText',
    '$encImg':           'Image',
    '$encLanguage':      'Language',
}
QualifiedVarMap = {k:'@'+v for k,v in VarMap.items() if v is not None}

ECreatureFlags = {
    '$canSubmit': 'ACCEPTS_SUBMISSION',
    '$trapCapture': 'TRAPPABLE',
    '$noCapture': 'CAPTURE_RESISTANT',
    '$stasisImmune': 'STASIS_IMMUNE',
    '$noAbsorb': 'NO_ABSORB',
    '$noFatMod': 'NO_FAT_MOD',
    '$naturalColorpool': 'NATURAL_COLORS',
    '$aggro': 'AGGRESSIVE',
    '$combatant': 'COMBATANT',
    '$brave': 'BRAVE',
    '$miniDonsair': 'MINI_DONSAIR',
    '$noButcher': 'NO_BUTCHER',
    '$encounterUnique': 'UNIQUE',
    '$creatureBreasts': 'HAS_BREASTS',
}

EFavorDeity = {
    '$nefirianFavored': 'NEFIRIAN',
    '$phoenixFavored': 'PHOENIX',
    '$namelessFavored': 'NAMELESS',
    '$kaolanFavored': 'KAOLAN',
}

VarToItem = {
    '$waterResource': 'Water',
    '$milkResource': 'Milk',
    '$meatResource': 'Meat',
    '$fishResource': 'Fish',
    '$poultryResource': 'Poultry',
    '$eggResource': 'Egg',
    '$grainResource': 'Grain',
    '$rootResource': 'Roots',
    '$fruitResource': 'Fruit',
    '$vegResource': 'Vegetable',
    '$sugarResource': 'Sugar',
    '$nutResource': 'Nuts',
    '$fatResource': 'Fat',
    '$herbResource': 'Herbs',
    '$jellyResource': 'Jelly',
    '$dragonResource': 'DragonMeat',
    '$leupaiResource': 'LeupaiFlesh',
    '$fatteniumResource': 'FatteniumPowder',
    '$miraResource': 'MiracrystalDust',
    #<<set $resourceLiquid = 0>>
    #<<set $traceFattenium = 0>>
    #<<set $traceMirajin = 0>>
}

ALL_UNHANDLED = set()


def processNode(destination: dict, node: ast.Node, root: bool) -> dict:
    for child in node.children():
        # print(str(child))
        if isinstance(child, nodes.TweeIfNode):
            if 'Blocks' not in destination:
                destination['Blocks'] = []
            ifBlock = {}
            for condition in child.children():
                condstr = ' '+nodes.treeToString(nodes.prepTreeForCoffee(condition.condition)) if condition.condition is not None else ''
                key = condition.tagName + condstr
                data = processNode({}, condition, False)
                if data is None:
                    return None
                ifBlock[key] = data
            destination['Blocks'].append(ifBlock)
        if isinstance(child, nodes.TweeSetNode):
            tsnTree = ast.Block(child.source_elements)
            # dump_slimit_struct(tsnTree)
            tsnTree = nodes.fixCompsInTree(tsnTree)
            for tjsNode in nodevisitor.visit(tsnTree):
                if isinstance(tjsNode, ast.Assign):
                    varID = tjsNode.left.value.strip('\"\'')
                    if varID in DROP:
                        continue
                    elif varID in VarMap.keys():
                        if VarMap[varID] is None:
                            continue
                        destination[VarMap[varID]] = nodes.simplify(tjsNode.right, True)
                    elif varID in ECreatureFlags.keys():
                        if isinstance(tjsNode.right, ast.Number):
                            if 'Flags' not in destination:
                                destination['Flags'] = {}
                            key = ''
                            if tjsNode.right.value == '1':
                                key = 'always'
                            elif tjsNode.right.value == '0':
                                key = 'never'
                            else:
                                key = tjsNode.right.to_ecma()
                            if key not in destination['Flags']:
                                destination['Flags'][key] = []
                            destination['Flags'][key].append(ECreatureFlags[varID])
                    elif varID in VarToItem.keys():
                        if isinstance(tjsNode.right, ast.Number) and tjsNode.right.value == '1':
                            if 'PossibleLoot' not in destination:
                                destination['PossibleLoot'] = []
                            destination['PossibleLoot'].append(VarToItem[varID])
                    elif varID in EFavorDeity.keys():
                        destination['FavoredBy'] = EFavorDeity[varID]
                        destination['FavorCost'] = nodes.simplify(tjsNode.right, fail_to_raw=True)
                    elif varID == '$encounterStructure':
                        destination['Structure'] = tjsNode.right.value.upper().strip('\"\'')
                    elif varID == '$killmode':
                        if isinstance(tjsNode.right, ast.FunctionCall):
                            for i in range(len(tjsNode.right.args)):
                                argn = tjsNode.right.args[i]
                                if isinstance(argn, ast.Identifier):
                                    tjsNode.right.args[i] = argn.value.upper().strip('\"\'')
                        else:
                            destination['KillMode'] = tjsNode.right.value.upper().strip('\"\'')
                    elif varID == '$creatureColor':
                        if 'Colors' not in destination:
                            destination['Colors'] = []
                        if isinstance(tjsNode.right, ast.FunctionCall):
                            # print(tjsNode.right.identifier.value)
                            if tjsNode.right.identifier.value == 'either':
                                destination['Colors'] = [str(x).strip('\"\'') for x in nodes.either2list(tjsNode.right)]
                        else:
                            destination['Colors'] = [tjsNode.right.value.strip('\"\'')]
                    elif varID == '$encounterSpecial':
                        if 'Colors' not in destination:
                            destination['Colors'] = []
                        if isinstance(tjsNode.right, ast.FunctionCall):
                            # print(tjsNode.right.identifier.value)
                            if tjsNode.right.identifier.value == 'either':
                                destination['Specials'] = [str(x).strip('\"\'') for x in nodes.either2list(tjsNode.right)]
                        if isinstance(tjsNode.right, ast.Array):
                            destination['Specials'] = [x.value.strip('\"\'') for x in tjsNode.right.items]
                    elif varID in ('$encounter'):
                        return None
                    else:
                        #log.warning('UNHANDLED VARIABLE: ' + tjsNode.to_ecma())
                        if 'UNHANDLED' not in destination:
                            destination['UNHANDLED'] = {}
                        destination['UNHANDLED'][varID] = nodes.simplify(tjsNode.right, True)
                        ALL_UNHANDLED.add(varID)
    return destination


def processCreature(node):
    condition = node.GetConditionAsStr()
    # print(condition)
    if condition.startswith('$encounter is'):
        tree = node.GetConditionAsTree()
        binop = nodes.get_first_of(tree, ast.BinOp)
        creatureID = binop.right.value.strip('"')
        CreatureData = {
            'Race': creatureID,
            'MoveSet': [],
            'Colors': [],
        }
        CreatureData = processNode(CreatureData, node, True)
        if CreatureData is None:
            return
        CREATURES[creatureID] = CreatureData
        log.info('Processed %s', creatureID)


CREATURES = {}
pp = PassageParser()
pp.parseFile(os.path.join(PROJ_BASE, PASSAGE_BASE, 'CreatureDatabase.tw'))
for node in pp.passage.GetAllChildren():
    if isinstance(node, nodes.TweeIfBlockNode):
        processCreature(node)
with open(os.path.join(PROJ_BASE, 'src', 'data', 'creatures.yml'), 'w') as f:
    yaml.dump(CREATURES, f, default_flow_style=False)


def printFlagsSet(w,varName,enumName,flagList):
    if len(flagList) > 0:
        w.writeline('@{} |= ({})'.format(varName, '|'.join([enumName + '.' + x for x in flagList])))
def printFlagsUnset(w,varName,enumName,flagList):
    if len(flagList) == 1:
        w.writeline('@{} &= ~{}'.format(varName, flagList[0]))
    elif len(flagList) > 1:
        w.writeline('@{} &= ~({})'.format(varName, '|'.join([enumName + '.' + x for x in flagList])))
def printFlags(w, creature, varName, enumName, root):
    flagDefs = creature[varName]
    for k,flagList in flagDefs.items():
        if 'always' == k:
            if root:
                w.writeline('@{} = ({})'.format(varName, '|'.join([enumName + '.' + x for x in flagList])))
            else:
                printFlagsSet(w,varName,enumName,flagList)
        elif 'never' == k:
            if not root:
                printFlagsUnset(w,varName,enumName,flagList)
        else:
            with w.writeline('if '+k):
                printFlagsSet(w, varName, enumName, flagList)
            with w.writeline('else'):
                printFlagsUnset(w, varName, enumName, flagList)


def printEnum(w, creature, varName, enumName):
    w.writeline('@{} = {}'.format(varName, enumName + '.' + creature[varName]))


def writeCreature(w: IndentWriter, creature: dict, root: bool):
    if root:
        w.writeline('super \'{}\''.format(creature['Race']))
    #w.writeline('@Flags = '+'|'.join(['ECreatureFlags.'+x for x in creature['Flags']]))
    if root and 'Flags' in creature:
        printFlags(w, creature, 'Flags', 'ECreatureFlags', True)
    #printEnum(w, 'KillMode', 'EKillMode')
    dropped={}
    for k, v in creature.items():
        if k == 'Blocks':
            for block in v:
                for condition, children in block.items():
                    with w.writeline(condition):
                        writeCreature(w, children, False)
            continue
        if k in ('Race', 'Flags') and root:
            continue
        if k == 'UNHANDLED': continue
        if k in DROP:
            dropped[k]=v
            continue
        if k == 'Flags':
            printFlags(w, creature, 'Flags', 'ECreatureFlags', False)
            continue
        if k == 'FavoredBy':
            printEnum(w, creature, 'FavoredBy', 'EFavorDeity')
            continue
        if k == 'KillMode':
            printEnum(w, creature, 'KillMode', 'EKillMode')
            continue
        if k == 'Structure':
            printEnum(w, creature, 'Structure', 'EEncounterStructure')
            continue
        if k == 'PossibleLoot':
            for item in v:
                w.writeline('@PossibleLoot.push "{}"'.format(item))
            continue
        if isinstance(v, nodes.RawTJSWrapper):
            v = nodes.prepTreeForCoffee(v.val)
        #origT = type(v)
        if isinstance(v, str):
            v = repr(v)
        if isinstance(v, ast.Node):
            v = nodes.treeToString(v)
        w.writeline('@{} = {}'.format(k, v))
    if 'UNHANDLED' in creature:
        w.writeline('############')
        w.writeline('# UNHANDLED:')
        w.writeline('############')
        for k,v in creature['UNHANDLED'].items():
            if isinstance(v, nodes.RawTJSWrapper):
                v = nodes.treeToString(v.val)
            elif isinstance(v, str):
                v = repr(v)
            tjs = '{} = {}'.format(k,v)
            #log.info(tjs)
            cTree = nodes.prepTreeForCoffee(nodes.parse_twinescript(tjs))
            coff = nodes.treeToString(cTree)
            #log.info(coff)
            w.writeline(coff)
with log.info('Writing creatures.coffee...'):
    with open('tmp/creatures.coffee', 'w') as f:
        w = IndentWriter(f)
        for creature in CREATURES.values():
            if isinstance(creature['Race'], nodes.RawTJSWrapper):
                continue
            with w.writeline('class {} extends Creature'.format(CamelCasify(creature['Race']))):
                with w.writeline('constructor: ->'):
                    writeCreature(w, creature, True)

os_utils.ensureDirExists('tmp/creatures')
for creature in CREATURES.values():
    if isinstance(creature['Race'], nodes.RawTJSWrapper):
        continue
    clsName = CamelCasify(creature['Race'])
    with log.info('Writing %s.coffee...', clsName):
        with open('tmp/creatures/{}.coffee'.format(clsName), 'w') as f:
            w = IndentWriter(f)
            with w.writeline('class {} extends Creature'.format(clsName)):
                with w.writeline('constructor: ->'):
                    writeCreature(w, creature, True)
with log.info('All unhandled variables:'):
    for unh in sorted(ALL_UNHANDLED):
        log.info('* %s', unh)
