try {

version.extensions.whileMacros = {
    major: 1, minor: 0, revision: 0
};

macros['while'] = {
  handler: function(place, macroName, params, parser) {
    var srcOffset = parser.source.indexOf(">>", parser.matchStart) + 2,
      src = parser.source.slice(srcOffset),
      endPos = -1,
      condition = parser.fullArgs().trim(),
      body = "",
      nestlevel = 0,
      i = 0;

    for (; i < src.length; i++) {
      if (src.substr(i, 8) == "<<while ") {
        nestlevel++;
      }
      if (src.substr(i, 12) == "<<endwhile>>") {
        nestlevel--;
        if (nestlevel < 0) {
          endPos = srcOffset + i + 12;
          break;
        }
      }
      body += src.charAt(i);
    }
    body = body.trim();

    if (endPos != -1) {
      parser.nextMatch = endPos;
      try {
        while (internalEval(condition)) {
          new Wikifier(place, body);
        }
      } catch (e) {
        throwError(place, "<<while>> bad condition: " + condition, parser.fullMatch());
      }
    } else {
      throwError(place, "I can't find a matching <<endwhile>>", parser.fullMatch());
    }
  },
  init: function() { }
};

macros["endwhile"] = {
  handler: function () {}
};

} catch(e) {
  throwError(place,"Macro while Error: "+e.message);
}
