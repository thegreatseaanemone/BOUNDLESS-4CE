import os, codecs, shutil, re, yaml
from buildtools import log, os_utils
from buildtools.maestro.base_target import SingleBuildTarget

REG_IMAGEURL=re.compile(r'url\("([^"]+)"\)')
BASE_URI = 'http://download.jqueryui.com/themeroller/images/'
class DownloadJQueryUIIconsTarget(SingleBuildTarget):
    BT_TYPE = 'DownloadJQueryUIIcons'
    BT_LABEL = 'DLICONS'
    def __init__(self, target, origfile, basedir, dependencies=[]):
        self.origfile = origfile
        self.basedir = basedir
        super(type(self), self).__init__(target, dependencies=dependencies, files=[os.path.abspath(__file__), self.origfile])

    def serialize(self):
        data = super(type(self), self).serialize()
        data['basedir']=self.basedir
        if os.path.abspath(__file__) in data['files']:
            data['files'].remove(os.path.abspath(__file__))
        return data

    def deserialize(self, data):
        super(type(self), self).deserialize(data)
        self.basedir = data['basedir']
        if os.path.abspath(__file__) not in data['files']:
            data['files'].append(os.path.abspath(__file__))

    def build(self):
        os_utils.ensureDirExists(self.basedir)

        NEWCONFIG = {}
        with open(self.origfile, 'r') as f:
            for line in f:
                for url in REG_IMAGEURL.findall(line):
                    if url.startswith("images/"):
                        chunks = url[7:].split('_')
                        key = '_'.join([chunks[0], '{COLOR}', chunks[-1]])
                        color = '_'.join(chunks[1:-1])
                        if key not in NEWCONFIG:
                            NEWCONFIG[key]=[]
                        if color not in NEWCONFIG[key]:
                            NEWCONFIG[key].append(color)
        with open(self.target, 'w') as w:
            yaml.dump(NEWCONFIG, w, default_flow_style=False)
        for _filename, colors in NEWCONFIG.items():
            for color in colors:
                filename = _filename.format(COLOR=color)
                dlurl = BASE_URI+filename
                #log.info(dlurl)
                target = os.path.join(self.basedir, 'images', filename)
                os_utils.ensureDirExists(os.path.dirname(target))
                if not os.path.isfile(target):
                    os_utils.cmd(['wget', '-O', target, dlurl], critical=True, show_output=True, echo=True)
