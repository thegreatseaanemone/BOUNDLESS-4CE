from boundless.generators.gods.base import GodInfo, God

class MinorGod(GodInfo):
    def __init__(self, capitalizedID, title=None):
        super(MinorGod, self).__init__()
        self._quicksetup(capitalizedID)
        self.name = title or capitalizedID
# ''([A-Za-z']+):'' <<print \$favor([A-Za-z]+)>>( ♦ )?
# \n@God\nclass God$2(MinorGod):\n    def __init__(self):\n        super(type(self), self).__init__('$2', '$1')

@God
class GodAnder(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Ander', 'Ander')
@God
class GodLyric(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Lyric', 'Lyric\'Ai')
@God
class GodReks(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Reks', 'Reks')

@God
class GodNath(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Nath', 'Na\'than')
@God
class GodEva(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Eva', 'Eva')
@God
class GodAndreu(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Andreu', 'Andreu')
@God
class GodEliot(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Eliot', 'Eliot')

@God
class GodCello(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Cello', 'Cello')
@God
class GodTristan(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Tristan', 'Tristan')
@God
class GodTroie(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Troie', 'Troie')

@God
class GodArches(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Arches', 'Arches')
@God
class GodSilent(MinorGod):
    def __init__(self):
        super(type(self), self).__init__('Silent', 'Silent')
