from enum import auto, IntFlag


class GodFlags(IntFlag):
    MAJOR_PLAYER = auto()


class GodInfo(object):

    def __init__(self):
        self.name = ''
        self.favorVar = ''
        self.mobFavorVar = ''
        self.icon = ''
        self.flags = GodFlags(0)
        self.offering_info={}

    def _quicksetup(self, capitalized):
        self.name = capitalized
        self.favorVar = '$favor'+capitalized
        self.mobFavorVar = '${}Favored'.format(capitalized.lower())

    def setup_writer(self, w):
        w.variables = {
            'GOD_NAME': self.name,
            'GOD_FAVOR_VAR': self.favorVar,
            'GOD_MOB_FAVOR_VAR': self.mobFavorVar,
            'GOD_ICON': self.icon
        }


def DefineGod():
    registry = {}

    def handler(_class):
        # packageID = _class.__module__ + "." + _class.__name__
        _id = _class().name
        registry[_id] = _class
        return _class
    handler.all = registry
    return handler

God = DefineGod()


def get_gods_sorted():
    for name in sorted(God.all.keys()):
        yield God.all[name]()

def get_god_by_name(name):
    return God.all[name]()
