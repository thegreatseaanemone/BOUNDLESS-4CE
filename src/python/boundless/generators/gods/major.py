from boundless.generators.gods.base import GodInfo, God, GodFlags

class MajorGod(GodInfo):
    def __init__(self):
        super(MajorGod, self).__init__()
        self.flags |= GodFlags.MAJOR_PLAYER

@God
class GodNefirian(MajorGod):

    def __init__(self):
        super(type(self), self).__init__()
        self.name = 'Nefirian'
        self.favorVar = '$favorNefirian'
        self.mobFavorVar = '$nefirianFavored'
        self.icon = 'icon_eye'


@God
class GodPhoenix(MajorGod):

    def __init__(self):
        super(type(self), self).__init__()
        self.name = 'Phoenix'
        self.favorVar = '$favorPhoenix'
        self.mobFavorVar = '$phoenixFavored'
        self.icon = 'icon_sigil'


@God
class GodNameless(MajorGod):

    def __init__(self):
        super(type(self), self).__init__()
        self.name = 'Nameless'
        self.favorVar = '$favorNameless'
        self.mobFavorVar = '$namelessFavored'
        self.icon = 'icon_crescent'


@God
class GodKaolan(MajorGod):

    def __init__(self):
        super(type(self), self).__init__()
        self.name = 'Kaolan'
        self.favorVar = '$favorKaolan'
        self.mobFavorVar = '$kaolanFavored'
        self.icon = 'icon_eagle'
@God
class GodGiovan(MajorGod):

    def __init__(self):
        super(type(self), self).__init__()
        self.name = 'Giovan'
        self.favorVar = '$favorGiovan'
        self.mobFavorVar = '$giovanFavored'
        self.icon = 'icon_blackhole'
