import datetime
import enum
import os
import re
import sys
import typing
from buildtools import log, os_utils
from buildtools.indentation import IndentWriter
from buildtools.maestro.base_target import SingleBuildTarget

import yaml
from slimit4twine import ast
from boundless.generators.gods import (GodFlags, get_god_by_name,
                                       get_gods_sorted)
from twinehack.ast import nodes
from twinehack.ast.parser import PassageParser

moddir = os.path.dirname(os.path.abspath(__file__))
depfiles = [
    os.path.abspath(__file__),
    os.path.join(moddir, 'gods', '__init__.py'),
    os.path.join(moddir, 'gods', 'base.py'),
    os.path.join(moddir, 'gods', 'major.py'),
    os.path.join(moddir, 'gods', 'minor.py'),
]


class BuildTargetGenFavorEditorTwee(SingleBuildTarget):
    BT_TYPE = 'GenFavorEditor'
    BT_LABEL = 'FAVOREDITOR'

    def __init__(self, target, dependencies=[]):
        super(BuildTargetGenFavorEditorTwee, self).__init__(target, dependencies=dependencies, files=[os.path.abspath(__file__)])

    def build(self):
        os_utils.ensureDirExists(os.path.dirname(self.target))
        major = []
        minor = []
        for god in get_gods_sorted():
            if god.flags & GodFlags.MAJOR_PLAYER:
                major += [god]
            else:
                minor += [god]
        log.debug('Found %d major gods', len(major))
        log.debug('Found %d minor gods', len(minor))
        with open(self.target, 'w') as f:
            out = IndentWriter(f)
            out.writeline('<h2>Favor Editor</h2>')
            out.writeline('<h3>Major Gods</h3>')
            for god in major:
                god.setup_writer(out)
                # out.writeline('<<nobr>>')
                out.writeline('<fieldset>')
                out.writeline('<legend>[img[{GOD_ICON}]] {GOD_NAME}</legend>')
                out.writeline('<<button [[MIN|passage()][{GOD_FAVOR_VAR} = -500]]>>')
                out.writeline('<<button [[-100|passage()][{GOD_FAVOR_VAR} = Math.max({GOD_FAVOR_VAR}-100, -500)]]>>')
                out.writeline('<<button [[-10|passage()][{GOD_FAVOR_VAR} = Math.max({GOD_FAVOR_VAR}-10, -500)]]>>')
                out.writeline('<<button [[-1|passage()][{GOD_FAVOR_VAR} = Math.max({GOD_FAVOR_VAR}-1, -500)]]>>')
                out.writeline('<<print {GOD_FAVOR_VAR}>>')
                out.writeline('<<button [[+1|passage()][{GOD_FAVOR_VAR} = Math.min({GOD_FAVOR_VAR}+1, 500)]]>>')
                out.writeline('<<button [[+10|passage()][{GOD_FAVOR_VAR} = Math.min({GOD_FAVOR_VAR}+10, 500)]]>>')
                out.writeline('<<button [[+100|passage()][{GOD_FAVOR_VAR} = Math.min({GOD_FAVOR_VAR}+100, 500)]]>>')
                out.writeline('<<button [[MAX|passage()][{GOD_FAVOR_VAR} = 500]]>>')
                out.writeline('</fieldset>')
                # out.writeline('<<endnobr>>')
            out.writeline('<h3>Minor Gods</h3>')
            for god in minor:
                god.setup_writer(out)
                # out.writeline('<<nobr>>')
                out.writeline('<fieldset>')
                out.writeline('<legend>{GOD_NAME}</legend>')
                out.writeline('<<button [[MIN|passage()][{GOD_FAVOR_VAR} = -500]]>>')
                out.writeline('<<button [[-100|passage()][{GOD_FAVOR_VAR} = Math.max({GOD_FAVOR_VAR}-100, -500)]]>>')
                out.writeline('<<button [[-10|passage()][{GOD_FAVOR_VAR} = Math.max({GOD_FAVOR_VAR}-10, -500)]]>>')
                out.writeline('<<button [[-1|passage()][{GOD_FAVOR_VAR} = Math.max({GOD_FAVOR_VAR}-1, -500)]]>>')
                out.writeline('<<print {GOD_FAVOR_VAR}>>')
                out.writeline('<<button [[+1|passage()][{GOD_FAVOR_VAR} = Math.min({GOD_FAVOR_VAR}+1, 500)]]>>')
                out.writeline('<<button [[+10|passage()][{GOD_FAVOR_VAR} = Math.min({GOD_FAVOR_VAR}+10, 500)]]>>')
                out.writeline('<<button [[+100|passage()][{GOD_FAVOR_VAR} = Math.min({GOD_FAVOR_VAR}+100, 500)]]>>')
                out.writeline('<<button [[MAX|passage()][{GOD_FAVOR_VAR} = 500]]>>')
                out.writeline('</fieldset>')
                # out.writeline('<<endnobr>>')


class EFavorCategory(enum.IntEnum):
    FAVORABLE = enum.auto()
    UNFAVORABLE = enum.auto()
    OTHER = enum.auto()
    NEF_FORCEFEED = enum.auto()
    NEF_EATIT = enum.auto()


favcat2name = {
    EFavorCategory.FAVORABLE: 'Favorable',
    EFavorCategory.UNFAVORABLE: 'Unfavorable',
    EFavorCategory.OTHER: 'Other',
    EFavorCategory.NEF_FORCEFEED: 'Forcefed',
    EFavorCategory.NEF_EATIT: 'Encouraged',
}


def get_cat_name(catID: EFavorCategory) -> str:
    return favcat2name[catID]


class OfferingInfo(object):
    def __init__(self):
        self.ItemID = ''
        self.ItemCount = 0
        self.FavorDeltas = []
        self.FavorMessage = ''
        self.FavorType = EFavorType.PLAIN
        self.FavorCategory = EFavorCategory.OTHER
        self.OtherCommands = []
        self.ItemsRemoved = {}
        self.ItemsAdded = {}
        self.Rejected = False
        self.TrueItemCount = 0


class EFavorType(enum.IntEnum):
    RANDOM = enum.auto()
    EITHER = enum.auto()
    PLAIN = enum.auto()


class CommandInfo(object):
    def __init__(self):
        self.Name = ''
        self.Desc = ''
        self.Aliases = []


class GenCommandDocs(SingleBuildTarget):
    BT_LABEL='DOCUMENT'
    def __init__(self, infile, outfile):
        self.inputfile = infile
        self.outputfile = outfile
        super().__init__(outfile, files=[infile], dependencies=[])

    def build(self):
        #import logging
        # log.log.setLevel(logging.DEBUG)
        Commands = {}
        with log.debug('Analyzing %s...', self.outputfile):
            pp = PassageParser()
            pp.parseFile(self.inputfile)
            for curnode in pp.passage.GetAllChildren():
                if isinstance(curnode, nodes.TweeIfNode):
                    tifn = curnode  # type: twinehack.ast.nodes.TweeIfNode
                    for block in tifn.children():
                        #log.info(f'{block.tagName} {block.GetConditionAsStr()!r}')
                        if block.tagName != 'else' and '$abitteCommanded' in block.GetConditionAsStr():
                            curCommand = CommandInfo()
                            for cmdchild in block.children():
                                if isinstance(cmdchild, nodes.TweeCommentNode):
                                    cmdchild.value = cmdchild.value[2:-2]
                                    log.debug(cmdchild.value.strip())
                                    if cmdchild.value.strip().lower().startswith('command: '):
                                        '''
                                        /% Command: Transfers to Nef's 'true' abittes, with a non-humanoid statue and other weird shit (think about it) %/
                                        '''
                                        curCommand.Desc = cmdchild.value.strip().split(':', 2)[1].strip()
                                    elif cmdchild.value.strip().startswith('Name: '):
                                        '''
                                        /% Name: True Form %/
                                        '''
                                        curCommand.Name = cmdchild.value.strip().split(':', 2)[1].strip()

                            with log.debug(block.condition.to_ecma()):
                                #nodes.dump_slimit_struct(block.condition)
                                for cmdalias in list(nodes.NodeVisitor().visit(block.condition))+[block.condition]:
                                    '''
                                    <<if ($abitteCommanded == "unveil") or ($abitteCommanded == "true form") or ($abitteCommanded == "show me your true form")>>
                                    '''
                                    if isinstance(cmdalias, ast.BinOp) and isinstance(cmdalias.left, ast.Identifier) and cmdalias.left.value == '$abitteCommanded':
                                        #log.info(cmdalias.to_ecma())
                                        curCommand.Aliases.append(cmdalias.right.value.strip('"\''))
                            if curCommand.Name == '':
                                #print(repr(curCommand.Aliases))
                                curCommand.Name = curCommand.Aliases[0]
                            Commands[curCommand.Name] = curCommand
        os_utils.ensureDirExists(os.path.dirname(self.outputfile))
        with open(self.outputfile, 'w', encoding='utf-8') as f:
            f.write('{{{{SpoilerStart}}}}\n{{{{Note|Generated automatically from {} on {}.}}}}\n'.format(os.path.basename(self.inputfile), datetime.datetime.now()))
            for cmd in sorted(Commands.values(), key=lambda x: x.Name):
                f.write(f'{{{{AdministreCommand\n|Name={cmd.Name}\n|Desc={cmd.Desc}\n|Aliases=\n')
                for alias in sorted(cmd.Aliases):
                    f.write(f'* {alias}\n')
                f.write(f'}}}}\n')
            f.write('{{SpoilerEnd}}\n')


class GenFavorDocs(SingleBuildTarget):
    BT_LABEL='DOCUMENT'
    def __init__(self, favor_mwdoc_file, offerings_file, godID):
        self.god=godID
        self.offerings_file = offerings_file
        self.favor_mwdoc_file = favor_mwdoc_file
        super().__init__(favor_mwdoc_file, files=[offerings_file], dependencies=[])

    def build(self):
        #import logging
        # log.log.setLevel(logging.DEBUG)
        Offerings = {}
        currentGod = None
        with log.debug('Analyzing %s...', self.offerings_file):
            pp = PassageParser()
            pp.parseFile(self.offerings_file)
            for curnode in pp.passage.GetAllChildren():
                if isinstance(curnode, nodes.TweeIfNode):
                    # <<elseif $administreStatue is "Nefirian">>
                    tifn = curnode  # type: twinehack.ast.nodes.TweeIfNode
                    for block in tifn.children():
                        #log.info(f'{block.tagName} {block.GetConditionAsStr()!r}')
                        if block.tagName != 'else' and block.GetConditionAsStr().startswith('$administreStatue is "'):
                            lenstart = len('$administreStatue is "')
                            curGodID = block.GetConditionAsStr()[lenstart:-1]
                            if curGodID != self.god:
                                continue
                            currentGod = get_god_by_name(curGodID)
                            Offerings[currentGod] = {}
                            for favcat in list(EFavorCategory):
                                Offerings[currentGod][favcat] = []
                            with log.debug(curGodID):
                                for itemif in nodes.NodeVisitor().visit(block):
                                    if isinstance(itemif, nodes.TweeIfNode):
                                        '''
10/07/2017 09:19:26 PM [INFO    ]:   $player.GetItemCount(BalloonThighedLeupaiCarving) gt 0
10/13/2017 06:17:08 PM [INFO    ]:     <slimit4twine.ast.Program object at 0x0000000005BD3940>
10/13/2017 06:17:08 PM [INFO    ]:       <slimit4twine.ast.ExprStatement object at 0x0000000005C3C470>
10/13/2017 06:17:08 PM [INFO    ]:         <slimit4twine.ast.BinOp object at 0x0000000005BD3BE0>             gt
10/13/2017 06:17:08 PM [INFO    ]:           <slimit4twine.ast.FunctionCall object at 0x0000000005BF03C8>
10/13/2017 06:17:08 PM [INFO    ]:             <slimit4twine.ast.DotAccessor object at 0x0000000005BD3630>   .
10/13/2017 06:17:08 PM [INFO    ]:               <slimit4twine.ast.Identifier object at 0x0000000005C3CE10>  $player
10/13/2017 06:17:08 PM [INFO    ]:               <slimit4twine.ast.Identifier object at 0x0000000005277F98>  GetItemAmount
10/13/2017 06:17:08 PM [INFO    ]:             <slimit4twine.ast.Identifier object at 0x0000000005BF02B0>    BalloonThighedLeupaiCarving
10/13/2017 06:17:08 PM [INFO    ]:           <slimit4twine.ast.Number object at 0x0000000005C3C898>          0
                                        '''
                                        # log.info(itemif.GetFirstConditionAsStr())
                                        if itemif.GetFirstConditionAsStr().startswith('$player.GetItemCount'):
                                            offerspec = OfferingInfo()
                                            ictree = itemif.GetFirstConditionAsTree()
                                            # nodes.dump_slimit_struct(ictree) #FI XME
                                            # sys.exit(1) #FI XME
                                            binop = nodes.get_first_of(ictree, ast.BinOp)
                                            offset = 0 if binop.op in ('>=', 'gte') else 1
                                            offerspec.TrueItemCount = offerspec.ItemCount = int(binop.right.value)  # Left side of the comparison.
                                            offerspec.ItemCount += offset
                                            offerspec.ItemID = nodes.get_first_of(ictree, ast.FunctionCall).args[0].value  # ItemID in the function call.

                                            for buttonnode in itemif.children()[0].children():
                                                if isinstance(buttonnode, nodes.TweeButtonNode):
                                                    actions = buttonnode.link.callback
                                                    # nodes.dump_slimit_struct(actions)
                                                    for action in actions:
                                                        if isinstance(action, ast.ExprStatement):
                                                            action = action.expr
                                                        if isinstance(action, ast.FunctionCall):
                                                            ident = action.identifier.to_ecma()  # Cheating so I don't have to parse long DotAccessors.
                                                            if ident == '$player.RemoveItem':
                                                                itemid, amt = nodes.either2list(action)
                                                                #log.info('- %s %d', itemid, amt)
                                                                offerspec.ItemsRemoved[itemid] = amt
                                                                continue
                                                            if ident == '$player.AddItem':
                                                                itemid, amt = nodes.either2list(action)
                                                                offerspec.ItemsAdded[itemid] = amt
                                                                #log.info('+ %s %d', itemid, amt)
                                                                continue
                                                        elif isinstance(action, ast.Assign):
                                                            lhs, op, rhs = action.left, action.op, action.right
                                                            if isinstance(lhs, ast.Identifier):
                                                                if lhs.value == '$service' and rhs.value == '1':
                                                                    continue
                                                                if lhs.value == '$rejectOffering' and rhs.value == '1':
                                                                    offerspec.Rejected = True
                                                                    continue
                                                                elif lhs.value == currentGod.favorVar:
                                                                    if isinstance(rhs, ast.FunctionCall):
                                                                        if 'either' == rhs.identifier.value:
                                                                            eitherout = nodes.either2list(rhs)
                                                                            offerspec.FavorDeltas = list(sorted(set(eitherout)))
                                                                            offerspec.FavorType = EFavorType.EITHER
                                                                        elif 'random' == rhs.identifier.value:
                                                                            offerspec.FavorDeltas = [int(x) for x in nodes.either2list(rhs)]
                                                                            offerspec.FavorType = EFavorType.RANDOM
                                                                    else:
                                                                        offerspec.FavorDeltas = [int(rhs.value)]
                                                                        offerspec.FavorType = EFavorType.PLAIN
                                                                    if op == '-=':
                                                                        offerspec.FavorDeltas = [-x for x in offerspec.FavorDeltas]
                                                                elif lhs.value == '$adminResponse':
                                                                    offerspec.FavorMessage = rhs.value[1:-1]
                                                                    continue
                                                                else:
                                                                    offerspec.OtherCommands += [action.to_ecma()]
                                                                continue
                                                        else:
                                                            offerspec.OtherCommands += [action.to_ecma()]
                                            if offerspec.FavorMessage in ("can feel Nefirian gently urging you to use it on jeir behalf!", "can feel Nefirian gently urging you to drink it on jeir behalf!", "can feel Nefirian gently urging you to eat it on jeir behalf!"):
                                                offerspec.FavorCategory = EFavorCategory.NEF_EATIT
                                            elif "cry out in shock" in offerspec.FavorMessage:
                                                offerspec.FavorCategory = EFavorCategory.NEF_FORCEFEED
                                            elif sum(offerspec.FavorDeltas) < 0:
                                                offerspec.FavorCategory = EFavorCategory.UNFAVORABLE
                                            elif sum(offerspec.FavorDeltas) > 0:
                                                offerspec.FavorCategory = EFavorCategory.FAVORABLE
                                            else:
                                                offerspec.FavorCategory = EFavorCategory.OTHER
                                            Offerings[currentGod][offerspec.FavorCategory] += [offerspec]
        #log.info('All referenced vars: %r', known_vars)
        with open(self.favor_mwdoc_file, 'w', encoding='utf-8') as f:
            for god, godstuff in Offerings.items():
                f.write('{{{{SpoilerStart}}}}\n{{{{Note|Generated automatically from {} on {}.}}}}\n'.format(os.path.basename(self.offerings_file), datetime.datetime.now()))
                for catID, catcontents in godstuff.items():
                    if len(catcontents) == 0:
                        continue
                    f.write('{| class="wikitable sortable"\n')
                    f.write('|+ {}\n'.format(get_cat_name(catID)))
                    f.write('|-\n')
                    f.write('! Item ID\n')
                    f.write('! Item Count\n')
                    f.write('! Favor Change\n')
                    f.write('! Notes\n')
                    for offerspec in catcontents:
                        f.write('|-\n')
                        f.write(f'! {offerspec.ItemID}\n')
                        f.write(f'| {offerspec.ItemCount}\n')
                        if offerspec.FavorType == EFavorType.PLAIN and len(offerspec.FavorDeltas) == 1:
                            f.write(f'| {offerspec.FavorDeltas[0]}\n')
                        elif offerspec.FavorType == EFavorType.EITHER:
                            f.write('| Any of: {}\n'.format(', '.join([str(x) for x in offerspec.FavorDeltas])))
                        elif offerspec.FavorType == EFavorType.RANDOM:
                            f.write('| Random: {} to {}\n'.format(offerspec.FavorDeltas[0], offerspec.FavorDeltas[1]))
                        elif len(offerspec.FavorDeltas) == 0:
                            f.write('| 0\n')
                        else:
                            f.write('| BROKEN: {!r}\n'.format(offerspec.FavorDeltas))

                        weirdshit = []
                        foundRemoved = False
                        if len(offerspec.ItemsRemoved) > 0:
                            for itemID, amt in offerspec.ItemsRemoved.items():
                                if itemID == offerspec.ItemID:
                                    if amt != offerspec.ItemCount:
                                        weirdshit += ['* Requires at least {}, but removes {} instead!\n'.format(offerspec.ItemCount, amt)]
                                    foundRemoved = True
                                else:
                                    weirdshit += ['* Removes {} &times; {}\n'.format(amt, itemID)]
                        if not foundRemoved and not offerspec.Rejected:
                            weirdshit += ["* Does '''NOT''' remove any {}!\n".format(offerspec.ItemID)]
                        if len(offerspec.ItemsAdded) > 0:
                            for itemID, amt in offerspec.ItemsRemoved.items():
                                weirdshit += ['* Adds {} &times; {}\n'.format(amt, itemID)]
                        if offerspec.Rejected:
                            weirdshit += ['* Offer is rejected\n']
                        if len(offerspec.OtherCommands) > 0:
                            weirdshit += ['* Also does:\n']
                            for line in offerspec.OtherCommands:
                                weirdshit += ['** <code>{}</code>\n'.format(line)]
                        if len(weirdshit) > 0:
                            f.write('|\n')
                            for weird in weirdshit:
                                f.write(weird)

                    f.write('|}\n\n')
                f.write('\n{{SpoilerEnd}}\n')
