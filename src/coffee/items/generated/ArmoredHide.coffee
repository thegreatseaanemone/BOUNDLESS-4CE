class ArmoredHide extends Item
  constructor: ->
    super()
    @ID = 'hide_armored'
    @Type = 'gubbin'
    @Name = 'Armored Hide'
    @Encounter = 'armored hide'
    @Description = "Tough, thick-plated beast's hide."
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ArmoredHide

