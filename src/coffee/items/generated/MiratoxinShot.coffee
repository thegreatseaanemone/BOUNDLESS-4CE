class MiratoxinShot extends Item
  constructor: ->
    super()
    @ID = 'shot_miratoxin'
    @Type = 'status'
    @Name = 'Miratoxin Shot'
    @Encounter = 'miratoxin shot'
    @Description = 'An injector full of leupai venom. The mutagenic kind.'
    # [5000.0, 5000.0, 5000.0]
    # OLD: @Value = 5000.0
    @Value = 5000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'Your flesh seems to crawl as your entire body spasms!\'\'</deepalert> "'
      getVariables().miraPoisoning += random(8, 25)
      getVariables().pain += random(2, 7)
    return
InventoryController.RegisterItemType MiratoxinShot

