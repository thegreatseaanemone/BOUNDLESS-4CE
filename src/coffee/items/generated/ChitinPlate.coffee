class ChitinPlate extends Item
  constructor: ->
    super()
    @ID = 'chitin_plate'
    @Type = 'gubbin'
    @Name = 'Chitin Plate'
    @Encounter = 'chitin plate'
    @Description = 'A hard chunk of exoskeleton.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ChitinPlate

