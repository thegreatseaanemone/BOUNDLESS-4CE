class RoastLeupai extends Item
  constructor: ->
    super()
    @ID = 'cooked_leupai'
    @Type = 'food'
    @Name = 'Roast Leupai'
    @Encounter = ''
    @Description = 'Chubby crystalizardslugfae make for good eating. Who knew? Well, just ask the leupai...'
    @Value = 30
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(49, 155)
      getVariables().bellyBloat += random(24, 55)
      getVariables().bonusEnergy += random(16, 38)
      getVariables().miraPoisoning += random(0, 2)
      getVariables().pain -= random(5, 16)
      getVariables().favorNefirian += random(0, 2)
    return
InventoryController.RegisterItemType RoastLeupai

