class HypothesisPowerHelmet extends Item
  constructor: ->
    super()
    @ID = 'set_hypo_head'
    @Type = 'wearable'
    @Name = 'HYPOTHESIS Power Helmet'
    @Encounter = 'HYPOTHESIS power helmet'
    @Description = 'A helmet with all sorts of sensors and communications equipment.'
    @Value = 2500
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType HypothesisPowerHelmet

