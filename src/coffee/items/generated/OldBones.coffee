class OldBones extends Item
  constructor: ->
    super()
    @ID = 'bones_old'
    @Type = ''
    @Name = 'Old Bones'
    @Encounter = ''
    @Description = 'Chewed and yellowed. What did these come from?'
    @Value = 0
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType OldBones

