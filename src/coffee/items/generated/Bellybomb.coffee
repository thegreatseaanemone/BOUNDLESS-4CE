class Bellybomb extends Item
  constructor: ->
    super()
    @ID = 'pill_bellybomb'
    @Type = 'status'
    @Name = 'Bellybomb'
    @Encounter = 'bellybomb'
    @Description = 'A drug that instantly bloats the belly up, frequently used for recreation among the Revix.'
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 3)
      getVariables().bellyLiquid += 10
      if getVariables().itemEffect >= 3
        window.twinePrint '"You groan softly as \'\'your belly tightens, swelling up rounder..!\'\'"'
        getVariables().bellyBloat += (getVariables().maxBelly / 2)
        getVariables().inflaToxicity += random(1, 5)
      else if getVariables().itemEffect == 2
        window.twinePrint '"You groan and squirm as \'\'your belly blows up like a balloon, creaking softly as it expands...!\'\'"'
        getVariables().bellyBloat = getVariables().maxBelly * 1.1
        getVariables().inflaToxicity += random(3, 8)
      else if getVariables().itemEffect == 1
        window.twinePrint '"You writhe and moan aloud \'\'as your belly suddenly blimps up //enormously//, creaking loudly as it stretches tight!\'\'"'
        getVariables().bellyBloat = getVariables().maxBelly * 1.2
        getVariables().inflaToxicity += random(6, 10)
        getVariables().pain += random(1, 3)
    return
InventoryController.RegisterItemType Bellybomb

