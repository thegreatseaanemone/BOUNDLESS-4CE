class ThickPelt extends Item
  constructor: ->
    super()
    @ID = 'pelt_thick'
    @Type = 'gubbin'
    @Name = 'Thick Pelt'
    @Encounter = 'thick pelt'
    @Description = 'A warm, fluffy pelt-- great for insulation from the cold.'
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ThickPelt

