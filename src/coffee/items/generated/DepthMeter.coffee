class DepthMeter extends Item
  constructor: ->
    super()
    @ID = 'special_depthmeter'
    @Type = 'gubbin'
    @Name = 'Depth Meter'
    @Encounter = 'depth meter'
    @Description = 'A handy little device that allows for more deliberate travel through subterranean areas.'
    @Value = 5000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType DepthMeter

