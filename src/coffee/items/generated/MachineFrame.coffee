class MachineFrame extends Item
  constructor: ->
    super()
    @ID = 'machine_frame'
    @Type = 'gubbin'
    @Name = 'Machine Frame'
    @Encounter = ''
    @Description = 'A solid steel frame that machine parts bolt on to.'
    @Value = 10000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType MachineFrame

