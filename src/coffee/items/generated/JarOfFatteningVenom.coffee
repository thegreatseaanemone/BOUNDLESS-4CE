class JarOfFatteningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_gain'
    @Type = 'status'
    @Name = 'Jar of Fattening Venom'
    @Encounter = 'jar of fattening venom'
    @Description = 'A strange sort of venom that causes steady weight gain. The result of mirajin mutation?'
    # [1200.0, 1200.0, 1200.0]
    # OLD: @Value = 1200.0
    @Value = 1200.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().gainVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfFatteningVenom

