class IndulgenceBiscuits extends Item
  constructor: ->
    super()
    @ID = 'pastry_indulgence'
    @Type = 'food'
    @Name = 'Indulgence Biscuits'
    @Encounter = 'indulgence biscuits'
    @Description = 'A small package of nondescript cookies.'
    @Value = 15
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 70
      getVariables().bellyBloat += 30
      getVariables().bonusEnergy += 30
    return
InventoryController.RegisterItemType IndulgenceBiscuits

