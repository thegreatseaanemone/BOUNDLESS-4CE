class Bulkberry extends Item
  constructor: ->
    super()
    @ID = 'berry_bulk'
    @Type = 'status'
    @Name = 'Bulkberry'
    @Encounter = 'bulkberries'
    @Description = "These bumpy little fruits can do interesting things to one's physique, depending on the circumstances."
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      twineNewline()
      if (getVariables().calories >= 100) && (getVariables().bonusEnergy >= 100)
        window.twinePrint '"You gasp as \'\'you plump up, your muscles bulging slightly underneath!\'\' "'
        getVariables().strength += random(0.1, 1.3)
        getVariables().fatteningSurge = random(4, 16)
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        twineNewline()
      else if (getVariables().calories >= 100) && (getVariables().bonusEnergy <= 100)
        window.twinePrint '"You shiver a bit as \'\'you plump up!\'\'"'
        getVariables().fatteningSurge = random(4, 12)
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        twineNewline()
      else if (getVariables().calories <= 100) && (getVariables().bonusEnergy >= 100)
        window.twinePrint '"You wince as \'\'your muscles expand slightly!\'\'"'
        getVariables().strength += random(0.1, 1.1)
        twineNewline()
      else
        window.twinePrint '"You shiver as \'\'your body seems to shrink away a bit...\'\'"'
        window.twineDisplayInline 'WeightLoss'
        window.twineDisplayInline 'WeightLoss'
      twineNewline()
      twineNewline()
      window.twinePrint '" Maybe it\'s what you\'ve been eating lately...?"'
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Bulkberry

