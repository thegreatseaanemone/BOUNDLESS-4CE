class BalloonBreastedLeupaiCarving extends Item
  constructor: ->
    super()
    @ID = 'special_carving_breast'
    @Type = 'gubbin'
    @Name = 'Balloon-Breasted Leupai Carving'
    @Encounter = 'balloon-breasted leupai carving'
    @Description = 'A little crystal carving in the shape of a leupai with huge, balloon breasts. Holding it makes your own chest tingle...'
    @Value = 0
    @Tags = ['cursed']
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BalloonBreastedLeupaiCarving

