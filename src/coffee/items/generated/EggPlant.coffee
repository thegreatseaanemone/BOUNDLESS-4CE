class EggPlant extends Item
  constructor: ->
    super()
    @ID = 'plant_egg'
    @Type = 'statfood'
    @Name = 'Plant Egg'
    @Encounter = 'egg plant'
    @Description = 'What.'
    @Value = 300
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"Delicious!"'
      twineNewline()
      twineNewline()
      getVariables().calories += random(20, 80)
      getVariables().randomizer = random(1, 16)
      twineNewline()
      twineNewline()
      if getVariables().randomizer == 1
        # Leupai eggs 
        twineNewline()
        getVariables().bellyBloat += (getVariables().maxBelly * 1.5)
        window.twineDisplayInline 'LeupaiEggGen'
        twineNewline()
        twineNewline()
      else if (getVariables().randomizer >= 8) && (getVariables().randomizer <= 10)
        # Calorie dump 
        twineNewline()
        getVariables().calories += random(100, 500)
        getVariables().bellyBloat += (getVariables().maxBelly * 1.5)
        twineNewline()
        twineNewline()
      else if getVariables().randomizer == 11
        # Dire worm eggs 
        twineNewline()
        getVariables().direParasite = 1
        getVariables().direParaLoad += random(50, 999)
        getVariables().bellyBloat += (getVariables().maxBelly * 1.5)
        twineNewline()
        twineNewline()
      else if getVariables().randomizer == 12
        # Amplibaenes 
        twineNewline()
        getVariables().ampliParasite = 1
        getVariables().ampliParaLoad += either(1, 1, 1, 2, 3)
        twineNewline()
        twineNewline()
      else if getVariables().randomizer == 15
        # Daedlan gene 
        twineNewline()
        getVariables().geneDaedlan = 1
        twineNewline()
        twineNewline()
      else if getVariables().randomizer == 16
        # Ovivore gene 
        twineNewline()
        getVariables().geneOvivore = 1
        twineNewline()
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType EggPlant

