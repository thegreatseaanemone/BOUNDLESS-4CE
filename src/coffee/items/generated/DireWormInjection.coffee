class DireWormInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_worm'
    @Type = 'status'
    @Name = 'Dire Worm Injection'
    @Encounter = 'dire worm injection'
    @Description = 'A syringe full of dire worm eggs...'
    # [600.0, 600.0]
    # OLD: @Value = 600.0
    @Value = 600.0 # item.Value
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\'"'
      getVariables().direParasite = 1
      getVariables().direParaLoad += either(50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150)
    return
InventoryController.RegisterItemType DireWormInjection

