class HospitalArmband extends Item
  constructor: ->
    super()
    @ID = 'set_hospital_arms'
    @Type = 'wearable'
    @Name = 'hospital armband'
    @Encounter = 'hospital armband'
    @Description = "Printed with something in a strange, squiggly language you can't read."
    @Value = 3000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType HospitalArmband

