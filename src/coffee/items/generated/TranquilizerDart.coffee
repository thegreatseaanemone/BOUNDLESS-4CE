class TranquilizerDart extends Item
  constructor: ->
    super()
    @ID = 'dart_tranq'
    @Type = 'gubbin'
    @Name = 'Tranquilizer Dart'
    @Encounter = 'tranquilizer dart'
    @Description = 'Full of a potent serum for inducing sleep.'
    # [266.6666666666667, 200.0, 200.0]
    # OLD: @Value = 222.22222222222226
    @Value = 222.22 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType TranquilizerDart

