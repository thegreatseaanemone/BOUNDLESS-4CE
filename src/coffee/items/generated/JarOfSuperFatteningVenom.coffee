class JarOfSuperFatteningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_supergain'
    @Type = 'status'
    @Name = 'Jar of Super-Fattening Venom'
    @Encounter = 'jar of super-fattening venom'
    @Description = 'This gunk will help you rapidly pack on the pounds, but at what cost?'
    @Value = 5000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().supergainVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfSuperFatteningVenom

