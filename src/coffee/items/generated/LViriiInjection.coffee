class LViriiInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_parasite_virii'
    @Type = 'status'
    @Name = 'L. virii Injection'
    @Encounter = 'M. virii injection'
    @Description = 'A syringe full of Lactosa virii micro-organisms, which cause and gradually increase lactation.'
    @Value = 5000
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\'"'
      getVariables().lactationParasite = 1
      getVariables().lacParaLoad += 10
    return
InventoryController.RegisterItemType LViriiInjection

