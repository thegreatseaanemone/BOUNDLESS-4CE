class Gravel extends Item
  constructor: ->
    super()
    @ID = 'material_gravel'
    @Type = ''
    @Name = 'Gravel'
    @Encounter = ''
    @Description = "Crunchy... but please don't eat it."
    @Value = 1
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Gravel

