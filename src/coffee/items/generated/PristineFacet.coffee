class PristineFacet extends Item
  constructor: ->
    super()
    @ID = 'facet_pristine'
    @Type = 'gubbin'
    @Name = 'Pristine Facet'
    @Encounter = 'pristine facet'
    @Description = 'All that remains of a slain leupai. This one is unmarred.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType PristineFacet

