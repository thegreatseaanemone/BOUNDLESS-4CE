class CaloriePowder extends Item
  constructor: ->
    super()
    @ID = 'powder_calorie'
    @Type = 'status'
    @Name = 'Calorie Powder'
    @Encounter = 'calorie powder'
    @Description = 'A powdered formula that becomes a delicious milkshake when mixed with liquid. Handy for bulking up quickly.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your belly gurgles oddly, and you rub it carefully.\'\'"'
      getVariables().calories += either(30, 40, 50, 60)
      getVariables().calpowderToxicity += random(1, 6)
    return
InventoryController.RegisterItemType CaloriePowder

