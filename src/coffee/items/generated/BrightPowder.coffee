class BrightPowder extends Item
  constructor: ->
    super()
    @ID = 'material_powder_bright'
    @Type = 'gubbin'
    @Name = 'Bright Powder'
    @Encounter = 'bright powder'
    @Description = 'A handful of sparkling, iridescent dust...'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BrightPowder

