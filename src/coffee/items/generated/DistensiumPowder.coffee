class DistensiumPowder extends Item
  constructor: ->
    super()
    @ID = 'distensiumStock'
    @Type = 'food'
    @Name = 'Distensium Powder'
    @Encounter = ''
    @Description = 'A handful of powdered distensium ore.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyBloat += either(70, 80, 90, 100)
    return
InventoryController.RegisterItemType DistensiumPowder

