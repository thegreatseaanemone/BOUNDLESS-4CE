class CablePart extends Item
  constructor: ->
    super()
    @ID = 'cable_part'
    @Type = 'gubbin'
    @Name = 'Electrical Cable'
    @Encounter = ''
    @Description = 'A strong wire for use in high-current applications.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType CablePart

