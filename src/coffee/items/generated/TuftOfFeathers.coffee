class TuftOfFeathers extends Item
  constructor: ->
    super()
    @ID = 'feather_tuft'
    @Type = 'gubbin'
    @Name = 'Tuft of Feathers'
    @Encounter = 'tuft of feathers'
    @Description = 'A handful of downy feathers.'
    # [0.5, 0.5]
    # OLD: @Value = 0.5
    @Value = 0.5 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType TuftOfFeathers

