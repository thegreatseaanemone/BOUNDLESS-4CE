class ImperialApple extends Item
  constructor: ->
    super()
    @ID = 'produce_apple'
    @Type = 'food'
    @Name = 'Imperial Apple'
    @Encounter = 'imperial apple'
    @Description = 'A monster of an apple, fit for a king.'
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(5, 20)
      getVariables().liquidBloat = random(40, 85)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(24, 60)
      getVariables().bellyLiquid += (getVariables().liquidBloat / 2)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(8, 16)
      getVariables().fruitStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit]\'\'"'
    return
InventoryController.RegisterItemType ImperialApple

