class Puddingfruit extends Item
  constructor: ->
    super()
    @ID = 'produce_pudding'
    @Type = 'food'
    @Name = 'Puddingfruit'
    @Encounter = 'puddingfruit'
    @Description = 'A curious, thin-skinned fruit full of sweet, creamy custard.'
    @Value = 6
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(18, 60)
      getVariables().bellyBloat += random(3, 8)
      getVariables().bonusEnergy += random(6, 30)
    return
InventoryController.RegisterItemType Puddingfruit

