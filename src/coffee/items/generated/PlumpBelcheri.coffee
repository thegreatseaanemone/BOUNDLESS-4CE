class PlumpBelcheri extends Item
  constructor: ->
    super()
    @ID = 'produce_belcheri'
    @Type = 'food'
    @Name = 'Plump Belcheri'
    @Encounter = 'plump belcheri'
    @Description = "A huge Revix cherry, first cultivated in Nefirian's own groves. Whether that makes you want to eat it more or less, is the question."
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(5, 20)
      getVariables().liquidBloat = random(10, 30)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(5, 15)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(3, 5)
      getVariables().fruitStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit]\'\'"'
    return
InventoryController.RegisterItemType PlumpBelcheri

