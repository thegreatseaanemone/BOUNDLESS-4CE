class BodyScannerPCB extends Item
  constructor: ->
    super()
    @ID = 'bodyscan_pcb_part'
    @Type = 'gubbin'
    @Name = 'Body Scanner PCB'
    @Encounter = ''
    @Description = 'The controller board for Body Scanner.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BodyScannerPCB

