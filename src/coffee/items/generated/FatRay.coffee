class FatRay extends Item
  constructor: ->
    super()
    @ID = 'gun_fatray'
    @Type = 'gubbin'
    @Name = 'Fat Ray'
    @Encounter = 'fat ray'
    @Description = 'A raygun that emits strange energy. HYPOTHESIS uses these to harmlessly incapacitate would-be threats... Mostly harmlessly. Requires batteries to use.'
    # [8000.0]
    # OLD: @Value = 8000.0
    @Value = 8000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType FatRay

