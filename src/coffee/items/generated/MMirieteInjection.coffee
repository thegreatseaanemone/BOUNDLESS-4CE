class MMirieteInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_parasite_miriete'
    @Type = 'status'
    @Name = 'M. miriete Injection'
    @Encounter = 'M. miriete injection'
    @Description = 'A syringe full of Microleupii miriete micro-organisms, which cause gradual, spontaneous belly weight gain.'
    @Value = 5000
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\'"'
      getVariables().bellyPlumperParasite = 1
      getVariables().bellywgParaLoad += 10
    return
InventoryController.RegisterItemType MMirieteInjection

