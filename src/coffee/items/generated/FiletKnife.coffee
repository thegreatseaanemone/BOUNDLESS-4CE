class FiletKnife extends Item
  constructor: ->
    super()
    @ID = 'key_filetknife'
    @Type = 'gubbin'
    @Name = 'Filet Knife'
    @Encounter = 'filet knife'
    @Description = 'Skillful hands can use this knife to make the most of any fresh carcass!'
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType FiletKnife

