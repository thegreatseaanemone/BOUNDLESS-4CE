class ThreadbarePants extends Item
  constructor: ->
    super()
    @ID = 'set_threadbare_pants'
    @Type = 'wearable'
    @Name = 'threadbare pants'
    @Encounter = 'threadbare pants'
    @Description = 'These pants are holy in all the wrong ways.'
    # [275.0, 275.0]
    # OLD: @Value = 275.0
    @Value = 275.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ThreadbarePants

