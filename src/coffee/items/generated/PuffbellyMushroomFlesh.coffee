class PuffbellyMushroomFlesh extends Item
  constructor: ->
    super()
    @ID = 'produce_puffshroom'
    @Type = 'food'
    @Name = 'Puffbelly Mushroom Flesh'
    @Encounter = 'puffbelly mushroom flesh'
    @Description = 'A round, belly-like mushroom which expels a cloud of spores when upset. This one is inert now... probably.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += 1
      getVariables().bellyBloat += either(1, 1, 10, 20, 50)
      getVariables().belly += either(0, 0, 0, 0, 0, 1, 3, 5)
      getVariables().bonusEnergy += random(2, 5)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(3, 5)
      getVariables().vegStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Vegetables]\'\'"'
    return
InventoryController.RegisterItemType PuffbellyMushroomFlesh

