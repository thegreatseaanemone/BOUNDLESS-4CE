class HoneybutterComb extends Item
  constructor: ->
    super()
    @ID = 'candy_honeybuttercomb'
    @Type = 'food'
    @Name = 'Honeybutter Comb'
    @Encounter = 'honeybutter comb'
    @Description = 'Chewy wax full of fresh, sweet honeybutter.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(25, 42)
      getVariables().bellyBloat += 20
      getVariables().bonusEnergy += 30
    return
InventoryController.RegisterItemType HoneybutterComb

