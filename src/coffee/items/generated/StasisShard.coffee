class StasisShard extends Item
  constructor: ->
    super()
    @ID = 'shard_stasis'
    @Type = 'gubbin'
    @Name = 'Stasis Shard'
    @Encounter = 'stasis shard'
    @Description = 'You feel as though this chunk of miracrystal is tugging on your very essence..!'
    @Value = 4000
    @Tags = ['rare']
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType StasisShard

