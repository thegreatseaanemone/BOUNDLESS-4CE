class SuperThighPlumperShot extends Item
  constructor: ->
    super()
    @ID = 'shot_thighplump_super'
    @Type = 'status'
    @Name = 'Super Thigh Plumper Shot'
    @Encounter = 'super thigh plumper shot'
    @Description = 'Use for big, fat, squishy thighs!'
    @Value = 0
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 3)
      if getVariables().itemEffect != 1
        window.twinePrint '"You writhe and moan aloud \'\'as your thighs "'
        window.twinePrint '"'+either("swell", "balloon", "swell up", "puff up", "plump up", "bulge", "bloat")+'"'
        window.twinePrint '" //considerably fatter..!//\'\'"'
        getVariables().thigh += either(60, 70, 80, 90, 100) + getVariables().bonusThigh
        getVariables().thighFirmness -= getVariables().thigh
        getVariables().thighfatteniumToxicity += random(3, 9)
        getVariables().pain += random(1, 3)
      else if getVariables().itemEffect == 1
        window.twinePrint '"You writhe and moan aloud \'\'as your thighs "'
        window.twinePrint '"'+either("swell", "balloon", "swell up", "puff up", "plump up", "bulge", "bloat")+'"'
        window.twinePrint '" //enormously..!//\'\'"'
        getVariables().thigh += either(90, 100, 110, 120, 130, 150) + getVariables().bonusThigh
        getVariables().thighFirmness -= getVariables().thigh
        getVariables().thighfatteniumToxicity += random(5, 16)
        getVariables().pain += random(1, 3)
      twineNewline()
      twineNewline()
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType SuperThighPlumperShot

