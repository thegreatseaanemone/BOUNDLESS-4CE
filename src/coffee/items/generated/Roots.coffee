class Roots extends Item
  constructor: ->
    super()
    @ID = 'rootStock'
    @Type = 'food'
    @Name = 'Roots'
    @Encounter = ''
    @Description = 'A root vegetable dug up from the soil.'
    # [1.0, 1.0]
    # OLD: @Value = 1.0
    @Value = 1.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 30
      getVariables().bellyBloat += 30
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType Roots

