class JarOfFattenium extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_fattenium'
    @Type = 'status'
    @Name = 'Jar of Fattenium'
    @Encounter = 'jar of fattenium'
    @Description = "A jar full of liquid fattenium. Don't spill it on yourself!"
    @ChemID = 'Fattenium'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"You cry out, \'\'trembling as you feel your skin instantly beginning to bulge! "'
      getVariables().itemEffect = random(1, 3)
      getVariables().fatteniumCascade = 1
      window.twinePrint '"You feel \'\'"'
      if getVariables().itemEffect == 1
        getVariables().player.Chemicals.Add(FatteniumToxinChemical, random(80, 150))
        getVariables().bellyBloat = getVariables().maxBelly * 1.5
        getVariables().breastBloat += getVariables().maxBreast * 2
        getVariables().bellyLiquid += 50
        getVariables().pain += random(5, 10)
        getVariables().lethalKO = 1
        getVariables().deathCause = "explosion"
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twinePrint '"\'\'//like a balloon...//\'\'"'
      if getVariables().itemEffect == 2
        getVariables().player.Chemicals.Add(FatteniumToxinChemical, random(100, 220))
        getVariables().bellyBloat = getVariables().maxBelly * 2
        getVariables().breastBloat += getVariables().maxBreast * 5
        getVariables().bellyLiquid += 50
        getVariables().pain += random(8, 12)
        getVariables().lethalKO = 1
        getVariables().deathCause = "explosion"
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twinePrint '"\'\'//like a blimp...//\'\'"'
      if getVariables().itemEffect == 3
        getVariables().player.Chemicals.Add(FatteniumToxinChemical, random(200, 300))
        getVariables().bellyBloat = getVariables().maxBelly * 2.5
        getVariables().breastBloat += getVariables().maxBreast * 5
        getVariables().bellyLiquid += 50
        getVariables().pain += random(12, 18)
        getVariables().lethalKO = 1
        getVariables().deathCause = "explosion"
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twineDisplayInline 'FatSurge'
        window.twinePrint '"\'\'//like you\'re going to explode!//\'\'"'
      window.twineDisplayInline 'ExpandBodytype'
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfFattenium

