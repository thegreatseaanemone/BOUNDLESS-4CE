class BloatedGut extends Item
  constructor: ->
    super()
    @ID = 'special_bloatgut'
    @Type = 'status'
    @Name = 'Bloated Gut'
    @Encounter = 'bloated gut'
    @Description = "...there's something inside."
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().randomizer = random(1, 13)
      twineNewline()
      window.twinePrint '"You slice open the bloated gut.... and find"'
      twineNewline()
      window.twinePrint '"\'\'"'
      if getVariables().randomizer == 1
        window.twinePrint '" a half-digested haunch of meat!"'
        getVariables().player.AddItem(HaunchOfMeat, 1)
        twineNewline()
      else if getVariables().randomizer == 2
        window.twinePrint '" a half-digested fat haunch of meat!"'
        getVariables().player.AddItem(FatHaunchOfMeat, 1)
        twineNewline()
      else if getVariables().randomizer == 3
        window.twinePrint '" a sparkling gem!"'
        getVariables().player.AddItem(SparklingGem, 1)
        twineNewline()
      else if getVariables().randomizer == 4
        window.twinePrint '" a fist-sized gem!"'
        getVariables().player.AddItem(FistSizedGem, 1)
        twineNewline()
      else if getVariables().randomizer == 5
        window.twinePrint '" a tuft of fur..."'
        getVariables().player.AddItem(TuftOfFur, 1)
        twineNewline()
      else if getVariables().randomizer == 6
        window.twinePrint '" some old bones..."'
        getVariables().player.AddItem(OldBones, random(2, 5))
        twineNewline()
      else if getVariables().randomizer == 7
        window.twinePrint '" an old skull..."'
        getVariables().player.AddItem(DistortedSkull, 1)
        twineNewline()
      else if getVariables().randomizer == 8
        window.twinePrint '" a stasis shard!"'
        getVariables().player.AddItem(StasisShard, 1)
        twineNewline()
      else if getVariables().randomizer == 9
        window.twinePrint '" some thick-rimmed glasses!"'
        getVariables().player.AddItem(ThickRimmedGlasses, 1)
        twineNewline()
      else if getVariables().randomizer == 10
        window.twinePrint '" a spectrum prisma shard!"'
        getVariables().player.AddItem(SpectrumPrismaShard, 1)
        twineNewline()
      else if getVariables().randomizer == 11
        window.twinePrint '" a handful of tiny gems!"'
        getVariables().player.AddItem(TinyGem, random(3, 12))
        twineNewline()
      else
        window.twinePrint '" a liquified mess..."'
      window.twinePrint '"\'\'"'
      twineNewline()
    return
InventoryController.RegisterItemType BloatedGut

