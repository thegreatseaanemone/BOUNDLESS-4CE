class AntibioticInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_antibio'
    @Type = 'status'
    @Name = 'Antibiotic Injection'
    @Encounter = 'antibiotic injection'
    @Description = 'Helps kill off microorganisms in the body.'
    # [350.0, 300.0]
    # OLD: @Value = 325.0
    @Value = 325.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You hiss and grit your teeth as the meds flow into your system!\'\'</deepalert> "'
      getVariables().slimeBellyParaLoad -= random(40, 90)
      getVariables().slimeBreastParaLoad -= random(40, 90)
      getVariables().stomachBug -= random(20, 60)
      getVariables().miracoSpores -= random(10, 30)
      getVariables().breastParaLoad -= either(20, 30, 40, 50)
      getVariables().bellyParaLoad -= either(20, 30, 40, 50)
      getVariables().wgParaLoad -= either(20, 30, 40, 50)
      getVariables().bellywgParaLoad -= either(20, 30, 40, 50)
      getVariables().breastwgParaLoad -= either(20, 30, 40, 50)
      getVariables().lacParaLoad -= either(20, 30, 40, 50)
      getVariables().metaParaLoad -= either(20, 30)
      getVariables().direParaLoad -= either(30, 60, 90)
      getVariables().ampliParaLoad -= random(2, 5)
      getVariables().pain += random(8, 12)
      getVariables().lethalKO = 0
    return
InventoryController.RegisterItemType AntibioticInjection

