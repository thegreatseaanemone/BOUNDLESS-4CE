class GlassRose extends Item
  constructor: ->
    super()
    @ID = 'flower_rose_glass'
    @Type = 'food'
    @Name = 'Glass Rose'
    @Encounter = ''
    @Description = 'A rose with petals seemingly shaped from opalescent glass... A jewel from the dreamlands.'
    @Value = 5000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType GlassRose

