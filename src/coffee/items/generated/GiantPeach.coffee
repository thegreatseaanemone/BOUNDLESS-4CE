class GiantPeach extends Item
  constructor: ->
    super()
    @ID = 'produce_peach'
    @Type = 'food'
    @Name = 'Giant Peach'
    @Encounter = 'giant peach'
    @Description = "You'll need two hands to hold onto this one. And a big mouth to bite it."
    @Value = 40
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(8, 32)
      getVariables().liquidBloat = random(35, 80)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(12, 24)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(5, 12)
      getVariables().fruitStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit]\'\'"'
    return
InventoryController.RegisterItemType GiantPeach

