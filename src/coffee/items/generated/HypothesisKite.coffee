class HypothesisKite extends Item
  constructor: ->
    super()
    @ID = 'set_hypo_top'
    @Type = 'wearable'
    @Name = 'HYPOTHESIS kite'
    @Encounter = 'HYPOTHESIS kite'
    @Description = 'A traditional Revecroix chestpiece. This one is made of tough, hardened rubber with soft, black fabric sleeves-- HYPOTHESIS-issued armor.'
    # [800.0, 800.0]
    # OLD: @Value = 800.0
    @Value = 800.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType HypothesisKite

