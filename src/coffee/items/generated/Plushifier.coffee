class Plushifier extends Item
  constructor: ->
    super()
    @ID = 'shot_plushifier'
    @Type = 'status'
    @Name = 'Plushifier'
    @Encounter = 'plushifier'
    @Description = "A medicine that relieves bloat, instantly converting it into a proportionate amount of fat. There's a warning on the bottle: SEVERE DRUG REACTION-- DO NOT USE with Helium Pills!!"
    # [1500.0, 1250.0, 1083.3333333333333]
    # OLD: @Value = 1277.7777777777776
    @Value = 1300 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if (getVariables().bellyBloat > 0) || (getVariables().breastBloat > 0)
        window.twinePrint '"Your belly and breasts deflate, your body \'\'rapidly growing softer\'\' as their contents are absorbed and converted into fat!"'
        getVariables().belly += getVariables().bellyBloat / 10
        getVariables().bellyBloat -= getVariables().bellyBloat / 10
        getVariables().breast += getVariables().breastBloat / 10
        getVariables().breastBloat -= getVariables().breastBloat / 10
      else
        window.twinePrint '"But nothing happens..."'
      if getVariables().heliumBallooned > 0
        twineNewline()
        window.twinePrint '"Your belly and breasts \'\'suddenly bloat and swell rapidly, expanding with frightening speed!\'\'"'
        getVariables().bellyPlushsplosion = getVariables().heliumBallooned
        getVariables().breastPlushsplosion = getVariables().heliumBallooned
    return
InventoryController.RegisterItemType Plushifier

