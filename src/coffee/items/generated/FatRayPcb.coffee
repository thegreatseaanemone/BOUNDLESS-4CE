class FatRayPCB extends Item
  constructor: ->
    super()
    @ID = 'fatray_pcb_part'
    @Type = 'gubbin'
    @Name = 'Fat Ray PCB'
    @Encounter = ''
    @Description = 'The controller board for a Fat Ray.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType FatRayPCB

