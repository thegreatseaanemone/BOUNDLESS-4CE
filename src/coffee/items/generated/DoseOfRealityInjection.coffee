class DoseOfRealityInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_reality'
    @Type = 'status'
    @Name = 'Dose of Reality Injection'
    @Encounter = 'dose of reality injection'
    @Description = 'Reduces mirajin radiation and miratoxins absorbed by the body. Essential for survival in high-mirajin areas.'
    # [500.0, 500.0]
    # OLD: @Value = 500.0
    @Value = 500.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You hiss and grit your teeth as the meds flow into your system!\'\'</deepalert> "'
      getVariables().mirajinRads -= random(50, 85)
      getVariables().miraPoisoning -= random(8, 12)
      getVariables().miraberryToxicity -= random(20, 40)
      getVariables().miraberryPoisoning = 0
      getVariables().helenoToxicity -= random(20, 30)
      getVariables().helenoPoisoning = 0
      getVariables().calories += either(10, 10, 20)
      getVariables().pain += random(2, 3)
      getVariables().lethalKO = 0
    return
InventoryController.RegisterItemType DoseOfRealityInjection

