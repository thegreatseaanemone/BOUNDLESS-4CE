class JarOfDiluteFattenium extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_fattenium_weak'
    @Type = 'status'
    @Name = 'Jar of Dilute Fattenium'
    @Encounter = 'jar of dilute fattenium'
    @Description = "A jar of diluted fattenium. It's still probably not a great idea to drink..."
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"You feel "'
      getVariables().itemEffect = random(1, 3)
      if getVariables().itemEffect == 1
        getVariables().bellyBloat += (getVariables().maxBelly * 0.6)
        getVariables().calories += either(200, 250, 300, 350, 400, 450, 500, 550)
        getVariables().bellyLiquid += 50
        getVariables().lethalKO = 1
        getVariables().deathCause = "explosion"
        window.twinePrint '"\'\'//bloated...//\'\'"'
      if getVariables().itemEffect == 2
        getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(10, 20, 30, 40, 50))
        getVariables().bellyBloat += (getVariables().maxBelly * 0.8)
        getVariables().calories += either(500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000)
        getVariables().bellyLiquid += 50
        getVariables().lethalKO = 1
        getVariables().deathCause = "explosion"
        window.twinePrint '"\'\'//like a balloon...//\'\'"'
      if getVariables().itemEffect == 3
        getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(50, 60, 70, 80))
        getVariables().bellyBloat = getVariables().maxBelly * 1.1
        getVariables().breastBloat += getVariables().breast
        getVariables().bellyLiquid += 50
        getVariables().pain += random(2, 7)
        getVariables().lethalKO = 1
        getVariables().deathCause = "explosion"
        window.twinePrint '"\'\'//your skin tightening rapidly around your body...//\'\'"'
      window.twineDisplayInline 'ExpandBodytype'
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfDiluteFattenium

