class Grass extends Item
  constructor: ->
    super()
    @ID = 'material_grass'
    @Type = 'gubbin'
    @Name = 'Grass'
    @Encounter = 'grass'
    @Description = "A bundle of long grass. It'll do for tying things together."
    @Value = 1
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Grass

