class TopHat extends Item
  constructor: ->
    super()
    @ID = 'set_town_head'
    @Type = 'wearable'
    @Name = 'top hat'
    @Encounter = 'top hat'
    @Description = 'The most classic of all hats; looks good on anyone!'
    # [5000.0, 2500.0, 2500.0]
    # OLD: @Value = 3333.3333333333335
    @Value = 3333.3333333333335 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType TopHat

