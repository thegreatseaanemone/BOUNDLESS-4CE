class CoffeeBerry extends Item
  constructor: ->
    super()
    @ID = 'produce_coffee'
    @Type = 'food'
    @Name = 'Coffee Berry'
    @Encounter = 'coffee berry'
    @Description = 'A round, deep red berry. An excellent pick-me-up, but...'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories -= random(0, 5)
      getVariables().bellyBloat += random(0, 8)
      getVariables().bonusEnergy += random(8, 36)
      getVariables().bellyLiquid += random(0, 3)
      getVariables().caffeineToxicity += random(2, 8)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = either(1, 1, 1, 1, 2, 3)
      getVariables().nutStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Nuts]\'\'"'
    return
InventoryController.RegisterItemType CoffeeBerry

