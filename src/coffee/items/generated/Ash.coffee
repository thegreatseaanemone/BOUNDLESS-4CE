class Ash extends Item
  constructor: ->
    super()
    @ID = 'material_ash'
    @Type = 'gubbin'
    @Name = 'Ash'
    @Encounter = 'ash'
    @Description = 'A pile of ashes. Useful..?'
    @Value = 1
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Ash

