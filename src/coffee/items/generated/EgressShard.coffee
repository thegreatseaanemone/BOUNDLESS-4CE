class EgressShard extends Item
  constructor: ->
    super()
    @ID = 'shard_egress'
    @Type = 'gubbin'
    @Name = 'Egress Shard'
    @Encounter = 'egress shard'
    @Description = 'This little chunk of crystal seems to blur whenever you look directly at it... Use in combat to escape immediately!'
    @Value = 5000
    @Tags = ['rare']
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType EgressShard

