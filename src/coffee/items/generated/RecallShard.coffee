class RecallShard extends Item
  constructor: ->
    super()
    @ID = 'shard_recall'
    @Type = 'gubbin'
    @Name = 'Recall Shard'
    @Encounter = 'recall shard'
    @Description = 'This little chunk of miracrystal will summon an aetherhole to take you back home from anywhere on the same planet!'
    # [800.0, 666.6666666666666]
    # OLD: @Value = 733.3333333333333
    @Value = 733.3333333333333 # item.Value
    @Tags = ['mira']
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType RecallShard

