class FatHaunchOfMeat extends Item
  constructor: ->
    super()
    @ID = 'meat_fattyhaunch'
    @Type = 'food'
    @Name = 'Fat Haunch of Meat'
    @Encounter = 'fat haunch of meat'
    @Description = 'Whatever this came off of was eating well.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(6, 65)
      getVariables().bellyBloat += random(24, 72)
      getVariables().bonusEnergy += random(25, 65)
      getVariables().pain -= random(3, 12)
    return
InventoryController.RegisterItemType FatHaunchOfMeat

