class ChemotherapyStationPCB extends Item
  constructor: ->
    super()
    @ID = 'cts_pcb_part'
    @Type = 'gubbin'
    @Name = 'Chemotherapy Station PCB'
    @Encounter = ''
    @Description = 'The controller board for a Chemotherapy Station.'
    @Value = 10000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ChemotherapyStationPCB

