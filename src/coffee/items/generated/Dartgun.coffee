class Dartgun extends Item
  constructor: ->
    super()
    @ID = 'gun_dart'
    @Type = 'gubbin'
    @Name = 'Dartgun'
    @Encounter = 'dartgun'
    @Description = 'A HYPOTHESIS-issue dartgun. Usable with all standard darts.'
    # [5000.0, 3000.0, 3000.0]
    # OLD: @Value = 3666.6666666666665
    @Value = 3666.6666666666665 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Dartgun

