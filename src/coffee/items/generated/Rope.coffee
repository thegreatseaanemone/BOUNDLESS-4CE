class Rope extends Item
  constructor: ->
    super()
    @ID = 'material_rope'
    @Type = 'gubbin'
    @Name = 'Rope'
    @Encounter = 'rope'
    @Description = 'A strong and sturdy woven rope.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Rope

