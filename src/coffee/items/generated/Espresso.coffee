class Espresso extends Item
  constructor: ->
    super()
    @ID = 'drink_espresso'
    @Type = 'food'
    @Name = 'Espresso'
    @Encounter = 'espresso'
    @Description = 'Even the Revix can appreciate the benefits of a nice bit of caffeine.'
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyBloat += 10
      getVariables().bellyLiquid += 30
      getVariables().bonusEnergy += 120
      getVariables().caffeineToxicity += random(10, 18)
    return
InventoryController.RegisterItemType Espresso

