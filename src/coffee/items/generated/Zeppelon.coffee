class Zeppelon extends Item
  constructor: ->
    super()
    @ID = 'produce_zeppelon'
    @Type = 'food'
    @Name = 'Zeppelon'
    @Encounter = 'zeppelon'
    @Description = 'A mutant melon big enough to ride on. If it was, you know, animate.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(0, 30)
      getVariables().liquidBloat = random(800, 2000)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += getVariables().liquidBloat
      getVariables().bellyLiquid += getVariables().liquidBloat
      twineNewline()
      getVariables().belly += either(0, 0, 0, 0, 0, 0.1, 0.2, 0.3, 0.3, 0.5)
      twineNewline()
      twineNewline()
      # Seeds 
      twineNewline()
      getVariables().seedFruit = 1
      getVariables().seedID = "seed_zeppelon"
      getVariables().seedQuantity = random(4, 12)
    return

  Harvest: ->
    if super()
      twineNewline()
      getVariables().seedFruit = 1
      getVariables().seedID = "seed_zeppelon"
      getVariables().seedQuantity = random(4, 12)
      twineNewline()
      if getVariables().seedQuantity > 0
        getVariables().seed_zeppelon += getVariables().seedQuantity
      twineNewline()
      twineNewline()
      getVariables().resourceYield = random(8, 32)
      getVariables().fruitStock += getVariables().resourceYield
      getVariables().waterStock += (getVariables().resourceYield * 10)
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit +"'
      window.twinePrint '"'+getVariables().resourceYield * 10+'"'
      window.twinePrint '" Water"'
      if getVariables().seedQuantity > 0
        window.twinePrint '" +"'
        window.twinePrint '"'+getVariables().seedQuantity+'"'
        window.twinePrint '" Seeds"'
      window.twinePrint '"]\'\'"'
    return
InventoryController.RegisterItemType Zeppelon

