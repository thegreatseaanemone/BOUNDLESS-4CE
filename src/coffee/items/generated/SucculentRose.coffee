class SucculentRose extends Item
  constructor: ->
    super()
    @ID = 'produce_succulent'
    @Type = 'food'
    @Name = 'Succulent Rose'
    @Encounter = 'succulent rose'
    @Description = 'A small, plump, rose-shaped succulent. Tender and light.'
    @Value = 4
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(0, 3)
      getVariables().liquidBloat = random(2, 5)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(0, 3)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(1, 3)
      getVariables().vegStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Vegetables]\'\'"'
    return
InventoryController.RegisterItemType SucculentRose

