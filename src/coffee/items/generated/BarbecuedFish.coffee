class BarbecuedFish extends Item
  constructor: ->
    super()
    @ID = 'cooked_fish'
    @Type = 'food'
    @Name = 'Barbecued Fish'
    @Encounter = ''
    @Description = 'Fresh fish, roasted over a campfire-- a simple and sweet way to dine like a king.'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(4, 38)
      getVariables().bellyBloat += random(16, 50)
      getVariables().bonusEnergy += random(25, 39)
      getVariables().pain -= random(2, 5)
    return
InventoryController.RegisterItemType BarbecuedFish

