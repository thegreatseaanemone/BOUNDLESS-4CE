class DryPetals extends Item
  constructor: ->
    super()
    @ID = 'material_petals'
    @Type = ''
    @Name = 'Dry Petals'
    @Encounter = ''
    @Description = 'Beautiful flowers blown from nearby trees. Or perhaps, fallen from the sky...'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType DryPetals

