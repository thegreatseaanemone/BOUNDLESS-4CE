class TinyGem extends Item
  constructor: ->
    super()
    @ID = 'gem_tiny'
    @Type = 'gubbin'
    @Name = 'Tiny Gem'
    @Encounter = 'tiny gem'
    @Description = 'A small, sparkling jewel. Certain to be worth something to someone.'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType TinyGem

