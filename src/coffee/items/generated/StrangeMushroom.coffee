class StrangeMushroom extends Item
  constructor: ->
    super()
    @ID = 'mush_strange'
    @Type = 'status'
    @Name = 'Strange Mushroom'
    @Encounter = 'strange mushroom'
    @Description = 'It might not be the best idea to eat random unidentified mushrooms.'
    @Value = 6
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 16)
      getVariables().bellyBloat += either(10, 20, 30)
      getVariables().calories += either(20, 30, 40, 50)
      getVariables().mushroomToxicity += either(1, 1, 3, 3, 5, 7, 9, 12)
      if getVariables().itemEffect == 1
        window.twinePrint '"You feel...\'\'//strange.// Your belly gurgles as it puffs slightly outward!\'\' "'
        getVariables().belly += either(0, 10, 20)
      else if getVariables().itemEffect == 2
        window.twinePrint '"You feel...\'\'//strange.// Your belly seems to retract...\'\' "'
        getVariables().belly -= either(0, 10, 20)
      else if getVariables().itemEffect == 3
        window.twinePrint '"You feel...\'\'//strange.// Your belly gurgles....\'\' "'
        getVariables().calories += either(0, 10, 30, 50, 100)
      else if getVariables().itemEffect == 4
        window.twinePrint '"You feel...\'\' unusually warm!\'\' "'
        getVariables().metaBurn += either(0, 0, 10)
      else if getVariables().itemEffect == 5
        window.twinePrint '"You feel...\'\' lethargic...\'\' "'
        getVariables().metaGain += either(0, 0, 10)
      else if getVariables().itemEffect == 6
        window.twinePrint '"You feel...\'\'//strange.// Your belly suddenly bloats up tight!\'\' "'
        getVariables().bellyBloat = getVariables().maxBelly * 1.1
        getVariables().pain += random(1, 3)
        twineNewline()
      else if getVariables().itemEffect == 7
        window.twinePrint '"You feel...\'\'//strange.// Your muscles suddenly bulge and tighten!\'\' "'
        getVariables().strength += 1
        twineNewline()
      else if getVariables().itemEffect == 8
        window.twinePrint '"You feel...\'\'//strange.// Your muscles feel warm and flexible!\'\' "'
        getVariables().finesse += 1
        twineNewline()
      else if getVariables().itemEffect == 9
        window.twinePrint '"You feel...\'\'//strange.// Your head feels oddly clear...\'\' "'
        getVariables().imagination += 1
        twineNewline()
      else if getVariables().itemEffect == 10
        window.twinePrint '"You feel...\'\'//more stable//!\'\' "'
        getVariables().naturalMaxPain += 1
        twineNewline()
      else if getVariables().itemEffect == 11
        window.twinePrint '"You feel...\'\'//clumsy//...\'\' "'
        getVariables().finesse -= 1
        twineNewline()
      else if getVariables().itemEffect == 12
        window.twinePrint '"You feel...\'\'//foggy...//\'\' "'
        getVariables().imagination -= 1
        twineNewline()
      else if getVariables().itemEffect == 13
        window.twinePrint '"You feel...\'\'//more stable//!\'\' "'
        getVariables().naturalMaxPain += 3
        twineNewline()
      else if getVariables().itemEffect > 14
        window.twinePrint '"You feel...\'\'//hungry//...\'\' "'
        getVariables().esurience += 0.5
        twineNewline()
      else if getVariables().itemEffect > 15
        window.twinePrint '"You feel...\'\'//sated//...\'\' "'
        getVariables().esurience -= 0.5
        twineNewline()
      else if getVariables().itemEffect >= 16
        window.twinePrint '"You feel strange..."'
    return
InventoryController.RegisterItemType StrangeMushroom

