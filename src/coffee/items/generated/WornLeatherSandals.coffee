class WornLeatherSandals extends Item
  constructor: ->
    super()
    @ID = 'set_threadbare_feet'
    @Type = 'wearable'
    @Name = 'worn leather sandals'
    @Encounter = 'worn leather sandals'
    @Description = 'Soft leather sandals, well-worn and a bit chewed-on.'
    # [225.0, 225.0]
    # OLD: @Value = 225.0
    @Value = 225.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType WornLeatherSandals

