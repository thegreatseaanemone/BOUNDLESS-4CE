class Miraberry extends Item
  constructor: ->
    super()
    @ID = 'berry_mira'
    @Type = 'status'
    @Name = 'Miraberry'
    @Encounter = 'miraberries'
    @Description = 'A handful of strange, crystalline berries.'
    @Value = 1
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your belly gurgles strangely...\'\'"'
      getVariables().fatteningSurge = random(1, 20)
      window.twineDisplayInline 'FatSurge'
      getVariables().calories += random(5, 30)
      getVariables().bonusEnergy += random(5, 30)
      getVariables().bellyBloat += 10
      getVariables().miraberryToxicity += getVariables().fatteningSurge
    return
InventoryController.RegisterItemType Miraberry

