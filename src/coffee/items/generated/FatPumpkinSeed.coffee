class FatPumpkinSeed extends Item
  constructor: ->
    super()
    @ID = 'seed_fatpumpkin'
    @Type = 'food'
    @Name = 'Fat Pumpkin Seed'
    @Encounter = ''
    @Description = "I've seen fatter seeds, honestly."
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(2, 8)
      getVariables().bonusEnergy += random(0, 6)
    return
InventoryController.RegisterItemType FatPumpkinSeed

