class JarOfImperialMousse extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_mousse_imperial'
    @Type = 'food'
    @Name = 'Jar of Imperial Mousse'
    @Encounter = ''
    @Description = 'A thick, sweet, swooningly rich dairy cream suitable for eating with a spoon. Intensely fattening!'
    @Value = 5000
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 250
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 30
      getVariables().bonusEnergy += 200
      getVariables().jarReturn = 1
    return
InventoryController.RegisterItemType JarOfImperialMousse

