class ExpandingBelt extends Item
  constructor: ->
    super()
    @ID = 'set_magicgain_belt'
    @Type = 'wearable'
    @Name = 'Expanding Belt'
    @Encounter = 'expanding belt'
    @Description = 'No matter what you do, this elastic belt always makes your belly seem fatter...'
    @Value = 4000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ExpandingBelt

