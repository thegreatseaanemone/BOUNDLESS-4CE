class CookingPot extends Item
  constructor: ->
    super()
    @ID = 'key_cookpot'
    @Type = 'gubbin'
    @Name = 'Cooking Pot'
    @Encounter = 'cooking pot'
    @Description = 'A durable metal pot for cooking and boiling all sorts of things in. An essential cooking tool.'
    @Value = 120
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType CookingPot

