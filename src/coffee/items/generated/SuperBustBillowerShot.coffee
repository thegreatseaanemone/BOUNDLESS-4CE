class SuperBustBillowerShot extends Item
  constructor: ->
    super()
    @ID = 'shot_bustbillow_super'
    @Type = 'status'
    @Name = 'Super Bust Billower Shot'
    @Encounter = 'super bust billower shot'
    @Description = 'Big, round, wobbly breasts are a single shot away, thanks to the wonders of Revix science!'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 3)
      if getVariables().itemEffect != 1
        window.twinePrint '"You writhe and moan aloud \'\'as your breasts "'
        window.twinePrint '"'+either("swell", "balloon", "swell up", "puff up", "plump up", "bulge", "bloat")+'"'
        window.twinePrint '" //considerably fatter..!//\'\'"'
        getVariables().breast += either(60, 70, 80, 90, 100) + getVariables().bonusBreast
        getVariables().breastBloat = getVariables().maxBreast
        getVariables().breastfatteniumToxicity += random(3, 9)
        getVariables().pain += random(1, 3)
      else if getVariables().itemEffect == 1
        window.twinePrint '"You writhe and moan aloud \'\'as your breasts "'
        window.twinePrint '"'+either("swell", "balloon", "swell up", "puff up", "plump up", "bulge", "bloat")+'"'
        window.twinePrint '" //enormously..!//\'\'"'
        getVariables().breast += either(90, 100, 110, 120, 130, 140, 150) + getVariables().bonusBreast
        getVariables().breastBloat = getVariables().maxBreast
        getVariables().breastfatteniumToxicity += random(5, 16)
        getVariables().pain += random(1, 3)
      twineNewline()
      twineNewline()
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType SuperBustBillowerShot

