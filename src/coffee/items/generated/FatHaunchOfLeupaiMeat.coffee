class FatHaunchOfLeupaiMeat extends Item
  constructor: ->
    super()
    @ID = 'meat_fattyleuphaunch'
    @Type = 'food'
    @Name = 'Fat Haunch of Leupai Meat'
    @Encounter = 'fat haunch of leupai meat'
    @Description = 'A gut-busting feast on a stick.'
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(54, 185)
      getVariables().bellyBloat += random(20, 70)
      getVariables().bonusEnergy += random(12, 46)
      getVariables().miraPoisoning += random(0, 6)
      getVariables().pain -= random(4, 24)
      getVariables().favorNefirian += random(0, 2)
    return
InventoryController.RegisterItemType FatHaunchOfLeupaiMeat

