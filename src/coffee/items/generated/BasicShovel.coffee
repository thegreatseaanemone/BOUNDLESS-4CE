class BasicShovel extends Item
  constructor: ->
    super()
    @ID = 'key_shovel'
    @Type = ''
    @Name = 'Basic Shovel'
    @Encounter = ''
    @Description = "If you need a lot of dirt or dirt-like substances, you can't go wrong with a shovel. This one is a little flimsy, though..."
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BasicShovel

