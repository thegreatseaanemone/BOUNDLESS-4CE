class JarOfLeupaiBlood extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_leupaiblood'
    @Type = 'status'
    @Name = 'Jar of Leupai Blood'
    @Encounter = 'jar of leupai blood'
    @Description = "Full of sparkling, thick blue leupai's blood. But why?"
    @Value = 200
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"Your mouth feels odd as you drink the strange, thick liquid...\'\'Your belly feels a little bit more elastic!\'\'"'
      getVariables().miraPoisoning += random(1, 6)
      getVariables().stretchMod += 5
      getVariables().player.Chemicals.Add(LeupaiToxinChemical, random(1, 6))
      getVariables().calories += either(30, 40, 50, 60, 70, 80, 90)
      getVariables().miraPoisoning += random(1, 8)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfLeupaiBlood

