class JarOfThickCream extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_cream_thick'
    @Type = 'drink'
    @Name = 'Jar of Thick Cream'
    @Encounter = 'jar of thick cream'
    @Description = 'A jar full of fresh heavy cream.'
    # [90.0]
    # OLD: @Value = 90.0
    @Value = 90.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 80
      getVariables().bellyBloat += 40
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 60
      getVariables().jarReturn = 1
      twineNewline()
      window.twineDisplayInline 'MilkCure'
    return
InventoryController.RegisterItemType JarOfThickCream

