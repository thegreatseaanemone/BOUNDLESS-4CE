class VenombloatInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_venombloat'
    @Type = 'status'
    @Name = 'Venombloat Injection'
    @Encounter = 'venombloat injection'
    @Description = 'Stimulates venom production in creatures with glands. Just kind of makes everything else feel weird and swollen.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().venomGland == 0
        window.twinePrint '"\'\'Your belly protrudes as it bloats up tight!\'\'"'
        getVariables().miraPoisoning += random(1, 3)
        getVariables().bellyBloat += (getVariables().maxBelly * 0.5)
      else if getVariables().venomGland == 1
        window.twinePrint '"\'\'Your belly protrudes and wobbles as your venom glands suddenly inflate!\'\'"'
        getVariables().storedVenom += (getVariables().venomProd * random(2, 3))
        getVariables().miraPoisoning += random(1, 3)
        getVariables().bellyBloat += (getVariables().maxBelly * 0.5)
        getVariables().randomizer = random(1, 64)
        twineNewline()
        twineNewline()
        if getVariables().randomizer == 1
          getVariables().geneVenomBloated = 1
    return
InventoryController.RegisterItemType VenombloatInjection

