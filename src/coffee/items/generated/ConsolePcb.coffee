class ConsolePCB extends Item
  constructor: ->
    super()
    @ID = 'console_pcb_part'
    @Type = 'gubbin'
    @Name = 'Console PCB'
    @Encounter = ''
    @Description = 'The controller board for a Console.'
    @Value = 5000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ConsolePCB

