class MetalShovel extends Item
  constructor: ->
    super()
    @ID = 'key_shovel_metal'
    @Type = ''
    @Name = 'Metal Shovel'
    @Encounter = ''
    @Description = "If you need a lot of dirt or dirt-like substances, you can't go wrong with a shovel."
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType MetalShovel

