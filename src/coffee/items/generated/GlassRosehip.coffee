class GlassRosehip extends Item
  constructor: ->
    super()
    @ID = 'produce_rosehip_glass'
    @Type = 'food'
    @Name = 'Glass Rosehip'
    @Encounter = 'glass rosehip'
    @Description = 'A sparkling fruit picked from a glass rose plant. Makes excellent tea.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += 0
      getVariables().bellyBloat += random(0, 2)
      getVariables().bonusEnergy += random(1, 5)
      getVariables().bellyLiquid += random(0, 2)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(1, 2)
      getVariables().seedYield = random(1, 2)
      getVariables().fruitStock += getVariables().resourceYield
      getVariables().seed_rose_glass += getVariables().seedYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit +"'
      window.twinePrint '"'+getVariables().seedYield+'"'
      window.twinePrint '" Glass Rose Seed]\'\'"'
    return
InventoryController.RegisterItemType GlassRosehip

