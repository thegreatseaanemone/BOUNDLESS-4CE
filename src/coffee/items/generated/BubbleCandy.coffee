class BubbleCandy extends Item
  constructor: ->
    super()
    @ID = 'candy_slimebait'
    @Type = 'food'
    @Name = 'Bubble Candy'
    @Encounter = 'bubble candies'
    @Description = 'Brightly colored, fragile little sugar-bubbles full of some kind of liquid goo. Slimes seem particularly interested in these...'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 0
      getVariables().bellyBloat += 10
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType BubbleCandy

