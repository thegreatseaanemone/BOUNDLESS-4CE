class Cramberry extends Item
  constructor: ->
    super()
    @ID = 'produce_cramberry'
    @Type = 'food'
    @Name = 'Cramberry'
    @Encounter = 'cramberry'
    @Description = 'A round, tart red berry. Lip-puckering when fresh, but aids digestion, soothes bloat, and stimulates the appetite.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories -= random(0, 3)
      getVariables().bellyBloat -= random(2, 6)
      getVariables().bonusEnergy += random(1, 3)
      getVariables().bellyLiquid += random(0, 2)
      getVariables().esurience += random(0.1, 0.8)
      twineNewline()
      if getVariables().calories >= getVariables().metaGain
        window.twineDisplayInline 'Bodytype'
        twineNewline()
      else if getVariables().bonusEnergy >= getVariables().metaBurn
        getVariables().bonusEnergy -= getVariables().metaBurn
        twineNewline()
      else
        window.twineDisplayInline 'WeightLoss'
    return
InventoryController.RegisterItemType Cramberry

