class VitalShard extends Item
  constructor: ->
    super()
    @ID = 'shard_vital'
    @Type = 'status'
    @Name = 'Vital Shard'
    @Encounter = 'vital shard'
    @Description = 'Just holding this beautiful crystal makes your pain seem to fade away.'
    @Value = 2000
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"<affirmative>You breathe a sigh of relief as \'\'your wounds begin to fade away!\'\'</affirmative>"'
      getVariables().pain -= getVariables().maxPain * 0.5
      twineNewline()
    return
InventoryController.RegisterItemType VitalShard

