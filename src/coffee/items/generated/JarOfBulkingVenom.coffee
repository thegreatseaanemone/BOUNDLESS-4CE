class JarOfBulkingVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_bulk'
    @Type = 'status'
    @Name = 'Jar of Bulking Venom'
    @Encounter = 'jar of bulking venom'
    @Description = "It's hard to say whether this will get you ripped, or just rip you open."
    # [5000.0]
    # OLD: @Value = 5000.0
    @Value = 5000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().bulkVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBulkingVenom

