class MilkyCoffee extends Item
  constructor: ->
    super()
    @ID = 'drink_coffee_milky'
    @Type = 'drink'
    @Name = 'Milky Coffee'
    @Encounter = 'milky coffee'
    @Description = 'A mug of simple, sweet milky coffee. Delicious!'
    # [50.0, 50.0]
    # OLD: @Value = 50.0
    @Value = 50.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 10
      getVariables().bellyBloat += 30
      getVariables().bellyLiquid += 30
      getVariables().bonusEnergy += 60
      getVariables().caffeineToxicity += random(3, 8)
    return
InventoryController.RegisterItemType MilkyCoffee

