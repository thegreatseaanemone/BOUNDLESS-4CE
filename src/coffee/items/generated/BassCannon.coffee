class BassCannon extends Item
  constructor: ->
    super()
    @ID = 'gun_bass'
    @Type = 'gubbin'
    @Name = 'Bass Cannon'
    @Encounter = 'bass cannon'
    @Description = 'A powerful sonic weapon used by disciples of Andreu to subdue attackers... Kick it. Requires batteries to use.'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BassCannon

