class RedPrismaShard extends PrismaShard
  constructor: ->
    super('red', 'Red')
    # [600.0, 533.3333333333334]
    # OLD: @Value = 566.6666666666667
    @Value = 500.0 # item.Value
    @Tags = []
InventoryController.RegisterItemType RedPrismaShard

