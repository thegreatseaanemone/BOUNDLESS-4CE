class Bounceberry extends Item
  constructor: ->
    super()
    @ID = 'berry_bounce'
    @Type = 'status'
    @Name = 'Bounceberry'
    @Encounter = 'bounceberries'
    @Description = 'These firm berries are quite rubbery, and tend to squirt when bitten into.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your breasts wobble as they suddenly inflate!\'\'"'
      twineNewline()
      twineNewline()
      getVariables().breast += random(6, 18)
      getVariables().breastBloat += (getVariables().maxBreast * 0.5)
      getVariables().calories += random(2, 8)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Bounceberry

