class AbsorptionAcceleratorPCB extends Item
  constructor: ->
    super()
    @ID = 'aa_pcb_part'
    @Type = 'gubbin'
    @Name = 'Absorption Accelerator PCB'
    @Encounter = ''
    @Description = 'The controller board for an Absorption Accelerator.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType AbsorptionAcceleratorPCB

