class ThreadbareCape extends Item
  constructor: ->
    super()
    @ID = 'set_threadbare_cape'
    @Type = 'wearable'
    @Name = 'threadbare cape'
    @Encounter = 'threadbare cape'
    @Description = 'The height of patchwork fashion, assuredly.'
    # [350.0, 350.0]
    # OLD: @Value = 350.0
    @Value = 350.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ThreadbareCape

