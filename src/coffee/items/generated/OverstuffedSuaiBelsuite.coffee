class OverstuffedSuaiBelsuite extends Item
  constructor: ->
    super()
    @ID = 'pastry_belsuite_fat'
    @Type = 'food'
    @Name = 'Overstuffed Suai Belsuite'
    @Encounter = 'overstuffed suai belsuite'
    @Description = 'A pastry in the shape of a leupai, stuffed full of something delicious. This one looks fit to burst!'
    @Value = 150
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(90, 100, 110, 120)
      getVariables().bellyBloat += 40
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType OverstuffedSuaiBelsuite

