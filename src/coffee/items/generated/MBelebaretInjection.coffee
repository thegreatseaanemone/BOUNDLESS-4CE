class MBelebaretInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_parasite_belebaret'
    @Type = 'status'
    @Name = 'M. belebaret Injection'
    @Encounter = 'M. belebaret injection'
    @Description = 'A syringe full of Microleupii belebaret micro-organisms, which cause gradual, spontaneous breast weight gain.'
    @Value = 5000
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\'"'
      getVariables().breastPlumperParasite = 1
      getVariables().breastwgParaLoad += 10
    return
InventoryController.RegisterItemType MBelebaretInjection

