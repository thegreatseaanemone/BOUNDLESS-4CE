class TatteredHide extends Item
  constructor: ->
    super()
    @ID = 'hide_tattered'
    @Type = 'gubbin'
    @Name = 'Tattered Hide'
    @Encounter = 'tattered hide'
    @Description = "This one didn't turn out too well."
    @Value = 1
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType TatteredHide

