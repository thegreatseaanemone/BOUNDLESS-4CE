class ExpansionShard extends Item
  constructor: ->
    super()
    @ID = 'shard_expansion'
    @Type = 'status'
    @Name = 'Expansion Shard'
    @Encounter = 'expansion shard'
    @Description = 'You feel strange and bloated, just looking at this...'
    @Value = 4000
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"You gasp in shock as your body \'\'suddenly inflates with fat\'\', jiggling and quaking as the explosive growth slows to a stop!"'
      twineNewline()
      twineNewline()
      if getVariables().bodyType == "busty"
        twineNewline()
        getVariables().girth = getVariables().girth * 1.1
        getVariables().belly = getVariables().belly * 1.1
        getVariables().breast = getVariables().breast * 1.5
        getVariables().thigh = getVariables().thigh * 1.1
        getVariables().breastBloat += getVariables().maxBreast * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "hourglass"
        twineNewline()
        getVariables().girth = getVariables().girth * 1.1
        getVariables().belly = getVariables().belly * 1.1
        getVariables().breast = getVariables().breast * 1.3
        getVariables().thigh = getVariables().thigh * 1.3
        getVariables().breastBloat += getVariables().maxBreast * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "boviete"
        twineNewline()
        getVariables().breast = getVariables().breast * 1.8
        getVariables().breastBloat += getVariables().maxBreast * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "cubus"
        twineNewline()
        getVariables().breast = getVariables().breast * 1.5
        getVariables().thigh = getVariables().thigh * 1.5
        getVariables().breastBloat += getVariables().maxBreast * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "round"
        twineNewline()
        getVariables().belly = getVariables().belly * 1.3
        getVariables().thigh = getVariables().thigh * 1.3
        getVariables().bellyBloat += getVariables().maxBelly * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "bulging"
        twineNewline()
        getVariables().belly = getVariables().belly * 1.5
        getVariables().thigh = getVariables().thigh * 1.5
        getVariables().bellyBloat += getVariables().maxBelly * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "belly"
        twineNewline()
        getVariables().girth = getVariables().girth * 1.1
        getVariables().belly = getVariables().belly * 1.5
        getVariables().breast = getVariables().breast * 1.1
        getVariables().thigh = getVariables().thigh * 1.1
        getVariables().bellyBloat += getVariables().maxBelly * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "guguetelle"
        twineNewline()
        getVariables().belly = getVariables().belly * 1.8
        getVariables().bellyBloat += getVariables().maxBelly * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "pear"
        twineNewline()
        getVariables().girth = getVariables().girth * 1.1
        getVariables().belly = getVariables().belly * 1.3
        getVariables().breast = getVariables().breast * 1.1
        getVariables().thigh = getVariables().thigh * 1.3
        getVariables().bellyBloat += getVariables().maxBelly * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "bloatpear"
        twineNewline()
        getVariables().breast = getVariables().breast * 1.5
        getVariables().thigh = getVariables().thigh * 1.5
        getVariables().bellyBloat += getVariables().maxBelly * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "thigh"
        twineNewline()
        getVariables().girth = getVariables().girth * 1.1
        getVariables().belly = getVariables().belly * 1.1
        getVariables().breast = getVariables().breast * 1.1
        getVariables().thigh = getVariables().thigh * 1.5
        twineNewline()
        twineNewline()
      else if getVariables().bodyType == "blimpthigh"
        twineNewline()
        getVariables().thigh = getVariables().thigh * 1.8
        twineNewline()
        twineNewline()
      else
        # Default 
        twineNewline()
        getVariables().girth = getVariables().girth * 1.2
        getVariables().belly = getVariables().belly * 1.2
        getVariables().breast = getVariables().breast * 1.2
        getVariables().thigh = getVariables().thigh * 1.2
      twineNewline()
      twineNewline()
      twineNewline()
      # Cleanup 
      twineNewline()
      getVariables().girth = Math.round(getVariables().girth)
      getVariables().breast = Math.round(getVariables().breast)
      getVariables().belly = Math.round(getVariables().belly)
      getVariables().thigh = Math.round(getVariables().thigh)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType ExpansionShard

