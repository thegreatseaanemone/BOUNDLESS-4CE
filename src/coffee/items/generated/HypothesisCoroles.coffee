class HypothesisCoroles extends Item
  constructor: ->
    super()
    @ID = 'set_hypo_pants'
    @Type = 'wearable'
    @Name = 'HYPOTHESIS coroles'
    @Encounter = 'HYPOTHESIS coroles'
    @Description = 'Traditional Revecroix pants in a flower-petal shape. These are made of soft, comfortable fabric. HYPOTHESIS-issued armor.'
    # [800.0, 800.0, 3000.0, 3000.0, 4500.0, 4500.0]
    # OLD: @Value = 2766.6666666666665
    @Value = 2766.6666666666665 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType HypothesisCoroles

