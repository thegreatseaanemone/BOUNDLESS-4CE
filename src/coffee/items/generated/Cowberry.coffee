class Cowberry extends Item
  constructor: ->
    super()
    @ID = 'berry_cow'
    @Type = 'status'
    @Name = 'Cowberry'
    @Encounter = 'cowberries'
    @Description = 'Milky and sweet.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your chest suddenly plumps!\'\'"'
      twineNewline()
      twineNewline()
      getVariables().breast += random(8, 21)
      getVariables().calories += random(2, 8)
      twineNewline()
      if getVariables().geneDry == 0
        getVariables().breastBloat += (getVariables().maxBreast * 0.5)
        getVariables().storedMilk = getVariables().breastBloat
        getVariables().cowberryBloat += random(2, 5)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Cowberry

