class BolsteringVitaminSlurry extends Item
  constructor: ->
    super()
    @ID = 'statboost_stamina'
    @Type = 'status'
    @Name = 'Bolstering Vitamin Slurry'
    @Encounter = 'bolstering vitamin slurry'
    @Description = 'A thick, flavorful concoction full of calories and body-strengthening ingredients.'
    @Value = 500
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'You breathe a deep sigh, feeling revitalized!\'\'"'
      getVariables().naturalMaxPain += either(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 10)
      getVariables().esurience += either(0, 0, 0, 0, 0.1)
      getVariables().calories += either(70, 80, 90, 100, 110)
      getVariables().pain = 0
      getVariables().health = "Lively"
    return
InventoryController.RegisterItemType BolsteringVitaminSlurry

