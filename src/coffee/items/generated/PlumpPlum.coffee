class PlumpPlum extends Item
  constructor: ->
    super()
    @ID = 'produce_plum'
    @Type = 'food'
    @Name = 'Plump Plum'
    @Encounter = 'plump plum'
    @Description = "A plum grown by Revix farmers. The giveaway? It's roughly the size of a human head."
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(6, 30)
      getVariables().liquidBloat = random(30, 70)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(6, 20)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(4, 8)
      getVariables().fruitStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit]\'\'"'
    return
InventoryController.RegisterItemType PlumpPlum

