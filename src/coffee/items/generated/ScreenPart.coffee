class ScreenPart extends Item
  constructor: ->
    super()
    @ID = 'screen_part'
    @Type = 'gubbin'
    @Name = 'MCD Screen'
    @Encounter = ''
    @Description = "A scratchproof MiraCrystal Display (MCD) screen for use in machines. You wish they'd figure out how to get colors other than black and magenta on the damned things."
    @Value = 10000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ScreenPart

