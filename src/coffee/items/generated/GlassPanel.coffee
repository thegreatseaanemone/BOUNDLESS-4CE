class GlassPanel extends Item
  constructor: ->
    super()
    @ID = 'glass_panel'
    @Type = 'gubbin'
    @Name = 'Glass Panel'
    @Encounter = ''
    @Description = 'A glass panel for viewing something in a machine without the dangers of exposing the work area.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType GlassPanel

