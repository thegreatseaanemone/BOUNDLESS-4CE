class Buttercake extends Item
  constructor: ->
    super()
    @ID = 'pastry_buttercake'
    @Type = 'food'
    @Name = 'Buttercake'
    @Encounter = 'buttercakes'
    @Description = 'A small, sticky, buttery cake often served at abittes.'
    # [30.0, 26.666666666666668]
    # OLD: @Value = 28.333333333333336
    @Value = 28.33 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 30
      getVariables().bellyBloat += 10
      getVariables().bonusEnergy += 10
    return
InventoryController.RegisterItemType Buttercake

