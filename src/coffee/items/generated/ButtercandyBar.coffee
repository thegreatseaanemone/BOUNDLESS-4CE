class ButtercandyBar extends Item
  constructor: ->
    super()
    @ID = 'candy_buttercandy'
    @Type = 'food'
    @Name = 'Buttercandy Bar'
    @Encounter = 'buttercandy'
    @Description = 'A thick bar of sweet, flavored buttercream coated in chocolate; a common Revix treat.'
    # [10.0]
    # OLD: @Value = 10.0
    @Value = 10.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 30
      getVariables().bellyBloat += 20
      getVariables().bonusEnergy += 30
    return
InventoryController.RegisterItemType ButtercandyBar

