class Fish extends Item
  constructor: ->
    super()
    @ID = 'meatStock'
    @Type = 'food'
    @Name = 'Fish'
    @Encounter = ''
    @Description = 'A plump fish, cleaned and ready to cook or eat.'
    # [5.0, 5.0]
    # OLD: @Value = 5.0
    @Value = 5.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(3, 20)
      getVariables().bellyBloat += 30
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType Fish

