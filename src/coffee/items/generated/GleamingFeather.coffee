class GleamingFeather extends Item
  constructor: ->
    super()
    @ID = 'feather_gleaming'
    @Type = 'gubbin'
    @Name = 'Gleaming Feather'
    @Encounter = 'gleaming feather'
    @Description = 'A beautiful, brilliantly-hued feather.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType GleamingFeather

