class Grain extends Item
  constructor: ->
    super()
    @ID = 'grainStock'
    @Type = 'resource'
    @Name = 'Grain'
    @Encounter = ''
    @Description = 'A handful of raw grain. No good to eat like this, but...'
    # [0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
    # OLD: @Value = 0.5
    @Value = 0.5 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Grain

