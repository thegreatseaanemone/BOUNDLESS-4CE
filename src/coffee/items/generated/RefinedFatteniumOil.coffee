class RefinedFatteniumOil extends Item
  constructor: ->
    super()
    @ID = 'oil_fattenium_refined'
    @Type = 'status'
    @Name = 'Refined Fattenium Oil'
    @Encounter = 'refined fattenium oil'
    @Description = 'A potent fattening agent, far less dangerous to use than raw liquid fattenium. Can be used as a cooking oil by the truly adventurous.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"This isn\'t the most enjoyable thing you\'ve ever drunk."'
      twineNewline()
      window.twinePrint '"\'\'You feel //intensely// bloated...\'\'"'
      twineNewline()
      twineNewline()
      getVariables().calories += random(360, 1300)
      getVariables().bellyBloat += (getVariables().maxBelly * 1.2)
      getVariables().indigestion += (getVariables().girth / 400)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType RefinedFatteniumOil

