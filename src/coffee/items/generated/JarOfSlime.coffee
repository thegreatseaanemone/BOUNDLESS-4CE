class JarOfSlime extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_slime'
    @Type = 'drink'
    @Name = 'Jar of Slime'
    @Encounter = 'jar of slime'
    @Description = "This is slime. It's... probably not very refreshing..?"
    # [10.0, 7.0]
    # OLD: @Value = 8.5
    @Value = 8.5 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 0
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 50
      getVariables().jarReturn = 1
    return
InventoryController.RegisterItemType JarOfSlime

