class VoracityShot extends Item
  constructor: ->
    super()
    @ID = 'shot_voracity'
    @Type = 'status'
    @Name = 'Voracity Shot'
    @Encounter = 'voracity shot'
    @Description = 'A curious serum capable of inducing ravening hunger. Useful for fattening creatures up, or for help handling big meals...'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'Your stomach growls and gurgles ominously...\'\' You feel the urge to <redalert>\'\'consume\'\'</redalert> rise up in you like a great wave!</deepalert> "'
      getVariables().esurience += random(4, 8)
      getVariables().voreFrenzy += random(12, 36)
      getVariables().pain += random(2, 3)
      getVariables().lethalKO = 0
    return
InventoryController.RegisterItemType VoracityShot

