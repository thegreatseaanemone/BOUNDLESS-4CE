class BulkPowder extends Item
  constructor: ->
    super()
    @ID = 'statboost_strength'
    @Type = 'status'
    @Name = 'Bulk Powder'
    @Encounter = 'bulk powder'
    @Description = 'A special powdered mix for building muscle and putting on weight.'
    @Value = 500
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'You gasp as your muscles clench and bulge!\'\'"'
      getVariables().strength += either(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2)
      getVariables().esurience += either(0, 0, 0, 0, 0.1, 0.2, 0.5)
      getVariables().calories += either(20, 30, 40, 50)
    return
InventoryController.RegisterItemType BulkPowder

