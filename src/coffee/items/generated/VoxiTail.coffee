class VoxiTail extends Item
  constructor: ->
    super()
    @ID = 'tail_voxi'
    @Type = 'gubbin'
    @Name = 'Voxi Tail'
    @Encounter = 'voxi tail'
    @Description = 'The cute, fluffy tail of a voxi.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType VoxiTail

