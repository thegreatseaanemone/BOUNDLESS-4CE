class JarOfVitalWater extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_water_vital'
    @Type = 'status'
    @Name = 'Jar of Vital Water'
    @Encounter = 'jar of vital water'
    @Description = 'A jar full of pristine water. It almost seems to glow...'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"You feel soothed by the crystalline, gently-glowing waters! "'
      getVariables().miraPoisoning -= random(1, 6)
      getVariables().mirajinRads -= random(2, 4)
      getVariables().pain -= random(10, 30)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfVitalWater

