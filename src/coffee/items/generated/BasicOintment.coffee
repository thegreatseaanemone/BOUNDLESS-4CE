class BasicOintment extends Item
  constructor: ->
    super()
    @ID = 'med_ointment'
    @Type = 'status'
    @Name = 'Basic Ointment'
    @Encounter = 'basic ointment'
    @Description = 'This pulverized blend of herbs will soothe pain somewhat.'
    @Value = 30
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<affirmative>\'\'The mash of herbs soothes your pain\'\'!</affirmative>"'
      getVariables().pain -= getVariables().maxPain * 0.1
    return
InventoryController.RegisterItemType BasicOintment

