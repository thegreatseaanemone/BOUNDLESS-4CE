class JarOfVoracityVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_glutton'
    @Type = 'status'
    @Name = 'Jar of Voracity Venom'
    @Encounter = 'jar of voracity venom'
    @Description = 'A relatively harmless venom that causes increasing and ravenous hunger...'
    # [800.0, 800.0, 800.0]
    # OLD: @Value = 800.0
    @Value = 800.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().gluttonVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfVoracityVenom

