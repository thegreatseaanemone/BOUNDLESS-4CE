class ClassicLabcoat extends Item
  constructor: ->
    super()
    @ID = 'set_science_top'
    @Type = 'wearable'
    @Name = 'classic labcoat'
    @Encounter = 'classic labcoat'
    @Description = 'A must-have for medical and scientific personnel.'
    # [750.0, 750.0]
    # OLD: @Value = 750.0
    @Value = 750.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ClassicLabcoat

