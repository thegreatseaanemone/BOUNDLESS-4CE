class JarOfBreastBloatingVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_breastbloat'
    @Type = 'status'
    @Name = 'Jar of Breast-Bloating Venom'
    @Encounter = 'jar of breast-bloating venom'
    @Description = 'A startling, showy, and painful way to go...'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your skin begins to tighten...\'\'</brightalert>"'
      getVariables().breastbloatVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBreastBloatingVenom

