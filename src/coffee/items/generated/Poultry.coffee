class Poultry extends Item
  constructor: ->
    super()
    @ID = 'poultryStock'
    @Type = 'food'
    @Name = 'Poultry'
    @Encounter = ''
    @Description = 'A chunk of birdflesh, smooth and pale.'
    # [2.4, 2.4]
    # OLD: @Value = 2.4
    @Value = 2.4 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 10
      getVariables().bonusEnergy += 20
      getVariables().bellyBloat += 30
      getVariables().stomachBug += random(0, 20)
    return
InventoryController.RegisterItemType Poultry

