class HugeCroissantSandwich extends Item
  constructor: ->
    super()
    @ID = 'pastry_croissant'
    @Type = 'food'
    @Name = 'Huge Croissant Sandwich'
    @Encounter = 'croissant'
    @Description = 'A soft, sweet pastry fried in hot fattenium-- a Revix favorite.'
    @Value = 150
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 80
      getVariables().bellyBloat += 100
      getVariables().bonusEnergy += 50
    return
InventoryController.RegisterItemType HugeCroissantSandwich

