class MiracrystalDust extends Item
  constructor: ->
    super()
    @ID = 'miraStock'
    @Type = 'food'
    @Name = 'Miracrystal Dust'
    @Encounter = 'miracrystal ore'
    @Description = 'Shimmering magenta miracrystal powder. Who knows what eating this may do to you..?'
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(40, 50, 60, 70)
      getVariables().bellyBloat += either(10, 20, 30)
      getVariables().mirajinSaturation += random(1, 4)
    return
InventoryController.RegisterItemType MiracrystalDust

