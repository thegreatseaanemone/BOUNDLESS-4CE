class CompressionChamberPCB extends Item
  constructor: ->
    super()
    @ID = 'compresschamber_pcb_part'
    @Type = 'gubbin'
    @Name = 'Compression Chamber PCB'
    @Encounter = ''
    @Description = 'The controller board for a Compression Chamber.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType CompressionChamberPCB

