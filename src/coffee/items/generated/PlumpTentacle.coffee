class PlumpTentacle extends Item
  constructor: ->
    super()
    @ID = 'tentacle_plump'
    @Type = 'food'
    @Name = 'Plump Tentacle'
    @Encounter = 'plump tentacle'
    @Description = 'A soft, flabby tendril.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(35, 125)
      getVariables().bellyBloat += random(20, 160)
      getVariables().bonusEnergy += random(6, 60)
      getVariables().miraPoisoning += random(0, 12)
      getVariables().pain -= random(4, 24)
    return
InventoryController.RegisterItemType PlumpTentacle

