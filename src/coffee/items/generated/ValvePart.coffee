class ValvePart extends Item
  constructor: ->
    super()
    @ID = 'valve_part'
    @Type = 'gubbin'
    @Name = 'Valve Assembly'
    @Encounter = ''
    @Description = 'A small valve assembly used in machines.  Has a solenoid to actuate the valve electrically and two hose ports.'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ValvePart

