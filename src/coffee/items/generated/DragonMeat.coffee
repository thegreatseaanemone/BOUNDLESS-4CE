class DragonMeat extends Item
  constructor: ->
    super()
    @ID = 'dragonStock'
    @Type = 'food'
    @Name = 'Dragon Meat'
    @Encounter = ''
    @Description = 'A thick chunk of dragon meat. Pretty tough.'
    @Value = 40
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 50
      getVariables().bellyBloat += 30
      getVariables().bonusEnergy += 10
    return
InventoryController.RegisterItemType DragonMeat

