class JarOfLeupaiMilk extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_milk_leupai'
    @Type = 'drink'
    @Name = 'Jar of Leupai Milk'
    @Encounter = 'jar of leupai milk'
    @Description = 'A jar of milk from a... leupai. Um...'
    # [350.0, 350.0, 350.0]
    # OLD: @Value = 350.0
    @Value = 350.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(80, 90, 100, 110)
      getVariables().bellyBloat += 50
      getVariables().bonusEnergy += 50
      getVariables().bellyLiquid += 50
      getVariables().miraPoisoning += random(0, 3)
      getVariables().jarReturn = 1
      twineNewline()
      window.twineDisplayInline 'MilkCure'
    return
InventoryController.RegisterItemType JarOfLeupaiMilk

