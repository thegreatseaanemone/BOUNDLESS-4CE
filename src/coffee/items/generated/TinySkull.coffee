class TinySkull extends Item
  constructor: ->
    super()
    @ID = 'skull_tiny'
    @Type = 'gubbin'
    @Name = 'Tiny Skull'
    @Encounter = 'tiny skull'
    @Description = 'This little skull fits neatly in the palm of your hand.'
    # [5.0, 5.0, 5.0, 5.0, 3.0, 3.0]
    # OLD: @Value = 4.333333333333333
    @Value = 4.333333333333333 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType TinySkull

