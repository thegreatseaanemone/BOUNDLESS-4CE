class Milk extends Item
  constructor: ->
    super()
    @ID = 'milkStock'
    @Type = 'drink'
    @Name = 'Milk'
    @Encounter = ''
    @Description = 'Fresh, sweet milk, free of any unusual... additives.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 40
      getVariables().bellyBloat += 10
      getVariables().bellyLiquid += 10
      getVariables().bonusEnergy += 20
      getVariables().breast += either(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.2, 0.3)
      getVariables().milkRate += either(0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.2, 0.3, 0.5)
      getVariables().milkFat += either(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1)
      twineNewline()
      window.twineDisplayInline 'MilkCure'
      twineNewline()
    return
InventoryController.RegisterItemType Milk

