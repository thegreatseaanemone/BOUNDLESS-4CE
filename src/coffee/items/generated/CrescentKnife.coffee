class CrescentKnife extends Item
  constructor: ->
    super()
    @ID = 'knife_crescent'
    @Type = ''
    @Name = 'Crescent Knife'
    @Encounter = ''
    @Description = 'This vicious blade will rip flesh wide open!'
    @Value = 200
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType CrescentKnife

