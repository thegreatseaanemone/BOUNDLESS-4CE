class BottleOfFragrant extends Item
  constructor: ->
    super()
    @ID = 'drink_fragrant'
    @Type = 'drink'
    @Name = 'Bottle of Fragrant'
    @Encounter = 'fragrant'
    @Description = 'A bubbly, lightly-sweetened drink. Fruit and floral flavors are common.'
    # [10.0]
    # OLD: @Value = 10.0
    @Value = 10.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 10
      getVariables().bellyBloat += 60
      getVariables().bellyLiquid += 30
      getVariables().bonusEnergy += 10
    return
InventoryController.RegisterItemType BottleOfFragrant

