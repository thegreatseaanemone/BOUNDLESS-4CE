class ExperimentalButtercake extends Item
  constructor: ->
    super()
    @ID = 'pastry_buttercake_spiked'
    @Type = 'food'
    @Name = 'Experimental Buttercake'
    @Encounter = 'experimental buttercake'
    @Description = 'Something about this buttercake seems off. Not in the gross way, just the... suspicious way.'
    @Value = 2500
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(50, 60, 70, 80, 90, 100, 150, 200, 250)
      getVariables().bellyBloat += either(10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150)
      getVariables().bonusEnergy += either(0, 10, 20, 30, 40, 50, 100)
      getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(0, 0, 0, 10, 20))
    return
InventoryController.RegisterItemType ExperimentalButtercake

