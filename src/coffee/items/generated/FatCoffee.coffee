class FatCoffee extends Item
  constructor: ->
    super()
    @ID = 'drink_coffee_fat'
    @Type = 'drink'
    @Name = 'Fat Coffee'
    @Encounter = ''
    @Description = 'Coffee blended with milk and a generous splash of imperial cream, an extra indulgent pick-me-up.'
    # [125.0, 125.0]
    # OLD: @Value = 125.0
    @Value = 125.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 80
      getVariables().bellyBloat += 50
      getVariables().bellyLiquid += 30
      getVariables().bonusEnergy += 150
      getVariables().caffeineToxicity += random(5, 12)
    return
InventoryController.RegisterItemType FatCoffee

