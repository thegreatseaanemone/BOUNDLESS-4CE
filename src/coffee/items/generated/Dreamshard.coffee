class Dreamshard extends Item
  constructor: ->
    super()
    @ID = 'special_dreamshard'
    @Type = 'gubbin'
    @Name = 'Dreamshard'
    @Encounter = 'dreamshard'
    @Description = 'A small chunk of magenta crystal, pleasantly cool to the touch.'
    # [300.0, 300.0, 250.0, 250.0, 300.0, 300.0, 125.0, 120.0, 100.0, 100.0]
    # OLD: @Value = 214.5
    @Value = 214.5 # item.Value
    @Tags = ['mira']
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Dreamshard

