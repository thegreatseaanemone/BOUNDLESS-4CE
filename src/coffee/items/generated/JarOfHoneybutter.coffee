class JarOfHoneybutter extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_honeybutter'
    @Type = 'drink'
    @Name = 'Jar of Honeybutter'
    @Encounter = 'honeybutter'
    @Description = 'A rich, sweet, and delicious product collected from honeygorgers. Used as a source of high-calorie sugar in many Revix recipes.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += 90
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 20
      getVariables().bonusEnergy += 100
      getVariables().jarReturn = 1
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(6, 8)
      getVariables().sugarStock += getVariables().resourceYield
      getVariables().fatteniumStock += getVariables().resourceYield / 2
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Sugar +"'
      window.twinePrint '"'+getVariables().resourceYield / 2+'"'
      window.twinePrint '" Fattenium Powder]\'\'"'
    return
InventoryController.RegisterItemType JarOfHoneybutter

