class JarOfThighFatteningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_thighgain'
    @Type = 'status'
    @Name = 'Jar of Thigh-Fattening Venom'
    @Encounter = 'jar of thigh-fattening venom'
    @Description = 'This stuff will make short work of your pants.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().thighgainVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfThighFatteningVenom

