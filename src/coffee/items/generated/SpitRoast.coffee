class SpitRoast extends Item
  constructor: ->
    super()
    @ID = 'cooked_meat'
    @Type = 'food'
    @Name = 'Spit Roast'
    @Encounter = ''
    @Description = 'Campfire-roasted meat. Delicious.'
    @Value = 30
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(6, 38)
      getVariables().bellyBloat += random(24, 55)
      getVariables().bonusEnergy += random(25, 45)
      getVariables().pain -= random(4, 10)
    return
InventoryController.RegisterItemType SpitRoast

