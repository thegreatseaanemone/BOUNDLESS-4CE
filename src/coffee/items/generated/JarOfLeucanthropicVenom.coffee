class JarOfLeucanthropicVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_leucanthropy'
    @Type = 'status'
    @Name = 'Jar of Leucanthropic Venom'
    @Encounter = 'jar of leucanthropic venom'
    @Description = 'A slow-acting toxin...'
    # [8000.0]
    # OLD: @Value = 8000.0
    @Value = 8000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().leucanthropicVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfLeucanthropicVenom

