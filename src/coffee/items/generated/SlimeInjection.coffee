class SlimeInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_slime'
    @Type = 'status'
    @Name = 'Slime Injection'
    @Encounter = 'slime injection'
    @Description = 'A syringe full of living parasitic slime.'
    # [1000.0, 1000.0]
    # OLD: @Value = 1000.0
    @Value = 1000.0 # item.Value
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\'"'
      if getVariables().injectLoc == "breast"
        getVariables().slimeBreastParasite = 1
        getVariables().slimeBreastParaLoad += 50
        if getVariables().bodyType == 0
          getVariables().bodyType = either(0, 0, 0, "busty")
      else if getVariables().injectLoc == "belly"
        getVariables().slimeBellyParasite = 1
        getVariables().slimeBellyParaLoad += 50
        twineNewline()
        twineNewline()
        if getVariables().bodyType == 0
          getVariables().bodyType = either(0, 0, 0, "round", "belly")
    return
InventoryController.RegisterItemType SlimeInjection

