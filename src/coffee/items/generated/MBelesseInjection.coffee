class MBelesseInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_parasite_belesse'
    @Type = 'status'
    @Name = 'M. belesse Injection'
    @Encounter = 'M. belesse injection'
    @Description = 'A syringe full of Microleupii belesse micro-organisms, which cause gradual, spontaneous weight gain.'
    @Value = 5000
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\' "'
      getVariables().bodyPlumperParasite = 1
      getVariables().wgParaLoad += 10
    return
InventoryController.RegisterItemType MBelesseInjection

