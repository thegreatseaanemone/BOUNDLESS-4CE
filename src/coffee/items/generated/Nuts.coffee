class Nuts extends Item
  constructor: ->
    super()
    @ID = 'nutStock'
    @Type = 'food'
    @Name = 'Nuts'
    @Encounter = ''
    @Description = 'Aw, nuts...'
    # [0.5, 0.5]
    # OLD: @Value = 0.5
    @Value = 0.5 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 30
      getVariables().bellyBloat += 10
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType Nuts

