class PumpPCB extends Item
  constructor: ->
    super()
    @ID = 'pump_pcb_part'
    @Type = 'gubbin'
    @Name = 'Pump PCB'
    @Encounter = ''
    @Description = 'The controller board for a Pump.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType PumpPCB

