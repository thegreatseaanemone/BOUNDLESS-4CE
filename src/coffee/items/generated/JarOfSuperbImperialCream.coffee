class JarOfSuperbImperialCream extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_cream_superimperial'
    @Type = 'drink'
    @Name = 'Jar of Superb Imperial Cream'
    @Encounter = 'jar of superb imperial cream'
    @Description = 'Fatty, incredibly thick cream milked from specially-bred dairy creatures. Prized as a delicacy by the Revix.'
    @Value = 2000
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 160
      getVariables().bellyBloat += 40
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 150
      getVariables().jarReturn = 1
    return
InventoryController.RegisterItemType JarOfSuperbImperialCream

