class FatDemonBelt extends Item
  constructor: ->
    super()
    @ID = 'set_fatdemon_belt'
    @Type = 'wearable'
    @Name = 'Fat Demon Belt'
    @Encounter = 'fat demon belt'
    @Description = 'A grinning, huge-bellied demon adorns the oversized buckle of this belt. Wearing it makes it look as though the demon is kneading its claws into your stomach.'
    @Value = 5000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType FatDemonBelt

