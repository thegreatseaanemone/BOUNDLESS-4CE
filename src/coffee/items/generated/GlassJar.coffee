class GlassJar extends Item
  constructor: ->
    super()
    @ID = 'special_jar'
    @Type = 'gubbin'
    @Name = 'Glass Jar'
    @Encounter = 'glass jar'
    @Description = 'A simple empty jar. You could probably keep something in it!'
    # [50.0, 300.0]
    # OLD: @Value = 175.0
    @Value = 175.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType GlassJar

