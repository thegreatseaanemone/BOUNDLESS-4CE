class JarOfFatteniumVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_fattenium'
    @Type = 'status'
    @Name = 'Jar of Fattenium Venom'
    @Encounter = 'jar of fattenium venom'
    @Description = 'Incredibly, whatever creature this came from has evolved to use fattenium as a weapon. Scary stuff.'
    @Value = 4000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().fatteniumVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfFatteniumVenom

