class LeupaiFlesh extends Item
  constructor: ->
    super()
    @ID = 'leupaiStock'
    @Type = 'food'
    @Name = 'Leupai Flesh'
    @Encounter = ''
    @Description = 'A fatty, pillowy chunk of succulent leupai meat. The marbling is phenomenal.'
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 80
      getVariables().bellyBloat += 30
      getVariables().bonusEnergy += 20
      getVariables().direParaLoad += either(0, 0, 0, 0, 0, 0, 0, 1)
      getVariables().miraPoisoning += random(0, 3)
      getVariables().favorNefirian += random(0, 2)
    return
InventoryController.RegisterItemType LeupaiFlesh

