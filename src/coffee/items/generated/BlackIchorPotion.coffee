class BlackIchorPotion extends Item
  constructor: ->
    super()
    @ID = 'potion_puffbelly'
    @Type = 'status'
    @Name = 'Black Ichor Potion'
    @Encounter = 'black ichor potion'
    @Description = 'A bottle full of thick black goo that smells of mushrooms...'
    @Value = 5
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().belly += random(20, 60)
      getVariables().bellyBloat += getVariables().maxBelly * 2
      getVariables().miracoSpores += 200
      twineNewline()
      window.twinePrint '"You gag and choke as the sickly-sweet fungal goo slicks down your throat."'
      twineNewline()
      window.twinePrint '"\'\'As your belly begins to bloat and pulse and your mind slowly fogs, you realize you\'ve made a terrible mistake....\'\'"'
    return
InventoryController.RegisterItemType BlackIchorPotion

