class RipeTomato extends Item
  constructor: ->
    super()
    @ID = 'produce_tomato'
    @Type = 'food'
    @Name = 'Ripe Tomato'
    @Encounter = 'ripe tomato'
    @Description = 'A beautiful, fragrant tomato, just waiting to be eaten.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(3, 6)
      getVariables().bellyBloat += random(5, 8)
      getVariables().bonusEnergy += random(0, 3)
      getVariables().bellyLiquid += random(2, 6)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(2, 5)
      getVariables().vegStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Vegetables]\'\'"'
    return
InventoryController.RegisterItemType RipeTomato

