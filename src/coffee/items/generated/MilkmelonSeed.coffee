class MilkmelonSeed extends Item
  constructor: ->
    super()
    @ID = 'seed_milkmelon'
    @Type = 'food'
    @Name = 'Milkmelon Seed'
    @Encounter = ''
    @Description = 'So, if a zeppelon seed would grow inside your belly...?'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().randomizer = random(1, 48)
      twineNewline()
      twineNewline()
      # Parasitic germination chance 
      twineNewline()
      if (getVariables().randomizer == 1) && (getVariables().milkmelonParasite == 0)
        getVariables().milkmelonParasite = 1
        getVariables().breastMelonType = either(0, 0, "creaking")
      twineNewline()
    return
InventoryController.RegisterItemType MilkmelonSeed

