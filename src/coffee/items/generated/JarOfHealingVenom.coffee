class JarOfHealingVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_heal'
    @Type = 'status'
    @Name = 'Jar of Healing Venom'
    @Encounter = 'jar of healing venom'
    @Description = 'Perhaps the most unusual of all venoms, this liquid can actually promote healing.'
    # [1200.0]
    # OLD: @Value = 1200.0
    @Value = 5000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().healVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfHealingVenom

