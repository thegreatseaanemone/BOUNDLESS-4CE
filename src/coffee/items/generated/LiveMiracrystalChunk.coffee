class LiveMiracrystalChunk extends Item
  constructor: ->
    super()
    @ID = 'special_miracrystal'
    @Type = 'food'
    @Name = 'Live Miracrystal Chunk'
    @Encounter = 'live miracrystal chunk'
    @Description = 'This chunk of miracrystal seems to buzz unsettlingly in your hand...'
    # [500.0, 350.0, 60.0, 60.0, 60.0, 60.0, 100.0, 100.0, 120.0, 120.0, 150.0, 150.0, 160.0, 160.0, 200.0, 200.0]
    # OLD: @Value = 159.375
    @Value = 159.375 # item.Value
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyBloat += 50
      getVariables().mirajinSaturation += random(5, 25)
    return
InventoryController.RegisterItemType LiveMiracrystalChunk

