class CreakingMilkmelon extends Item
  constructor: ->
    super()
    @ID = 'produce_milkmelon_creaking'
    @Type = 'food'
    @Name = 'Creaking Milkmelon'
    @Encounter = 'creaking milkmelon'
    @Description = 'This milkmelon is overripe, and looks ready to burst.'
    @Value = 6
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories = random(30, 70)
      getVariables().liquidBloat = random(160, 400)
      getVariables().bellyBloat = getVariables().liquidBloat
      getVariables().bonusEnergy = random(30, 60)
      getVariables().bellyLiquid += getVariables().liquidBloat
      twineNewline()
      window.twineDisplayInline 'MilkCure'
      twineNewline()
      getVariables().breast += either(0, 0, 0, 0, 0, 0.1, 0.1, 0.3, 0.3, 0.5)
      getVariables().milkRate += either(0, 0, 0, 0, 0, 0.1, 0.1, 0.2, 0.2, 0.3, 0.5)
      twineNewline()
      twineNewline()
      # Seeds 
      twineNewline()
      getVariables().seedFruit = 1
      getVariables().seedID = "seed_milkmelon"
      getVariables().seedQuantity = random(0, 8)
    return

  Harvest: ->
    if super()
      twineNewline()
      getVariables().seedFruit = 1
      getVariables().seedID = "seed_milkmelon"
      getVariables().seedQuantity = random(0, 8)
      twineNewline()
      if getVariables().seedQuantity > 0
        getVariables().seed_milkmelon += getVariables().seedQuantity
      twineNewline()
      twineNewline()
      getVariables().resourceYield = random(2, 6)
      getVariables().fruitStock += getVariables().resourceYield
      getVariables().milkStock += (getVariables().resourceYield * 5)
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit +"'
      window.twinePrint '"'+getVariables().resourceYield * 5+'"'
      window.twinePrint '" Milk"'
      if getVariables().seedQuantity > 0
        window.twinePrint '" +"'
        window.twinePrint '"'+getVariables().seedQuantity+'"'
        window.twinePrint '" Seeds"'
      window.twinePrint '"]\'\'"'
    return
InventoryController.RegisterItemType CreakingMilkmelon

