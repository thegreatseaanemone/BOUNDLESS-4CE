class BalloonBelliedLeupaiCarving extends Item
  constructor: ->
    super()
    @ID = 'special_carving_belly'
    @Type = 'gubbin'
    @Name = 'Balloon-Bellied Leupai Carving'
    @Encounter = 'balloon-bellied leupai carving'
    @Description = 'A little crystal carving in the shape of a leupai with an ENORMOUS round belly. Holding it makes your own belly tingle...'
    @Value = 0
    @Tags = ['cursed']
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BalloonBelliedLeupaiCarving

