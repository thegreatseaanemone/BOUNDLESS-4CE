class VoluptuousPear extends Item
  constructor: ->
    super()
    @ID = 'produce_pear'
    @Type = 'food'
    @Name = 'Voluptuous Pear'
    @Encounter = 'voluptuous pear'
    @Description = 'Huge, and so fat you could almost mistake it for an apple.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(10, 40)
      getVariables().liquidBloat = random(17, 40)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(16, 35)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(6, 14)
      getVariables().fruitStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit]\'\'"'
    return
InventoryController.RegisterItemType VoluptuousPear

