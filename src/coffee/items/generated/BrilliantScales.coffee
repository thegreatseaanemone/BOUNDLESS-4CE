class BrilliantScales extends Item
  constructor: ->
    super()
    @ID = 'scales_brilliant'
    @Type = 'gubbin'
    @Name = 'Brilliant Scales'
    @Encounter = 'brilliant scales'
    @Description = 'A handful of scales that shimmer in the faintest light.'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BrilliantScales

