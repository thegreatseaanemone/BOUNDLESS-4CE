class JarOfCream extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_cream'
    @Type = 'drink'
    @Name = 'Jar of Cream'
    @Encounter = 'jar of cream'
    @Description = "A jar full of fresh cream, for cooking... or drinking outright if you're feeling brave or thirsty."
    # [60.0]
    # OLD: @Value = 60.0
    @Value = 60.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += 50
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 50
      getVariables().jarReturn = 1
      twineNewline()
      window.twineDisplayInline 'MilkCure'
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = 6
      getVariables().milkStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Milk]\'\'"'
    return
InventoryController.RegisterItemType JarOfCream

