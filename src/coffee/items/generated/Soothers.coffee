class Soothers extends Item
  constructor: ->
    super()
    @ID = 'pill_soother'
    @Type = 'status'
    @Name = 'Soothers'
    @Encounter = 'soothers'
    @Description = 'A potent pain medicine manufactured by HYPOTHESIS for general use, commonly available throughout Revix territories.'
    # [800.0, 700.0]
    # OLD: @Value = 750.0
    @Value = 750.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>Hopefully, \'\'you\'ll start to feel better soon..!\'\'</brightalert>"'
      getVariables().amalgaToxicity += random(8, 14)
    return
InventoryController.RegisterItemType Soothers

