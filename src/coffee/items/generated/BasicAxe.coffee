class BasicAxe extends Item
  constructor: ->
    super()
    @ID = 'key_axe'
    @Type = ''
    @Name = 'Basic Axe'
    @Encounter = ''
    @Description = "HEEERE'S JOHNNY!"
    @Value = 75
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BasicAxe

