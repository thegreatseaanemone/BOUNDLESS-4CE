class JarOfDirtyWater extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_water_dirty'
    @Type = 'status'
    @Name = 'Jar of Dirty Water'
    @Encounter = 'jar of dirty water'
    @Description = 'This water looks unpalatable, and is probably dangerous, too...'
    @Value = 1
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"You feel uneasy drinking this tainted-tasting water... "'
      getVariables().stomachBug = either(0, 1)
      getVariables().pain -= random(0, 3)
      getVariables().nuclearRads += random(1, 12)
      #(TODO: parasite call passage)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfDirtyWater

