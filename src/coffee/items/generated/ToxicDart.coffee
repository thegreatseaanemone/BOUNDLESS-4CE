class ToxicDart extends Item
  constructor: ->
    super()
    @ID = 'dart_toxic'
    @Type = 'gubbin'
    @Name = 'Toxic Dart'
    @Encounter = 'toxic dart'
    @Description = "This dart's only purpose is to kill... The Asotirix design is unmistakable."
    # [400.0, 400.0, 180.0]
    # OLD: @Value = 326.6666666666667
    @Value = 326.66 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ToxicDart

