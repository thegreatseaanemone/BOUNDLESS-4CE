class TuftOfFur extends Item
  constructor: ->
    super()
    @ID = 'fur_tuft'
    @Type = 'gubbin'
    @Name = 'Tuft of Fur'
    @Encounter = 'tuft of fur'
    @Description = 'A cute little puff of soft fur.'
    # [0.5, 0.5, 0.3, 0.3]
    # OLD: @Value = 0.4
    @Value = 0.4 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType TuftOfFur

