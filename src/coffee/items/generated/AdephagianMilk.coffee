class AdephagianMilk extends Item
  constructor: ->
    super()
    @ID = 'drink_milk'
    @Type = 'drink'
    @Name = 'Adephagian Milk'
    @Encounter = 'adephagian milk'
    @Description = 'A bottle of fresh adephagian milk, straight from the source.'
    # [20.0, 16.0, 30.0]
    # OLD: @Value = 22.0
    @Value = 22.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += either(10, 20, 30)
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 30
      twineNewline()
      window.twineDisplayInline 'MilkCure'
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = 3
      getVariables().milkStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Milk]\'\'"'
    return
InventoryController.RegisterItemType AdephagianMilk

