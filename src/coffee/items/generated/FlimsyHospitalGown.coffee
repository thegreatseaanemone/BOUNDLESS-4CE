class FlimsyHospitalGown extends Item
  constructor: ->
    super()
    @ID = 'set_hospital_top'
    @Type = 'wearable'
    @Name = 'flimsy hospital gown'
    @Encounter = 'flimsy hospital gown'
    @Description = 'Drafty. These things never cover anyone up...'
    # [250.0, 250.0]
    # OLD: @Value = 250.0
    @Value = 250.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType FlimsyHospitalGown

