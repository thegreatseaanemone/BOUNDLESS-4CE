class Driftwood extends Item
  constructor: ->
    super()
    @ID = 'material_driftwood'
    @Type = 'gubbin'
    @Name = 'Driftwood'
    @Encounter = 'driftwood'
    @Description = 'A bit waterlogged...'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Driftwood

