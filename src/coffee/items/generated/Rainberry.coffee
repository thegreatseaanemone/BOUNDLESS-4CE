class Rainberry extends Item
  constructor: ->
    super()
    @ID = 'berry_rain'
    @Type = 'status'
    @Name = 'Rainberry'
    @Encounter = 'rainberries'
    @Description = "A handful of colorful, magical berries; there's no telling what they'll do..."
    @Value = 6
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your belly gurgles strangely...\'\'"'
      twineNewline()
      twineNewline()
      # Effects to-do 
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Rainberry

