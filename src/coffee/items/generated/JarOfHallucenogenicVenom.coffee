class JarOfHallucenogenicVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_hallucenogen'
    @Type = 'status'
    @Name = 'Jar of Hallucenogenic Venom'
    @Encounter = 'jar of hallucenogenic venom'
    @Description = "Although this toxin likely won't hurt its victims, rest assured that they will still suffer horribly!"
    # [1800.0]
    # OLD: @Value = 1800.0
    @Value = 1800.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You feel faint...\'\'</deepalert>"'
      getVariables().hallucenoVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfHallucenogenicVenom

