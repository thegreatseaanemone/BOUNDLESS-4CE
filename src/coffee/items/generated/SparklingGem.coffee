class SparklingGem extends Item
  constructor: ->
    super()
    @ID = 'gem_sparkling'
    @Type = 'gubbin'
    @Name = 'Sparkling Gem'
    @Encounter = 'sparkling gem'
    @Description = 'A beautiful jewel. Certain to be worth something to someone.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType SparklingGem

