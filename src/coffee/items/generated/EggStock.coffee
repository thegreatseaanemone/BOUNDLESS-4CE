class EggStock extends Item
  constructor: ->
    super()
    @ID = 'eggStock'
    @Type = 'food'
    @Name = 'Egg Stock'
    @Encounter = ''
    @Description = 'Your stock of egg yolks.'
    # [0.8333333333333334, 0.8333333333333334]
    # OLD: @Value = 0.8333333333333334
    @Value = 10 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 10
      getVariables().bellyBloat += 10
      getVariables().bonusEnergy += 20
      getVariables().stomachBug += random(0, 20)
    return
InventoryController.RegisterItemType EggStock

