class Obsidian extends Item
  constructor: ->
    super()
    @ID = 'material_obsidian'
    @Type = ''
    @Name = 'Obsidian'
    @Encounter = ''
    @Description = 'Cooled black volcanic glass, beloved by diabolical architects everywhere.'
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Obsidian

