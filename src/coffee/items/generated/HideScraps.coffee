class HideScraps extends Item
  constructor: ->
    super()
    @ID = 'hide_scraps'
    @Type = 'gubbin'
    @Name = 'Hide Scraps'
    @Encounter = 'hide scraps'
    @Description = 'A learning experience?'
    @Value = 2
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType HideScraps

