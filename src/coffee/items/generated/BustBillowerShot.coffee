class BustBillowerShot extends Item
  constructor: ->
    super()
    @ID = 'shot_bustbillow'
    @Type = 'status'
    @Name = 'Bust Billower Shot'
    @Encounter = 'bust billower shot'
    @Description = 'A popular cosmetic medicine that fills out the breast.'
    # [900.0, 900.0]
    # OLD: @Value = 900.0
    @Value = 900.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 3)
      if getVariables().itemEffect != 1
        window.twinePrint '"You groan softly as your breasts \'\'swell a little bit "'
        window.twinePrint '"'+either("fatter", "rounder", "plumper", "fuller", "pudgier", "squishier", "softer")+'"'
        window.twinePrint '"...\'\'"'
        getVariables().breast += either(10, 10, 10, 20, 30) + getVariables().bonusBreast
        getVariables().breastBloat += (getVariables().maxBreast * 0.3)
        getVariables().breastfatteniumToxicity += random(1, 11)
      else if getVariables().itemEffect == 1
        window.twinePrint '"You writhe and moan aloud \'\'as your breasts "'
        window.twinePrint '"'+either("swell", "balloon", "swell up", "puff up", "plump up", "bulge", "bloat")+'"'
        window.twinePrint '" //considerably fatter..!//\'\'"'
        getVariables().breast += either(30, 40, 50) + getVariables().bonusBreast
        getVariables().breastBloat = getVariables().maxBreast
        getVariables().breastfatteniumToxicity += random(3, 16)
        getVariables().pain += random(1, 3)
      twineNewline()
      twineNewline()
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType BustBillowerShot

