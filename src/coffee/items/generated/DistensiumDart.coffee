class DistensiumDart extends Item
  constructor: ->
    super()
    @ID = 'dart_distensium'
    @Type = 'gubbin'
    @Name = 'Distensium Dart'
    @Encounter = 'distensium dart'
    @Description = 'Full of liquid distensium-- useful if you like balloons!'
    # [166.66666666666666, 166.66666666666666]
    # OLD: @Value = 166.66666666666666
    @Value = 166.66 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType DistensiumDart

