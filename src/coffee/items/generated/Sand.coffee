class Sand extends Item
  constructor: ->
    super()
    @ID = 'material_sand'
    @Type = ''
    @Name = 'Sand'
    @Encounter = ''
    @Description = "Soft, warm sand. Don't let any get in your food... or delicate places."
    @Value = 2
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Sand

