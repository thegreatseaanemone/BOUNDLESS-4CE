class JarOfVitae extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_vitae'
    @Type = 'status'
    @Name = 'Jar of Vitae'
    @Encounter = 'jar of vitae'
    @Description = 'GET RID OF IT. WHAT IS WRONG WITH YOU?!'
    # [5000.0]
    # OLD: @Value = 5000.0
    @Value = 5000.0 # item.Value
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<redalert>\'\'The moment your flesh touches the glowing liquid, you grasp the severity of your error! You scream in unimaginable agony as the vitae courses through your bloodstream, dissolving you into little more than a puddle of sizzling ichor and a distant memory...\'\'</redalert>"'
      #(TODO: vitae soulsickness)
      getVariables().blackout = 1
      getVariables().lethalKO = 1
      getVariables().deathCause = "vitae annihilation"
      getVariables().pain += getVariables().maxPain * 10
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfVitae

