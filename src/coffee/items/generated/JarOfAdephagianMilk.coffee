class JarOfAdephagianMilk extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_milk_adephagian'
    @Type = 'drink'
    @Name = 'Jar of Adephagian Milk'
    @Encounter = 'jar of adephagian milk'
    @Description = 'A jar of fresh adephagian milk, straight from the source.'
    @Value = 30
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += 40
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 40
      getVariables().jarReturn = 1
      twineNewline()
      window.twineDisplayInline 'MilkCure'
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = 8
      getVariables().milkStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Milk]\'\'"'
    return
InventoryController.RegisterItemType JarOfAdephagianMilk

