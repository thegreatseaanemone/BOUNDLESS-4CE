class MetalPickaxe extends Item
  constructor: ->
    super()
    @ID = 'key_pickaxe_metal'
    @Type = ''
    @Name = 'Metal Pickaxe'
    @Encounter = ''
    @Description = 'Break away loose rocks and ores with this handy tool-- built to last.'
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType MetalPickaxe

