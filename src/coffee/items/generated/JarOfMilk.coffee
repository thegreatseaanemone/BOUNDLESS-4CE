class JarOfMilk extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_milk'
    @Type = 'drink'
    @Name = 'Jar of Milk'
    @Encounter = 'jar of milk'
    @Description = 'Fresh milk by the jarful.'
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += 30
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 30
      getVariables().jarReturn = 1
      twineNewline()
      window.twineDisplayInline 'MilkCure'
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = 5
      getVariables().milkStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Milk]\'\'"'
    return
InventoryController.RegisterItemType JarOfMilk

