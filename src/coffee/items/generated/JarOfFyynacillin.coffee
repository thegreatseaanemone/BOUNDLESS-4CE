class JarOfFyynacillin extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_fyynacillin'
    @Type = 'status'
    @Name = 'Jar of Fyynacillin'
    @Encounter = 'jar of fyynacillin'
    @Description = 'A jar of Fyynacillin, a powerful anti-fyynling mutation drug. The dosages on the side are really, really small, so you probably should drink the whole jar.  Right?'
    @ChemID = 'Fyynacillin'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().player.Chemicals.Add(Fyynacillin, 10)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfFyynacillin

