class Starberry extends Item
  constructor: ->
    super()
    @ID = 'produce_starberry'
    @Type = 'food'
    @Name = 'Starberry'
    @Encounter = 'starberry'
    @Description = 'A tiny, star-shaped berry that glows with its own soft light. Aww! Tastes like lemonade.'
    @Value = 0
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += 0
      getVariables().bellyBloat += random(0, 3)
      getVariables().bonusEnergy += random(3, 8)
      getVariables().bellyLiquid += random(0, 3)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = either(1, 1, 1, 1, 2, 3)
      getVariables().fruitStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit]\'\'"'
    return
InventoryController.RegisterItemType Starberry

