class BloodyHospitalGown extends Item
  constructor: ->
    super()
    @ID = 'set_hospital_top_bloody'
    @Type = 'wearable'
    @Name = 'bloody hospital gown'
    @Encounter = 'bloody hospital gown'
    @Description = "You're more than a little concerned about the bloodstains and gashes decorating this predictably sparse garment..."
    # [500.0, 500.0]
    # OLD: @Value = 500.0
    @Value = 500.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BloodyHospitalGown

