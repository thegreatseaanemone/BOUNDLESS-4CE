class Vegetable extends Item
  constructor: ->
    super()
    @ID = 'vegStock'
    @Type = 'food'
    @Name = 'Vegetable'
    @Encounter = ''
    @Description = 'An uncooked vegetable, maybe nice for a snack.'
    # [1.6, 1.6]
    # OLD: @Value = 1.6
    @Value = 1.6 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(0, 10)
      getVariables().bellyBloat += 20
      getVariables().bonusEnergy += 30
    return
InventoryController.RegisterItemType Vegetable

