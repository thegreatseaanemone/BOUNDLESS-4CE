class Roundberry extends Item
  constructor: ->
    super()
    @ID = 'berry_round'
    @Type = 'status'
    @Name = 'Roundberry'
    @Encounter = 'roundberries'
    @Description = 'These berries look ready to burst at any instant!'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your belly gurgles as it puffs up rounder!\'\'"'
      twineNewline()
      twineNewline()
      getVariables().belly += random(5, 15)
      getVariables().bellyBloat += (getVariables().maxBelly * 0.2)
      getVariables().calories += random(2, 8)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Roundberry

