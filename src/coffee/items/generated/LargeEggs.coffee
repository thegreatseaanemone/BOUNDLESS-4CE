class LargeEggs extends Item
  constructor: ->
    super()
    @ID = 'egg_large'
    @Type = 'food'
    @Name = 'Large Egg'
    @Encounter = 'large eggs'
    @Description = "A generously-sized egg. They'd make for a nice meal."
    @Value = 200
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType LargeEggs

