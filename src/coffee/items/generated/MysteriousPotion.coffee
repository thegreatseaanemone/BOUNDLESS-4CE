class MysteriousPotion extends Item
  constructor: ->
    super()
    @ID = 'potion_mystery'
    @Type = 'status'
    @Name = 'Mysterious Potion'
    @Encounter = 'mysterious potion'
    @Description = 'This bottle of unidentified liquid could be or do anything...'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      # Insert encounter names of valid types 
      getVariables().bellyBloat += random(8, 25)
      getVariables().bellyLiquid += random(8, 25)
      getVariables().itemEncounter = either("miraberries", "roundberries", "burstberries", "cowberries", "plushberries", "bulgeberries", "bounceberries", "butterberries", "strange mushroom", "glowing mushroom", "calorie powder", "extra strength calorie powder", "maximum strength calorie powder", "experimental calorie powder", "gainer pills", "leupai oil", "helium pills", "bolstering vitamin slurry", "bulk powder", "limber tonic", "belly booster", "super belly booster", "puffbelly ichor", "bust billower shot", "super bust billower shot", "thigh plumper shot", "super thigh plumper shot", "bellybomb", "mega bellybomb", "fattenium shot", "pure fattenium shot", "unmarked injection", "miradetoxin shot", "dose of reality injection", "deflator injection", "antibiotic injection", "antifungal injection", "solvent injection", "voracity shot", "satiator shot", "miratoxin shot", "tiny fyynling specimen", "soothers", "strange milk", "jar of soothing milk", "jar of toxic venom", "jar of lethal venom", "jar of fattening venom", "jar of super-fattening venom", "jar of belly-fattening venom", "jar of belly super-fattening venom", "jar of breast-fattening venom", "jar of breast super-fattening venom", "jar of thigh-fattening venom", "jar of thigh super-fattening venom", "jar of fattenium venom", "jar of bulking venom", "jar of flabbifying venom", "jar of bloating venom", "jar of breast-bloating venom", "jar of muscle-bloating venom", "jar of bursting venom", "jar of healing venom", "jar of weakening venom", "jar of voracity venom", "jar of caloric venom", "jar of hallucenogenic venom", "jar of mutagenic venom", "jar of leucanthropic venom", "egg accelerator injection", "venombloat injection", "jar of somnus", "jar of strong somnus", "jar of miratoxin", "jar of wild miratoxin", "jar of dilute fattenium", "jar of dilute miraplasm", "jar of vital water", "jar of dirty water", "jar of blood", "jar of leupai blood", "jar of fattenium", "jar of vitae")
      getVariables().mysteryPotioned = 1
    return
InventoryController.RegisterItemType MysteriousPotion

