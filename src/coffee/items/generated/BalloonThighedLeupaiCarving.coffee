class BalloonThighedLeupaiCarving extends Item
  constructor: ->
    super()
    @ID = 'special_carving_thigh'
    @Type = 'gubbin'
    @Name = 'Balloon-Thighed Leupai Carving'
    @Encounter = 'balloon-thighed leupai carving'
    @Description = "A little crystal carving in the shape of a leupai with the biggest, fattest thighs you've ever seen. Holding it makes your own thighs tingle..."
    @Value = 0
    @Tags = ['cursed']
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BalloonThighedLeupaiCarving

