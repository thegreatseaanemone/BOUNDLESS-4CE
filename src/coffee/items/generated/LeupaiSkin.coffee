class LeupaiSkin extends Item
  constructor: ->
    super()
    @ID = 'hide_leupai'
    @Type = 'gubbin'
    @Name = 'Leupai Skin'
    @Encounter = 'leupai skin'
    @Description = 'A thick, rubbery sheet of leupai skin.'
    @Value = 30
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType LeupaiSkin

