class EggAcceleratorInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_eggGrowth'
    @Type = 'status'
    @Name = 'Egg Accelerator Injection'
    @Encounter = 'egg accelerator injection'
    @Description = 'An injection used by leupai and hosts to increase the rate at which eggs develop. Can have side-effects on hatchlings...'
    # [1000.0, 1000.0]
    # OLD: @Value = 1000.0
    @Value = 1000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().leupaiEggTimer == 0
        window.twinePrint '"...but nothing happens."'
        getVariables().miraPoisoning += random(2, 8)
      else if getVariables().leupaiEggTimer > 0
        window.twinePrint '"\'\'Your belly expands as the eggs inside it grow bigger and heavier!\'\'"'
        if getVariables().geneDaedlan == 1
          getVariables().quaggyGirth += either(10, 10, 20)
        getVariables().leupaiEggTimer -= random(2, 3)
        getVariables().quaggyGirth += either(10, 10, 20)
        getVariables().randomizer = either(0, 0, 0, 1)
        getVariables().miraPoisoning += random(2, 8)
        getVariables().health = "Sore"
        getVariables().lethalKO = 1
        getVariables().deathCause = "burst belly"
        getVariables().esurience += either(0, 0, 0, 0, 0.1, 0.2, 0.5, 1, 2)
        if getVariables().randomizer == 1
          getVariables().quaggySuperfattened = 1
    return
InventoryController.RegisterItemType EggAcceleratorInjection

