class FatteniumDonut extends Item
  constructor: ->
    super()
    @ID = 'donut_fattenium'
    @Type = 'food'
    @Name = 'Fattenium Donut'
    @Encounter = 'fattenium donuts'
    @Description = 'A soft, sweet pastry fried in hot fattenium-- a Revix favorite.'
    # [300.0, 500.0]
    # OLD: @Value = 400.0
    @Value = 400.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(65, 110)
      getVariables().bellyBloat += 100
      getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(0, 0, 0, 10))
      getVariables().bonusEnergy += 100
    return
InventoryController.RegisterItemType FatteniumDonut

