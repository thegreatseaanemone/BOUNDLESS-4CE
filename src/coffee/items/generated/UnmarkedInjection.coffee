class UnmarkedInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_unmarked'
    @Type = 'status'
    @Name = 'Unmarked Injection'
    @Encounter = ''
    @Description = 'A syringe full of *something*. Are you feeling lucky AND brave..?'
    # [900.0, 900.0, 800.0, 800.0]
    # OLD: @Value = 850.0
    @Value = 850.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType UnmarkedInjection

