class FatteniumDart extends Item
  constructor: ->
    super()
    @ID = 'dart_fattenium'
    @Type = 'gubbin'
    @Name = 'Fattenium Dart'
    @Encounter = 'fattenium dart'
    @Description = 'Full of liquid fattenium, good for rapidly fleshing out targets.'
    # [200.0, 200.0, 200.0, 200.0, 180.0]
    # OLD: @Value = 196.0
    @Value = 196.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType FatteniumDart

