class PlagueDoctorHood extends Item
  constructor: ->
    super()
    @ID = 'set_plague_head'
    @Type = 'wearable'
    @Name = 'plague doctor hood'
    @Encounter = 'plague doctor hood'
    @Description = 'A heavy hood made for protecting hair and skin from chemical and biological hazards alike.'
    @Value = 250
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType PlagueDoctorHood

