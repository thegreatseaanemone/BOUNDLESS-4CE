class JarOfBellySuperFatteningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_bellysupergain'
    @Type = 'status'
    @Name = 'Jar of Belly Super-Fattening Venom'
    @Encounter = 'jar of belly super-fattening venom'
    @Description = 'This venom causes dangerously rapid accumulation of belly fat.'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().bellySupergainVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBellySuperFatteningVenom

