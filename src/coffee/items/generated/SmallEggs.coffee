class SmallEggs extends Item
  constructor: ->
    super()
    @ID = 'egg_small'
    @Type = 'food'
    @Name = 'Small Egg'
    @Encounter = 'small eggs'
    @Description = 'Tiny eggs. Adorable.'
    @Value = 0.5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType SmallEggs

