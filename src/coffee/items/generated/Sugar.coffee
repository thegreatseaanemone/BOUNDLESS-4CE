class Sugar extends Item
  constructor: ->
    super()
    @ID = 'sugarStock'
    @Type = 'food'
    @Name = 'Sugar'
    @Encounter = ''
    @Description = 'A handful of crystallized sugar. Better for a recipe than eaten outright, but it may do in a pinch.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 30
      getVariables().bellyBloat += 10
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType Sugar

