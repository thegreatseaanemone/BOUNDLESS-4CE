class Honeycarrot extends Item
  constructor: ->
    super()
    @ID = 'produce_honeycarrot'
    @Type = 'food'
    @Name = 'Honeycarrot'
    @Encounter = 'honeycarrot'
    @Description = 'A long, sweet root vegetable, delicious raw or roasted.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(4, 6)
      getVariables().bellyBloat += random(6, 8)
      getVariables().bonusEnergy += random(6, 10)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(1, 3)
      getVariables().rootStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Root]\'\'"'
    return
InventoryController.RegisterItemType Honeycarrot

