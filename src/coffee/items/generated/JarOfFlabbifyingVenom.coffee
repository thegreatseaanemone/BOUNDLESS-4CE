class JarOfFlabbifyingVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_flab'
    @Type = 'status'
    @Name = 'Jar of Flabbifying Venom'
    @Encounter = 'jar of flabbifying venom'
    @Description = 'A strange and dangerous venom capable of causing a blubbery fate.'
    # [1600.0, 1600.0]
    # OLD: @Value = 1600.0
    @Value = 1600.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().flabbyVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfFlabbifyingVenom

