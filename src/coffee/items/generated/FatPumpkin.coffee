class FatPumpkin extends Item
  constructor: ->
    super()
    @ID = 'produce_fatpumpkin'
    @Type = 'food'
    @Name = 'Fat Pumpkin'
    @Encounter = 'fat pumpkin'
    @Description = 'This pumpkin is ENORMOUS!'
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(8, 50)
      getVariables().liquidBloat = random(100, 600)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(35, 85)
      getVariables().bellyLiquid += getVariables().liquidBloat
      twineNewline()
      getVariables().belly += either(0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.2, 0.3)
      twineNewline()
      twineNewline()
      # Seeds 
      twineNewline()
      getVariables().seedFruit = 1
      getVariables().seedID = "seed_fatpumpkin"
      getVariables().seedQuantity = random(6, 16)
    return

  Harvest: ->
    if super()
      twineNewline()
      getVariables().seedFruit = 1
      getVariables().seedID = "seed_fatpumpkin"
      getVariables().seedQuantity = random(6, 16)
      twineNewline()
      if getVariables().seedQuantity > 0
        getVariables().seed_fatpumpkin += getVariables().seedQuantity
      twineNewline()
      twineNewline()
      getVariables().resourceYield = random(8, 24)
      getVariables().vegStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Vegetables"'
      if getVariables().seedQuantity > 0
        window.twinePrint '" +"'
        window.twinePrint '"'+getVariables().seedQuantity+'"'
        window.twinePrint '" Seeds"'
      window.twinePrint '"]\'\'"'
    return
InventoryController.RegisterItemType FatPumpkin

