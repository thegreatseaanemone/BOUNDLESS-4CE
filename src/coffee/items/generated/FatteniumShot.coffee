class FatteniumShot extends Item
  constructor: ->
    super()
    @ID = 'shot_fattenium'
    @Type = 'status'
    @Name = 'Fattenium Shot'
    @Encounter = 'fattenium shot'
    @Description = 'A syringe full of somewhat dilute liquid fattenium. Filling!'
    # [10000.0, 8000.0, 8000.0]
    # OLD: @Value = 8666.666666666666
    @Value = 8666.6 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"[img[icon_alert_critical]]<brightalert>You tremble, quake, and groan as \'\'your body suddenly swells and puffs outwards in a cascade of new, rippling fat! You feel like a water balloon... Oh God, was that even a good idea?\'\'</brightalert>"'
      twineNewline()
      twineNewline()
      window.twinePrint '"You feel dazed, heavy, and sluggish..."'
      getVariables().girth += either(50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150)
      getVariables().calories += either(100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200)
      getVariables().belly += either(20, 30, 40, 50) + getVariables().bonusBelly
      getVariables().bellyBloat = getVariables().maxBelly * 1.1
      getVariables().breast += either(0, 10, 20, 30) + getVariables().bonusBreast
      getVariables().breastBloat += getVariables().breast
      getVariables().thigh += either(20, 30, 40, 50) + getVariables().bonusThigh
      getVariables().thighFirmness -= random(20, 50)
      getVariables().metaGain += either(0, 0, 0, 10, 20)
      getVariables().metaBurn -= either(0, 1)
      getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(10, 20, 20, 30, 40, 50))
      getVariables().pain += random(1, 6)
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType FatteniumShot

