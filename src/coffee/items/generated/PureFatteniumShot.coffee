class PureFatteniumShot extends Item
  constructor: ->
    super()
    @ID = 'shot_fattenium_pure'
    @Type = 'status'
    @Name = 'Pure Fattenium Shot'
    @Encounter = 'pure fattenium shot'
    @Description = 'This is probably dangerous...'
    # [15000.0, 35000.0, 16000.0, 16000.0]
    # OLD: @Value = 20500.0
    @Value = 2050.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"[img[icon_alert_red]]<deepalert>Immediately, you crumple onto your knees, screaming in agony as \'\'your body quivers, bulges, and blimps rapidly, creaking as your weight skyrockets and your skin threatens to rip apart!\'\'</deepalert>"'
      getVariables().girth += either(100, 110, 120, 130, 140, 150, 180, 300)
      getVariables().calories += either(500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000)
      getVariables().belly += either(50, 60, 70, 80, 90, 100) + getVariables().bonusBelly
      getVariables().bellyBloat = getVariables().maxBelly * 1.3
      getVariables().breast += either(40, 50, 60, 70, 80, 90, 100) + getVariables().bonusBreast
      getVariables().breastBloat += getVariables().breast
      getVariables().thigh += either(40, 50, 60, 70, 80, 90, 100) + getVariables().bonusThigh
      getVariables().thighFirmness -= getVariables().thigh
      getVariables().metaGain += either(10, 10, 20, 30)
      getVariables().metaBurn -= either(10, 10, 20, 30)
      getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(30, 40, 50, 60, 70, 80, 90, 100))
      getVariables().pain += random(9, 16)
      getVariables().health = "Sore"
      getVariables().lethalKO = 1
      getVariables().deathCause = "explosion"
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType PureFatteniumShot

