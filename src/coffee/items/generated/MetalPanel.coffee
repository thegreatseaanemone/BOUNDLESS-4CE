class MetalPanel extends Item
  constructor: ->
    super()
    @ID = 'metal_panel'
    @Type = 'gubbin'
    @Name = 'Metal Panel'
    @Encounter = ''
    @Description = 'A metal panel used to conceal the innards of machines.'
    @Value = 200
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType MetalPanel

