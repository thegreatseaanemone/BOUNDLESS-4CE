class Rockroot extends Item
  constructor: ->
    super()
    @ID = 'produce_rockroot'
    @Type = 'food'
    @Name = 'Rockroot'
    @Encounter = 'rockroot'
    @Description = 'A root commonly used in Revix cooking. Incredibly hard and crunchy when raw, but grinds into an excellent flour.'
    @Value = 7
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(8, 16)
      getVariables().bellyBloat += random(10, 16)
      getVariables().bonusEnergy += random(3, 5)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(2, 4)
      getVariables().rootStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Root]\'\'"'
    return
InventoryController.RegisterItemType Rockroot

