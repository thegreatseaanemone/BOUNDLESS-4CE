class StrangeMilk extends Item
  constructor: ->
    super()
    @ID = 'milk_strange'
    @Type = 'status'
    @Name = 'Strange Milk'
    @Encounter = 'strange milk'
    @Description = "It's definitely milk. There's no telling what it's milk FROM, though..."
    # [1250.0, 1250.0, 1500.0, 2000.0, 1500.0, 1500.0]
    # OLD: @Value = 1500.0
    @Value = 500.0 # item.Value
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'A chill ices down your spine...\'\' what the hell was //that?!//"'
      twineNewline()
      getVariables().calories += either(200, 250, 300, 350)
      getVariables().bellyBloat += (getVariables().maxBelly / 3)
      getVariables().bellyLiquid += 50
      getVariables().milkRate += random(0, 3)
      getVariables().helenoToxicity += random(1, 6)
      getVariables().breastfatteniumToxicity += random(1, 3)
      getVariables().miraPoisoning += random(1, 6)
      getVariables().esurience += either(0, 0, 0, 0, 0.1, 0.2, 0.5, 1, 2, 3)
      twineNewline()
      twineNewline()
      if getVariables().bodyType == 0
        getVariables().bodyType = either(0, 0, 0, 0, 0, "round", "belly", "pear")
      else if getVariables().bodyType == "busty"
        getVariables().bodyType = either("busty", "hourglass")
      else if getVariables().bodyType == "thigh"
        getVariables().bodyType = either("thigh", "pear")
      twineNewline()
      twineNewline()
      getVariables().randomizer = either(0, 0, 1, 2, 3)
      if getVariables().randomizer == 1
        window.twinePrint '"You groan softly as your breasts \'\'swell a little bit "'
        window.twinePrint '"'+either("fatter", "rounder", "plumper", "fuller", "pudgier", "squishier", "softer")+'"'
        window.twinePrint '"...\'\'"'
        getVariables().breast += either(10, 10, 10, 20, 30) + getVariables().bonusBreast
        getVariables().breastBloat += (getVariables().maxBreast / 3)
        getVariables().helenoToxicity += random(1, 3)
        twineNewline()
        twineNewline()
      else if getVariables().randomizer == 2
        window.twinePrint '"You writhe and moan aloud \'\'as your breasts "'
        window.twinePrint '"'+either("swell", "balloon", "swell up", "puff up", "plump up", "bulge", "bloat")+'"'
        window.twinePrint '" //considerably fatter..!//\'\'"'
        getVariables().breast += either(30, 40, 50) + getVariables().bonusBreast
        getVariables().breastBloat = getVariables().maxbreast * 2
        getVariables().helenoToxicity += random(2, 5)
        getVariables().pain += random(1, 3)
        twineNewline()
        twineNewline()
      else if getVariables().randomizer == 3
        window.twinePrint '"You writhe and moan aloud \'\'as your breasts "'
        window.twinePrint '"'+either("swell", "balloon", "swell up", "puff up", "plump up", "bulge", "bloat")+'"'
        window.twinePrint '" //considerably fatter,// creaking and jiggling as they rapidly inflate"'
        if getVariables().geneDry == 0
          window.twinePrint '" with milk!"'
        window.twinePrint '"\'\'"'
        getVariables().lactation = 1
        getVariables().breast += either(30, 40, 50) + getVariables().bonusBreast
        getVariables().breastBloat = getVariables().maxBreast * 5
        getVariables().storedMilk = (getVariables().maxBreast * 0.5)
        getVariables().milkFat += random(1, 3)
        getVariables().milkRate += 10
        getVariables().helenoToxicity += random(2, 8)
        getVariables().pain += random(1, 3)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType StrangeMilk

