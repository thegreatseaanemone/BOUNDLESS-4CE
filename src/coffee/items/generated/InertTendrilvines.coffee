class InertTendrilvines extends Item
  constructor: ->
    super()
    @ID = 'produce_tendrilvine'
    @Type = 'food'
    @Name = 'Inert Tendrilvines'
    @Encounter = 'inert tendrilvines'
    @Description = 'A bunch of mercifully lifeless tendrilvines, still fat with nectar.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(20, 38)
      getVariables().liquidBloat = random(32, 55)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(18, 32)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(10, 16)
      getVariables().vegStock += getVariables().resourceYield
      getVariables().waterStock += (getVariables().resourceYield * 3)
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Vegetables +"'
      window.twinePrint '"'+getVariables().resourceYield * 3+'"'
      window.twinePrint '" Water]\'\'"'
    return
InventoryController.RegisterItemType InertTendrilvines

