class Jelly extends Item
  constructor: ->
    super()
    @ID = 'jellyStock'
    @Type = 'food'
    @Name = 'Jelly'
    @Encounter = ''
    @Description = 'A blob of squishy, sweet gelatin. A... resource?'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 30
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 10
      getVariables().bonusEnergy += 10
      getVariables().miraPoisoning += random(0, 3)
    return
InventoryController.RegisterItemType Jelly

