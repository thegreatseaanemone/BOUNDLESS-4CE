class ZeppelonSeed extends Item
  constructor: ->
    super()
    @ID = 'seed_zeppelon'
    @Type = 'food'
    @Name = 'Zeppelon Seed'
    @Encounter = ''
    @Description = 'Rumor has it that if you swallow too many of these, a zeppelon might grow inside you. Rumors are silly.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().randomizer = random(1, 48)
      twineNewline()
      twineNewline()
      # Parasitic germination chance 
      twineNewline()
      if (getVariables().randomizer == 1) && (getVariables().zeppelonParasite == 0) && (getVariables().bellyLiquid >= 20)
        getVariables().zeppelonParasite = 1
      twineNewline()
    return
InventoryController.RegisterItemType ZeppelonSeed

