class JarOfWildMiratoxin extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_miratoxin_wild'
    @Type = 'status'
    @Name = 'Jar of Wild Miratoxin'
    @Encounter = 'jar of wild miratoxin'
    @Description = 'This leupai venom is mutagenic, but it may also have strange side effects...'
    # [3000.0]
    # OLD: @Value = 3000.0
    @Value = 3000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"You feel "'
      getVariables().itemEffect = random(1, 3)
      getVariables().mirajinRads += random(3, 12)
      if getVariables().itemEffect == 1
        getVariables().miraPoisoning += random(4, 12)
        getVariables().bellyBloat += (getVariables().maxBelly * 0.5)
        getVariables().calories += either(30, 40, 50, 60, 70, 80, 90, 100)
        getVariables().bellyLiquid += 50
        getVariables().health = "Sore"
        getVariables().lethalKO = 1
        getVariables().deathCause = "burst belly"
        window.twinePrint '"\'\'//bloated...//\'\'"'
      if getVariables().itemEffect == 2
        getVariables().miraPoisoning += random(6, 18)
        getVariables().bellyBloat += (getVariables().maxBelly * 0.8)
        getVariables().calories += either(50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150)
        getVariables().bellyLiquid += 50
        getVariables().health = "Sore"
        getVariables().lethalKO = 1
        getVariables().deathCause = "burst belly"
        window.twinePrint '"\'\'//strange...//\'\'"'
      if getVariables().itemEffect == 3
        getVariables().miraPoisoning += random(12, 36)
        getVariables().bellyBloat = getVariables().maxBelly * 1.1
        getVariables().calories += either(100, 110, 120, 130, 140, 150, 200, 250)
        getVariables().bellyLiquid += 50
        getVariables().pain += random(1, 3)
        getVariables().health = "Sore"
        getVariables().lethalKO = 1
        getVariables().deathCause = "burst belly"
        window.twinePrint '"\'\'//unwell...//\'\'"'
      twineNewline()
      twineNewline()
      # Side effects 
      twineNewline()
      twineNewline()
      getVariables().randomizer = random(1, 26)
      twineNewline()
      twineNewline()
      # High-mirajin venom 
      twineNewline()
      if getVariables().randomizer == 1
        getVariables().mirajinRads += random(60, 300)
        twineNewline()
        twineNewline()
        # Extra potency venom 
        twineNewline()
      else if getVariables().randomizer == 2
        getVariables().miraPoisoning += random(20, 150)
        twineNewline()
        twineNewline()
        # High-calorie venom 
        twineNewline()
      else if getVariables().randomizer == 3
        getVariables().calories += random(50, 200)
        twineNewline()
        twineNewline()
        # Bloat venom 
        twineNewline()
      else if getVariables().randomizer == 4
        getVariables().bellyBloat += (getVariables().maxBelly * random(0, 5))
        twineNewline()
        twineNewline()
        # Fattenium venom 
        twineNewline()
      else if getVariables().randomizer == 5
        getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(10, 20, 30, 40, 50))
        twineNewline()
        twineNewline()
        # Oily venom 
        twineNewline()
      else if getVariables().randomizer == 6
        getVariables().stretchMod += random(5, 25)
        getVariables().player.Chemicals.Add(LeupaiToxinChemical, random(3, 24))
        getVariables().calories += random(20, 30, 40, 150)
        twineNewline()
        twineNewline()
        # Living venom 
        twineNewline()
      else if getVariables().randomizer == 7
        getVariables().slimeBreastParasite = 1
        getVariables().slimeBreastParaLoad += random(0, 100)
        getVariables().slimeBellyParasite = 1
        getVariables().slimeBellyParaLoad += random(0, 200)
        twineNewline()
        twineNewline()
        # Healing venom 
        twineNewline()
      else if getVariables().randomizer == 8
        getVariables().healVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Caffeine venom 
        twineNewline()
      else if getVariables().randomizer == 9
        getVariables().caffeinePoisoning = 1
        getVariables().bonusEnergy += random(100, 800)
        twineNewline()
        twineNewline()
        # Greed venom 
        twineNewline()
      else if getVariables().randomizer == 10
        getVariables().esurience += either(0.1, 0.2, 0.5, 1, 2, 3, 5, 10)
        twineNewline()
        twineNewline()
        # Were venom 
        twineNewline()
      else if getVariables().randomizer == 11
        getVariables().leucanthropicVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Weakening venom 
        twineNewline()
      else if getVariables().randomizer == 12
        getVariables().weakeningVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Breast Bloat venom 
        twineNewline()
      else if getVariables().randomizer == 13
        getVariables().breastbloatVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Burst venom 
        twineNewline()
      else if getVariables().randomizer == 14
        getVariables().burstVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Bulk venom 
        twineNewline()
      else if getVariables().randomizer == 15
        getVariables().bulkVenom += random(0, 12)
        twineNewline()
        twineNewline()
        # Musclebloat venom 
        twineNewline()
      else if getVariables().randomizer == 16
        getVariables().musclebloatVenom += random(0, 12)
        twineNewline()
        twineNewline()
        # Fattening venom 
        twineNewline()
      else if getVariables().randomizer == 17
        getVariables().gainVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Belly venom 
        twineNewline()
      else if getVariables().randomizer == 18
        getVariables().bellygainVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Breast venom 
        twineNewline()
      else if getVariables().randomizer == 19
        getVariables().breastgainVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Thigh venom 
        twineNewline()
      else if getVariables().randomizer == 20
        getVariables().thighgainVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Super-Fattening venom 
        twineNewline()
      else if getVariables().randomizer == 21
        getVariables().supergainVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Super-Belly venom 
        twineNewline()
      else if getVariables().randomizer == 22
        getVariables().bellySupergainVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Super-Breast venom 
        twineNewline()
      else if getVariables().randomizer == 23
        getVariables().breastSupergainVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Super-Thigh venom 
        twineNewline()
      else if getVariables().randomizer == 24
        getVariables().thighSupergainVenom += random(0, 64)
        twineNewline()
        twineNewline()
        # Overdose venom 
        twineNewline()
      else if getVariables().randomizer == 25
        getVariables().miraPoisoning += getVariables().girth * random(1, 5)
        twineNewline()
        twineNewline()
        # Worm venom 
        twineNewline()
      else if getVariables().randomizer == 26
        getVariables().direParasite = 1
        getVariables().direParaLoad += random(50, 500)
        twineNewline()
        twineNewline()
      twineNewline()
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfWildMiratoxin

