class PumpPart extends Item
  constructor: ->
    super()
    @ID = 'pump_part'
    @Type = 'gubbin'
    @Name = 'Pump Assembly'
    @Encounter = ''
    @Description = 'Small cylindrical thing with two barbed hose fittings and some power connections.'
    @Value = 6000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType PumpPart

