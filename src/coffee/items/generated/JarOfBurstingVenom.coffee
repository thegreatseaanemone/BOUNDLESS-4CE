class JarOfBurstingVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_burst'
    @Type = 'status'
    @Name = 'Jar of Bursting Venom'
    @Encounter = 'jar of bursting venom'
    @Description = 'A particularly frightening venom, this can turn most creatures into a blimp in seconds. It tends to keep going from there.'
    # [1800.0]
    # OLD: @Value = 1800.0
    @Value = 1800.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your skin begins to tighten...\'\'</brightalert>"'
      getVariables().burstVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBurstingVenom

