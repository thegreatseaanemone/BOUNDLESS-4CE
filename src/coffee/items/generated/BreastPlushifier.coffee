class BreastPlushifier extends Item
  constructor: ->
    super()
    @ID = 'shot_plushifier_breast'
    @Type = 'status'
    @Name = 'Breast Plushifier'
    @Encounter = 'breast plushifier'
    @Description = "Rapidly softens bloated breasts. There's a warning on the bottle: SEVERE DRUG REACTION-- DO NOT USE with Helium Pills!!"
    @Value = 1300
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      if getVariables().showDebug == 1
        window.twinePrint '"<p class="debug">DEBUG: $breastBloat = "'
        window.twinePrint '"'+getVariables().breastBloat+'"'
        window.twinePrint '"</p>"'
      twineNewline()
      if getVariables().breastBloat > 0
        twineNewline()
        window.twinePrint '"    <p>Your breasts rapidly soften as their contents are absorbed and converted into fat!</p>"'
        twineNewline()
        getVariables().breast += getVariables().breastBloat / 10
        getVariables().breastBloat -= getVariables().breastBloat / 10
        twineNewline()
      else
        twineNewline()
        window.twinePrint '"    <p>But nothing happens...</p>"'
        twineNewline()
      twineNewline()
      if getVariables().heliumBallooned > 0
        twineNewline()
        window.twinePrint '"    <p>Your breasts suddenly \'\'expand rapidly, bloating into a pair of fat, round globes... and continuing to inflate, growing tighter and tighter as you watch!\'\'</p>"'
        twineNewline()
        getVariables().breastPlushsplosion = getVariables().heliumBallooned
        twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType BreastPlushifier

