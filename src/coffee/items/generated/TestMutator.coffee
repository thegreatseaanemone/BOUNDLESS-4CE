class TestMutator extends Item
  constructor: ->
    super()
    @ID = 'potion_mutest'
    @Type = 'status'
    @Name = 'Test Mutator'
    @Encounter = 'test mutator'
    @Description = 'Nefirian still loves you, even though you obviously cheated to get this. Or <s>Pai</s>VG broke something, in which case mbad. This is a boring item, though.'
    @Value = 0
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().randomizer = random(1, 8)
      twineNewline()
      twineNewline()
      if getVariables().randomizer == 1
        twineNewline()
        getVariables().species = "leupai"
        getVariables().leupaiFlag = 0
        twineNewline()
      else if getVariables().randomizer == 2
        twineNewline()
        getVariables().species = "somnian"
        getVariables().leupaiFlag = 0
        twineNewline()
      else if getVariables().randomizer == 3
        twineNewline()
        getVariables().species = "helene"
        getVariables().leupaiFlag = 0
        twineNewline()
      else if getVariables().randomizer == 4
        twineNewline()
        getVariables().species = "fyynling"
        getVariables().leupaiFlag = 0
        twineNewline()
      else if getVariables().randomizer == 5
        twineNewline()
        getVariables().species = "blubberslime"
        getVariables().leupaiFlag = 0
        twineNewline()
      else if getVariables().randomizer == 6
        twineNewline()
        getVariables().species = "adephagian"
        getVariables().leupaiFlag = 0
        twineNewline()
      else if getVariables().randomizer == 7
        twineNewline()
        getVariables().species = "mythosi"
        getVariables().leupaiFlag = 0
        twineNewline()
      else if getVariables().randomizer == 8
        twineNewline()
        getVariables().species = "slime"
        getVariables().leupaiFlag = 0
        twineNewline()
        twineNewline()
      twineNewline()
      twineNewline()
      window.twinePrint '"You cry out halfheartedly as you blandly turn into a "'
      window.twinePrint '"'+getVariables().species+'"'
      window.twinePrint '", as per the parameters of this debug item."'
      twineNewline()
      window.twinePrint '"//Allegedly//."'
    return
InventoryController.RegisterItemType TestMutator

