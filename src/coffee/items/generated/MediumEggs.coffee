class MediumEggs extends Item
  constructor: ->
    super()
    @ID = 'egg_med'
    @Type = 'food'
    @Name = 'Medium Egg'
    @Encounter = 'eggs'
    @Description = 'A perfect compact food.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType MediumEggs

