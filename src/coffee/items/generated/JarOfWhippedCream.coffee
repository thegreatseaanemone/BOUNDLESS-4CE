class JarOfWhippedCream extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_cream_whipped'
    @Type = 'food'
    @Name = 'Jar of Whipped Cream'
    @Encounter = 'jar of whipped cream'
    @Description = 'A soft, sweet pastry fried in hot fattenium-- a Revix favorite.'
    # [70.0]
    # OLD: @Value = 70.0
    @Value = 70.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 100
      getVariables().bellyBloat += 60
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 80
      getVariables().jarReturn = 1
    return
InventoryController.RegisterItemType JarOfWhippedCream

