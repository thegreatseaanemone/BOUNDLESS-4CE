class BarbecuedPoult extends Item
  constructor: ->
    super()
    @ID = 'cooked_poult'
    @Type = 'food'
    @Name = 'Barbecued Poult'
    @Encounter = ''
    @Description = 'Fresh birdflesh, roasted over a campfire-- a simple and sweet way to dine like a king.'
    @Value = 30
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(7, 18)
      getVariables().bellyBloat += random(12, 45)
      getVariables().bonusEnergy += random(20, 35)
      getVariables().pain -= random(2, 5)
    return
InventoryController.RegisterItemType BarbecuedPoult

