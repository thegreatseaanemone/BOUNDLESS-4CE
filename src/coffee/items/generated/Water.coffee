class Water extends Item
  constructor: ->
    super()
    @ID = 'waterStock'
    @Type = 'drink'
    @Name = 'Water'
    @Encounter = ''
    @Description = 'A portion of relatively clean water, suitable for cooking or drinking.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyBloat += 10
      getVariables().bellyLiquid += 10
      getVariables().bonusEnergy += 5
    return
InventoryController.RegisterItemType Water

