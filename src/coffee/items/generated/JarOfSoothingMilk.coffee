class JarOfSoothingMilk extends Item
  constructor: ->
    super()
    @ID = 'milk_soothing'
    @Type = 'status'
    @Name = 'Jar of Soothing Milk'
    @Encounter = 'jar of soothing milk'
    @Description = 'A jar of pure, creamy milk. Good for you!'
    # [800.0]
    # OLD: @Value = 800.0
    @Value = 800.0 # item.Value
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().toxicVenom = 0
      getVariables().lethalVenom = 0
      getVariables().gainVenom = 0
      getVariables().supergainVenom = 0
      getVariables().bellygainVenom = 0
      getVariables().bellySupergainVenom = 0
      getVariables().breastgainVenom = 0
      getVariables().breastSupergainVenom = 0
      getVariables().thighgainVenom = 0
      getVariables().thighSupergainVenom = 0
      getVariables().fatteniumVenom = 0
      getVariables().bulkVenom = 0
      getVariables().flabbyVenom = 0
      getVariables().bloatVenom = 0
      getVariables().breastbloatVenom = 0
      getVariables().burstVenom = 0
      getVariables().healVenom = 0
      getVariables().weakeningVenom = 0
      getVariables().gluttonVenom = 0
      getVariables().caloricVenom = 0
      getVariables().hallucenoVenom = 0
      getVariables().mutagenVenom = 0
      getVariables().noxiousVenom = 0
      getVariables().calories += 50
      getVariables().bellyBloat += 20
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 50
      window.twinePrint '"<affirmative>\'\'You feel undeniably refreshed!\'\'</affirmative>"'
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfSoothingMilk

