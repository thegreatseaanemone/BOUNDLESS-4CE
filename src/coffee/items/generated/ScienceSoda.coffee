class ScienceSoda extends Item
  constructor: ->
    super()
    @ID = 'drink_science'
    @Type = 'drink'
    @Name = 'Science Soda'
    @Encounter = 'science soda'
    @Description = "A vial full of brightly-colored foamy, fizzy liquid, presumably extremely scientific. That's all you can really say."
    # [800.0, 800.0, 800.0]
    # OLD: @Value = 800.0
    @Value = 800.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 200, 500)
      getVariables().bellyBloat += either(0, 10, 20, 30, 40, 50, 100, 150, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 450, 500, 1000, 1500, 2000, 5000, 10000, 500000)
      getVariables().bonusEnergy += either(0, 10, 20, 30, 40, 50, 100, 150, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 450, 500)
      getVariables().bellyLiquid += either(0, 10, 20, 30, 40, 50, 100, 150, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 450, 500, 1000, 2000, 5000, 50000)
      getVariables().naturalMaxPain += either(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
      getVariables().finesse += either(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
      getVariables().strength += either(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
    return
InventoryController.RegisterItemType ScienceSoda

