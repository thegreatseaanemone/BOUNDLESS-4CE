class AntifungalInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_antifungal'
    @Type = 'status'
    @Name = 'Antifungal Injection'
    @Encounter = 'antifungal injection'
    @Description = 'Use this to purge fungal bodies from your system before they get up to any sinister business.'
    # [350.0, 300.0]
    # OLD: @Value = 325.0
    @Value = 325.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You hiss and grit your teeth as the meds flow into your system!\'\'</deepalert> "'
      getVariables().miracoSpores -= random(30, 60)
      getVariables().mushroomToxicity -= random(40, 80)
      getVariables().pain += random(2, 3)
      getVariables().lethalKO = 0
    return
InventoryController.RegisterItemType AntifungalInjection

