class InventoryController extends GameObject
  @ALL_TYPES=[]
  @TYPE_2_ID={}
  @ID_2_TYPE={}

  constructor: (@Creature) ->
    @Items = {}

  _getItemAndIDFromArg: (item) ->
    id = null
    switch typeof item
      when 'string'
        id = item
        item = InventoryController.CreateFromID(id)
        if item == null
          return [null,null]
      when 'function'
        item = new item()
        id = item.ID
      when 'object'
        id = item.ID
      else
        console.error "%s is %s, not a string, function, or object!", item, typeof item
    return [id, item]

  _getItemIDFromArg: (item) ->
      id = null
      switch typeof item
        when 'string'
          id = item
        when 'function'
          item = new item()
          id = item.ID
        when 'object'
          id = item.ID
        else
          console.error "%s is %s, not a string, function, or object!", item, typeof item
      return id

  _getIDFromArg: (arg) ->
    id = null
    switch typeof arg
      when 'string'
        id = arg
        if !(arg of InventoryController.ID_2_TYPE)
          return null
      when 'function'
        item = new arg()
        id = item.ID
      when 'object'
        id = arg.ID
      else
        console.error "%s is %s, not a string, function, or object!", arg, typeof arg
        return null
    return id

  AddItem: (arg, amount=0) ->
    [id, item] = @_getItemAndIDFromArg(arg)
    if id == null
      console.log "Invalid argument to AddItem: id=%s, item=%s, amount=%d", id, item, amount
      return
    if item == null
      console.log "Invalid argument to AddItem: id=%s, item=%s, amount=%d", id, item, amount
      return
    if !(id of @Items)
      @Items[id]=item
    if amount == 0
      amount = item.Amount
    if amount == 0
      amount = 1
    @Items[id].Amount += amount
    return @Items[id]

  RemoveItem: (item, amount=1) ->
    id = @_getIDFromArg(item)
    if id == null
      console.log "Invalid argument to RemoveItem: id=%s, item=%s, amount=%d", id, item, amount
      return
    if !(id of @Items)
      console.log "Invalid ID in RemoveItem: id=%s", id
      return
    @Items[id].Amount -= amount
    if @Items[id].Amount <= 0
      delete @Items[id]

    return
  Empty: ->
    @Items={}

  GetItem: (item) ->
    id = @_getIDFromArg(item)
    return @Items[id]

  GetOrEmpty: (item) ->
    id = @_getIDFromArg(item)
    if id of @Items
      return @Items[id]
    else
      return InventoryController.CreateFromID(id)


  GetItemCount: (item) ->
    id = @_getIDFromArg(item)
    if !(id of @Items)
      return 0
    else
      return @Items[id].Amount

  @RegisterItemType: (TYPE) ->
    id = (new TYPE()).ID
    InventoryController.ALL_TYPES.push TYPE
    InventoryController.TYPE_2_ID[TYPE]=id
    InventoryController.ID_2_TYPE[id]=TYPE

  @CreateFromID: (id) ->
    if !(id of InventoryController.ID_2_TYPE)
      console.error "Unknown type ID `%s`!", id
      return null
    return new InventoryController.ID_2_TYPE[id]()

  serialize: () ->
    data = {}
    for own key, item of @Items
      data[item.ID]=item.Amount
    return data
  deserialize: (data) ->
    @Items.length=0
    for own id, amount of data
      if id == '_type'
        continue
      @AddItem(id, amount)
  clone: ->
    inv = new InventoryController(@Creature)
    for own id, item of @Items
      inv.Items[id] = clone(item)
    return inv
