# requires: machines/Constants.coffee
class PumpTankWaterPart extends PumpTank
  constructor: ->
    super('pump_tank_water_part', 'Pump Water Tank')
    @Description = 'A tank filled with water, for use with Pumps.'
    @Value = 4000
    @Tags = []
    @Contents = EPumpFluid.WATER
    @ShortName = 'water tank'
InventoryController.RegisterItemType PumpTankWaterPart
PUMP_TANK_TYPES.push 'pump_tank_water_part'
