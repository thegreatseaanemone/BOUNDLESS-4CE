# requires: machines/Constants.coffee
class PumpTankAirPart extends PumpTank
  constructor: ->
    super('pump_tank_air_part', 'Pump Air Tank')
    @Description = 'A tank filled with air, for use with Pumps.'
    @Value = 4000
    @Tags = []
    @Contents = EPumpFluid.AIR
    @ShortName = 'air tank'
InventoryController.RegisterItemType PumpTankAirPart
PUMP_TANK_TYPES.push 'pump_tank_air_part'
