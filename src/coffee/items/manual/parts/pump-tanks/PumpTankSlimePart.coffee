# requires: machines/Constants.coffee
class PumpTankSlimePart extends PumpTank
  constructor: ->
    super('pump_tank_slime_part', 'Pump Slime Tank')
    @Description = 'A tank filled with living slime, for use with Pumps.'
    @Value = 4000
    @Tags = []
    @Contents = EPumpFluid.SLIME
    @ShortName = 'tank'
InventoryController.RegisterItemType PumpTankSlimePart
PUMP_TANK_TYPES.push 'pump_tank_slime_part'
