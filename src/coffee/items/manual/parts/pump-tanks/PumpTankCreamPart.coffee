# requires: machines/Constants.coffee
class PumpTankCreamPart extends PumpTank
  constructor: ->
    super('pump_tank_cream_part', 'Pump Cream Tank')
    @Description = 'A tank filled with cream, for use with Pumps.'
    @Value = 4000
    @Tags = []
    @Contents = EPumpFluid.CREAM
    @ShortName = 'tank'
InventoryController.RegisterItemType PumpTankCreamPart
PUMP_TANK_TYPES.push 'pump_tank_cream_part'
