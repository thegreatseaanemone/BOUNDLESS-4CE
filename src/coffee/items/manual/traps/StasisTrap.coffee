# Last checked against 0.2.1-4CE-0.5.0
class StasisTrap extends Trap
  constructor: ->
    super()
    @ID = 'trap_stasis'
    @Type = 'trap'
    @Name = 'Stasis Trap'
    @Encounter = 'stasis trap'
    @Description = 'An inescapable trap, made to capture small creatures too weak to be tranquilized. Leaving the area before it activates will abandon the trap!'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.NONE

  GetTrapEfficacy: ->
    return getVariables().trapEvade * 10
InventoryController.RegisterItemType StasisTrap
