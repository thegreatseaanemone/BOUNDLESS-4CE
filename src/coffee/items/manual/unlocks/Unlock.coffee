class Unlock extends Item
  constructor: (_id, _name, _desc, @UnlockVar, @UnlockMessage)->
    super()
    @ID = _id
    @Type = 'key'
    @Name = _name
    #@Encounter = ''
    @Description = _desc
    @Value = 50000
    @Other = []
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables()[@UnlockVar] = 1
      twinePrint '"\\\n\'\'<affirmative>'+@UnlockMessage+'</affirmative>\'\'"'
