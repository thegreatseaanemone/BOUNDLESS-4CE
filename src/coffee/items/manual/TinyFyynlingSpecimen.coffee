# Last checked against 0.2.1-4CE-0.5.0
class TinyFyynlingSpecimen extends Item
  constructor: ->
    super()
    @ID = 'specimen_fyynling'
    @Type = 'status'
    @Name = 'Tiny Fyynling Specimen'
    @Encounter = 'tiny fyynling specimen'
    @Description = 'A tiny fyynling caught in a jar. It looks happy to see you!'
    @Value = 250
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'The little fyynling crawls from the jar, throwing itself at you and merging with your flesh!\'\'</deepalert> "'
      getVariables().fyynlingMorph += random(4, 8)
      getVariables().bellyBloat += getVariables().maxBelly
      getVariables().pain += random(2, 5)
      getVariables().lethalKO = 0
      getVariables().calories += either(10, 20, 30, 40, 50, 50)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
  GetUseControls: ->
    o = super()
    o += '<<button [[Shatter Jar|passage()][$usedItem = 1; $used = "special"; $specialUsed = "shatter"; $specialUseDesc = "It begins to crawl out of its broken container!"; $presetEncounter = 1; $presetType = "creature"; $preEncounter = "fyynling"; $exploring = 0; $befriendable = 1]]>>'
    return o
InventoryController.RegisterItemType TinyFyynlingSpecimen

