# requires: lib/mocha.coffee
OnTest.Attach ->
  describe "Saves", ->
    describe '_getItemAndIDFromArg()', ->
      it 'gets ID from type', ->
        inv = new InventoryController()
        [id, item] = inv._getItemAndIDFromArg JarOfHealingVenom
        id.should.equal 'collectionjar_venom_heal'
        item.constructor.name.should.equal 'JarOfHealingVenom'
        return
      it 'gets ID from ID', ->
        inv = new InventoryController()
        [id, item] = inv._getItemAndIDFromArg 'collectionjar_venom_heal'
        id.should.equal 'collectionjar_venom_heal'
        item.constructor.name.should.equal 'JarOfHealingVenom'
        return
      return
    describe '_getIDFromArg()', ->
      it 'gets ID from type', ->
        inv = new InventoryController()
        id = inv._getIDFromArg JarOfHealingVenom
        id.should.equal 'collectionjar_venom_heal'
        return
      it 'gets ID from ID', ->
        inv = new InventoryController()
        id = inv._getIDFromArg 'collectionjar_venom_heal'
        id.should.equal 'collectionjar_venom_heal'
        return
      return
    describe 'AddItem()', ->
      it "adds by type", ->
        inv = new InventoryController()
        inv.AddItem(JarOfHealingVenom, 1)
        inv.GetItemCount(JarOfHealingVenom).should.equal 1
        return
      it "adds by ID", ->
        inv = new InventoryController()
        inv.AddItem('collectionjar_venom_heal', 1)
        inv.GetItemCount(JarOfHealingVenom).should.equal 1
        return
      return
    describe 'RemoveItem()', ->
      it "removes by type", ->
        inv = new InventoryController()
        inv.AddItem(JarOfHealingVenom, 1)
        inv.RemoveItem(JarOfHealingVenom, 1)
        inv.GetItemCount(JarOfHealingVenom).should.equal 0
        return
      it "removes by ID", ->
        inv = new InventoryController()
        inv.AddItem('collectionjar_venom_heal', 1)
        inv.RemoveItem('collectionjar_venom_heal', 1)
        inv.GetItemCount(JarOfHealingVenom).should.equal 0
        return
      return
    return
  return false
