# requires: lib/mocha.coffee
OnTest.Attach ->
  describe "Saves", ->
    describe 'PlayerCharacter', ->
      it "serializes properly", ->
        twineDisplayInline 'StatusClear'
        pc = new PlayerCharacter()
        data = pc.serialize()

        # We use chai here.
        should.exist data
        data.should.be.an 'object'
        data.should.have.own.property '_type'
        data._type.should.equal 'PlayerCharacter'
        data.should.have.own.property 'Chemicals'
        data.should.have.own.property 'Inventory'
        #done()
        return
      it 'deserializes properly', ->
        data =
          player:
            _type: 'PlayerCharacter'
            Inventory:
              collectionjar_venom_heal: 1
            Chemicals:
              _type: 'ChemistryController'
              Fyynacillin:
                _type: 'Fyynacillin'
                amount: 1
                ingestion_method: 1

        dsdata = deserialize data
        should.exist dsdata.player
        pc = dsdata.player
        console.log pc.constructor.name
        pc.should.be.an 'object'
        pc.constructor.name.should.equal 'PlayerCharacter'

        pc.should.have.property 'Chemicals'
        should.exist pc.Chemicals
        pc.Chemicals.should.be.an 'object'
        pc.Chemicals.constructor.name.should.equal 'ChemistryController'
        pc.Chemicals.GetAmount(Fyynacillin).should.equal 1

        pc.should.have.property 'Inventory'
        should.exist pc.Inventory
        pc.Inventory.should.be.an 'object'
        pc.Inventory.constructor.name.should.equal 'InventoryController'
        pc.Inventory.GetItemCount(JarOfHealingVenom).should.equal 1
        return #deserializes
      return # PC
    describe 'Planets', ->
      it 'serializes properly', ->
        planet = new TemperateLivingPlanet(yes)
        # Making some random stuff static for testing purposes.
        planet.Type = 'overgrown'
        planet.PopulationDensity = EPlanetPopDensity.HIGH

        data = planet.serialize()

        should.exist data._type
        data._type.should.be.a 'string'
        data._type.should.equal 'TemperateLivingPlanet'

        should.exist data.Class
        data.Class.should.be.a 'string'
        data.Class.should.equal 'Temperate Living'

        should.exist data.Denizens
        data.Denizens.should.be.an 'object'
        data.Denizens.should.have.all.keys ['human', 'revix', 'aiouran']
        return #serializes
      return # Planets
    return # Saves
  return false
