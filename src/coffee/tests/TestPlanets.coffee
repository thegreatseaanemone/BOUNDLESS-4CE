# requires: lib/mocha.coffee
OnTest.Attach ->
  [
    TemperateLivingPlanet
    HotLivingPlanet
    DesolateLivingPlanet
    MutantLivingPlanet
    HostileColdPlanet
    HostileHotPlanet
    DeadPlanet
  ].forEach (planet_type) ->
    planet_proto = new planet_type(true)
    describe planet_proto.constructor.name, ->
      describe 'PlanetGen Simulation', ->
        it 'instantiates without imploding', ->
          planet = new planet_type(true)
          should.exist planet
          planet.should.be.an 'object'
          return

        it 'runs PostGenerate without imploding', ->
          planet = new planet_type(true)
          planet.PostGenerate()
          return

        it 'generates ambient effects', ->
          planet = new planet_type(true)
          planet.RollForAmbientEffects()
          return
        return
      describe 'AddDenizen()', ->
        it 'adds one denizen correctly', ->
          planet = new planet_type(false)
          planet.AddDenizen('revix', 1, EDenizenFlags.CIVILIAN)
          planet.should.have.property 'Denizens'
          planet.Denizens.should.have.property 'revix'

          denizen = planet.Denizens['revix']
          denizen.should.be.an 'object'
          denizen.constructor.name.should.equal 'Denizen'
          denizen.ID.should.equal 'revix'
          denizen.Likelihood.should.equal 1
          denizen.Flags.should.equal EDenizenFlags.CIVILIAN
          return
        return
      describe 'EZPopulate()', ->
        it 'add one denizen properly', ->
          planet = new planet_type(false)
          planet.EZPopulate ['a']
          planet.should.have.property 'Denizens'
          planet.Denizens.should.have.property 'a'

          probsum = 0
          for dzID, dz of planet.Denizens
            probsum += dz.Likelihood
          probsum.should.equal 1
          return
        it 'add two denizens properly', ->
          planet = new planet_type(false)
          planet.EZPopulate ['a', 'b']
          planet.should.have.property 'Denizens'
          planet.Denizens.should.have.all.own.keys ['a', 'b']

          probsum = 0
          for dzID, dz of planet.Denizens
            probsum += dz.Likelihood
          probsum.should.equal 1
          return
        it 'add three denizens properly', ->
          planet = new planet_type(false)
          planet.EZPopulate ['a', 'b', 'c']
          planet.should.have.property 'Denizens'
          planet.Denizens.should.have.all.own.keys ['a', 'b', 'c']

          probsum = 0
          for dzID, dz of planet.Denizens
            probsum += dz.Likelihood
          probsum.should.equal 1
          return
      return
    return
  return false
