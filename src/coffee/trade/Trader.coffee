class TradeGood
  constructor: (@TradeDialog, @Owner, @Item)->
    @Name = @Item.Name
    @Available = @Item.Amount
    @HTML=null
    @NameCell=null
    @ValueCell=null
    @OfferCell=null
    @OfferButton=null
    @Offer=null

  buildHTML: (parent) ->
    @HTML = $('<tr class="tradegood">')
    @NameCell = $('<th class="colTradeName">')
    @HTML.append @NameCell
    @ValueCell = $('<td class="colTradeValue">')
    @HTML.append @ValueCell
    @AvailableCell = $('<td class="colTradeAvailable">')
    @HTML.append @AvailableCell
    @OfferCell = $('<td class="colTradeOffer">')
    @OfferButton = $('<button class="internalButton visitedButton">???</button>')
    @OfferButton.text(if @TradeDialog.GetSeller(@) == @TradeDialog.Player then "Sell" else "Buy")
    @OfferCell.append @OfferButton
    @OfferButton.on 'click', (e) =>
      @TradeDialog.SelectedGood = @
      @TradeDialog.OpenOfferDialog()
    @HTML.append @OfferCell
    @Refresh()
    parent.append @HTML

  GetMaxPurchasable: (buyer) ->
    consiee = buyer.Consiee
    return Math.floor(consiee/@TradeDialog.GetValue(@))

  Refresh: () ->
    @NameCell.text @Name
    @Value = @TradeDialog.GetValue(@)
    @ValueCell.text(formatConsiee(@Value))
    @AvailableCell.empty()
    @AvailableCell.append("<span class=\"amount-avail\">x#{@Available}</span>")
    @HTML.removeClass('offered')
    if @Offer
      @AvailableCell.append("(<span class=\"amount-offered\">-#{@Offer.Amount}</span>)")
      @HTML.addClass('offered')

  Remove: ->
    @OfferButton.remove()
    @HTML.remove()

class Offer
  constructor: (@Dialog, @Good, @Buyer, @Seller, @Amount) ->
    @Item = @Good.Item

  Remove: ->
    for i in [0...@Dialog.Offers.length]
      if @ == @Dialog.Offers[i]
        @Dialog.Offers.splice(i, 1)
    @Good.Offer = null
    @Dialog.UpdateTotals()



class TradePanelController
  constructor: (@Trader) ->
    @PlayerGoods = []
    @TraderGoods = []
    @TraderConsiee = 0
    @Offers = []
    @SelectedGood = null

    @dlgMakeOffer = null
    @PlayerPane = null
    @TraderPane = null
    @TradingControlsPane = null

  OpenOfferDialog: ->
    ###
    <div id="dlgMakeOffer" class="offer">
      <div class="offer-info">
        <h2 id="offer-heading">HEADER</h2>
        <div id="offer-text">TEXT</div>
      </div>
    </div>
    ###
    @dlgMakeOffer = $('<div id="dlgMakeOffer" class="offer"></div>')
    header = $('<h2 id="offer-heading"></h2>')
    blurbtext = $('<div id="offer-text"></div>')
    offerInfo = $('<div class="offer-info"></div>')
      .append(header)
      .append(blurbtext)
    @spinOfferAmount = $('<input id="spinOfferAmount" name="spinOfferAmount"></input>')
    prepend = $('<div id="prepend-to-buttonpane"></div>')
      .append('OFFER:')
      .append(@spinOfferAmount)
      .append(' units @ ')
      .append(priceEl = $('<span id="offer-price-per-unit" class="consiee loss"></span>'))
      .append('.')
    @dlgMakeOffer.append(offerInfo)
    .dialog
      autoDisplay: false
      autoOpen: true
      title: 'Make Offer'
      width: 550
      modal: true
      open: (ui, e) =>

        ###
        <div id="prepend-to-buttonpane">
          OFFER: <input id="spinOfferAmount" name="spinOfferAmount"> units @ <span id="offer-price-per-unit" class="consiee loss"></span>.
        </div>
        ###
        $('.ui-dialog-buttonpane').prepend(prepend)
        $('.ui-dialog-buttonpane').append($('<div id="trade-max"></div>'))
        header.text(@SelectedGood.Item.Name)
        blurbtext.text(@SelectedGood.Item.Description)
        priceEl.text(formatConsiee(@GetValue(@SelectedGood)))
        maxunits = Math.min(@SelectedGood.Available, @SelectedGood.GetMaxPurchasable(@GetBuyer(@SelectedGood)))
        if maxunits < @SelectedGood.Available
          you = if @SelectedGood.Owner == @Trader then "You" else @Trader.Name
          $('#trade-max').text("#{you} can only afford to buy #{maxunits} units.").show()
        else
          $('#trade-max').text("").hide()
        if @spinOfferAmount == null || @spinOfferAmount.empty()
          @spinOfferAmount = $('#spinOfferAmount')
        @spinOfferAmount.spinner
          min: 1
          max: maxunits
        @spinOfferAmount.spinner("value", if @SelectedGood.Offer != null then @SelectedGood.Offer.Amount else 0)
      close: (ui, e) ->
        $(this).remove()
      buttons:
        "Make Offer": (e) =>
          if @SelectedGood.Offer == null
            @SelectedGood.Offer = @AddOffer(@SelectedGood, @spinOfferAmount.spinner("value"))
          else
            @SelectedGood.Offer.Amount = @spinOfferAmount.spinner("value")
          @UpdateTotals()
          @dlgMakeOffer.dialog('close')
        Cancel: (e) =>
          @dlgMakeOffer.dialog('close')

  Initialize: ->
    @Player = getPlayer()
    @PlayerPane = JQHelper.GetElement('#player-items')
    @TraderPane = JQHelper.GetElement('#trader-items')
    @TradingControlsPane = JQHelper.GetElement('#trading-controls-pane')

    JQHelper.GetElement('#cmdClearOffers').on 'click', (e) =>
      @ClearOffers()
    JQHelper.GetElement('#cmdMakeSale').on 'click', (e) =>
      deltaPlayer=0
      deltaTrader=0
      for offer in @Offers
        if getVariables().showDebug == 1
          console.log @GetValue(offer.Good), offer.Amount
        consieeOffer = @GetValue(offer.Good) * offer.Amount
        if offer.Buyer == @Player
          deltaPlayer -= consieeOffer
          deltaTrader += consieeOffer
        else
          deltaPlayer += consieeOffer
          deltaTrader -= consieeOffer
      JQConfirm
        title: 'Complete Sale?'
        text: "Total sale is #{formatConsiee(deltaPlayer)}.  Continue?"
        'yes': (ui, e) =>
          @Player.Consiee += deltaPlayer
          @Trader.Consiee += deltaTrader
          for offer in @Offers
            if offer.Buyer == @Player
              @Trader.Inventory.RemoveItem(offer.Item.ID, offer.Amount)
              @Player.Inventory.AddItem(offer.Item.ID, offer.Amount)
            else
              @Player.Inventory.RemoveItem(offer.Item.ID, offer.Amount)
              @Trader.Inventory.AddItem(offer.Item.ID, offer.Amount)
            console.log "Transferred %d %s to %s", offer.Amount, offer.Item.Name, offer.Buyer.Name
          @ClearOffers()
          @RefreshInventoryState true
          @UpdateTotals()
          window.updateStoryMenu() # Sidebar
          #window.state.display(getVariables().salePrevPassage)


    @RefreshInventoryState()
    @UpdateTotals()

  RefreshInventoryState: (fullreset=false) ->
    if fullreset
      @PlayerGoods.length = 0
      @TraderGoods.length = 0
    @RefreshCreatureInventory(@Player, @PlayerPane, @PlayerGoods)
    @RefreshCreatureInventory(@Trader, @TraderPane, @TraderGoods)

  RefreshCreatureInventory: (creature, pane, goods)->
    table = $('<table class="prettytable"><tr><th>Item</th><th>Value</th><th>Available</th></tr></table>')
    if goods.length == 0
      for own key, item of creature.Inventory.Items
        if item instanceof Item
          good = new TradeGood(@, creature, item)
          goods.push good
      for good in goods
        good.buildHTML(table)
      pane.html(table)
    else
      for good in goods
        good.Refresh()
    return

  ClearOffers: ->
    for offer in @Offers.slice()
      offer.Remove()
    @Offers.length = 0
    @RefreshInventoryState()
    @UpdateTotals()

  GetBuyer: (good) ->
    if good.Owner == @Player
      return @Trader
    else
      return @Player

  GetSeller: (good) ->
    if good.Owner != @Player
      return @Trader
    else
      return @Player

  GetValue: (good) ->
    value = good.Item.Value
    if @GetBuyer(good) != @Trader
      value += value*(@Trader.TradingMarkup/100)
    else
      value -= value*(@Trader.TradingMarkup/100)
    return value

  AddOffer: (good, amount) ->
    good.Refresh()
    offer = new Offer(@, good, @GetBuyer(good), @GetSeller(good), amount)
    @Offers.push offer
    return offer

  UpdateTotals: ->
    # Deliberately leaving these as locals.
    deltaPlayer=0
    deltaTrader=0
    for offer in @Offers
      consieeOffer = @GetValue(offer.Good) * offer.Amount
      if offer.Buyer == @Player
        deltaPlayer -= consieeOffer
        deltaTrader += consieeOffer
      else
        deltaPlayer += consieeOffer
        deltaTrader -= consieeOffer
      offer.Good.Refresh()
    @RenderConsieeDelta('#player-consiee', @Player.Consiee, 0)
    @RenderConsieeDelta('#player-changed', deltaPlayer, deltaPlayer)
    @RenderConsieeDelta('#player-total', @Player.Consiee + deltaPlayer, deltaPlayer)
    @RenderConsieeDelta('#trader-consiee', @Trader.Consiee, 0)
    @RenderConsieeDelta('#trader-changed', deltaTrader, deltaTrader)
    @RenderConsieeDelta('#trader-total', @Trader.Consiee + deltaTrader, deltaTrader)

    $('#cmdMakeSale').disable((@Offers.length <= 0) or ((@Player.Consiee + deltaPlayer) < 0 or (@Trader.Consiee + deltaTrader) < 0))
    $('#cmdClearOffers').disable(@Offers.length <= 0)

  RenderConsieeDelta: (selector, toDisplay, delta)->
    td = $('#passages').find(selector)
      .text(formatConsiee(toDisplay))
      .removeClass('loss')
      .removeClass('gain')
      .removeClass('even')
    if delta == 0
      td.addClass('even')
    else if delta < 0
      td.addClass('loss')
    else if delta > 0
      td.addClass('gain')
