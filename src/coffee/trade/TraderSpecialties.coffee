ETraderType =
  Miner:        1
  Revix:        2
  BlackMarket:  3
  Weapons:      4

class TraderSpecialty extends GameObject
  @BUYPOOL2SPECIALTY: {}
  @ALL: []

  @Register: (instance) ->
    TraderSpecialty.BUYPOOL2SPECIALTY[instance.Type]=instance
    TraderSpecialty.ALL.push(instance)

  @GetByID: (typeid) ->
    return TraderSpecialty.BUYPOOL2SPECIALTY[typeid]

  constructor: ->
    @Desc = ''
    @Occupation = 'jobber' # You see #{@A} #{@Occupation} with goods for trade!
    @A = 'a'
    @ConsieeBounds = [0,1000]
    @MarkupBounds = [5,10] # %
    @Selling = {} # Item => probability, min, max
    @Interests = {} # Item => base price override
    @Type = 0

  serialize: ->
    data =
      Desc: @Desc
      Occupation: @Occupation
      A: @A
      ConsieeBounds: @ConsieeBounds
      MarkupBounds: @MarkupBounds
      Selling: @Selling
      Interests: @Interests
      Type: @Type
    return data
  deserialize: (data) ->
    @Desc = if 'Desc' of data then data['Desc'] else @Desc
    @Occupation = if 'Occupation' of data then data['Occupation'] else @Occupation
    @A = if 'A' of data then data['A'] else @A
    @ConsieeBounds = if 'ConsieeBounds' of data then data['ConsieeBounds'] else @ConsieeBounds
    @MarkupBounds = if 'MarkupBounds' of data then data['MarkupBounds'] else @MarkupBounds
    @Selling = if 'Selling' of data then data['Selling'] else @Selling
    @Interests = if 'Interests' of data then data['Interests'] else @Interests
    @Type = if 'Type' of data then data['Type'] else @Type

  GenInvCount: (prob, min, max) ->
    if prob == 1 or prob < Math.random()
      return random(min, max)
    return 0

  GetSpecies: ->
    return either(getVariables().majorDenizen,getVariables().majorDenizen,getVariables().minorDenizen1,getVariables().minorDenizen2)

  clone: ->
    return @ # Don't clone, these are singletons.


  GenerateNPC: () ->
    C = new NPC()
    C.Name = getVariables().revName
    C.Species = @GetSpecies()
    C.Specialty = @
    #C.Disposition = EDisposition.Trader
    C.Consiee = random @ConsieeBounds[0], @ConsieeBounds[1]
    C.TradingMarkup = random @MarkupBounds[0], @MarkupBounds[1]
    for itemID, itemSpec of @Selling
      C.Inventory.AddItem itemID, @GenInvCount(itemSpec[0], itemSpec[1], itemSpec[2])
    return C

class MinerTraderSpecialty extends TraderSpecialty
  constructor: ->
    @Type = ETraderType.Miner
    @Desc = 'The grizzled guy in front of you wears a mining suit liberally festooned with lanterns, lantern oil, and pickaxes.  They smell like sweat, rock dust, and dead canaries.'
    @Occupation = 'miner'
    @A = 'a'
    @ConsieeBounds = [500,5000] # Poor
    @MarkupBounds = [5,75] # Not very experienced with trade, markup varies wildly
    @Selling = # Not a whole lot of anything, but what they do have is expensive and rare.
      'special_dreamshard': [1.00, 5, 10]
      'special_battery':    [0.25, 1, 10]
      'shard_recall':       [0.25, 1, 10]
      'dye_red':            [0.50, 1, 20]
      'dye_green':          [0.50, 1, 20]
      'dye_blue':           [0.50, 1, 20]
      'dye_brown':          [0.50, 1, 20]
TraderSpecialty.Register(new MinerTraderSpecialty())

class RevixTraderSpecialty extends TraderSpecialty
  constructor: ->
    @Type = ETraderType.Revix
    @Desc = 'In typical Revix form, they carry a load of //completely// unhealthy food, as well as the trademark overly-hospitable smile that sets off alarm bells at the back of your mind.'
    @Occupation = 'trader'
    @A = 'a'
    @ConsieeBounds = [5000,10000] # Middle class
    @MarkupBounds  = [5,10]      # Generous.
    @Selling =
      'special_dreamshard': [0.50, 0, 5] #rare

      'candy_buttercandy':  [1.00, 1, 100]
      'pastry_buttercake':  [1.00, 1, 100]
      'pastry_belsuite':    [1.00, 1, 100]
      'donut_fattenium':    [1.00, 1, 100]
      'cake_enormous':      [1.00, 1, 100]

      'drink_fragrant':     [1.00, 1, 200]
      'drink_milk':         [1.00, 1, 200]
      'drink_science':      [1.00, 1, 200]
      'milk_strange':       [1.00, 1, 200]

      'dye_magenta':        [0.50, 0, 5]
      'dye_teal':           [0.50, 0, 5]
      'dye_black':          [0.50, 0, 5]

  GetSpecies: ->
    return 'revix'
TraderSpecialty.Register(new RevixTraderSpecialty())

class BlackMarketTraderSpecialty extends TraderSpecialty
  constructor: ->
    @Type = ETraderType.BlackMarket
    @Desc = 'This person is constantly on edge.  Their head swivels constantly, looking for authorities, rivals, and potential scores while poking and prodding their stuffed pockets.  They appear tense at all times, and walk with a characteristic swagger. In short, a perfect drug dealer.'
    @Occupation = 'drug dealer'
    @A = 'a'
    @ConsieeBounds = [10000,100000] # Richer than most.
    @MarkupBounds  = [20, 30]      # Stingy.
    @Selling = #Supply is all over the place, and small
      'special_jar':         [1.00, 30, 50] # Not illegal.  I'm just a canning enthusiast, ossifer.
      'special_miracrystal': [0.50, 1,  10]
      'donut_fattenium':     [1.00, 1,  10]
      'drink_science':       [1.00, 1,  10]
      'milk_strange':        [1.00, 1,  10]
      'cake_enormous':       [1.00, 1,  10]
      'oil_leupai':          [0.75, 1,  10]
      'pill_bellybomb_mega': [0.25, 5,  10]
      'shot_unmarked':       [0.75, 1,  10]
      'pill_gainer':         [0.50, 1,  10]
      'powder_calorie_max':  [0.25, 5,  10]
      'gun_dart':            [0.25, 1,  3]
      'shot_miratoxin':      [0.25, 5,  30] # Easier to smuggle
      'shot_fattenium':      [0.25, 5,  30]
      'shot_fattenium_pure': [0.10, 1,  5] # MUCH harder to find.
TraderSpecialty.Register(new BlackMarketTraderSpecialty())

class WeaponsTraderSpecialty extends TraderSpecialty
  constructor: ->
    @Type = ETraderType.Weapons
    @Desc = 'This individual walks with a confidence characteric of someone loaded with guns.  That, and the fact that they\'re wearing an expensive suit, expensive sunglasses, and expensive cologne tells you they\'re probably a gun dealer.'
    @Occupation = 'gun dealer'
    @A = 'a'
    @ConsieeBounds = [100000,200000] # Rich
    @MarkupBounds  = [20, 25]      # Stingy, inflexible
    @Selling = #Supply is all over the place, and small
      'special_dreamshard':  [0.10, 1, 5]
      'special_battery':     [0.10, 1, 5]
      'special_miracrystal': [0.10, 1, 5]
      'drink_science':       [0.25, 1, 5]
      'shot_unmarked':       [0.25, 1, 5]
      'dart_tranq':          [0.95, 1, 10]
      'dart_toxic':          [0.95, 1, 10]
      'gun_dart':            [0.50, 1, 5]
TraderSpecialty.Register(new WeaponsTraderSpecialty())

GenTrader = () ->
  return either(TraderSpecialty.ALL).GenerateNPC()
