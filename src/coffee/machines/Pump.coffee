

# Only half of these make sense.
PUMP_FLUID_TANKS = [
  "[ERROR]"
  "air",
  "water",
  "",
  "",
  "water",
  "air"
]

EPumpTargetFlags =
  BELLY:   1
  BREASTS: 2

class Pump extends Machine
  constructor: (data = null) ->
    #     TypeName Name    SeenItVar
    super('Pump',  'Pump', 'pump', PumpPCB, MachineFrame, data)

    # $pumpType
    @Tank = null
    # $pumpTarget
    @Target = EPumpTargetFlags.BELLY

    @NeededParts =
      cable_part: 3
      pump_part: 2
      valve_part: 2
      hose_part: 2


  clone: ->
    machine = super(new Pump())
    machine.Tank=@Tank
    machine.Target=@Target
    return machine

  GenerateForDungeon: () ->
    super()
    @Tank = InventoryController.CreateFromID(either(PUMP_TANK_TYPES))
    @AmpHours = random(20, @MaxAmpHours/2) # Enough for a wheeze.

  CanDisassemble: ->
    return @Tank == null

  DisplayExtraParts: (parent) ->
    fieldset = insertElement parent, 'fieldset'
    insertElement fieldset, 'legend', null, null, 'Tank Operations'

    if @Tank != null
      twineButton "Remove Tank", passage(), (=>
        getPlayer().AddItem @Tank.ID, 1
        AlertUtils.AddInfo "You remove the #{@Tank.Name}, and add it to your inventory!"
        @Tank = null
      ), fieldset
    else
      for ptid in PUMP_TANK_TYPES
        cTankInst = InventoryController.CreateFromID ptid
        if getPlayer().GetItemCount(ptid) > 0
          twineButton "Install #{cTankInst.Name}", passage(), ((_this, _ptid) ->
            ->
              _this.Tank = InventoryController.CreateFromID _ptid
              getPlayer().RemoveItem _ptid, 1
          )(@, ptid), fieldset

  twPrintMenuButton: ->
    super(@Name)
    $player = getPlayer()

  Leave: () ->
    super()
    #@Fluid = 0 - We want to keep the fluid being stored.
    @Target = 0

  # Need to save tank type.
  serialize: () ->
    data = super()
    if @Tank != null
      data['tank_type'] = @Tank.ID
    return data

  deserialize: (data) ->
    super(data)
    if 'tank_type' of data
      @Tank = InventoryController.CreateFromID data['tank_type']
