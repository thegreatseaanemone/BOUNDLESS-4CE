EOilTubLiquid =
  NONE:       0
  LEUPAI_OIL: 1
  FATTENIUM:  2

class OilTub extends Machine
  @UsableJarTypes: [
    'oil_leupai',
    'collectionjar_fattenium',
  ]
  constructor: (data=null) ->
    #     TypeName  Name       SeenItVar PCB        Frame         serialized data
    super('OilTub', 'Oil Tub', 'OilTub', OilTubPCB, MachineFrame, data)
    @NeededParts =
      metal_panel: 10

    @UsesEnergy = no
    @MaxAmpHours = 2
    @AmpHours = 1
    @MinAmpHours = 0

    ###
    # Character girth that can be jammed into this tub
    ###
    @Girth  = 20000

    ###
    # The chemical filling the tub.
    ###
    @Chemical = null

  clone: ->
    machine = super(new OilTub())
    machine.Girth=@Girth
    machine.Chemical=@Chemical
    machine.MaxAmount=@MaxAmount
    return machine

  serialize: ->
    data = super()
    data['girth'] = @Girth
    if @Chemical != null
      data['chemical'] = @Chemical.toJSON()
    data['max-amount'] = @MaxAmount
    return data

  deserialize: (data)->
    super(data)
    @Girth = data['girth']
    if 'chemical' of data and 'Name' of data['chemical']
      @Chemical = ChemistryController.CreateFromName data['chemical']['Name']
      @Chemical.fromJSON(data)
    @MaxAmount = data['max-amount']

  GenerateForDungeon: () ->
    super()
    @Girth = either(4000, 8000, 12000, 16000, 20000)
    #@Liquid = either(EOilTubLiquid.LEUPAI_OIL, EOilTubLiquid.LEUPAI_OIL, EOilTubLiquid.FATTENIUM)
    $chemtype = either(LeupaiToxinChemical, LeupaiToxinChemical, FatteniumToxinChemical)
    @Chemical = new $chemtype @
    @MaxAmount = @Chemical.Amount = random(30, 360)
    #@SetupPower()

  twPrintMenuButton: ->
    super(@Name)

  Leave: ->
    super()

  # This is DOM stuff.
  DisplayExtraParts: (parent) ->
    fieldset = insertElement parent, 'fieldset'
    insertElement fieldset, 'legend', null, null, 'Fluid Operations'
    for jarID in OilTub.UsableJarTypes
      if getPlayer().GetItemCount(jarID) > 0
        cJarInst = InventoryController.CreateFromID jarID
        if cJarInst == null
          console.error "Unrecognized itemID #{jarID} in OilTub.UsableJarTypes!"
          continue
        chemInst = ChemistryController.CreateFromName cJarInst.ChemID
        if chemInst == null
          console.error "Unrecognized chem name #{cJarInst.ChemID} in #{cJarInst.constructor.name}!"
          continue
        if @Chemical == null || @Chemical.Name == chemInst.Name
          twineButton "Pour In #{cJarInst.Name}", passage(), ((_this, _chemID, _itemID, _jarDose) ->
            return ->
              if _this.Chemical == null || _this.Chemical.Name != _chemID
                _this.Chemical = ChemistryController.CreateFromName(_chemID)
              _this.Chemical.Amount += _jarDose
              getPlayer().RemoveItem _itemID, 1
              getPlayer().AddItem 'special_jar', 1
              return
          )(@, cJarInst.ChemID, jarID, chemInst.JarDosage), fieldset

    if @Chemical && @Chemical.Amount > 0
      twineButton "Drain Tub", passage(), (=>
        if @Chemical && @Chemical.Amount > 0
          nJars = @Chemical.ToJars(getPlayer())
          AlertUtils.AddSuccess("You drain the tub into #{nJars} jars.")
          if @Chemical.Amount < @Chemical.JarDosage
            @Chemical = null
      ), fieldset
    twineButton "FIX (hax)", passage(), (=>
      @GenerateForDebug()
    ), fieldset
