EConsoleActions =
  CHANGE_SPECIES: 1
  BLOAT:          2
  DEFLATE:        3
  FATTEN:         4
  DEPLETE:        5
  ELASTICIZE:     6
  TIGHTEN:        7
  ADD_METAB:      8
  SUB_METAB:      9
  ADD_ESURIENCE:  10
  PRESET_BOVIATE: 11
  PRESET_REALIZE: 12
  PRESET_GUGUE:   13
  REALLOCATE:     14
  SHAPE:          15
  BULK:           16
  HUE:            17
  DEL_ESURIENCE:  18
  INFEST:         666
  RESTORE:        333

class Console extends Machine
  constructor: (data = null) ->
    #     TypeName   Name       SeenItVar
    super('Console', 'Console', 'Console', ConsolePCB, MachineFrame, data)
    @NeededParts =
      metal_panel:      2
      glass_panel:      5
      cable_part:       3
      hose_part:        4
      screen_part:      1
    # Censored words
    # The words presented here are for the version in your mound. null = Doesn't appear.
    @Words = {
      'Infest':    'I',
      'Restore':   'R'
      'Liquify':   'L'
    }

    ###
    # Maximum capacity of this device.
    ###
    @MaxAmpHours = 600

    ###
    # Point at which the battery is considered dead.
    #
    # This should be calculated to be the smallest possible operation the machine
    #  can manage.
    ###
    @MinAmpHours = 25

  ###
  # Used to get random "words" for the "experimental" buttons.
  #
  # Without this, the word would change each time you clicked on a button.
  ###
  GetWord: (wordID) ->
    if !(wordID in Object.keys(@Words))
      @Words[wordID]=if !@InDungeon or passedRoll(0.6) then redacted_sentence(1,3) else null
    return @Words[wordID]

  GenerateForDungeon: () ->
    super()
    # <<if $specialArea is "Console">><<set $machineBattery += $machineBasePower>><<endif>>
    @Battery += @BasePower
    @SetupPower()
    @RollNewWords()

  RollNewWords: ->
    newWords={}
    keys = Object.keys(@Words)
    shuffle keys
    for k of keys
      @Words[k]=if !@InDungeon or passedRoll(0.6) then redacted_sentence(1,3) else null
    @Words=newWords

  clone: ->
    machine = super(new Console())
    machine.Words = @Words
    return machine

  serialize: ->
    data = super()
    data['Words'] = @Words
    return data

  deserialize: (data) ->
    super(data)
    if !('Words' in data) or (Object.keys(data['Words']).length < Object.keys(@Words).length)
      @RollNewWords()
    else
      @Words = data['Words']
    return data

  twPrintMenuButton: ->
    super(@Name)
