class AbsorptionAccelerator extends Machine
  constructor: (data=null) ->
    #     TypeName                 Name                      SeenItVar     PCB                       MachineFrame
    super('AbsorptionAccelerator', 'Absorption Accelerator', 'Absorption', AbsorptionAcceleratorPCB, MachineFrame, data)
    @NeededParts =
      metal_panel: 4
      glass_panel: 1
      cable_part:  5

  GenerateForDungeon: () ->
    super()
    @SetupPower()

  twPrintMenuButton: ->
    super(@Name)

  clone: ->
    machine = super(new AbsorptionAccelerator())
    return machine
