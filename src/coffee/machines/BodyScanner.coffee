class BodyScanner extends Machine
  constructor: (data=null) ->
    #     TypeName       Name            SeenItVar,  PCB, MachineFrame
    super('BodyScanner', 'Body Scanner', 'bodyScan', BodyScannerPCB, MachineFrame, data)
    @NeededParts =
      metal_panel:     2
      glass_panel:     4
      cable_part:      5

  GenerateForDungeon: () ->
    super()
    #<<set $machineBattery += either(40,60,80,100)>>
    @Battery += random(40, 60)
    @SetupPower()

  clone: ->
    machine = super(new BodyScanner())
    return machine

  twPrintMenuButton: ->
    super(@Name)

  Leave: ->
    super()
    getVariables().parasiteCheck = 0
    getVariables().statusCheck = 0
