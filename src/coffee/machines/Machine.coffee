window.EMachineEfficiency =
  POOR:      1
  FAIR:      2
  GOOD:      3
  EXCELLENT: 4
  FLAWLESS:  5

window.Int2MachineEfficiency=[
  'poor',
  'fair',
  'good',
  'excellent',
  'flawless',
]
window._postdisplay_machine = (title) ->
  if getVariables().machine != undefined
    window._prgMachineAmpHours = JQHelper.GetElement('.machine-amphours-progress')
    window._prgMachineAmpHours.progressbar
      value: (getVariables().machine.AmpHours/getVariables().machine.MaxAmpHours)*100
    JQHelper.GetElement('.machine-amphours-label').text("#{getVariables().machine.AmpHours}/#{getVariables().machine.MaxAmpHours}Ah")

window.incrementMachineEfficiency = (oldval, delta) ->
  return Int2MachineEfficiency[Math.max(Math.min(Int2MachineEfficiency.indexOf(oldval) + delta, 0), Int2MachineEfficiency.length - 1)]

class Machine extends GameObject
  constructor: (@TypeName, @Name, @SeenItVar, @PCB, @MachineFrame='machine_frame', data=null) ->
    @Location = '' # Mostly for reference.
    @Efficiency = EMachineEfficiency.FLAWLESS
    @AmpHours = 0
    @MaxAmpHours = 255 # 8D 12V battery = 255Ah
    @MinAmpHours = 5 # Enough to get one use.

    # Dungeon-generated machines behave very differently.
    @InDungeon = false
    # Number of maintenance attempts left. - $maintained
    @MaintAttemptsLeft = 0
    # Been fixed already. $maintenanceCap
    @PlayerMaintSuccessful = false

    @Parts = new InventoryController(@)
    # PartType => nNeeded
    @NeededParts = {}
    @PartsInitialized=false

    # The problem with the machine preventing IsUsable from returning true.
    @Problem = ''

    ###
    # Prevent players from removing parts.
    ###
    @LockParts = false

    ###
    # Some machines work differently in the mound.
    # This also triggers synchronization of $machine and $moundMachines.
    ###
    @IsMoundMachine = false

    ###
    # Disable to hide everything energy-related.
    ###
    @UsesEnergy = true

  Disassemble: () ->
    getPlayer().AddItem(@MachineFrame, 1)
    getPlayer().AddItem(@PCB,          1)

  Leave: () ->
    # Anything you have to do to global vars when leaving the machine passage.
    #<<button [[Back.|$saveStateSpecial][$machine.Fluid = 0; $inUse = 0; $machine.Target = 0; $repeatUse = 0]]>></center>
    getVariables().inUse=0
    getVariables().repeatUse=0
    getVariables().carryBG=0

  ###
  # Can we consume the amp-hours specified?
  # @param Ah {float} Amp-hours to consume
  # @return {Boolean} Whether the amp-hours specified can be consumed from the machine.
  ###
  CanConsumeAmpHours: (Ah) ->
    return !@UsesEnergy or Ah <= @AmpHours

  AddAmpHours: (Ah) ->
    @AmpHours = Math.min(@MaxAmpHours, @AmpHours + Ah)

  ###
  # @param check_first {Boolean} Check to see if the amount desired can actually be consumed, first.
  # @param Ah {float} Amp-hours to try and consume.
  # @return {float} Amp-hours actually consumed.
  ###
  ConsumeAmpHours: (Ah, check_first=false) ->
    if !@CanConsumeAmpHours(Ah)
      return 0
    orig_Ah = @AmpHours
    @AmpHours = Math.max(0, @AmpHours - Ah)
    return orig_Ah - @AmpHours

  IsUsable: () ->
    # <<if $machinePower gt $machineBattery>> ***IF UNUSABLE***
    for pid, n_needed of @NeededParts
      p = @Parts.GetItem(pid)
      if p == null or p == undefined
        @Problem = "The #{@Name} is missing some parts."
        return false
      if p.Amount < n_needed
        @Problem = "The #{@Name} is missing some parts."
        return false
    if @InDungeon and @AmpHours < @MinAmpHours
      @Problem = "The #{@Name} looks like it's run out of power."
      return false
    return true

  GetIntro: () ->
    desc = ''
    if getVariables()["seenIt_"+@SeenItVar] == 1
      desc = "a #{@Name}"
    else
      desc = "some old lab equipment"
    return "You find #{desc}! It looks to be in #{@GetEfficiencyAsString()} condition."

  twPrintMenuButton: (passageName)->
    twinePrint "\"<<button [[Access #{@Name}|#{passageName}][$machine = $moundMachines['#{@TypeName}']; $inMoundMachine = 1]]>>\""

  DisplayParts: (root=window.twineCurrentEl) ->
    #if !@PartsInitialized
    #  @InitializeParts()
    if @LockParts
      return
    # Fieldset
    fieldset = insertElement root, 'fieldset'
    insertElement fieldset, 'legend', null, null, 'Parts'

    # <ul class="machine-partlist">
    partlist = insertElement fieldset, 'ul', null, 'machine-partlist'

    # Construction of parts list.
    nPartsPresent=0
    $player = getPlayer()
    for pid, n_needed of @NeededParts
      p = @Parts.GetItem(pid)
      partli = insertElement(partlist, 'li')
      if p == null or p == undefined
        p = InventoryController.CreateFromID(pid)
        insertElement partli, 'span', null, 'disabled', "0/#{n_needed} × #{p.Name}"
      else
        nPartsPresent += p.Amount
        insertText partli, "#{p.Amount}/#{n_needed} × #{p.Name} "

      if $player.GetItemCount(pid) > 0 and n_needed > p.Amount
        # pid and @ both have to be bound, fat arrows don't allow this.
        twineButton 'Add Part', passage(), ((_this, _pid) ->
          ->
            getPlayer().RemoveItem _pid, 1
            _this.Parts.AddItem _pid, 1
            window.state.display passage()
            return
        )(@, pid), partli
      else
        twinePrint '"<unframe>Add Part</unframe>"', partli
      if p.Amount > 0
        # pid and @ both have to be bound, fat arrows don't allow this.
        twineButton "Remove Part", passage(), ((_this, _pid) ->
          ->
            getPlayer().AddItem _pid, 1
            _this.Parts.RemoveItem _pid, 1
            window.state.display passage()
        )(@,pid), partli
      else
        twinePrint '"<unframe>Remove Part</unframe>"', partli

    # Pumps currently use this for displaying the tank swap menu.
    @DisplayExtraParts(root)
    # Ensure [Disassemble] is on its own line.
    disassemblediv = insertElement root, 'div'
    if nPartsPresent == 0 and @CanDisassemble()
      twineButton 'Disassemble', getVariables().saveStateSpecial, (=>
        @Disassemble()
        @Leave()
        if @IsMoundMachine
          delete getVariables().moundMachines[@constructor.name]
      ), disassemblediv
    else
      twinePrint "'<unframe>Disassemble</unframe>'", disassemblediv
    return

  DisplayExtraParts: ->
    return

  CanDisassemble: ->
    return true

  InitializeParts: ->
    if !@PartsInitialized
      for part, count of @NeededParts
        @Parts.AddItem(part, count)
      @PartsInitialized=true

  Setup: (@LockParts=true) ->
    @InitializeParts()
    @AmpHours = @MaxAmpHours
    @Efficiency = EMachineEfficiency.FLAWLESS

  GenerateForDungeon: () ->
    @InDungeon = true
    # From SpecialDatabase:
    #<<set $machineBattery = either(20,20,40,60,80,100); $machineBasePower = either(20,40,60,80,100); $machineEfficiency = either("poor","poor","poor","fair","fair","good")>>
    @AmpHours = random(@MinAmpHours, @MaxAmpHours)
    @Efficiency = random(EMachineEfficiency.POOR, EMachineEfficiency.GOOD)
    # From every machine ever:
    #<<set $maintained = 3; $maintenanceCap = 0;>>
    @MaintAttemptsLeft = 3
    @PlayerMaintSuccessful = 0

    @Parts.Empty()
    for part, count of @NeededParts
      @Parts.AddItem(part, random(0, count))
    @PartsInitialized=true

  ###
  # Sets up the machine after spawning from the admin panel.
  # Should fully assemble and charge the machine.
  ###
  GenerateForDebug: ->
    @InDungeon = true

    @AmpHours = @MaxAmpHours
    @Efficiency = EMachineEfficiency.FLAWLESS

    @MaintAttemptsLeft = 3
    @PlayerMaintSuccessful = 0

    @Parts.Empty()
    for part, count of @NeededParts
      @Parts.AddItem(part, count)
    @PartsInitialized=true

  PerformMaintenance: ->
    @Efficiency++
    @PlayerMaintSuccessful=1
    @SetupPower()

  CanMaintain: () ->
    return !@LockParts

  SetupPower: () ->
    ###
    Should be called at the end of GenerateForDungeon.
    ###
    if @UsesEnergy
      @AmpHours=999999

    # You should get to use everything at least once.
    # SpecialDatabase:
    switch(@Efficiency)
      when EMachineEfficiency.POOR
        @AmpHours = @MaxAmpHours / 5
        break
      when EMachineEfficiency.FAIR
        @AmpHours = (@MaxAmpHours / 5) * 2
        break
      when EMachineEfficiency.GOOD
        @AmpHours = (@MaxAmpHours / 5) * 3
        break
      when EMachineEfficiency.EXCELLENT
        @AmpHours = (@MaxAmpHours / 5) * 4
        break
      when EMachineEfficiency.FLAWLESS
        @AmpHours = @MaxAmpHours
        break
    @AmpHours = Math.min(@MaxAmpHours, @AmpHours + random(0, @MaxAmpHours/10))

  GetEfficiencyAsString: ->
    return Int2MachineEfficiency[@Efficiency]

  serialize: () ->
    data = super()
    data['name'] = @Name
    data['typename'] = @TypeName
    data['location'] = @Location
    data['battery'] = @AmpHours
    data['efficiency'] = @Efficiency
    data['lock-parts'] = @LockParts
    data['is-mound-machine'] = @IsMoundMachine
    data['parts'] = @Parts.serialize()
    return data

  deserialize: (data) ->
    super(data)
    @Name           = data['name']
    @Location       = data['location']
    @AmpHours       = Math.max(Math.max(data['battery'], 0), @MaxAmpHours)
    @Efficiency     = data['efficiency']
    @LockParts      = data['lock-parts']
    @IsMoundMachine = data['is-mound-machine']
    @Parts = new InventoryController(@)
    @Parts.deserialize data['parts']

  clone: (machine) ->
    machine.Location = @Location
    machine.AmpHours = @AmpHours
    machine.Efficiency = @Efficiency
    machine.InDungeon = @InDungeon
    machine.MaintAttemptsLeft = @MaintAttemptsLeft
    machine.PlayerMaintSuccessful = @PlayerMaintSuccessful
    machine.Parts = @Parts.clone()
    machine.LockParts = @LockParts
    machine.IsMoundMachine = @IsMoundMachine
    return machine
