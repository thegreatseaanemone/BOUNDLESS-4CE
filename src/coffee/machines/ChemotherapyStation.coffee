class ChemotherapyStation extends Machine
  constructor: (data = null) ->
    #     TypeName               Name                    SeenItVar
    super('ChemotherapyStation', 'Chemotherapy Station', 'ChemotherapyStation', ChemotherapyStationPCB, MachineFrame, data)
    @Chemicals = {}
      #"Fyynacillin":  new Fyynacillin()
      #"Leupro": new Leupro() - Not implemented yet.
    @SelectedChem = null
    @CurrentDosage = 1
    @NeededParts =
      metal_panel: 3
      glass_panel: 4
      cable_part:  5
      valve_part: 5
      pump_part: 4
      hose_part:  10
      screen_part: 1


  clone: ->
    machine = super(new ChemotherapyStation())
    #machine.Chemicals = {}
    for k,v of @Chemicals
      machine.Chemicals[k]=v.clone()
    machine.SelectedChem = @SelectedChem
    machine.CurrentDosage = @CurrentDosage
    return machine

  EjectChemicals: () ->
    collected = []
    for key, chem of @Chemicals
      if getVariables().special_jar == 0
        continue
      jars = chem.ToJars()
      if jars == 1
        collected.push("a jar of #{chem.Name}")
      else if jars > 1
        collected.push("#{jars} jars of #{chem.Name}")
    return collected

  AddChemical: (chemical, dose_in_cc) ->
    if !(chemical.Name of @Chemicals)
      chem = ChemistryController.CreateFromName(chemical.Name)
      @Chemicals[chem.Name]=chem
    @Chemicals[chemical.Name].Amount += dose_in_cc

  AddChemicalFromJar: (chemical, njars) ->
    chem = null
    if !(chemical.Name of @Chemicals)
      chem = ChemistryController.CreateFromName(chemical.Name)
      @Chemicals[chem.Name]=chem
    else
      chem = @Chemicals[chemical.Name]
    if chem.JarType != null and getPlayer().Inventory.GetItemCount(chem.JarType) >= njars
      chem.Amount += njars * chem.JarDosage
      getPlayer().Inventory.RemoveItem chem.JarType,  njars
      getPlayer().Inventory.AddItem    'special_jar', njars

  HasChems: () ->
    for key, chem of @Chemicals
      if chem.Amount > 0
        return true
    return false

  GetChemAmount: (chemName) ->
    if chemName of @Chemicals
      return @Chemicals[chemName].Amount
    console.log "Unable to find chem %s.", chemName
    return 0

  GenerateForDungeon: () ->
    super()
    @Battery += @BasePower
    @AddChemical(new Fyynacillin(), random(5, 100))
    #@AddChemical(new Leupro(), random(5, 100))
    @SetupPower()

  GetBloodLevel: () ->
    return getPlayer().Chemicals.GetAmount(@SelectedChem)

  DoTick: ->
    for key, chem of @Chemicals
      if @SelectedChem == key and chem.Amount > 0
        currentBloodLevel = @GetBloodLevel()
        toInject = Math.max(Math.floor(@CurrentDosage - currentBloodLevel), 0)
        if toInject > 0
          getPlayer().Chemicals.Add chem, toInject
          chem.Amount -= toInject
          AlertUtils.AddWarning "The #{@Name} injects #{toInject}cc of #{chem.Name} into you."
        return toInject
    return 0
  twPrintMenuButton: ->
    super(@Name)
  Leave: ->
    super()
    getVariables().lockDrink = 0
    getVariables().addCTChem=0
    getVariables().nJars=0;
    getVariables().cumDose=0
