class DeathGuide extends NPC
  constructor: ->
    super()
    @RollForSpecies getVariables().allPlayableHumanoids
    # Simple, succinct.
    @Mind.Name = @Name = 'Death'
    @Desc = either("sleek", "plump", "round", "fat", "blubbery", "busty", "blubber-stuffed", "blimplike", "gelatinous", "blimp-bellied", "moon-bellied", "blimp-breasted", "blimp-thighed")
    @Voice = either("singsong", "cheery", "chirpy", "gruff", "warbling", "shrill", "deep", "rich", "melodic", "honeyed", "smooth", "silky", "bright", "suggestive", "flirty", "sweet", "kindly")
    @Color = either getVariables().fullChromaColors
    @Mind.Identity.Pronouns = new NeutralPronouns()
