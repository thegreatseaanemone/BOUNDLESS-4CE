# Sapient creature.
NPCs=[]
class NPC extends Creature
  constructor: () ->
    super()
    @ID = -1
    @Name = ''
    @Specialty = null # For traders
    #@Disposition = EDisposition.NEUTRAL
    # Handled upstream now
    #@Species = null
    @Consiee = 0
    @TradingMarkup = 0
    @Inventory = new InventoryController(@)

  IsPC: ->
    return false

  RollForSpecies: (possibilities) ->
    console.log possibilities
    chosen = either possibilities
    Species.GetByID(chosen).Become(@)

  clone: ->
    npc = super(new NPC())
    npc.ID=@ID
    npc.Name = @Name
    npc.Specialty = @Specialty
    #npc.Disposition = @Disposition
    #npc.Species = @Species
    npc.Consiee = @Consiee
    npc.TradingMarkup = @TradingMarkup
    npc.Inventory = @Inventory.clone()
    return npc

  serialize: ->
    data = super()
    data['ID']=@ID
    data['Name']=@Name
    data['Consiee'] = @Consiee
    data['TradingMarkup'] = @TradingMarkup
    data['Specialty'] = @Specialty.Type
    #data['Species']   = if @Species != null then @Species.ID else null
    data['Inventory'] = if @Inventory != null then @Inventory.serialize() else null
    #console.log "SER: Specialty = #{data['Specialty']}"
    return data

  deserialize: (data) ->
    super(data)
    #console.log "DES: Specialty = #{data['Specialty']}"
    @ID = if 'ID' of data then data['ID'] else @ID
    @Name = if 'Name' of data then data['Name'] else @Name
    @Consiee = if 'Consiee' of data then data['Consiee'] else @Consiee
    @Species = if 'Species' of data then Species.GetByID(data['Species']) else @Species
    @TradingMarkup = if 'TradingMarkup' of data then data['TradingMarkup'] else @TradingMarkup
    @Specialty = TraderSpecialty.GetByID(data['Specialty'])
    if 'Inventory' of data
      @Inventory = new InventoryController()
      @Inventory.deserialize(data['Inventory'])
