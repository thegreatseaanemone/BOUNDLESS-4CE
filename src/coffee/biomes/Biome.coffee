# requires: Directions.coffee
# requires: biomes/BiomeNames.coffee

EAtmosphereHumidity =
  LOW: 0
  AVERAGE: 1
  HIGH: 2 # $wetWeather

EAtmosphereTemperature =
  VERY_COLD: 0 # $lethalCold
  COLD: 1 # $coldWeather
  AVERAGE: 2
  HOT: 3 # $hotWeather
  VERY_HOT: 4 # $lethalHeat

EAtmosphereFlags =
  TOXIC: 1 # $toxicFumes

EAtmospherePressure =
  VACUUM: 0 # $vaccuum
  LOW: 1 # $unbreathable
  AVERAGE: 2 #
  HIGH: 3
  VERY_HIGH: 4 # $highPressure

class Atmosphere extends GameObject
  constructor: ->
    @Flags = 0
    @Pressure = EAtmospherePressure.AVERAGE
    @Humidity = EAtmosphereHumidity.AVERAGE
    @Temperature = EAtmosphereTemperature.AVERAGE
    @WeatherTypes = []

  Setup: (planet) ->
    switch planet.Temperature
      when "very cold"
        @Temperature = EAtmosphereTemperature.VERY_COLD
      when "very hot"
        @Temperature = EAtmosphereTemperature.VERY_HOT

  clone: ->
    o = new Atmosphere()
    o.Flags = @Flags
    o.Pressure = @Pressure
    o.Humidity = @Humidity
    o.Temperature = @Temperature
    o.WeatherTypes = @WeatherTypes
    return o

  serialize: ->
    o = super()
    o['Flags'] = @Flags
    o['Pressure'] = @Pressure
    o['Humidity'] = @Humidity
    o['Temperature'] = @Temperature
    o['WeatherTypes'] = @WeatherTypes
    return o

  deserialize: (data) ->
    super(data)
    @Flags = data['Flags'] or @Flags
    @Pressure = data['Pressure'] or @Pressure
    @Humidity = data['Humidity'] or @Humidity
    @Temperature = data['Temperature'] or @Temperature
    @WeatherTypes = data['WeatherTypes'] or @WeatherTypes

  ###
  # @oldcode $weatherType
  ###
  GetWeatherType: ->
    return either @WeatherTypes

EBiomeFlags =
  UNDERWATER: 1 # $underwater
  DARK: 2 # $darkArea
  EDIBLE: 4 # $edibleBiome
  DRINKABLE: 8 # $drinkableBiome

ALL_BIOMES={}
ALL_BIOME_CLASSES=[]

class Biome extends GameObject
  @ReqFoliages = []
  @ReqHumidities = []
  @ReqTemperatures = []

  @MinDepth = Number.NEGATIVE_INFINITY
  @MaxDepth = Number.POSITIVE_INFINITY

  @ConnectingBiomes = null

  # Used in "[NAME]'s Folly"-style names
  @AcceptableNameVerbSuffixes=['Delight', 'Folly', 'Paradise', 'Grief']

  ###
  # @param plane {int} Plane ID number (0 for overworld, default is $plane)
  # @param planet {Planet} planet instance. (optional, default is $planet)
  # @param current_depth {int} Depth underground. (optional, default is $depthMeter)
  # @param current_biome {Biome} The biome the player is currently in. (optional, default is $biome)
  # @param biome_list {Array} List of available biome classes. (optional, default is current_biome.ConnectingBiomes or ALL_BIOME_CLASSES.)
  ###
  @RollForBiome: (plane=null, planet=null, current_depth=null, current_biome=null, biome_list=null) ->
    if !(plane)
      plane = getVariables().plane
    if !(planet)
      planet = getVariables().planet
    if !(current_depth)
      current_depth = getVariables().depthMeter
    if !(current_biome)
      current_biome = getVariables().biome
    if !(biome_list)
      biome_list = if current_biome and current_biome.constructor.ConnectingBiomes then current_biome.constructor.ConnectingBiomes else ALL_BIOME_CLASSES

    possibilities = []
    for candidate_type in biome_list
      if candidate_type.CanBeSpawned(plane, planet, current_depth, current_biome)
        possibilities.push candidate_type
    if possibilities.length == 0
      return null
    candidate_type = either possibilities
    biome = new candidate_type
    biome.Setup(planet)
    return biome

  RollForName: ->
    switch random(0,5)
      when 0
        all_possible_names = switch random(0,2)
          when 0
            getVariables().revStandardNames
          when 1
            getVariables().revHardNames
          else
            getVariables().revDictionaryNames
        return either(all_possible_names) + "'s " + either(@constructor.AcceptableNameVerbSuffixes)
      when 1,2
        return either(BIOME_NAMES_NOUNS)
      else
        return either(BIOME_NAMES_ADVERBS) + ' ' + either(BIOME_NAMES_NOUNS)

  ###
  # Determine whether conditions are met for spawning this biome.
  ###
  @CanBeSpawned: (plane, planet, current_depth, current_biome) ->
    if !(plane in @ReqPlanes) or !(planet.Humidity in @ReqHumidities) or !(planet.Temperature in @ReqTemperatures) or !(@MinDepth <= current_depth <= @MaxDepth)
      return false
    if current_biome and current_biome.constructor.ConnectingBiomes and !(@ID in current_biome.constructor.ConnectingBiomes)
      return false
    return true

  @Visit: (arg, to_dir=null) ->
    switch typeof arg
      when 'string'
        @VisitByName arg, to_dir
      when 'object'
        @VisitInstance arg, to_dir

  @VisitInstance: (newBiome, to_dir=null) ->
    oldBiome = getVariables().biome
    # Clear biomes, set biome behind us to be the old biome.
    getVariables().biomeNorth = if to_dir == EDirection.SOUTH then oldBiome else 0
    getVariables().biomeEast = if to_dir == EDirection.WEST then oldBiome else 0
    getVariables().biomeWest = if to_dir == EDirection.EAST then oldBiome else 0
    getVariables().biomeSouth = if to_dir == EDirection.NORTH then oldBiome else 0
    # From BiomeDPad.tw
    getVariables().forageDecay = 0
    getVariables().mineDecay = 0
    getVariables().trapArmed = 0
    # Assign the new biome.
    getVariables().biome = newBiome

  @VisitByName: (name, to_dir=null) ->
    biome = @CreateByName name
    biome.Setup(getVariables().planet)
    @VisitInstance biome, to_dir

  @CreateByName: (name) ->
    return new ALL_BIOMES[name]()

  constructor: ->
    @Planet = null

    ###
    # @oldcode $location
    ###
    @Class = ''

    ###
    # @oldcode $zoneType
    ###
    @Type = ''

    @Flags = 0

    ###
    # Climate name
    ###
    @Name = ''

    @Atmosphere = new Atmosphere @

    ###
    # The new Mirajin system has the planet and the biome contribute their own mirajin loads.
    # @oldcode $ambientMirajin = $planet.AmbientMiraRads
    ###
    @AmbientMiraRads = 0

    ###
    # The new Mirajin system has the planet and the biome contribute their own mirajin loads.
    # @oldcode $ambientRads = $planet.AmbientRads
    ###
    @AmbientRads = 0

    ###
    # @oldcode $zoneFoliage
    ###
    @Foliage = ''

    ###
    # @oldcode $administreStatue
    ###
    @Statues = []

    ###
    # @oldcode $biomeCreatureColor
    ###
    @CreatureColors = []

  Setup: (planet) ->
    @Planet = planet
    @Name = @RollForName()
    return

  clone: (o) ->
    o.AmbientMiraRads = @AmbientMiraRads
    o.AmbientRads = @AmbientRads
    o.Atmosphere = @Atmosphere.clone()
    o.Class = @Class
    o.CreatureColors = @CreatureColors
    o.Flags = @Flags
    o.Foliage = @Foliage
    o.FoliageTypes = @FoliageTypes
    o.MobTypes = @MobTypes
    o.Name = @Name
    o.Planet = @Planet
    o.Statues = @Statues
    o.Type = @Type
    o.WeatherTypes = @WeatherTypes
    return o

  serialize: () ->
    data = super()
    data['AmbientMiraRads'] = @AmbientMiraRads
    data['AmbientRads'] = @AmbientRads
    data['Atmosphere'] = @Atmosphere.serialize()
    data['Class'] = @Class
    data['CreatureColors'] = @CreatureColors
    data['Flags'] = @Flags
    data['Foliage'] = @Foliage
    data['FoliageTypes'] = @FoliageTypes
    data['MobTypes'] = @MobTypes
    data['Name'] = @Name
    data['Planet'] = @Planet
    data['Statues'] = @Statues
    data['Type'] = @Type
    data['WeatherTypes'] = @WeatherTypes
    return data

  deserialize: (data) ->
    super(data)
    @AmbientMiraRads = data['AmbientMiraRads']
    @AmbientRads = data['AmbientRads']
    @Atmosphere = new Atmosphere()
    @Atmosphere.deserialize data['Atmosphere']
    @Class = data['Class']
    @CreatureColors = data['CreatureColors']
    @Flags = data['Flags']
    @Foliage = data['Foliage']
    @FoliageTypes = data['FoliageTypes']
    @MobTypes = data['MobTypes']
    @Name = data['Name']
    @Planet = data['Planet']
    @Statues = data['Statues']
    @Type = data['Type']
    @WeatherTypes = data['WeatherTypes']
