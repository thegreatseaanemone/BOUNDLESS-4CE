# requires: biomes/Biome.coffee
class MoltenSeaBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['high']
	@ReqTemperatures = ['very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Molten Sea'
		@Flags = 0
		@Atmosphere.Flags |= EAtmosphereFlags.TOXIC
		@WeatherTypes = ["baked","wasted"]
		@CreatureColors = ["yellow","yellow","golden","golden","orange","orange","orange","red","red","red","brown","brown","gray","white","black","black"]

	clone: () ->
		return super(new MoltenSeaBiome())

ALL_BIOME_CLASSES.push MoltenSeaBiome
ALL_BIOMES["Molten Sea"] = MoltenSeaBiome
