# requires: biomes/Biome.coffee
class CrystalSpiresBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['barren','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Crystal Spires'
		@Flags = 0
		@Type = "faerie"
		@FoliageTypes = ["fey"]
		@AmbientMiraRads += either(0, 1, 1, 2)
		@Statues = ["Nefirian"]
		@WeatherTypes = ["lush","foggy","rainy","snowy","snowfog"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@MobTypes = ["boreal","fey"]
			@WeatherTypes = ["snowy","snowfog","foggy","blizzard"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["scrub","tropical","fey"]
			@WeatherTypes = ["lush","humid wet","arid"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new CrystalSpiresBiome())

ALL_BIOME_CLASSES.push CrystalSpiresBiome
ALL_BIOMES["Crystal Spires"] = CrystalSpiresBiome
