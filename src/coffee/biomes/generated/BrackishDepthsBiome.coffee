# requires: biomes/Biome.coffee
class BrackishDepthsBiome extends Biome
	@ReqPlanes = []
	@ReqHumidities = []
	@ReqTemperatures = []
	@ReqFoliages = []
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Brackish Depths'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.DRINKABLE | EBiomeFlags.SUBTERRANEAN | EBiomeFlags.UNDERWATER
		@Type = "saltwater"
		@FoliageTypes = ["moderate","abundant"]
		@MobTypes = ["oceanic","eldritch"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Arches","Arches","Nefirian","Nameless"]
		@WeatherTypes = ["aquatic"]
		@CreatureColors = ["golden","orange","red","brown","green","green","green","teal","teal","teal","blue","blue","indigo","indigo","white","black"]

	clone: () ->
		return super(new BrackishDepthsBiome())

ALL_BIOME_CLASSES.push BrackishDepthsBiome
ALL_BIOMES["Brackish Depths"] = BrackishDepthsBiome
