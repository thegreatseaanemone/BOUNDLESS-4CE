# requires: biomes/Biome.coffee
class FrozenBeachBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['barren','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Frozen Beach'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["moderate","scrub"]
		@MobTypes = ["boreal","oceanic"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowfog","coldsnap","blizzard"]
		@CreatureColors = ["yellow","golden","brown","gray","gray","silver","silver","silver","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new FrozenBeachBiome())

ALL_BIOME_CLASSES.push FrozenBeachBiome
ALL_BIOMES["Frozen Beach"] = FrozenBeachBiome
