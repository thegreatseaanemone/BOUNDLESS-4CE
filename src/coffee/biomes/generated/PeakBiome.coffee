# requires: biomes/Biome.coffee
class PeakBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Peak'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["moderate","scrub"]
		@MobTypes = ["woodland","boreal"]
		if either(0, 0, 0, 0, 1)
			@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowfog","coldsnap"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@WeatherTypes = ["snowy","snowfog","coldsnap","blizzard"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@Type = "earth"
			@FoliageTypes = ["moderate","scrub","barren"]
			@MobTypes = ["scrub","tropical"]
			@WeatherTypes = ["arid","baked"]
		@CreatureColors = ["pink","yellow","golden","brown","gray","gray","silver","silver","silver","teal","teal","blue","white","white","white","indigo"]

	clone: () ->
		return super(new PeakBiome())

ALL_BIOME_CLASSES.push PeakBiome
ALL_BIOMES["Peak"] = PeakBiome
