# requires: biomes/Biome.coffee
class FrozenLakeBiome extends Biome
	@ReqPlanes = []
	@ReqHumidities = []
	@ReqTemperatures = []
	@ReqFoliages = []
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Frozen Lake'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["moderate","scrub"]
		@MobTypes = ["boreal","oceanic"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowfog","coldsnap","blizzard"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new FrozenLakeBiome())

ALL_BIOME_CLASSES.push FrozenLakeBiome
ALL_BIOMES["Frozen Lake"] = FrozenLakeBiome
