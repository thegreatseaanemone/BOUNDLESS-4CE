# requires: biomes/Biome.coffee
class SwampBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['abundant','moderate']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Swamp'
		@Flags = EBiomeFlags.DRINKABLE
		@Type = "freshwater"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["freshwater","wetland"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Kaolan","Arches"]
		@WeatherTypes = ["lush","humid wet","humid wet","foggy","rainy"]
		@CreatureColors = ["pink","yellow","golden","orange","red","brown","brown","brown","gray","gray","green","green","teal","teal","silver","black"]

	clone: () ->
		return super(new SwampBiome())

ALL_BIOME_CLASSES.push SwampBiome
ALL_BIOMES["Swamp"] = SwampBiome
