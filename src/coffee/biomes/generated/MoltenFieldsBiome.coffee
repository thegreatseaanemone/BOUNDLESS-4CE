# requires: biomes/Biome.coffee
class MoltenFieldsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high']
	@ReqTemperatures = ['very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Molten Fields'
		@Flags = 0
		@Atmosphere.Flags |= EAtmosphereFlags.TOXIC
		@WeatherTypes = ["baked","wasted"]
		@CreatureColors = ["yellow","yellow","golden","golden","orange","orange","orange","red","red","red","brown","brown","gray","white","black","black"]

	clone: () ->
		return super(new MoltenFieldsBiome())

ALL_BIOME_CLASSES.push MoltenFieldsBiome
ALL_BIOMES["Molten Fields"] = MoltenFieldsBiome
