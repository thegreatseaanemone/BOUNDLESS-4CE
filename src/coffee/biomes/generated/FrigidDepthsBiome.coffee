# requires: biomes/Biome.coffee
class FrigidDepthsBiome extends Biome
	@ReqPlanes = []
	@ReqHumidities = []
	@ReqTemperatures = []
	@ReqFoliages = []
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Frigid Depths'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.SUBTERRANEAN
		@Type = "ice"
		@FoliageTypes = ["barren"]
		@MobTypes = ["oceanic","eldritch"]
		@Atmosphere.Pressure = EAtmospherePressure.VERY_HIGH
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent","Arches"]
		@WeatherTypes = ["dampcave","wetcave","crystalcave","icecave"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new FrigidDepthsBiome())

ALL_BIOME_CLASSES.push FrigidDepthsBiome
ALL_BIOMES["Frigid Depths"] = FrigidDepthsBiome
