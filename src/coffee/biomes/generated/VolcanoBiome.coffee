# requires: biomes/Biome.coffee
class VolcanoBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Volcano'
		@Flags = EBiomeFlags.DARK
		@Type = "fire"
		@FoliageTypes = ["barren"]
		@MobTypes = ["scrub","tropical","infernal","infernal"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Phoenix"]
		@WeatherTypes = ["arid","baked","wasted","drought"]
		@CreatureColors = ["yellow","yellow","golden","golden","orange","orange","orange","red","red","red","brown","brown","gray","white","black","black"]

	clone: () ->
		return super(new VolcanoBiome())

ALL_BIOME_CLASSES.push VolcanoBiome
ALL_BIOMES["Volcano"] = VolcanoBiome
