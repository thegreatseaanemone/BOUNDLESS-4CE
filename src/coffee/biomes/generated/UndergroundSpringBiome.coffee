# requires: biomes/Biome.coffee
class UndergroundSpringBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MaxDepth = 49
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Underground Spring'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.DRINKABLE | EBiomeFlags.SUBTERRANEAN
		@Type = "freshwater"
		@FoliageTypes = ["moderate"]
		@MobTypes = ["subterranean"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Arches","Nameless","Nameless","Nefirian"]
		@WeatherTypes = ["dampcave","wetcave"]
		@CreatureColors = ["brown","brown","brown","brown","gray","gray","gray","silver","silver","teal","blue","indigo","violet","white","golden","black"]

	clone: () ->
		return super(new UndergroundSpringBiome())

ALL_BIOME_CLASSES.push UndergroundSpringBiome
ALL_BIOMES["Underground Spring"] = UndergroundSpringBiome
