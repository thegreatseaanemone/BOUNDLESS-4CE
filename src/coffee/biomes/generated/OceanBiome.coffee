# requires: biomes/Biome.coffee
class OceanBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['high']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Ocean'
		@Flags = 0
		@Type = "saltwater"
		@FoliageTypes = ["moderate"]
		@MobTypes = ["oceanic"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Arches","Arches","Nefirian"]
		@WeatherTypes = ["aquatic"]
		@CreatureColors = ["pink","yellow","golden","orange","red","brown","brown","green","teal","blue","blue","indigo","gray","silver","white","black"]

	clone: () ->
		return super(new OceanBiome())

ALL_BIOME_CLASSES.push OceanBiome
ALL_BIOMES["Ocean"] = OceanBiome
