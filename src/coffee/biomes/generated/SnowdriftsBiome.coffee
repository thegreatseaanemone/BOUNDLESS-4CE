# requires: biomes/Biome.coffee
class SnowdriftsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Snowdrifts'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["moderate","scrub"]
		@MobTypes = ["boreal"]
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowy","snowfog","coldsnap","blizzard"]
		@CreatureColors = ["pink","yellow","golden","brown","gray","gray","silver","silver","silver","teal","teal","blue","white","white","white","indigo"]

	clone: () ->
		return super(new SnowdriftsBiome())

ALL_BIOME_CLASSES.push SnowdriftsBiome
ALL_BIOMES["Snowdrifts"] = SnowdriftsBiome
