# requires: biomes/Biome.coffee
class WrithingVoidBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Writhing Void'
		@Flags = 0
		@Atmosphere.Pressure = EAtmospherePressure.LOW
		@Atmosphere.Pressure = EAtmospherePressure.VACUUM
		@WeatherTypes = ["crystalcave","wetcave","dampcave"]
		@CreatureColors = ["golden","gray","gray","silver","silver","teal","teal","blue","blue","indigo","indigo","violet","magenta","white","white","black"]

	clone: () ->
		return super(new WrithingVoidBiome())

ALL_BIOME_CLASSES.push WrithingVoidBiome
ALL_BIOMES["Writhing Void"] = WrithingVoidBiome
