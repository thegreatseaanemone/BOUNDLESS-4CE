# requires: biomes/Biome.coffee
class FrozenCavernBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = []
	@MinDepth = 50
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Frozen Cavern'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.SUBTERRANEAN
		@Type = "ice"
		@FoliageTypes = ["barren"]
		@MobTypes = ["subterranean","boreal","eldritch"]
		if either(0, 0, 0, 0, 1)
			@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["dampcave","wetcave","crystalcave","icecave","icecave"]
		@CreatureColors = ["pink","yellow","golden","brown","gray","gray","silver","silver","silver","teal","teal","blue","white","white","white","indigo"]

	clone: () ->
		return super(new FrozenCavernBiome())

ALL_BIOME_CLASSES.push FrozenCavernBiome
ALL_BIOMES["Frozen Cavern"] = FrozenCavernBiome
