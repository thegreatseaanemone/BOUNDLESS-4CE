# requires: biomes/Biome.coffee
class MountainBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Mountain'
		@Flags = 0
		@Type = "earth"
		@FoliageTypes = ["scrub"]
		@MobTypes = ["scrub","mountain","mountain"]
		@Statues = ["Reks"]
		@WeatherTypes = ["lush","foggy","arid","rainy","drought"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@FoliageTypes = ["scrub","moderate"]
			@MobTypes = ["woodland","boreal","mountain","mountain"]
			@Type = "ice"
			@Statues = ["Nefirian"]
			@WeatherTypes = ["snowy","foggy","snowfog","rainy","coldsnap"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@FoliageTypes = ["barren"]
			@MobTypes = ["scrub","tropical","infernal","mountain"]
			@WeatherTypes = ["arid","baked","drought"]
		if (getVariables().tempSelect == "cold") || (getVariables().tempSelect == "vcold") || (getVariables().tempSelect == "temp")
			@CreatureColors = ["pink","yellow","golden","orange","brown","gray","gray","gray","silver","silver","silver","teal","white","white","white","blue"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@CreatureColors = ["pink","yellow","golden","golden","golden","orange","orange","red","red","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new MountainBiome())

ALL_BIOME_CLASSES.push MountainBiome
ALL_BIOMES["Mountain"] = MountainBiome
