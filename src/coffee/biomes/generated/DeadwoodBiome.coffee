# requires: biomes/Biome.coffee
class DeadwoodBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Deadwood'
		@Flags = 0
		if getVariables().indoorZone == 0
			getVariables().wildrunning = 1
		else
			getVariables().wildrunning = 0
		@CreatureColors = ["yellow","yellow","brown","brown","gray","gray","gray","gray","silver","silver","silver","white","white","black","black","indigo"]

	clone: () ->
		return super(new DeadwoodBiome())

ALL_BIOME_CLASSES.push DeadwoodBiome
ALL_BIOMES["Deadwood"] = DeadwoodBiome
