# requires: biomes/Biome.coffee
class PlainsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['barren','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Plains'
		@Flags = 0
		@Type = "earth"
		@FoliageTypes = ["moderate","scrub"]
		@MobTypes = ["scrub"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Reks"]
		@WeatherTypes = ["lush","arid","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","golden","orange","orange","red","red","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new PlainsBiome())

ALL_BIOME_CLASSES.push PlainsBiome
ALL_BIOMES["Plains"] = PlainsBiome
