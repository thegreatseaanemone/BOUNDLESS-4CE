# requires: biomes/Biome.coffee
class IcyVoidBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Icy Void'
		@Flags = 0
		@Atmosphere.Pressure = EAtmospherePressure.LOW
		@Atmosphere.Pressure = EAtmospherePressure.VACUUM
		@WeatherTypes = ["icecave"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new IcyVoidBiome())

ALL_BIOME_CLASSES.push IcyVoidBiome
ALL_BIOMES["Icy Void"] = IcyVoidBiome
