# requires: biomes/Biome.coffee
class PlanetSHeartBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = "Planet's Heart"
		@Flags = 0
		@Atmosphere.Flags |= EAtmosphereFlags.TOXIC
		@Atmosphere.Pressure = EAtmospherePressure.VERY_HIGH
		@WeatherTypes = ["crystalcave","wetcave","dampcave","smokecave","smogcave"]
		@CreatureColors = ["pink","golden","orange","brown","red","gray","gray","silver","silver","teal","blue","indigo","violet","white","black","black"]

	clone: () ->
		return super(new PlanetSHeartBiome())

ALL_BIOME_CLASSES.push PlanetSHeartBiome
ALL_BIOMES["Planet's Heart"] = PlanetSHeartBiome
