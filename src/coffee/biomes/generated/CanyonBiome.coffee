# requires: biomes/Biome.coffee
class CanyonBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['barren','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Canyon'
		@Flags = 0
		@Type = "earth"
		@FoliageTypes = ["scrub"]
		@MobTypes = ["scrub","mountain"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Reks"]
		@WeatherTypes = ["arid","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","golden","orange","orange","red","red","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new CanyonBiome())

ALL_BIOME_CLASSES.push CanyonBiome
ALL_BIOMES["Canyon"] = CanyonBiome
