# requires: biomes/Biome.coffee
class FrozenHeartBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Frozen Heart'
		@Flags = 0
		@Atmosphere.Pressure = EAtmospherePressure.VERY_HIGH
		@WeatherTypes = ["dampcave","wetcave","crystalcave","icecave"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new FrozenHeartBiome())

ALL_BIOME_CLASSES.push FrozenHeartBiome
ALL_BIOMES["Frozen Heart"] = FrozenHeartBiome
