# requires: biomes/Biome.coffee
class AcidPoolsBiome extends Biome
	@ReqPlanes = []
	@ReqHumidities = []
	@ReqTemperatures = []
	@ReqFoliages = []
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Acid Pools'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.SUBTERRANEAN
		@Type = "poison"
		@FoliageTypes = ["barren"]
		@MobTypes = ["infernal"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Phoenix"]
		@Atmosphere.Flags |= EAtmosphereFlags.TOXIC
		@WeatherTypes = ["heatcave","smogcave"]

	clone: () ->
		return super(new AcidPoolsBiome())

ALL_BIOME_CLASSES.push AcidPoolsBiome
ALL_BIOMES["Acid Pools"] = AcidPoolsBiome
