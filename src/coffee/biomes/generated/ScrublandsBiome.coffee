# requires: biomes/Biome.coffee
class ScrublandsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Scrublands'
		@Flags = 0
		@Type = "earth"
		@FoliageTypes = ["scrub"]
		@MobTypes = ["scrub","mountain"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Reks"]
		@WeatherTypes = ["arid","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","golden","orange","orange","red","red","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new ScrublandsBiome())

ALL_BIOME_CLASSES.push ScrublandsBiome
ALL_BIOMES["Scrublands"] = ScrublandsBiome
