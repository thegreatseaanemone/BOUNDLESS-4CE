# requires: biomes/Biome.coffee
class ReefBiome extends Biome
	@ReqPlanes = []
	@ReqHumidities = []
	@ReqTemperatures = []
	@ReqFoliages = []
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Reef'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.DRINKABLE | EBiomeFlags.SUBTERRANEAN | EBiomeFlags.UNDERWATER
		@Type = "saltwater"
		@FoliageTypes = ["moderate","abundant"]
		@MobTypes = ["oceanic","eldritch"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Arches","Arches","Nefirian","Nameless"]
		@WeatherTypes = ["aquatic"]
		@CreatureColors = ["red","orange","yellow","golden","green","teal","blue","indigo","violet","pink","magenta","black","gray","silver","white","brown"]

	clone: () ->
		return super(new ReefBiome())

ALL_BIOME_CLASSES.push ReefBiome
ALL_BIOMES["Reef"] = ReefBiome
