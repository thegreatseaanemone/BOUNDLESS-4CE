# requires: biomes/Biome.coffee
class FrostWastesBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Frost Wastes'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["moderate","scrub","barren"]
		@MobTypes = ["boreal"]
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowfog","coldsnap","blizzard"]
		@CreatureColors = ["pink","yellow","golden","brown","gray","gray","silver","silver","silver","teal","teal","blue","white","white","white","indigo"]

	clone: () ->
		return super(new FrostWastesBiome())

ALL_BIOME_CLASSES.push FrostWastesBiome
ALL_BIOMES["Frost Wastes"] = FrostWastesBiome
