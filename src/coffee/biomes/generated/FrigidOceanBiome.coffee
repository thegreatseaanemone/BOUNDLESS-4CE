# requires: biomes/Biome.coffee
class FrigidOceanBiome extends Biome
	@ReqPlanes = []
	@ReqHumidities = []
	@ReqTemperatures = []
	@ReqFoliages = []
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Frigid Ocean'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["scrub","barren"]
		@MobTypes = ["oceanic"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowfog","coldsnap","blizzard"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new FrigidOceanBiome())

ALL_BIOME_CLASSES.push FrigidOceanBiome
ALL_BIOMES["Frigid Ocean"] = FrigidOceanBiome
