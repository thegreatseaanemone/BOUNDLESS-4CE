# requires: biomes/Biome.coffee
class CrystalCavernsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Crystal Caverns'
		@Flags = EBiomeFlags.SUBTERRANEAN
		@Type = "faerie"
		@FoliageTypes = ["barren"]
		if either(0, 1) == 1
			@Flags = EBiomeFlags.DARK
		@MobTypes = ["fey","subterranean","eldritch"]
		@AmbientMiraRads += either(0, 1, 1, 2, 3)
		@Statues = ["Nefirian","Lyric'Ai","Nameless"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["fey","subterranean"]
			@WeatherTypes = ["crystalcave"]
		@CreatureColors = ["magenta","pink","pink","yellow","yellow","golden","golden","orange","orange","red","red","brown","brown","silver","white","black"]

	clone: () ->
		return super(new CrystalCavernsBiome())

ALL_BIOME_CLASSES.push CrystalCavernsBiome
ALL_BIOMES["Crystal Caverns"] = CrystalCavernsBiome
