# requires: biomes/Biome.coffee
class GardenBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Garden'
		@Flags = 0
		@Type = "faerie"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["woodland","fey"]
		@AmbientMiraRads += either(0, 1, 1)
		@Statues = ["Nefirian"]
		@WeatherTypes = ["lush","foggy","rainy"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@MobTypes = ["woodland","boreal","fey"]
			@WeatherTypes = ["snowy","snowfog","foggy"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["scrub","tropical","fey"]
			@WeatherTypes = ["lush","humid wet","foggy"]
		@CreatureColors = ["magenta","pink","pink","yellow","yellow","golden","golden","orange","orange","red","red","brown","brown","silver","white","black"]

	clone: () ->
		return super(new GardenBiome())

ALL_BIOME_CLASSES.push GardenBiome
ALL_BIOMES["Garden"] = GardenBiome
