# requires: biomes/Biome.coffee
class UndergladeBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Underglade'
		@Flags = EBiomeFlags.SUBTERRANEAN
		@Type = "faerie"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["fey","subterranean","eldritch"]
		if either(0, 1) == 1
			@Flags = EBiomeFlags.DARK
		@AmbientMiraRads += either(0, 1, 1, 2, 3)
		@Statues = ["Nefirian","Nefirian","Lyric'Ai","Nameless"]
		@WeatherTypes = ["crystalcave","wetcave","dampcave"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["fey","subterranean","eldritch","infernal"]
		@CreatureColors = ["magenta","pink","pink","yellow","yellow","golden","golden","orange","orange","red","red","brown","brown","silver","white","black"]

	clone: () ->
		return super(new UndergladeBiome())

ALL_BIOME_CLASSES.push UndergladeBiome
ALL_BIOMES["Underglade"] = UndergladeBiome
