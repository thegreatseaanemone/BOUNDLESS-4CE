# requires: biomes/Biome.coffee
class BoilingOceanBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['high']
	@ReqTemperatures = ['very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Boiling Ocean'
		@Flags = 0
		@WeatherTypes = ["baked","wasted"]
		@CreatureColors = ["yellow","yellow","golden","golden","orange","orange","orange","red","red","red","brown","brown","gray","white","black","black"]

	clone: () ->
		return super(new BoilingOceanBiome())

ALL_BIOME_CLASSES.push BoilingOceanBiome
ALL_BIOMES["Boiling Ocean"] = BoilingOceanBiome
