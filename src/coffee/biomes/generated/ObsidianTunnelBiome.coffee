# requires: biomes/Biome.coffee
class ObsidianTunnelBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 50
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Obsidian Tunnel'
		@Flags = EBiomeFlags.SUBTERRANEAN
		@Type = "fire"
		@FoliageTypes = ["scrub","barren"]
		@MobTypes = ["subterranean","subterranean","infernal","eldritch"]
		if either(0, 1) == 1
			@Flags = EBiomeFlags.DARK
		@Statues = ["Phoenix","Lyric'Ai","Lyric'Ai","Nefirian","Nameless"]
		@WeatherTypes = ["dampcave","heatcave","smokecave","smogcave"]
		@CreatureColors = ["yellow","golden","orange","red","red","brown","brown","gray","gray","blue","indigo","violet","black","black","black","white"]

	clone: () ->
		return super(new ObsidianTunnelBiome())

ALL_BIOME_CLASSES.push ObsidianTunnelBiome
ALL_BIOMES["Obsidian Tunnel"] = ObsidianTunnelBiome
