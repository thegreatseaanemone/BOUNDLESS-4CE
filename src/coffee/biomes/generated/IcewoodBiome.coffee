# requires: biomes/Biome.coffee
class IcewoodBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Icewood'
		@Flags = 0
		if getVariables().indoorZone == 0
			getVariables().wildrunning = 1
		else
			getVariables().wildrunning = 0
		@CreatureColors = ["pink","yellow","golden","brown","gray","gray","silver","silver","silver","teal","teal","blue","white","white","white","indigo"]

	clone: () ->
		return super(new IcewoodBiome())

ALL_BIOME_CLASSES.push IcewoodBiome
ALL_BIOMES["Icewood"] = IcewoodBiome
