# requires: biomes/Biome.coffee
class MoltenVoidBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Molten Void'
		@Flags = 0
		@Atmosphere.Pressure = EAtmospherePressure.LOW
		@Atmosphere.Flags |= EAtmosphereFlags.TOXIC
		@Atmosphere.Pressure = EAtmospherePressure.VACUUM
		@WeatherTypes = ["dampcave","heatcave","smokecave","smogcave"]
		@CreatureColors = ["yellow","golden","orange","red","red","brown","brown","gray","gray","blue","indigo","violet","black","black","black","white"]

	clone: () ->
		return super(new MoltenVoidBiome())

ALL_BIOME_CLASSES.push MoltenVoidBiome
ALL_BIOMES["Molten Void"] = MoltenVoidBiome
