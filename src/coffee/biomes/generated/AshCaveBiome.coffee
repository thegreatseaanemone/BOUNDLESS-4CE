# requires: biomes/Biome.coffee
class AshCaveBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MaxDepth = 49
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Ash Cave'
		@Flags = EBiomeFlags.SUBTERRANEAN
		@Type = "fire"
		@FoliageTypes = ["scrub","barren"]
		@MobTypes = ["subterranean","subterranean","infernal","eldritch"]
		if either(0, 1) == 1
			@Flags = EBiomeFlags.DARK
		@Statues = ["Phoenix","Lyric'Ai","Lyric'Ai","Nefirian","Nameless"]
		@WeatherTypes = ["dampcave","heatcave","smokecave","smogcave"]
		@CreatureColors = ["yellow","golden","orange","red","red","brown","brown","gray","gray","blue","indigo","violet","black","black","black","white"]

	clone: () ->
		return super(new AshCaveBiome())

ALL_BIOME_CLASSES.push AshCaveBiome
ALL_BIOMES["Ash Cave"] = AshCaveBiome
