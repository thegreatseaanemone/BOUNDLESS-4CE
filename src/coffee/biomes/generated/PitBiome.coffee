# requires: biomes/Biome.coffee
class PitBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 50
	@MaxDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Pit'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.SUBTERRANEAN
		@Type = "eldritch"
		@FoliageTypes = ["barren"]
		@MobTypes = ["subterranean","eldritch"]
		@Statues = ["Nameless","Nameless","Nefirian","Lyric'Ai"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@WeatherTypes = ["crystalcave","wetcave","dampcave","icecave"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["subterranean","infernal"]
			@WeatherTypes = ["crystalcave","wetcave","dampcave","smokecave","smogcave"]
		@CreatureColors = ["pink","golden","brown","gray","gray","silver","silver","teal","teal","blue","blue","indigo","violet","white","black","black"]

	clone: () ->
		return super(new PitBiome())

ALL_BIOME_CLASSES.push PitBiome
ALL_BIOMES["Pit"] = PitBiome
