# requires: biomes/Biome.coffee
class TheEndlessEchoesBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'The Endless Echoes'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.SUBTERRANEAN
		@Type = "eldritch"
		@FoliageTypes = ["barren"]
		@MobTypes = ["subterranean","eldritch"]
		@Statues = ["Nameless","Nameless","Nefirian","Lyric'Ai"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@WeatherTypes = ["crystalcave","wetcave","dampcave","icecave"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["subterranean","infernal"]
			@WeatherTypes = ["crystalcave","wetcave","dampcave","smokecave","smogcave"]
		@CreatureColors = ["pink","golden","brown","gray","gray","gray","silver","silver","silver","teal","blue","indigo","violet","white","black","black"]

	clone: () ->
		return super(new TheEndlessEchoesBiome())

ALL_BIOME_CLASSES.push TheEndlessEchoesBiome
ALL_BIOMES["The Endless Echoes"] = TheEndlessEchoesBiome
