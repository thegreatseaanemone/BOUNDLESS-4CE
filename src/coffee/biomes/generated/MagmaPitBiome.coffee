# requires: biomes/Biome.coffee
class MagmaPitBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Magma Pit'
		@Flags = 0
		@Atmosphere.Flags |= EAtmosphereFlags.TOXIC
		@WeatherTypes = ["dampcave","heatcave","smokecave","smogcave"]
		@CreatureColors = ["yellow","yellow","golden","golden","orange","orange","orange","red","red","red","brown","brown","gray","white","black","black"]

	clone: () ->
		return super(new MagmaPitBiome())

ALL_BIOME_CLASSES.push MagmaPitBiome
ALL_BIOMES["Magma Pit"] = MagmaPitBiome
