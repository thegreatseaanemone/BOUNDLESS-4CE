# requires: biomes/Biome.coffee
class MineBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MaxDepth = 49
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Mine'
		@Flags = EBiomeFlags.SUBTERRANEAN
		@Type = "steel"
		@FoliageTypes = ["barren"]
		@MobTypes = ["subterranean","subterranean","eldritch"]
		if either(0, 1) == 1
			@Flags = EBiomeFlags.DARK
		@Statues = ["Andreu","Nameless","Nefirian"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@WeatherTypes = ["crystalcave","wetcave","dampcave","icecave"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["subterranean","subterranean","infernal"]
			@WeatherTypes = ["crystalcave","wetcave","dampcave","smokecave"]
		@CreatureColors = ["brown","brown","brown","red","gray","gray","gray","silver","silver","teal","blue","indigo","violet","white","golden","black"]

	clone: () ->
		return super(new MineBiome())

ALL_BIOME_CLASSES.push MineBiome
ALL_BIOMES["Mine"] = MineBiome
