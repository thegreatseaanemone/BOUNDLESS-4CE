# requires: biomes/Biome.coffee
class ReedsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['abundant','moderate']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Reeds'
		@Flags = EBiomeFlags.DRINKABLE
		@Type = "freshwater"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["freshwater","wetland"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Kaolan","Arches"]
		@WeatherTypes = ["lush","humid wet","humid wet","foggy","rainy"]
		@CreatureColors = ["pink","yellow","yellow","golden","golden","orange","red","red","brown","brown","gray","green","green","teal","white","black"]

	clone: () ->
		return super(new ReedsBiome())

ALL_BIOME_CLASSES.push ReedsBiome
ALL_BIOMES["Reeds"] = ReedsBiome
