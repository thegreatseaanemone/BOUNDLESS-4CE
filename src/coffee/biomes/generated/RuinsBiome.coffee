# requires: biomes/Biome.coffee
class RuinsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Ruins'
		@Flags = 0
		@Type = "poison"
		@FoliageTypes = ["scrub","barren"]
		@MobTypes = ["scrub","tropical","infernal"]
		@Statues = ["Phoenix"]
		@WeatherTypes = ["arid","wasted","baked","drought"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@MobTypes = ["scrub"]
			@WeatherTypes = ["arid","snowfog","coldsnap"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@WeatherTypes = ["arid","wasted","baked","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","orange","orange","red","red","red","brown","brown","brown","gray","white","black","black"]

	clone: () ->
		return super(new RuinsBiome())

ALL_BIOME_CLASSES.push RuinsBiome
ALL_BIOMES["Ruins"] = RuinsBiome
