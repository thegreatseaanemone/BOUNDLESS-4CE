# requires: biomes/Biome.coffee
class JungleBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['abundant','moderate']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Jungle'
		@Flags = EBiomeFlags.DARK
		@Type = "woodland"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["tropical","scrub"]
		@Statues = ["Reks"]
		@WeatherTypes = ["lush","tropical","tropical","foggy","rainy"]
		@CreatureColors = ["pink","yellow","yellow","golden","golden","golden","golden","golden","brown","brown","gray","gray","gray","white","indigo","black"]

	clone: () ->
		return super(new JungleBiome())

ALL_BIOME_CLASSES.push JungleBiome
ALL_BIOMES["Jungle"] = JungleBiome
