# requires: biomes/Biome.coffee
class ObsidianWastesBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high']
	@ReqTemperatures = ['very hot']
	@ReqFoliages = ['barren','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Obsidian Wastes'
		@Flags = EBiomeFlags.DARK
		@Type = "fire"
		@FoliageTypes = ["barren"]
		@MobTypes = ["scrub","tropical","infernal","infernal"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Phoenix"]
		@WeatherTypes = ["arid","baked","wasted","drought"]
		@CreatureColors = ["yellow","golden","orange","red","red","brown","brown","gray","gray","blue","indigo","violet","black","black","black","white"]

	clone: () ->
		return super(new ObsidianWastesBiome())

ALL_BIOME_CLASSES.push ObsidianWastesBiome
ALL_BIOMES["Obsidian Wastes"] = ObsidianWastesBiome
