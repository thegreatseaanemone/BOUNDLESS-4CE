# requires: biomes/Biome.coffee
class HellmouthDepthsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Hellmouth Depths'
		@Flags = 0
		@Atmosphere.Flags |= EAtmosphereFlags.TOXIC
		@WeatherTypes = ["dampcave","heatcave","smokecave","smogcave"]
		@CreatureColors = ["yellow","golden","orange","red","red","brown","brown","gray","gray","blue","indigo","violet","black","black","black","white"]

	clone: () ->
		return super(new HellmouthDepthsBiome())

ALL_BIOME_CLASSES.push HellmouthDepthsBiome
ALL_BIOMES["Hellmouth Depths"] = HellmouthDepthsBiome
