class DoseDependentFilter extends ChemicalEffectFilter
  constructor: (chem, options) ->
    super chem, options
    @MinDose = options.min or (effect) ->
      return 0
    @MaxDose = options.max or (effect) ->
      return Math.INFINITY

  Initialize: (creature, ingestion_method) ->
    super(creature, ingestion_method)
    @Initialized = true

  CanTickAllChildren: ->
    @MinDose(@) <= @Chemical.Amount < @MaxDose(@)

  Remove: ->
    @Initialized=false
    super()
