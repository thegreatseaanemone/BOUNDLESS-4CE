class ChemicalEffectFilter extends ChemicalEffect
  constructor: (chem, options) ->
    super chem
    @Children = options.children or []

  Initialize: (creature, ingestion_method) ->
    super(creature, ingestion_method)
    @Initialized = true

  CanTickAllChildren: ->
    yes

  CanTickChild: (child) ->
    yes

  Tick: (time_delta) ->
    if !@CanTickAllChildren()
      return
    for tickee in @Children
      if @CanTickChild(tickee)
        tickee.Tick(time_delta)

  Remove: ->
    @Initialized=false
    @Children=[]
    super()
