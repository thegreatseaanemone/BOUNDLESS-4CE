EBloatTargetFlags =
  BELLY: 1
  BREASTS: 2

class BloatingEffect extends PersistentEffect
  constructor: (chem, @Target, @MinPerTick, @MaxPerTick, ttl=1, delay=0, mod_div=1) ->
    super(chem, ttl, delay, mod_div)

  ApplyEffect: (time_delta) ->
    targets = []
    if (@Target & EBloatTargetFlags.BELLY) == EBloatTargetFlags.BELLY
      @Creature.BellyBloat += random(@MinPerTick,@MaxPerTick)*time_delta
      targets.push 'belly'
    if (@Target & EBloatTargetFlags.BREASTS) == EBloatTargetFlags.BREASTS
      @Creature.BreastBloat += random(@MinPerTick,@MaxPerTick)*time_delta
      targets.push 'breasts'
    if @Creature.IsPC()
      AlertUtils.AddWarning "You groan as your #{andjoin(targets)} bloat up!"

  Tick: (time_delta) ->
    if super(time_delta)
      @ApplyEffect(time_delta)

  Remove: ->
    if @Creature.IsPC() and @TicksToLive > 1
      AlertUtils.AddInfo "It feels like the bloating has passed."
