EBloatTargetFlags =
  BELLY: 1
  BREASTS: 2

class RubberizeEffect extends PersistentEffect
  constructor: (chem, @MinPerTick, @MaxPerTick, ttl=1, delay=0, mod_div=1) ->
    super(chem, ttl, delay, mod_div)

  ApplyEffect: (time_delta) ->
    switch @IngestionMethod
      when EIngestionMethod.IMMERSION
        AlertUtils.AddInfo "You feel your skin slowly tightening, rubberizing gradually."
        delta = random(@MinPerTick,@MaxPerTick)
        @Creature.StretchMod += delta
        @Creature.MaxBelly   += either(0, 0, 0, 0, 0, 0, 0, 10) * delta
        @Creature.MaxBreast  += either(0, 0, 0, 0, 0, 10) * delta

  Tick: (time_delta) ->
    if super(time_delta)
      @ApplyEffect(time_delta)

  Remove: ->
    if @Creature.IsPC() and @TicksToLive > 1
      AlertUtils.AddInfo "Feels like you've stopped getting stretchier."
