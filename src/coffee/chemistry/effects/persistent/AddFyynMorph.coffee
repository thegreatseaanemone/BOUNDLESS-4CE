#TODO: Better flavor text.

# Levels of $fyynlingMorph above which $fyyningMutation increments.
# Effects.tw usually sets PRE_* states.
# Immediately afterwards, Alert.tw displays a message and sets the stable state.
EFyynMutations =
  NONE: 0
  # Warning, just causes fat bloat.
  PRE_WARNING: 1
  WARNING: 2 #Stable
  # Non-leupai turn into fyynlings.
  PRE_FYYNMORPH: 3
  FYYNMORPH: 4 #stable
  # Non-leupai turn into horrors.
  HORROR: 5 #stable

FYYNMORPH_LEVELS = [
  50, # Results in fyyningMutation = 1, which jumps to $fyyningMutation = 2 after alert.
  70,
  100
]
FYYN_SPECIES = ['fyynling', 'fyynling horror', 'fyynmorph']
class AddFyynMorphEffect extends PersistentEffect
  constructor: (chem, @MinPerTick, @MaxPerTick, ttl=1, delay=0, mod_div=1) ->
    super(chem, ttl, delay, mod_div)

  DetectFyynMutations: ->
    fyynmut = @Creature.FyynlingMutation
    if fyynmut == EFyynMutations.NONE
      switch @Creature.Species
        when "fyynling", "fyynling horror"
          return EFyynMutations.HORROR
        when "fyynmorph"
          return EFyynMutations.FYYNMORPH
      if @Creature.SubSpecies == "fyynmorphic" or @Creature.BackType == "tentacles"
          return EFyynMutations.FYYNMORPH
    return EFyynMutations.NONE

  ###
  This function is a clusterfuck, I know.  However, the logic in Alerts.tw
  and BodySystems.tw can result in a wide variety of different species and body
  types per each mutation level, and each major branch of the mutation tree
  (non-mutated, leupai, slime, dragon) has different Fyynification stages for
  each $fyyningMutation.

  I'd like to redo this from scratch, but that would be a lot of work.
  ###
  ApplyEffect: (time_delta) ->
    amountInBlood = @Chemical.Amount
    toAdd = random(@MinPerTick,@MaxPerTick) * time_delta  * (amountInBlood * 2)
    if toAdd < 0 and @Creature.FyynlingMorph <= 0 and @Creature.FyynlingMutation <= 0 and !(@Creature.SpeciesStore in FYYN_SPECIES)
      return
    @Creature.AddFyynlingMorph(toAdd)
    fyynmut = @DetectFyynMutations()
    if @Creature.FyynlingMorph <= 0
      @Creature.FyynlingMorph = 0
      if fyynmut > 0
        switch fyynmut
          when EFyynMutations.PRE_WARNING, EFyynMutations.WARNING
            # Nothing special, just drop to NONE and boost fyynMorph.
            @Creature.SetFyynlingMutation EFyynMutations.NONE
            @Creature.SetFyynlingMorph FYYNMORPH_LEVELS[0]-1

          when EFyynMutations.PRE_FYYNMORPH, EFyynMutations.FYYNMORPH
            @Creature.SetBodyType "blubbery"
            # If you're a fyynmorph, you turn into a leupai because BOUNDLESS
            # doesn't store what you were before you deepthroated a fyyn and
            # fuck if I know what to do after this.
            if @Creature.Species == "fyynmorph"
              # You're now an off-the-shelf leupai with no subspecies. Yay.
              @Creature.SetSpecies "leupai"
              @Creature.SetSubSpecies 0

              # Drop tp previous level, set Fyynmorph to max for that level -1
              # so we still have enough to cure, but won't increment mutations.
              @Creature.SetFyynlingMutation EFyynMutations.WARNING
              @Creature.SetFyynlingMorph FYYNMORPH_LEVELS[1]-1

              # This handles finalization of the species set, so it gives you
              # flags, sets up powers, et cetera.
              window.state.display "BodyDatabase", null, "silently"

              # Not sure if this is needed, but every $species = leupai flag
              # I've seen thus far has also set $withTeeth.
              @Creature.SetTeeth 1

              # And idfk, but heading UP the fyyn-mutation chain adds these, so
              # why not?
              @Creature.SetBackType "tentacles"
            # Alerts.tw
            getVariables().autoRest = 0

          when EFyynMutations.HORROR
            getVariables().autoRest = 0
            # If you're a fyynling, you drop to the previous stage and
            # get the maximum fyynlingMorph for that level without incrementing
            # fyynlingMutation.
            if @Creature.Species == 'fyynling'
              @Creature.SetSpecies "fyynmorph"
              @Creature.SetSubSpecies 0
              window.state.display "BodyDatabase", null, "silently"
              @Creature.SetTeeth 1
              @Creature.SetFyynlingMutation EFyynMutations.FYYNMORPH
              @Creature.SetFyynlingMorph FYYNMORPH_LEVELS[2]-1
            # If you're a horror, you turn into a fyynling and get max
            # $fyynlingMorph.
            else if @Creature.Species == 'fyynling horror'
              @Creature.SetSpecies "fyynling"
              @Creature.SetSubSpecies 0
              window.state.display "BodyDatabase", null, "silently"
              @Creature.SetTeeth 1
              @Creature.SetFyynlingMorph FYYNMORPH_LEVELS[3]-1

          when 50 # Reference.
            # Alerts.tw
            getVariables().autoRest = 0
            @Creature.SetBodyType "gelatinous"
            @Creature.SetBackType "tentacles"
            @Creature.SetFaceType either("monstrous","oneroiesque")
            @Creature.SetSkinType "jelly"
            @Creature.SetSpeciesLock 0
            @Creature.SetElement "mirajin"
            @Creature.SetFyynlingMorph FYYNMORPH_LEVELS[2]-1
            if @Creature.SlimeFlag == 1
              @Creature.SetSpecies "fyynling"
              @Creature.SetSubSpecies 0
            else if @Creature.LeupaiFlag == 1
              @Creature.SetSubStatus "fyynmorphic"
            else if @Creature.DragonFlag == 1
              @Creature.SetSubSpecies "fyynling"
            else if @Creature.LeupaiFlag == 0 and @Creature.SlimeFlag == 0 and @Creature.DragonFlag == 0
              @Creature.SetSpecies "fyynling horror"
            @Creature.SetTeeth 1
            #@Creature.SetCalories @Creature.FyynlingMorph
            #@Creature.SetFyynlingGain 1 - UNUSED
            @Creature.SetFyynlingMorph 0
            #@Creature.SetFyyningMutation 0 - WHY


      if fyynmut <= 0 and @Creature.FyynlingMorph <= 0
        @Creature.FyynlingMutation = 0
        @Creature.FyynlingMorph = 0
        # What *should* should happen is that the Fyynling mutation stores the original species.
        # But since it doesn't we take a stab in the fucking dark.
        if @Creature.Species in FYYN_SPECIES
          # Fyynling muts set these both, for some reason.
          if @Creature.LeupaiFlag > 0
            @Creature.LeupaiFlag = 0
          if @Creature.SlimeFlag > 0
            @Creature.SlimeFlag = 0
          # Unlocks species
          if @Creature.SpeciesStore > 0
            @Creature.SpeciesStore = 0
          @Creature.Species = "leupai"
          @Creature.SubSpecies = 0
          if @Creature.IsPC()
            AlertUtils.AddCritical "You feel your body morph into something else!"
        if @Creature.IsPC()
          AlertUtils.AddGain "You are ''cured'' of the Fyynling parasites!"

    if @Creature.IsPC()
      if toAdd < 0
        AlertUtils.AddInfo "You feel a bit more... Normal."

  Tick: (time_delta) ->
    if super(time_delta)
      @ApplyEffect(time_delta)
