class AddMessageEffect extends PersistentEffect
  constructor: (chem, @AlertType, @Message, ttl=1, delay=0, mod_div=1) ->
    super(chem, ttl, delay, mod_div)

  ApplyEffect: (time_delta) ->
    AlertUtils.Add @AlertType, @Message

  Tick: (time_delta) ->
    if super(time_delta)
      @ApplyEffect(time_delta)
