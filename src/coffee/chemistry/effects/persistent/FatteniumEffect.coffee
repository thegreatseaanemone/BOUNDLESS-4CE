EFatteniumTargetFlags =
  BODY:    1
  BREASTS: 2
  BELLY:   4
  THIGHS:   8

class FatteniumEffect extends PersistentEffect
  constructor: (chem, ttl=1, delay=0, mod_div=1) ->
    super(chem, ttl, delay, mod_div)

  ApplyEffect: (time_delta) ->
    if @Creature.IsPC()
      # Atmospheric Alerts.tw
      AlertUtils.AddGain "Your body creaks, jiggles, and groans, <brightalert>''swelling fatter and fatter with each passing moment!''</brightalert>"

    possible_parts = 0
    switch @Creature.BodyType
      when 0
        possible_parts = EFatteniumTargetFlags.BODY | EFatteniumTargetFlags.BREASTS | EFatteniumTargetFlags.BELLY | EFatteniumTargetFlags.THIGHS
      when "busty"
        possible_parts = EFatteniumTargetFlags.BODY | EFatteniumTargetFlags.BREASTS
      when "hourglass"
        possible_parts = EFatteniumTargetFlags.BODY | EFatteniumTargetFlags.BREASTS | EFatteniumTargetFlags.THIGHS
      when "round", "pear"
        possible_parts = EFatteniumTargetFlags.BODY | EFatteniumTargetFlags.BELLY | EFatteniumTargetFlags.THIGHS
      when "belly"
        possible_parts = EFatteniumTargetFlags.BODY | EFatteniumTargetFlags.BELLY
      when "thigh"
        possible_parts = EFatteniumTargetFlags.BODY | EFatteniumTargetFlags.THIGHS
      when "boviete", "cubus"
        possible_parts = EFatteniumTargetFlags.BREASTS | EFatteniumTargetFlags.THIGHS
      when "guguetelle"
        possible_parts = EFatteniumTargetFlags.BELLY
      when "bulging"
        possible_parts = EFatteniumTargetFlags.BELLY | EFatteniumTargetFlags.THIGHS
      when "blimpthigh"
        possible_parts = EFatteniumTargetFlags.THIGHS

    $metaGain = @Creature.MetaGain * time_delta
    if (possible_parts & EFatteniumTargetFlags.BODY) == EFatteniumTargetFlags.BODY
      @Creature.Girth += $metaGain * either(0, 0, 1)
    if (possible_parts & EFatteniumTargetFlags.BREASTS) == EFatteniumTargetFlags.BREASTS
      @Creature.Breast += $metaGain * either(0, 0, 1)
    if (possible_parts & EFatteniumTargetFlags.BELLY) == EFatteniumTargetFlags.BELLY
      @Creature.Belly += $metaGain * either(0, 0, 1)
    if (possible_parts & EFatteniumTargetFlags.THIGHS) == EFatteniumTargetFlags.THIGHS
      @Creature.Thigh += $metaGain * either(0, 0, 1)
      @Creature.ThighFirmness = Math.max(0, @Creature.ThighFirmness - ($metaGain * either(0, 0, 1)))

    # Atmospheric Alerts.tw
    @Creature.MilkFat += either(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,time_delta)
    # LeupaiEggs.tw
    @Creature.QuaggyGirth += (@Creature.BonusWG * $metaGain)/10
    getVariables().eggFattened = 1


  Tick: (time_delta) ->
    if super(time_delta)
      @ApplyEffect(time_delta)

  Remove: ->
    if @Creature.IsPC() and @TicksToLive > 1
      AlertUtils.AddSuccess "It feels like the fattenium poisoning has passed."
