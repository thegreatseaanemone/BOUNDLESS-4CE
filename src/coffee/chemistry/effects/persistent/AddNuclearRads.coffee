class AddNuclearRadsEffect extends PersistentEffect
  constructor: (chem, @MinPerTick, @MaxPerTick, ttl=1, delay=0, mod_div=1) ->
    super(chem, ttl, delay, mod_div)

  ApplyEffect: (time_delta) ->
    toAdd = random(@MinPerTick,@MaxPerTick)*time_delta
    @Creature.NuclearRads = Math.max(0, @Creature.NuclearRads + toAdd)

  Tick: (time_delta) ->
    if super(time_delta)
      @ApplyEffect(time_delta)
