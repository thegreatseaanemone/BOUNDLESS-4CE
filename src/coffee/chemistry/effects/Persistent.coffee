class PersistentEffect extends ChemicalEffect
  constructor: (chem, @TicksToLive, @TicksDelay=0, @ModuloDivisor=1) ->
    super(chem)
    @nTicks = 0
    @Initialized = false

  Initialize: (creature, ingestion_method) ->
    super(creature, ingestion_method)
    @nTicks = 0
    @Initialized = true


  Tick: (time_delta) ->
    @nTicks++
    return @nTicks >= @TicksDelay and (@nTicks % @ModuloDivisor) == 0

  Remove: ->
    @nTicks = 0
    @Initialized=false
    super()
