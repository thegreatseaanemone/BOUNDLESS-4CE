EChemicalFlags =
  TOXIC:   1
  MUTAGEN: 2
  SOLUTE:  4 # Destryoed by solvent
  MICROBE: 8 # Destroyed by antibiotics
  FUNGUS:  16 # Destroyed by antifungals

class Chemical extends GameObject
  constructor: () ->
    @Creature = null
    @Name = ''
    @Amount = 0
    @Description = ''
    @OverdoseLevel = 0 # cc
    @Effects = []
    @OverdoseEffects = []
    @OverdoseOverridesEffects = false
    @MetabolizeAmount = 1 # Amount metabolized per tick.
    @JarType = null #Like GlassJar
    @JarDosage = 10

    @IngestionMethod = EIngestionMethod.INGESTED

  Initialize: (creature, ingestion_method) ->
    @Creature = creature
    @IngestionMethod = ingestion_method
    for effect in @Effects
      effect.Initialize(creature, ingestion_method)
    for effect in @OverdoseEffects
      effect.Initialize(creature, ingestion_method)

  ###
  # One-shot effect on creature.
  ###
  Affect: (creature, ingestion_method, time_delta=1) ->
    @Initialize creature, ingestion_method
    @Tick time_delta
    @Remove false


  ToJars: (creature) ->
    filled = 0
    availJars = creature.Inventory.GetItemCount(GlassJar)
    if @JarVariableName != null and @JarDosage > 0
      found = Math.floor(@Amount / @JarDosage)
      filled = Math.min(availJars, found)
      creature.Inventory.AddItem @JarType, filled
      creature.Inventory.RemoveItem GlassJar, filled
      @Amount -= filled * @JarDosage
    return filled

  IsOverdosing: ->
    @Amount >= @OverdoseLevel

  Tick: (time) ->
    if @IsOverdosing() or !@OverdoseOverridesEffects
      for effect in @Effects
        #console.log " TICKING [%s]", effect.constructor.Name
        effect.Tick(time)
    if !@IsOverdosing()
      for effect in @OverdoseEffects
        #console.log " [OD] TICKING [%s]", effect.constructor.Name
        effect.Tick(time)
    return

  Remove: (clear_chem=true) ->
    for effect in @Effects
      effect.Remove()
    for effect in @OverdoseEffects
      effect.Remove()
    if clear_chem
      if @Name of @Creature.Chemicals
        delete @Creature.Chemicals[@Name]
    @Creature = null

  serialize: ->
    data = super()
    data['amount'] = @Amount
    data['ingestion-method'] = @IngestionMethod
    return data

  deserialize: (data) ->
    switch typeof data
      when 'number'
        @Amount = data
      when 'object'
        super(data)
        @Amount = if 'amount' of data then data['amount'] else 1
        @IngestionMethod = data['ingestion-method']

  clone: ->
    chem = ChemistryController.CreateFromName(@Name)
    chem.Amount = @Amount
    if @Creature
      chem.Initialize(@Creature, @IngestionMethod)
    return chem

EIngestionMethod =
  NONE:      0
  INGESTED:  1
  INJECTED:  2
  INHALED:   3
  IMMERSION: 4 # Oil tub

class ChemicalEffect
  constructor: (@Chemical) ->
    @Creature = null
    @IngestionMethod = EIngestionMethod.NONE

  Initialize: (@Creature, @IngestionMethod) ->
    return

  Tick: (time_delta) ->
    return

  Remove: ->
    @Creature = null
    @IngestionMethod = EIngestionMethod.NONE

class ChemistryController extends GameObject
  @CHEMICALS_BY_NAME: {}
  @ALL_CHEMICALS: []
  _getChemAndNameFromArg: (input) ->
    chem = null
    id = null
    switch typeof input
      when 'string'
        id = input
        chem = ChemistryController.CreateFromName(id)
        if chem == null
          return [null,null]
      when 'function'
        chem = new input()
        id = chem.Name
      when 'object'
        chem = input
        id = input.Name
      else
        console.error "%s is %s, not a string, function, or object!", chem, typeof chem
    return [id, chem]

  _getNameFromArg: (item) ->
    [name,_] = @_getChemAndNameFromArg item
    return name

  constructor: (@Owner=null) ->
    @Chemicals={}

  @Register: (chem_class) ->
    chem = new chem_class()
    ChemistryController.CHEMICALS_BY_NAME[chem.Name] = chem_class
    ChemistryController.ALL_CHEMICALS.push chem_class

  @CreateFromName: (name) ->
    $chemtype = ChemistryController.CHEMICALS_BY_NAME[name]
    return new $chemtype()

  Add: (key, amount, ingestion_method=EIngestionMethod.INGESTED) ->
    [name, newchem] = @_getChemAndNameFromArg key
    if !(name of @Chemicals)
      @Chemicals[name] = newchem
      @Chemicals[name].Amount = amount
      if @Owner != null
        @Chemicals[name].Initialize(@Owner, ingestion_method)
    else
      @Chemicals[name].Amount += amount

  Remove: (key, amount=null) ->
    name = @_getNameFromArg key
    if name of @Chemicals
      if amount == null
        amount = @Chemicals[name].Amount
      afterAmount = Math.max(0, @Chemicals[name].Amount - amount)
      delta = @Chemicals[name].Amount - afterAmount
      @Chemicals[name].Amount -= delta
      if @Chemicals[name].Amount <= 0
        if @Owner != null
          @Chemicals[name].Remove(@Owner)
        delete @Chemicals[name]
      return delta
    return 0

  GetAmount: (key) ->
    name = @_getNameFromArg key
    if !(name of @Chemicals)
      return 0
    else
      return @Chemicals[name].Amount

  Tick: (delta_time=1) ->
    removeAfter=[]
    for key, chem of @Chemicals
      if chem.Amount > 0
        chem.Tick(delta_time)
        chem.Amount -= chem.MetabolizeAmount
      if chem.Amount <= 0
        removeAfter.push(key)
    for key in removeAfter
      @Chemicals[key].Remove(@Owner)
      delete @Chemicals[key]

  serialize: ->
    data = super()
    for name, chem of @Chemicals
      data[name] = chem.serialize()
    return data

  deserialize: (data) ->
    super(data)
    for key, cdata of data
      if key == '_type'
        continue
      if key of ChemistryController.CHEMICALS_BY_NAME
        newchem = ChemistryController.CreateFromName key
        newchem.Creature = @Owner
        newchem.deserialize cdata
        @Chemicals[key]=newchem

  clone: ->
    o = new ChemistryController @Owner
    o.Chemicals= {}
    for key, chemical of @Chemicals
      o.Chemicals[key]=@Chemicals[key].clone()
    return o
