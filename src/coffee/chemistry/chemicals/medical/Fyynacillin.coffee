###
Fyynacillin is my answer to irreversible Fyynling mutations.  It's slow, a decent amount
can kill you, and the OD effects are terrible.  However, at low dosages it
is the only effective treatment, removing 0 to 5 fyynmorph points per tick.

So, perfect chemical for the chemo station.

No fynnmorph = just bloating.
###
class Fyynacillin extends Chemical
  constructor: ()->
    super()
    @Name = 'Fyynacillin'
    @Description = 'A restorative drug that specifically targets Fyynling mutations.'
    @JarType = JarOfFyynacillin
    @JarDosage = 10
    @Effects = [
      # Remove [0,5] units of $fyynlingMorph per tick. (Also does mutation-related stuff.)
      new AddFyynMorphEffect(@, 0, -5)
      # Add [0,5] units of bloating per tick.
      new BloatingEffect(@, EBloatTargetFlags.BELLY, 0, 5)
    ]
    @OverdoseLevel = 5 # Maximum dose of Fyynacillin before @OverdoseEffects kick in.
    @OverdoseOverridesEffects = true # Shuts off "normal" effects.
    @OverdoseEffects = [
      # Add 10-15 units of pain per tick.
      new AddPainEffect(@, 10, 15)
      # Add 5 to 10 units of bloating per tick.
      new BloatingEffect(@, EBloatTargetFlags.BELLY, 5, 10)
    ]
ChemistryController.Register Fyynacillin
