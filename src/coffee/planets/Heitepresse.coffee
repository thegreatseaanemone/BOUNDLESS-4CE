class Heitepresse extends Planet
  constructor: ->
    super('Mutant Living')
    @Name='Heitepresse'
    @ID = redacted(10)
    @Type='realized'
    @Temperature = EPlanetTemperature.TEMPERATE
    @Humidity = EPlanetHumidity.AVERAGE
    @Ambience = either("mira","mira","somnic","plump","chub","gainer");
    @Atmosphere = EPlanetAtmosphere.BREATHABLE
    @CoreType = 0
    @CoreFluid = 0
    @PopulationDensity = EPlanetPopDensity.MEDIUM
    @AddDenizen 'revix', 1, EDenizenFlags.CIVILIAN

  clone: ->
    return super(new Heitepresse())
