class HostileColdPlanet extends Planet
	constructor: (starter=false) ->
		super("Hostile Cold")
		@EZPopulate ["human", "revix", "aiouran"]
		@Type = either("frozen", "dead", "liquid", "barren")
		@Temperature = either("very cold", "very cold", "cold")
		@Atmosphere = either("breathable", "toxic", "no")
		@PopulationDensity = "low"
		if @Atmosphere == "no"
			@Humidity = "low"
		else
			@Humidity = "high"
	clone: () ->
		return super(new HostileColdPlanet())
ALL_PLANET_CLASSES.push HostileColdPlanet
