class PlanetStructureCaves extends PlanetStructure
  constructor: (planet) ->
    super planet, 'Caves'

  clone: ->
    return super(new PlanetStructureCaves(@Planet))
