class PlanetStructureFatieries extends PlanetStructure
  constructor: (planet) ->
    super planet, 'Fatieries'

  clone: ->
    return super(new PlanetStructureFatieries(@Planet))
