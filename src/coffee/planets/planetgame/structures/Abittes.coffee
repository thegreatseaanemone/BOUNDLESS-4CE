class PlanetStructureAbittes extends PlanetStructure
  constructor: (planet) ->
    super planet, 'Abittes'

  clone: ->
    return super(new PlanetStructureAbittes(@Planet))
