class PlanetStructureProcessingPlants extends PlanetStructure
  constructor: (planet) ->
    super planet, 'Processing Plants'

  clone: ->
    return super(new PlanetStructureProcessingPlants(@Planet))
