class PlanetStructurePumps extends PlanetStructure
  constructor: (planet) ->
    super planet, 'Pumps'

  clone: ->
    return super(new PlanetStructurePumps(@Planet))
