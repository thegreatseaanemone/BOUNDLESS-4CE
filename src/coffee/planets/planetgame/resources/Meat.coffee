class PlanetResourceMeat extends PlanetResource
  constructor: (planet) ->
    super planet, 'Meat'

  clone: ->
    return super(new PlanetResourceMeat(@Planet))
