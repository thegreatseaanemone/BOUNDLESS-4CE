class PlanetResourceMiracrystals extends PlanetResource
  constructor: (planet) ->
    super planet, 'Miracrystals'

  clone: ->
    return super(new PlanetResourceMiracrystals(@Planet))
