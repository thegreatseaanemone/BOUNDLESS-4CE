class PlanetResourceCrops extends PlanetResource
  constructor: (planet) ->
    super planet, 'Crops'

  clone: ->
    return super(new PlanetResourceCrops(@Planet))
