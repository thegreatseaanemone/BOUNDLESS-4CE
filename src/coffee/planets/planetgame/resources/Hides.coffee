class PlanetResourceHides extends PlanetResource
  constructor: (planet) ->
    super planet, 'Hides'

  clone: ->
    return super(new PlanetResourceHides(@Planet))
