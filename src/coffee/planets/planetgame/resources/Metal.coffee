class PlanetResourceMetal extends PlanetResource
  constructor: (planet) ->
    super planet, 'Metal'

  clone: ->
    return super(new PlanetResourceMetal(@Planet))
