class PlanetOccupationScientists extends PlanetOccupation
  constructor: (planet) ->
    super planet, 'Scientists', 'Scientists'

  clone: ->
    return super(new PlanetOccupationScientists(@Planet))
