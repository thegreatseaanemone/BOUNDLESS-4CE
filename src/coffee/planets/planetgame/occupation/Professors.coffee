class PlanetOccupationProfessors extends PlanetOccupation
  constructor: (planet) ->
    super planet, 'Professors', 'Professors'

  clone: ->
    return super(new PlanetOccupationProfessors(@Planet))
