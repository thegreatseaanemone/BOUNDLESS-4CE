class PlanetOccupationHunters extends PlanetOccupation
  constructor: (planet) ->
    super planet, 'Hunters', 'Hunters'

  clone: ->
    return super(new PlanetOccupationHunters(@Planet))

  Tick: ->
    if @Planet.Occupations.Hunters.Amount > 0
      # Civilian hunters are inferior to professional Wilderes; their chances of finding anything are lessened, they find less when they find anything at all, and they are more likely to be killed while hunting.
      # Crop chance
      if random(1, 5) == 1
        toAdd = 1
        # Bonus Crop chance
        if @Planet.Occupations.Hunters.Amount > 1
          toAdd += either(0, 0, 1)
        if @Planet.Occupations.Hunters.Amount >= 3
          toAdd += random(0, 2)
        if @Planet.Occupations.Hunters.Amount >= 5
          toAdd += random(0, 3)
        if @Planet.Occupations.Hunters.Amount >= 10
          toAdd += random(1, 5)
        window.twinePrint "\"<banner>[img[icon_alert]]Your Civilian Hunters brought back #{toAdd} ''Plants!''</banner>\""
        @Planet.Resources.Crops.Amount += toAdd
      # Hide chance
      if random(1, 16) == 1
        toAdd = 1
        getVariables().playerAnimalPopulation -= 1
        # Bonus Hide chance
        if @Planet.Occupations.Hunters.Amount > 1
          toAdd += either(0, 0, 0, 0, 0, 1)
        if @Planet.Occupations.Hunters.Amount >= 3
          toAdd += either(0, 0, 0, 1)
        if @Planet.Occupations.Hunters.Amount >= 5
          toAdd += either(0, 0, 1)
        if @Planet.Occupations.Hunters.Amount >= 10
          toAdd += either(0, 1)
        window.twinePrint "\"<banner>[img[icon_alert]]Your Civilian Hunters brought back #{toAdd} ''Hides!''</banner>\""
        @Planet.Resources.Hides.Amount += toAdd
      # Flesh chance
      if random(1, 8) == 1
        toAdd = 1
        getVariables().playerAnimalPopulation -= 1
        # Bonus Flesh chance
        if @Planet.Occupations.Hunters.Amount > 1
          toAdd += either(0, 0, 0, 1)
        if @Planet.Occupations.Hunters.Amount >= 3
          toAdd += either(0, 0, 1)
        if @Planet.Occupations.Hunters.Amount >= 5
          toAdd += either(0, 1)
        if @Planet.Occupations.Hunters.Amount >= 10
          toAdd += 1
        window.twinePrint "\"<banner>[img[icon_alert]]Your Civilian Hunters brought back #{toAdd}kg of ''Flesh!''</banner>\""
        @Planet.Resources.Meat.Amount += toAdd
      # Job-related events
      randomizer = random(1, 48)
      # Skill Promotion
      if randomizer == 1
        @Planet.Occupations.Hunters.Amount -= 1
        @Planet.Occupations.Wilderes.Amount += 1
        @Planet.Resources.Meat.Amount += 5
        @Planet.Resources.Hides.Amount += 10
        window.twinePrint '"<banner>[img[icon_fatstar]]One of your Civilian Hunters returns home with \'\'an exquisite catch! Armed with their new experience, they become a full-fledged Wildere!\'\'</banner>"'
      else if (randomizer >= 8) && (randomizer <= 12)
        # Leupai Attacks
        popSurge = random(1, 2)
        @Planet.Denizens.Leupai.Amount += popSurge
        @Planet.Occupations.Civilians.Amount += popSurge
        @Planet.AddFatness(1, 2)
        window.twinePrint '"<banner>[img[icon_alert]]One of your Civilian Hunters returns home with \'\'a belly full of leupai eggs!\'\'</banner>"'
      else if (randomizer >= 24) && (randomizer <= 30)
        # PAG Gain
        @Planet.AddFatness(1, 2)
        window.twinePrint '"<banner>[img[icon_gain]]One of your Civilian Hunters \'\'made friends with a helene, and came home fatter!\'\'</banner>"'
      else if randomizer >= 40
        # Deaths
        @Planet.Occupations.Hunters.Amount -= 1
        window.state.display 'PlanetGameSpecDeaths'
        deathtext = either("suffered a fatal fall", "tumbled down a bottomless pit", "was devoured by wild beasts", "lost the battle against their prey", "was never heard from again")
        window.twinePrint "\"<banner>[img[icon_redalert]]One of your Civilian Hunters ''#{deathtext}!''</banner>\""
        # Population loss hierarchy
        window.state.display 'PlanetGameSpecDeaths'
