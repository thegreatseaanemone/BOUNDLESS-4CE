class PlanetOccupationWilderes extends PlanetOccupation
  constructor: (planet) ->
    super planet, 'Wilderes', 'Wilderes'

  clone: ->
    return super(new PlanetOccupationWilderes(@Planet))
