class PlanetOccupationCavieres extends PlanetOccupation
  constructor: (planet) ->
    super planet, 'Cavieres', 'Cavieres'

  clone: ->
    return super(new PlanetOccupationCavieres(@Planet))
