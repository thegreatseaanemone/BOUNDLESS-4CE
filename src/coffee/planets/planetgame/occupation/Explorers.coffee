class PlanetOccupationExplorers extends PlanetOccupation
  constructor: (planet) ->
    super planet, 'Explorers', "Explorers"

  clone: ->
    return super(new PlanetOccupationExplorers(@Planet))
