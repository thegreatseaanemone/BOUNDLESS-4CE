EPlanetOccupation =
  EXPLORER: 'Explorer'
  HUNTER: 'Hunter'
  WILDERE: 'Wildere',
  CAVIERE: 'Caviere'
  PROFESSOR: 'Professor'
  CULINET: 'Culinet'
  SCIENTIST: 'Scientist'

class PlanetOccupation extends GameObject
  constructor: (id, name, population=0, flags=0) ->
    @ID = id
    @Name = name
    @Population = population
    @Flags = flags

  clone: (newinst) ->
    newinst.ID=@ID
    newinst.Name=@Name
    newinst.Population=@Population
    newinst.Flags=@Flags
    return newinst

  serialize: ->
    data = super()
    data['ID']=@ID
    data['Name']=@Name
    data['Population']=@Population
    data['Flags']=@Flags
    return data

  deserialize: (data) ->
    super(data)
    @ID=data['ID']
    @Name=data['Name']
    @Population=data['Population']
    @Flags=data['Flags']
