
class PlanetResource extends GameObject
  constructor: (@Planet, @Name) ->
    @Amount = 0

  clone: (newinst) ->
    newinst.Amount = @Amount
    newinst.Name = @Name
    return newinst

  serialize: ->
    return {
      Amount: @Amount
      Name: @Name
    }

  deserialize: (data) ->
    @Amount = data['Amount']
    @Name = data['Name']
