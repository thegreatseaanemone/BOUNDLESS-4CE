class DesolateLivingPlanet extends Planet
	constructor: (starter=false) ->
		super("Desolate Living")
		@EZPopulate(["human", "revix", "aiouran"])
		@Type = either("molten", "burning", "frozen", "dead", "dying", "liquid", "irradiated", "barren", "industrialized")
		@Temperature = either("very cold", "cold", "cold", "hot", "hot", "very hot")
		if @Temperature == "very cold"
			@Humidity = either("low", "none")
			@Atmosphere = either("breathable", "toxic", "no")
			@PopulationDensity = "low"
		if @Temperature == "cold"
			@Humidity = either("low", "low", "none")
			@Atmosphere = either("breathable", "toxic", "no")
			@PopulationDensity = either("low", "mid")
		if (@Temperature == "hot") || (@Temperature == "very hot")
			@Humidity = either("none", "low", "high")
			@Atmosphere = either("breathable", "toxic", "toxic", "no")
			@PopulationDensity = either("low", "mid")
		if (@Type == "molten") || (@Type == "irradiated") || (@Type == "burning") || (@Type == "liquid") || (@Type == "industrialized")
			@Humidity = "high"
			@Atmosphere = "toxic"
			@PopulationDensity = "low"
		if (@Type == "dead") || (@Type == "barren")
			@Humidity = "low"
			@Atmosphere = either(0, "toxic", "toxic", "no")
			@PopulationDensity = "low"
	clone: () ->
		return super(new DesolateLivingPlanet())
ALL_PLANET_CLASSES.push DesolateLivingPlanet
