class DeadPlanet extends Planet
	constructor: (starter=false) ->
		super("Dead")
		@EZPopulate ["revix", "spatial", "aiouran", "human"]
		@Type = either("shattered", "hollow", "dead")
		@Temperature = either("very hot", "hot", "cold", "very cold")
		@Atmosphere = "no"
		@Humidity = "low"
		@PopulationDensity = "low"
	clone: () ->
		return super(new DeadPlanet())
ALL_PLANET_CLASSES.push DeadPlanet
