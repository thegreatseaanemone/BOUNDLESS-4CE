class TemperateLivingPlanet extends Planet
  constructor: (starter=false) ->
    @Class = "Temperate Living"
    @EZPopulate(['human', 'revix', 'aiouran'])
    @Temperature = EPlanetTemperature.TEMPERATE
    @Humidity = EPlanetHumidity.AVERAGE
    @Atmosphere = EPlanetAtmosphere.BREATHABLE
    @PopulationDensity = EPlanetPopDensity.HIGH
    if starter
      @Type = either("overgrown","flourishing","flourishing","industrialized","industrialized","flourishing","distorted")
    else
      @Type = either("industrialized","flourishing","vitalized","overgrown","distorted","realized")
      @PopulationDensity = switch @Type
        when "flourishing" then either(EPlanetPopDensity.MEDIUM, EPlanetPopDensity.HIGH)
        when "industrialized", "overgrown", "distorted" then EPlanetPopDensity.MEDIUM
        when "vitalized", "realized" then either(EPlanetPopDensity.LOW, EPlanetPopDensity.MEDIUM)

  clone: () ->
    return super(new TemperateLivingPlanet())
ALL_PLANET_CLASSES.push TemperateLivingPlanet
