###
PLANET OBJECTS

RevecroixNotes:
All planets have

[surface biomes]
[subterranean biomes]

Most planets have

[planet's heart]

coreless planets have
[yawning void] or [shattered debris]

All planet types:
"shattered","hollow","molten","burning","frozen","dead","irradiated","barren","dying","liquid","industrialized","flourishing","vitalized","overgrown","distorted","realized","crystallized","roiling","bloated"

TODO: For later - give industrialized planets a higher chance of dungeons and settlements, but ALSO a higher chance of pollution and other shit
###

window.ALL_PLANET_CLASSES=[]

window.EPlanetFlags =
  ###
  # Your moundworld is where your mound is.
  ###
  MOUNDWORLD: 1

  ###
  # Living planet.  Meaning it's literally alive.
  ###
  LIVING: 2

  ###
  # Where you began life.
  ###
  HOMEWORLD: 4

  ###
  # You've been here.
  ###
  VISITED: 8


window.EPlanetTemperature =
  HOT: 'hot'
  VERY_HOT: 'very hot'
  TEMPERATE: 'temperate'
  COLD: 'cold'

window.EPlanetHumidity =
  AVERAGE: "average"
  HIGH: 'high'
  LOW: 'low'

# This entire fucking enum is because Revecroix has inconsistent naming schemes.
window.EPlanetPopDensity =
  HIGH: 'hi'
  MEDIUM: 'mid'
  LOW: 'low'

window.EPlanetAtmosphere =
  BREATHABLE: 'breathable'
  TOXIC: 'toxic'
  NO: 'no'

window.EPlanetCoreType =
  NONE: 0
  MOLTEN: 'molten'
  LIQUID: 'liquid'

class Planet extends GameObject
  constructor: (@Class) ->
    ###
    # The class of planet.  Hostile Hot, etc.
    # @oldcode $planetClass
    # CONVERTED 10/21/2017
    ###
    #@Class=''

    ###
    # Unique ID. Corresponds to position in $planets.
    # @oldcode $planetID
    # CONVERTED 10/21/2017
    ###
    @ID = 0

    ###
    # @oldcode $planet
    # CONVERTED 10/21/2017
    ###
    @Name=''

    @Flags = 0

    ###
    # The surface liquid (puddles).
    # @oldcode $planetFluid
    # CONVERTED 10/21/2017
    ###
    @SurfaceLiquid = ''

    ###
    # Used for biome generation, I think.
    # @oldcode $planetType
    # CONVERTED 10/21/2017
    ###
    @Type = ''

    ###
    # Average temperature of the planet.
    # @oldcode $planetTemp
    # CONVERTED 10/21/2017
    ###
    @Temperature = EPlanetTemperature.TEMPERATE

    ###
    # Average humidity of the planet.
    # @oldcode $planetHumidity
    # CONVERTED 10/21/2017
    ###
    @Humidity = EPlanetHumidity.AVERAGE

    ###
    # Atmosphere
    # @oldcode $planetAtmosphere
    # CONVERTED 10/21/2017
    ###
    @Atmosphere = EPlanetAtmosphere.BREATHABLE

    ###
    # Population density
    # @oldcode $popDensity
    # CONVERTED 10/21/2017
    ###
    @PopulationDensity = EPlanetPopDensity.HIGH

    ###
    # @oldcode $planetCoreType
    # CONVERTED 10/21/2017
    ###
    @CoreType = EPlanetCoreType.NONE

    ###
    # @oldcode $planetCoreFluid
    # CONVERTED 10/21/2017
    ###
    @CoreFluid = ''

    ###
    # @oldcode $planetAmbience
    # CONVERTED 10/21/2017
    ###
    @Ambience = 0

    ###
    # This is only used for leupai living planets.
    # @oldcode $livingPlanetBlood
    # CONVERTED 10/21/2017
    ###
    @Blood = ''

    ###
    # @oldcode $ambientMirajin - NOT REPLACED.
    ###
    @AmbientMiraRads = 0

    ###
    # @oldcode $ambientRads - NOT REPLACED.
    ###
    @AmbientRads = 0

    ###
    # @oldcode $corruptionPercentage  - NOT REPLACED.
    ###
    @Corruption = 0

    ###
    # @oldcode $livingPlanetType
    # CONVERTED 10/21/2017
    ###
    @LivingType = 0

    ###
    # All LivingPlanet population data coagulated into one place.
    # @oldcode $humanPop $elfPop $arkaPop $leupaiPop
    ###
    @Denizens = {}

  ###
  # Sets up denizen populations.
  ###
  AddDenizen: (denid, potential, flags=0) ->
    @Denizens[denid] = new Denizen(denid, potential, flags, random(200,250), random(0,3))

  ###
  # Old system output a simple numeric ID.
  # This outputs something like JSX-3985b.  Based on Kepler Object of Interest naming scheme (KOI-\d{4})
  ###
  RollNewID: ->
    @ID = ''
    for _ in [0 ... random(3,4)]
      @ID += random_char(CHARS_ALPHA_UC)
    @ID += '-'
    for _ in [0 ... 4]
      @ID += random_char(CHARS_NUMERIC)
    @ID += random_char(CHARS_ALPHA_LC)

  ###
  # Runs after constructor.
  ###
  PostGenerate: ->
    # The following was taken from AmbienceDatabase.tw and modified.
    switch @Type
      # Vital planet. Very green, densely packed with forest & jungle. Tainted pools are shining water or vitae.
      when "vitalized"
        @AmbientMiraRads -= either(1,1,2,3,5)
        @SurfaceLiquid = either("shining water","shining water","glowing teal goo","glowing teal goo")

        @CoreType = either("molten",0)
        if @CoreType == "molten"
          @CoreFluid = "molten teal goo"

      # Sub-Vital planet. Full of life. Tainted pools are shining water or vitae.
      when "overgrown"
        @AmbientMiraRads -= either(0,0,1,1,2)
        @SurfaceLiquid = either("shining water","shining water","shining water","shining water","shining water","shining water","shining water","shining water","glowing teal goo")

        @CoreType = either("molten",0)
        if @CoreType == "molten"
          @CoreFluid = "molten teal goo"

      # Sick planet. Tainted pools are dirty water, oil(NYI), or somnus.
      when "industrialized"
        @AmbientMiraRads -= either(0,0,0,0,1)
        @AmbientRads += either(0,0,0,1)
        @SurfaceLiquid = either("dirty water","dirty water","dirty water","dirty water","dirty water","somnus")

      # Damaged planet. Tainted pools are dirty water, oil(NYI), somnus, or plutonium.
      when "irradiated"
        @AmbientMiraRads -= either(0,0,0,0,1)
        @AmbientRads += either(0,1,1,2)
        @SurfaceLiquid = either("dirty water","dirty water","dirty water","somnus","somnus","glowing green goo")

      # Dying planet. Tainted pools are dirty water, oil(NYI), somnus, or plutonium.
      when "dying"
        @AmbientMiraRads -= either(0,0,0,0,1)
        @AmbientRads += either(0,0,1)
        @SurfaceLiquid = either("dirty water","dirty water","somnus","somnus","glowing green goo","glowing green goo","glowing green goo")

      # Hot planet. Tainted pools are boiling water or boiling oil.
      when "burning"
        @AmbientMiraRads -= either(0,0,0,0,1)
        @AmbientRads += either(0,0,1)
        @SurfaceLiquid = either("boiling water","boiling water","boiling water","boiling oil")

      # Molten planet. Tainted pools are boiling oil.
      when "molten"
        @AmbientMiraRads -= either(0,0,0,0,1)
        @SurfaceLiquid = "boiling oil"

      # Sub-Mirajinic planet. Life is beginning to mutate. Tainted pools are somnus, fattenium-tainted water, or mirajinic water
      when "distorted"
        @AmbientMiraRads += either(0,0,1,1,2)
        @SurfaceLiquid = either("somnus","golden water","pink water")
        @Corruption = either(20,50,80)

        @CoreType = either("molten",0)
        if @CoreType == "molten"
          @CoreFluid = either("molten orange goo","molten red goo","molten blue goo")

      # Mirajinic planet. Impregnated with miratoxin, nothing has been left untainted. Tainted pools are somnus, fattenium-tainted water, mirajinic water, or miraplasm.
      when "realized"
        @AmbientMiraRads += either(1,1,2,2,3)
        @SurfaceLiquid = either("somnus","pink water","golden water","glowing magenta goo")
        @Corruption = either(20,50,80)

        @CoreType = either("molten",0)
        if @CoreType == "molten"
          @CoreFluid = either("molten orange goo","molten red goo","molten blue goo")

      # Mirajinic planet. Crystallized by high mirajinic radiation. Tainted pools are fattenium-tainted water, fattenium, mirajinic water, or miraplasm.
      when "crystallized"
        @AmbientMiraRads += either(1,2,2,3,5)
        @SurfaceLiquid = either("golden water","golden oil","pink water","glowing magenta goo")
        @Corruption = either(20,50,80)

        @CoreType = either("molten", "molten",0)
        if @CoreType == "molten"
          @CoreFluid = either("molten orange goo","molten red goo","molten blue goo")

      # Mirajinic planet. Even the planet itself is transforming into something monstrous. Tainted pools are miraplasm, fattenium-tainted water, mirajinic water, or fattenium.
      when "roiling"
        @AmbientMiraRads += either(2,2,3,4,5)
        @SurfaceLiquid = either("golden water","golden oil","pink water","glowing magenta goo")
        @Corruption = either(50,80)
        @CoreType = 'fluid'

      # Mirajinic planet. This planet is a living creature! Tainted pools are blood (red, blue, or starblood; one choice per planet), miraplasm, miratoxin (leupai planets only), fattenium-tainted water, fattenium, or mirajinic water.
      when "bloated"
        @LivingType = either("leupai","non-leupai")>>
        @AmbientMiraRads += random(0,10)
        @CoreType = 'fluid'
        switch @LivingType
          when "leupai"
            @SurfaceLiquid = either("somnus","pink water","golden water","glowing magenta goo","ichor")
            @Blood = either("shimmering blue","glowing teal","glowing blue")
            @Corruption = random(20,80)
          when "non-leupai"
            @SurfaceLiquid = either("golden water","golden oil","glowing magenta goo","ichor")
            @Blood = "dark red"
        if @SurfaceLiquid == "ichor"
          @SurfaceLiquid = @Blood + " " + @SurfaceLiquid

      when "hollow", "shattered"
        @CoreType = 0
      else
        @CoreFluid = either("molten orange goo","molten orange goo","molten red goo","molten red goo","molten blue goo")

  RollForDenizen: ->
    denPossibilities = {}
    for own k,d of @Denizens
      denPossibilities[k]=d.Likelihood
    console.log denPossibilities
    return weighted_random(denPossibilities)

  RollForAmbientEffects: ->
    # This is only done in BilleportePlanetGen, for some reason.
    @Ambience = switch @Type
      when "overgrown" then either(0,0,0,0,0,"regen")
      when "flourishing" then either(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"regen","regen","regen","ferality")
      when "vitalized" then either(0,0,0,0,0,"regen","regen","ferality")
      when "industrialized", "barren" then either(0,0,0,0,0,"shrivel","shrivel","rad")
      when "dying" then either(0,0,0,"shrivel","rad")
      when "dead" then either(0,0,"shrivel","rad","rad")
      when "molten", "burning" then either(0,0,"rad")
      when "irradiated" then "rad"
      when "distorted" then either(0,0,0,0,0,0,0,0,0,0,0,0,0,0,"somnic","mira","plump","bloat","chub","gainer")
      when "realized" then either("mira","mira","somnic","somnic","plump","bloat","chub","gainer")
      when "crystallized" then either("mira","mira","mira","somnic","plump","bloat","chub","gainer")
      when "roiling" then either("mira","somnic","plump","bloat","chub","gainer")
      when "bloated" then either("mira","somnic","plump","bloat","chub","gainer")

  clone: (data) ->
    data.Class = @Class
    data.ID = @ID
    data.Name = @Name
    data.Flags = @Flags
    data.SurfaceLiquid = @SurfaceLiquid
    data.Denizens = @Denizens
    data.Type = @Type
    data.Temperature = @Temperature
    data.Humidity = @Humidity
    data.Atmosphere = @Atmosphere
    data.PopulationDensity = @PopulationDensity
    data.CoreType = @CoreType
    data.CoreFluid = @CoreFluid
    data.Ambience = @Ambience
    data.Blood = @Blood
    data.AmbientMiraRads = @AmbientMiraRads
    data.Corruption = @Corruption
    data.AmbientRads = @AmbientRads
    data.LivingType = @LivingType
    return data

  EZPopulate: (possible_denizens) ->
    @Denizens = {}
    potential=1
    available_denizens = possible_denizens.slice(0)
    for i in [0...possible_denizens.length]
      denid = pickndrop(available_denizens)
      if i < (possible_denizens.length - 1)
        potential /= 2
      @AddDenizen(denid, potential, EDenizenFlags.CIVILIAN)
    return
  # data\.([A-Za-z]+) = @\1
  serialize: () ->
    data = super()
    data['Class'] = @Class
    data['ID'] = @ID
    data['Name'] = @Name
    data['Flags'] = @Flags
    data['SurfaceLiquid'] = @SurfaceLiquid
    data['Denizens'] = {}
    for k,d of @Denizens
      data['Denizens'][k] = d.serialize()
    data['Type'] = @Type
    data['Temperature'] = @Temperature
    data['Humidity'] = @Humidity
    data['Atmosphere'] = @Atmosphere
    data['PopulationDensity'] = @PopulationDensity
    data['CoreType'] = @CoreType
    data['CoreFluid'] = @CoreFluid
    data['Ambience'] = @Ambience
    data['Blood'] = @Blood
    data['AmbientMiraRads'] = @AmbientMiraRads
    data['Corruption'] = @Corruption
    data['AmbientRads'] = @AmbientRads
    data['LivingType'] = @LivingType
    return data

  deserialize: (data) ->
    super(data)
    @Class = data['Class']
    @ID = data['ID']
    @Name = data['Name']
    @Flags = data['Flags']
    @SurfaceLiquid = data['SurfaceLiquid']
    @Denizens = {}
    for k,v of data['Denizens']
      @Denizens[k] = new Denizen()
      @Denizens[k].deserialize v
    @Type = data['Type']
    @Temperature = data['Temperature']
    @Humidity = data['Humidity']
    @Atmosphere = data['Atmosphere']
    @PopulationDensity = data['PopulationDensity']
    @CoreType = data['CoreType']
    @CoreFluid = data['CoreFluid']
    @Ambience = data['Ambience']
    @Blood = data['Blood']
    @AmbientMiraRads = data['AmbientMiraRads']
    @Corruption = data['Corruption']
    @AmbientRads = data['AmbientRads']
    @LivingType = data['LivingType']

window.RegisterPlanet = (planet) ->
  if !('planets' of getVariables())
    getVariables().planets={}
    console.log("Instantiated $planets.")
  while true
    planet.RollNewID()
    console.log("Generated ID=%s",planet.ID)
    if !(planet.ID of getVariables().planets)
      break
  getVariables().planets[planet.ID] = planet

window.DeregisterPlanet = (planet) ->
  if !('planets' of getVariables())
    getVariables().planets={}
    console.log("Instantiated $planets.")
  if planet.ID of getVariables().planets
    delete getVariables().planets[planet.ID]

window.GetPlanetByID = (id) ->
  if !('planets' of getVariables())
    return null
  if id of getVariables().planets
    return getVariables().planets[id]

window.VisitPlanet = (planetID) ->
  getVariables().planet = getVariables().planets[planetID]
  getVariables().planet.Flags &= EPlanetFlags.VISITED
  getVariables().startBiome = 0
  getVariables().location = 0
  getVariables().locName = 0
  getVariables().biomeNorth = 0
  getVariables().biomeSouth = 0
  getVariables().biomeEast = 0
  getVariables().biomeWest = 0
  getVariables().newPlanet = 1
  getVariables().hours += random(2,5)
  getVariables().weatherType = 0
  getVariables().newWeather = 1

window.TrimPlanetCache = ->
  #console.log("TRIM: %d items in", count(getVariables().planets))
  $planets = getVariables().planets
  $newplanets = {}
  for id, planet of $planets
    # If we lived there, were born there, or visited, we keep it.
    if (planet.Flags & (EPlanetFlags.HOMEWORLD|EPlanetFlags.MOUNDWORLD|EPlanetFlags.VISITED)) != 0
      $newplanets[planet.ID] = planet
    else
      console.warn("DISCARDING %s! (flags=%d)", planet.ID, planet.Flags)
  getVariables().planets = $newplanets
  #console.log("TRIM: %d items out", count(getVariables().planets))

window.GetVisitablePlanets = ->
  $planets = getVariables().planets
  o = []
  for id, planet of $planets
    if (planet.Flags & (EPlanetFlags.HOMEWORLD|EPlanetFlags.MOUNDWORLD)) != 0
      continue # We don't want to display homeworld or moundworlds, since we do those manually.
    o.push planet
  return o

window.GetPlanetClassByName = (name) ->
  for cls in ALL_PLANET_CLASSES
    clsI = new cls()
    console.log("Scanning (clsID=%s,name=%s)",clsI.Class,name)
    if clsI.Class == name
      return cls
  return null
