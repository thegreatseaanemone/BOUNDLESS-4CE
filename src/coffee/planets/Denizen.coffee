EDenizenFlags =
  CIVILIAN: 1
class Denizen extends GameObject
  constructor: (@ID, @Likelihood, @Flags=0, @Fatness=0, @Population=0) ->

  clone: () ->
    o = new Denizen()
    o.ID=@ID
    o.Likelihood=@Likelihood
    o.Flags=@Flags
    o.Fatness=@Fatness
    o.Population=@Population
    return o

  serialize: ->
    data = super()
    data['ID'] = @ID
    data['Likelihood'] = @Likelihood
    data['Flags'] = @Flags
    data['Fatness'] = @Fatness
    data['Population'] = @Population
    return data

  deserialize: (data) ->
    super(data)
    @ID = data['ID']
    @Likelihood = data['Likelihood']
    @Flags = data['Flags']
    @Fatness = data['Fatness']
    @Population = data['Population']

  Tick: (planet) ->
    if @Fatness == 0
      @Population = 0
