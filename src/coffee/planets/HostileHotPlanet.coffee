class HostileHotPlanet extends Planet
	constructor: (starter=false) ->
		super("Hostile Hot")
		@EZPopulate ["human", "revix", "aiouran"]
		@Type = either("molten", "burning", "frozen", "dead", "liquid", "barren", "crystallized")
		@Temperature = either("very hot", "very hot", "hot")
		@Atmosphere = either("breathable", "toxic", "no")
		@PopulationDensity = "low"
		if @Atmosphere == "none"
			@Humidity = "low"
		else if (@Type == "liquid") || (@Type == "molten")
			@Humidity = "high"
		else
			@Humidity = either("low", "high")
	clone: () ->
		return super(new HostileHotPlanet())
ALL_PLANET_CLASSES.push HostileHotPlanet
