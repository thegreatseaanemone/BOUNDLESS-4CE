class HotLivingPlanet extends Planet
	constructor: (starter=false) ->
		super("Hot Living")
		@EZPopulate ["human", "revix", "aiouran"]
		if starter
			@Type = either("dying", "barren", "industrialized", "industrialized")
			@Temperature = either("temperate", "hot", "hot")
			@Humidity = "high"
			@Atmosphere = "breathable"
			@PopulationDensity = either("mid", "hi")
			starter = 0
		else
			@Type = either("burning", "irradiated", "barren", "dying", "industrialized")
			@Temperature = either("hot", "hot", "very hot")
			@Humidity = "high"
			@Atmosphere = either("breathable", "breathable", "toxic")
			if @Type == "industrialized"
				@PopulationDensity = either("mid", "hi")
			else if (@Type == "barren") || (@Type == "irradiated") || (@Type == "dying")
				@PopulationDensity = either("low", "mid")
			else if @Type == "burning"
				@PopulationDensity = "low"
	clone: () ->
		return super(new HotLivingPlanet())
ALL_PLANET_CLASSES.push HotLivingPlanet
