window.json2blobURL = (json) ->
  blob = new Blob([json], {type: "octet/stream"});
  return URL.createObjectURL(blob);

window.ESaveAttemptCode =
  OK: 0
  FAILED: 1
  ABORTED: 2
window.save_game = (saveName) ->
  saveKey = "BOUNDLESS.Memory."+saveName
  console.log "Sanity checking..."
  if window.state == undefined
    console.error "window.state == undefined, cannot continue."
    return ESaveAttemptCode.FAILED
  if window.state.history == undefined
    console.error "window.state.history == undefined, cannot continue. Please make sure this is a Twine 1.4.2+ game."
    return ESaveAttemptCode.FAILED
  if window.state.history.length == 0
    console.error "window.state.history is empty. cannot continue."
    return ESaveAttemptCode.FAILED
  if window.localStorage == undefined
    console.error "This browser doesn't support localStorage!"
    return ESaveAttemptCode.FAILED
  if window.localStorage.getItem(saveKey) != null
    if !confirm("Save #{saveName} already exists.\n\nDo you want to overwrite it?")
      return ESaveAttemptCode.ABORTED
  window.localStorage.setItem(saveKey, JSON.stringify(window.state.history[0], null, '\t'))
  all_saves = JSON.parse(window.localStorage.getItem('BOUNDLESS.Memories') || "[]")
  if !(saveKey in all_saves)
    all_saves.push(saveKey)
    window.localStorage.setItem("BOUNDLESS.Memories", JSON.stringify(all_saves))
  return ESaveAttemptCode.OK

window.get_playable_species = () ->
  return JSON.parse(window.localStorage.getItem("BOUNDLESS.playable_species") || "[]")
window.set_playable_species = (values) ->
  window.localStorage.setItem("BOUNDLESS.playable_species", JSON.stringify(values))
window.add_playable_species = (species) ->
  all_known_species = window.get_playable_species()
  if !(species in all_known_species)
    all_known_species.push species
    window.set_playable_species all_known_species

window.save_game_to_file = (filename, data=window.state.history[0], mimetype='application/json') ->
  console.log data, filename
  blob = new Blob([JSON.stringify(data, null, '\t')], {type: mimetype})
  saveAs blob, if filename == 0 then data.variables.playerName else filename

window.filePicker = null
window.load_game_from_file = (title_after_load, variableset={}) ->
  filePicker = $('<input type="file" id="_savegame" accept="application/json"></input>')
  filePicker.click()
  filePicker.change ->
    for file in $(this).prop('files')
      rdr = new FileReader()
      rdr.onerror = ->
        alert "Failed to load file, something went wrong."
      rdr.onload = (e) =>
        window.state.history.length = 0
        data = JSON.parse(e.target.result)
        data = CheckForMigrations data
        data = deserialize data
        window.state.history.push data
        for own k of variableset
          window.state.history[0][k]=variableset[k]
        window.state.display(title_after_load, null, null)
        console.log "DONE!"
      rdr.readAsText(file)


window.get_all_saves = () ->
  return JSON.parse(window.localStorage.getItem('BOUNDLESS.Memories') || "[]")

window.ELoadAttemptCode =
  OK: 0
  FAILED: 1
  ABORTED: 2
window.load_game = (saveName) ->
  saveKey = "BOUNDLESS.Memory."+saveName
  if window.localStorage == undefined
    console.error "This browser doesn't support localStorage!"
    return ELoadAttemptCode.FAILED
  if window.localStorage.getItem(saveKey) == null
    alert "Save #{saveName} does not exist."
    return ELoadAttemptCode.ABORTED
  window.state.history.length = 0
  frame = JSON.parse(localStorage.getItem(saveKey))
  frame = CheckForMigrations frame
  frame = deserialize frame
  window.state.history.push frame
  return ELoadAttemptCode.OK

window.remove_save = (saveName) ->
  saveKey = "BOUNDLESS.Memory."+saveName
  if window.localStorage == undefined
    console.error "This browser doesn't support localStorage!"
    return false
  if window.localStorage.getItem(saveKey) == null
    console.error "Save #{saveName} does not exist."
    return false
  window.localStorage.removeItem(saveKey)
  all_saves = JSON.parse(window.localStorage.getItem('BOUNDLESS.Memories') || "[]")
  new_all_saves=[]
  for save in all_saves
    if saveKey == save
      continue
    new_all_saves.push save
  window.localStorage.setItem("BOUNDLESS.Memories", JSON.stringify(new_all_saves))
  return true

window.clean = (node) ->
  n = 0
  while n < node.childNodes.length
    child = node.childNodes[n]
    if child.nodeType == 8 or (child.nodeType == 3 and child.nodeValue == "")
      node.removeChild child
      n--
    else if child.nodeType == 1
      clean child
    n++
  return

window.GenerateSaveTable = ->
  o='<table class="memories">'
  psgTitle = window.state.history[0].passage.title
  for saveName in get_all_saves()
    memorySimpleName = saveName.replace("BOUNDLESS.Memory.", "")
    o += "<tr><th>#{memorySimpleName}</th>"
    o += "<td><a href='#' onclick='recall_save(\"#{memorySimpleName}\", \"#{psgTitle}\")' title=\"Load memory\"><span class='ui-icon ui-icon-folder-open'></span></a></td>"
    # o += "<td><<button [[Recall|passage()][$recallMemoryPasscode=\"#{memorySimpleName}\"; $service=1]]>></td>"
    if getVariables().newGame == 0
        o += "<td><a href='#' onclick='remember_save(\"#{memorySimpleName}\", \"#{psgTitle}\")' title=\"Overwrite memory\"><span class='ui-icon ui-icon-disk'></span></a></td>"
    #o += "<<if $newGame === 0>><td><<button [[Remember|passage()][$recallMemoryPasscode=\"#{memorySimpleName}\"; $service=2]]>></td><<endif>>"
    o += "<td><a href='#' onclick='forget_save(\"#{memorySimpleName}\", \"#{psgTitle}\")' title=\"Delete memory\"><span class='ui-icon ui-icon-circle-close'></span></a></td>"
    #o += "<td><<button [[Forget|passage()][$recallMemoryPasscode=\"#{memorySimpleName}\"; $service=3]]>></td>"
    o += "<td><a href='#' onclick='export_save(\"#{memorySimpleName}\", \"#{psgTitle}\")' title=\"Export memory\"><span class='ui-icon ui-icon-circle-arrow-s'></span></a></td>"
    #o += "<td><<button [[Export|passage()][$memoryPasscode = 0; $service = 0; _devnull = window.save_game_to_file(\"#{memorySimpleName.toLowerCase()}\", JSON.parse(localStorage.getItem(\"BOUNDLESS.Memory.#{memorySimpleName}\")))]]>></td>"
    o += "</tr>"
  o += '</table>'
  return o

window.recall_save = (memorySimpleName, target_passage) ->
  getVariables().recallMemoryPasscode = memorySimpleName;
  getVariables().service = 1;
  window.state.display(target_passage)

window.remember_save = (memorySimpleName, target_passage) ->
  getVariables().recallMemoryPasscode = memorySimpleName;
  getVariables().service = 2;
  window.state.display(target_passage)

window.forget_save = (memorySimpleName, target_passage) ->
  getVariables().recallMemoryPasscode = memorySimpleName;
  getVariables().service = 3;
  window.state.display(target_passage)

window.export_save = (memorySimpleName, target_passage) ->
  getVariables().memoryPasscode = 0;
  getVariables().service = 0;
  blob = new Blob([localStorage.getItem("BOUNDLESS.Memory."+memorySimpleName)], {type: "application/json;charset=utf-8"})
  saveAs(blob, memorySimpleName.toLowerCase()+".json", "utf-8")
  window.state.display(target_passage)
