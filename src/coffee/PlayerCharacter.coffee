class PlayerCharacter extends PlayerCharacterBase
  constructor: ->
    super()
    @Inventory = new InventoryController(@)
    @Chemicals = new ChemistryController(@)

  ## Inventory stuff.
  AddItem: (itemID, amount=1) ->
    @Inventory.AddItem(itemID, amount)
  RemoveItem: (itemID, amount=1) ->
    @Inventory.RemoveItem(itemID, amount)
  GetItem: (itemID) ->
    return @Inventory.GetItem(itemID)
  GetItemCount: (itemID) ->
    return @Inventory.GetItemCount(itemID)

  clone: ->
    npc = super(new PlayerCharacter())
    npc.Inventory = @Inventory.clone()
    npc.Chemicals = @Chemicals.clone()
    return npc

  serialize: ->
    data = super()
    data['_type'] = @constructor.name
    data['Inventory'] = if @Inventory != null then @Inventory.serialize() else null
    data['Chemicals'] = if @Chemicals != null then @Chemicals.serialize() else null
    #console.log "SER: Specialty = #{data['Specialty']}"
    return data

  deserialize: (data) ->
    super(data)
    if 'Inventory' of data
      @Inventory = new InventoryController(@)
      @Inventory.deserialize(data['Inventory'])
    if 'Chemicals' of data
      @Chemicals = new ChemistryController(@)
      @Chemicals.deserialize(data['Chemicals'])
