class GameObject
  constructor: ->

  serialize: () ->
    return {'_type':@constructor.name}

  clone: ->
    return

  deserialize: (data) ->

  decompile: ->
    return @serialize()

  toJSON: ->
    return @serialize() #JSON.stringify(@serialize())
