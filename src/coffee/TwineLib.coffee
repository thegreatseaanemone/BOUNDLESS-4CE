###
# This file contains interfaces with Twine.
###

# Used to determine what Twine's current element is.  Can also be set.
window.twineCurrentEl = null

# Used to get the current parser. Can probably be set, but, like, why?
window.twineCurrentParser = null

###
# Parses TwineScript, then inserts the resulting DOM nodes into place.
# @param arg {String} Twinescript to parse.
# @param place {DOMElement} Parent DOMElement which will contain the output.
###
window.twinePrint = window.twprint = (arg, place=window.twineCurrentEl) ->
  parser = twineCurrentParser
  try
    # See comment within macros.display
    output = internalEval(arg)
    if output != null and (typeof output != 'number' or !isNaN(output))
      new Wikifier(place, '' + output)
  catch e
    place.append $("<span class=\"twineError\"><b>twinePrint():</b> bad expression: <code>#{escapeJS(arg)}</code></span>")
    if TWINE_CATCH_ERRORS
      console.error e
    else
      throw e
  return

###
# Prints a line break.
###
window.twineNewline = (place=window.twineCurrentEl) ->
  twinePrint '"<br>"', place

###
# Acts like <<display>>.
###
window.twineDisplayInline = (name, place=window.twineCurrentEl) ->
  if !window.tale.has(name)
    twinePrint "<span class=\"twineError\"><b>twineDisplayInline():</b> Unknown passage <code>#{name}</code></span>", place
  else
    passage = tale.get(name)
    new Wikifier(place, passage.processText())
  return

###
# Create an internal twine Button.
#
# @param label {string} Text of the button.
# @param passage {string} Passage to display.
# @param callback {function} Callback to run after display() is called.
# @param place {HTMLElement} Parent of the button in DOM.
###
window.twineButton = (label, passage, callback, place=window.twineCurrentEl) ->
  button = Wikifier.createInternalLink place, passage, (->
    window.state.display passage, null, null, callback
    return
  ), 'button'
  insertText button, label
  return button



window.updateStoryMenu = ->
  document.getElementById('storyMenu').setAttribute 'style', ''
  window.setPageElement 'storyMenu', 'StoryMenu', ''
