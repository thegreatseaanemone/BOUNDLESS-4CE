postDisplay['Start'] = (a,b,c) ->
  JQHelper.GetElement('#run-tests').on 'click', ->
    JQConfirm
      title: 'Careful.'
      text: 'This button runs some automated unit tests.  <b>This is only intended for developers and does not confer any advantage.</b>' +
            '<br>In fact, continuing <b>WILL</b> break the game and require a refresh!'
      yes: ->
        window.doTests()
        return

postDisplay['QUnit'] = (a,b,c) ->
  setTimeout window.doTests, 1000
