updateDebugMenu = () ->
  $showDebugMenu = getVariables().showDebugMenu
  if $showDebugMenu
    console.log "Showing Debug Menu"
    $('#debugging').show()
  else
    console.log "Hiding Debug Menu"
    $('#debugging').hide()

updateAdminMenu = () ->
  $showAdminMenu = getVariables().showAdminMenu
  if $showAdminMenu
    console.log "Showing Admin Menu"
    $('#adminpanel').show()
  else
    console.log "Hiding Admin Menu"
    $('#adminpanel').hide()

getTrueAndFalseForType = (t) ->
  trueVal=1
  falseVal=0
  if trueVal == undefined
    t = "int"
  switch t
    when "int"
      trueVal = 1
      falseVal = 0
    when "str", "string"
      trueVal = "1"
      falseVal = "0"
    when "bool", "boolean"
      trueVal = true
      falseVal = false

  return [trueVal, falseVal]

updateOptions = () ->
    # <input id="chkShowDebug" name="chkDebugMessages" type="checkbox" class="togglevar" data-var="showDebug">
    $('input.togglevar').each ->
      varName = $(this).attr('data-var')
      dt = $(this).attr('data-type')
      [trueVal, falseVal] = getTrueAndFalseForType dt
      inverted = $(this).hasClass('inverted')
      currentValue = getVariables()[varName] == trueVal
      if inverted
        currentValue = !currentValue
      $(this).prop 'checked', currentValue

    $('input.togglevar').on 'change', (event) ->
      varName = $(this).attr('data-var')
      [trueVal, falseVal] = getTrueAndFalseForType $(this).attr('data-type')
      inverted = $(this).hasClass('inverted')
      currentValue = $(this).prop 'checked'
      if inverted
        currentValue = !currentValue
      getVariables()[varName] = if currentValue then trueVal else falseVal
      console.log "$#{varName} = ", getVariables()[varName]

    # Mutbars
    $('.opt_mutbars').each ->
      mutname = $(this).attr('data-hidemut')
      $(this).prop 'checked', !(mutname in getVariables().hideMutBars)

    $('.opt_mutbars').on 'change', (event) ->
      mutname = $(this).attr('data-hidemut')
      if $(this).prop('checked')
        console.log "hideMutBars += %s", mutname
        if !(mutname in getVariables().hideMutBars)
          getVariables().hideMutBars.push(mutname)
      else
        if mutname in getVariables().hideMutBars
          getVariables().hideMutBars = getVariables().hideMutBars.filter((item) ->
            return mutname != item)
