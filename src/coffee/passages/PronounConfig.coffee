# This is actually just used in other stuff, like NamelessResponses and the player setup.

window._procfg_setGenderPronouns = (sub, subplus, pos, refl) ->
  $("input[name='$pronounOne']").val sub
  $("input[name='$pronounOneCont']").val subplus
  $("input[name='$pronounTwo']").val pos
  $("input[name='$pronounThree']").val refl

window._procfg_lock = (islocked) ->
  $("input[name='$pronounOne']").prop 'disabled', islocked
  $("input[name='$pronounOneCont']").prop 'disabled', islocked
  $("input[name='$pronounTwo']").prop 'disabled', islocked
  $("input[name='$pronounThree']").prop 'disabled', islocked

window._postDisplayPronounConfig = ->
  ###
    <tr><th>Subjective:</th><td><<textinput $pronounOne>></td><td>he/she/they</tr>
    <tr><th>Subjective + Is/Are:</th><td><<textinput $pronounOneCont>></td><td>he's/she's/they're</tr>
    <tr><th>Possessive:</th><td><<textinput $pronounTwo>></td><td>his/her/their</td></tr>
    <tr><th>Reflexive:</th><td><<textinput $pronounThree>></td><td>himself/herself/themself</td></tr>
  ###

  $playerName = getVariables().playerName
  if $playerName != 0 and $playerName != undefined
    $("input[name='$newPlayerName']").val $playerName

  if getVariables().pronounOne != 0
    $pronounOne = getVariables().pronounOne
    $pronounOneCont = getVariables().pronounOneCont
    $pronounTwo = getVariables().pronounTwo
    $pronounThree = getVariables().pronounThree
    _procfg_setGenderPronouns($pronounOne, $pronounOneCont, $pronounTwo, $pronounThree)

  $pronounPreset = getVariables().pronounPreset
  if $pronounPreset != undefined
    $('#'+$pronounPreset).prop('checked', true).click()

  $('#proPresetMale').on 'click', (e) ->
    _procfg_lock true
    _procfg_setGenderPronouns 'he', "he's", "his", "himself"
    getVariables().pronounPreset = "proPresetMale"
  $('#proPresetFemale').on 'click', (e) ->
    _procfg_lock true
    _procfg_setGenderPronouns 'she', "she's", "her", "herself"
    getVariables().pronounPreset = "proPresetFemale"
  $('#proPresetThird').on 'click', (e) ->
    _procfg_lock true
    _procfg_setGenderPronouns 'they', "they're", "their", "themself"
    getVariables().pronounPreset = "proPresetThird"
  $('#proPresetCustom').on 'click', (e) ->
    _procfg_lock false
    getVariables().pronounPreset = "proPresetCustom"
