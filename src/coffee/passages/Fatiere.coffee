
class Fatiere
  @cmdSellGirth: null
  @spinGirth: null
  @cmdSellBreast: null
  @spinBreast: null
  @cmdSellBelly: null
  @spinBelly: null
  @cmdSellThigh: null
  @spinThigh: null

  @MIN_BREAST: 300
  @MIN_GIRTH: 2750
  @MIN_BELLY: 300
  @MIN_THIGH: 300

  @MaxGirthToSell = ->
    return Math.max(0, getVariables().girth - Fatiere.MIN_GIRTH)/10
  @MaxBreastToSell = ->
    return Math.max(0, getVariables().breast - Fatiere.MIN_BREAST)/10
  @MaxBellyToSell = ->
    return Math.max(0, getVariables().belly - Fatiere.MIN_BELLY)/10
  @MaxThighToSell = ->
    return Math.max(0, getVariables().thigh - Fatiere.MIN_THIGH)/10

  @UpdateControls = ->
    if getVariables().girth <= Fatiere.MIN_GIRTH
      Fatiere.cmdSellGirth.disable(true)
      Fatiere.spinGirth.spinner('disable').spinner('value', 0)
    else
      Fatiere.cmdSellGirth.disable(false)
      Fatiere.spinGirth.spinner('enable')
    if getVariables().breast <= Fatiere.MIN_BREAST
      Fatiere.cmdSellBreast.disable(true)
      Fatiere.spinBreast.spinner('disable').spinner('value', 0)
    else
      Fatiere.cmdSellBreast.disable(false)
      Fatiere.spinBreast.spinner('enable')
    if getVariables().belly <= Fatiere.MIN_BELLY
      Fatiere.cmdSellBelly.disable(true)
      Fatiere.spinBelly.spinner('disable').spinner('value', 0)
    else
      Fatiere.cmdSellBelly.disable(false)
      Fatiere.spinBelly.spinner('enable')
    if getVariables().thigh <= Fatiere.MIN_THIGH
      Fatiere.cmdSellThigh.disable(true)
      Fatiere.spinThigh.spinner('disable').spinner('value', 0)
    else
      Fatiere.cmdSellThigh.disable(false)
      Fatiere.spinThigh.spinner('enable')
    updateStoryMenu()


preDisplay['Fatiere'] = (a, b, c) ->
  dropButton Fatiere.cmdSellGirth
  dropButton Fatiere.cmdSellBreast
  dropButton Fatiere.cmdSellBelly
  dropButton Fatiere.cmdSellThigh
  dropSpinner Fatiere.spinGirth
  dropSpinner Fatiere.spinBreast
  dropSpinner Fatiere.spinBelly
  dropSpinner Fatiere.spinThigh

postDisplay['Fatiere'] = (a,b,c) ->
  Fatiere.spinGirth = $('#spinGirth')
  Fatiere.spinGirth.spinner
    min: 0
    max: Fatiere.MaxGirthToSell()
  Fatiere.spinGirth.spinner('disable').spinner('value', 0)
  if getVariables().showDebug == 1
    console.log(typeof $('#passages'))
  JQHelper.GetElement('#lnkZeroGirth').on 'click', (e) ->
    Fatiere.spinGirth.spinner('value', 0)
  JQHelper.GetElement('#lnkMaxGirth').on 'click', (e) ->
    Fatiere.spinGirth.spinner('value', Fatiere.MaxGirthToSell())
  Fatiere.spinGirth.spinner('disable').spinner('value', 0)
  Fatiere.cmdSellGirth = $('#cmdSellGirth')
  Fatiere.cmdSellGirth.on 'click', (e) ->
    amount = clamp(Fatiere.spinGirth.spinner('value'), 0, Fatiere.MaxGirthToSell())
    if getVariables().showDebug == 1
      console.log "Able to sell #{amount*10} girth"
    if amount > 0
      #getVariables().service = 2
      getVariables().girth -= amount * 10
      getVariables().fetiereStock += amount * 10
      getVariables().fatieeProgress += amount
      getVariables().food += getVariables().fatiereOffering
      #window.state.display "Fatiere"
      #return
    Fatiere.UpdateControls()


  Fatiere.spinBreast = $('#spinBreast')
  Fatiere.spinBreast.spinner
    min: 0
    max: Fatiere.MaxBreastToSell()
  JQHelper.GetElement('#lnkZeroBreast').on 'click', (e) ->
    Fatiere.spinBreast.spinner('value', 0)
  JQHelper.GetElement('#lnkMaxBreast').on 'click', (e) ->
    Fatiere.spinBreast.spinner('value', Fatiere.MaxBreastToSell())
  Fatiere.spinBreast.spinner('disable').spinner('value', 0)
  Fatiere.cmdSellBreast = $('#cmdSellBreast')
  Fatiere.cmdSellBreast.on 'click', (e) ->
    amount = clamp(Fatiere.spinBreast.spinner('value'), 0, Fatiere.MaxBreastToSell())
    if getVariables().showDebug == 1
      console.log "Able to sell #{amount*10} $breast"
    if amount > 0
      #getVariables().service = 2
      getVariables().breast -= amount * 10
      getVariables().fetiereStock += amount * 10
      getVariables().fatieeProgress += amount
      getVariables().food += getVariables().fatiereOffering
      #window.state.display "Fatiere"
      #return
    Fatiere.UpdateControls()


  Fatiere.spinBelly = $('#spinBelly')
  Fatiere.spinBelly.spinner
    min: 0
    max: Fatiere.MaxBellyToSell()
  JQHelper.GetElement('#lnkZeroBelly').on 'click', (e) ->
    Fatiere.spinBelly.spinner('value', 0)
  JQHelper.GetElement('#lnkMaxBelly').on 'click', (e) ->
    Fatiere.spinBelly.spinner('value', Fatiere.MaxBellyToSell())
  Fatiere.spinBelly.spinner('disable').spinner('value', 0)
  Fatiere.cmdSellBelly = $('#cmdSellBelly')
  Fatiere.cmdSellBelly.on 'click', (e) ->
    amount = clamp(Fatiere.spinBelly.spinner('value'), 0, Fatiere.MaxBellyToSell())
    if getVariables().showDebug == 1
      console.log "Able to sell #{amount*10} $belly"
    if amount > 0
      #getVariables().service = 2
      getVariables().belly -= amount * 10
      getVariables().fetiereStock += amount * 10
      getVariables().fatieeProgress += amount
      getVariables().food += getVariables().fatiereOffering
      #window.state.display "Fatiere"
      #return
    Fatiere.UpdateControls()


  Fatiere.spinThigh = $('#spinThigh')
  Fatiere.spinThigh.spinner
    min: 0
    max: Fatiere.MaxThighToSell()
  JQHelper.GetElement('#lnkZeroThigh').on 'click', (e) ->
    Fatiere.spinThigh.spinner('value', 0)
  JQHelper.GetElement('#lnkMaxThigh').on 'click', (e) ->
    Fatiere.spinThigh.spinner('value', Fatiere.MaxThighToSell())
  Fatiere.spinThigh.spinner('disable').spinner('value', 0)
  Fatiere.cmdSellThigh = $('#cmdSellThigh')
  Fatiere.cmdSellThigh.on 'click', (e) ->
    amount = clamp(Fatiere.spinThigh.spinner('value'), 0, Fatiere.MaxThighToSell())
    if getVariables().showDebug == 1
      console.log "Able to sell #{amount*10} $thigh"
    if amount > 0
      #getVariables().service = 2
      getVariables().thigh -= amount * 10
      getVariables().fetiereStock += amount * 10
      getVariables().fatieeProgress += amount
      getVariables().food += getVariables().fatiereOffering
      #window.state.display "Fatiere"
      #return
    Fatiere.UpdateControls()

  Fatiere.UpdateControls()
