# requires: passages/Options.coffee
# requires: Utils.coffee
window.adminPanel = null
window.debugPanel = null
window.consieeSpinner = null
window.numMutationID = null
window.adminToggle = null
window.debugToggle = null
window.optionsButton = null
window.optionsDialog = null
window._prgMachineAmpHours = null

preDisplay['*'] = (title, source, type) ->
  if getVariables().showDebug == 1
    console.log "PRE: "+title
  JQHelper.GC()
  dropButton window.adminToggle
  dropButton window.debugToggle
  dropButton window.optionsButton
  dropDialog window.optionsDialog
  dropTabs window.adminPanel
  dropTabs window.debugPanel
  dropSpinner window.consieeSpinner
  dropSpinner window.numMutationID

  if window._prgMachineAmpHours!=null
    window._prgMachineAmpHours.progressbar('destroy').remove()

postDisplay['*'] = (title, source, type) ->
  # FIX MEMORY LEAK IN TWINE
  if window.state.history.length > 0
    window.state.history=[window.state.history[0]]

  if getVariables().showDebug == 1
    console.log "POST: "+title

  # Fixes #23
  $(".passage").clipPath [[0,0],[0,100],[100,100],[100,0]],
    isPercentage:true
  $('.passage').css('clip-path', 'polygon(0% 0%, 0% 100%, 100% 100%, 100% 0%)')
  $('.passage').css('-webkit-clip-path', 'polygon(0% 0%, 0% 100%, 100% 100%, 100% 0%)')

  _postdisplay_machine(title)

  vstats = $('#vitality-statbar')
  if 'vitalityClasses' of getVariables()
    for classname in getVariables().vitalityClasses
      vstats.addClass(classname)

  window.debugToggle = $('#passages').find('#toggledebug')
  if window.debugToggle.exists()
    updateDebugMenu()
    window.debugToggle.on 'click', (e) ->
      getVariables().showDebugMenu = !getVariables().showDebugMenu
      updateDebugMenu()

  window.debugPanel = $('#passages').find('#debugging')
  if window.debugPanel.exists()
    window.debugPanel.tabs
      collapsible: true
      active: if 'debugToolsTab' of getVariables() then getVariables().debugToolsTab else 0
      activate: (ui, e) ->
        getVariables().debugToolsTab = window.debugPanel.tabs('option', 'active')
    window.debugPanel.tabs 'refresh'

  window.adminToggle = $('#passages').find('#toggleadmin')
  if window.adminToggle.exists()
    window.adminToggle.on 'click', (e) ->
      getVariables().showAdminMenu = !getVariables().showAdminMenu
      updateAdminMenu()
  window.adminPanel = $('#passages').find('#adminpanel')
  if window.adminPanel.exists()
    updateAdminMenu()
    window.adminPanel.tabs
      collapsible: true
      active: if 'adminToolsTab' of getVariables() then getVariables().adminToolsTab else 0
      activate: (ui, e) ->
        getVariables().adminToolsTab = window.adminPanel.tabs('option', 'active')
    window.adminPanel.tabs 'refresh'
  window.consieeSpinner = $('#passages').find('#spinner-consiee')
  if window.consieeSpinner.exists()
    window.consieeSpinner.spinner
      min: 0
      numberFormat: 'n'
      decimals: 2
      change: (event, ui) ->
        getVariables().food = $(this).spinner('value')
    window.consieeSpinner.spinner('value', getVariables().food)

  window.optionsButton = $('#passages').find('#toggleoptions')
  window.optionsDialog = $('#passages').find('#dialog-options')
  if window.optionsDialog.exists()
    window.optionsDialog.dialog
      resizable: false
      height: "auto"
      width: 800
      modal: true
      autoOpen: false
      open: (e, ui) ->
        updateOptions()
      buttons:
        "Exit": ->
          $(this).dialog("close");
  if window.optionsButton.exists()
    window.optionsButton.on 'click', (e) ->
      window.optionsDialog.dialog('open')

  $('#passages').find('#cmdRunMutation').on 'click', ->
    $(this).disable true
    $mutationStrength = $('#passages').find('#selMutationStrength').val()
    if $mutationStrength == null
      JQAlert
        title: 'Mutation'
        text: 'You need to enter a mutation strength.'
        ok_text: "ok jeez"
      return
    $geneRandomizerOverride = window.numMutationID.spinner 'value'
    #if $geneRandomizerOverride == 0
    #  return
    getVariables().mutationStrength=$mutationStrength
    getVariables().geneRandomizerOverride=$geneRandomizerOverride
    twineDisplayInline 'MutationDatabase', $('#passages').find('framedalert')[0]
    window.state.display window.passage(), null, null
    return

  window.numMutationID = $('#passages').find('#numMutationID')
  if window.numMutationID.exists()
    window.numMutationID.spinner
      min: 0
      numberFormat: 'n'
      decimals: 0
    window.numMutationID.spinner('value', if 'geneRandomizerOverride' of getVariables then getVariables().geneRandomizerOverride else 0)

  window.selMutationStrength = $('#passages').find('#selMutationStrength')
  window.selMutationStrength.selectmenu()

  $('.gene[data-gene-id]').each (i) ->
    varName = $(this).attr 'data-gene-id'
    if !(varName of getVariables()) or getVariables()[varName] != 1
      $(this).addClass 'disabled'
  $('.gene[data-gene-id]').on 'click', ->
    varName = $(this).attr 'data-gene-id'
    if getVariables()[varName] == 1
      $(this).addClass 'disabled'
      getVariables()[varName] = 0
    else
      $(this).removeClass 'disabled'
      getVariables()[varName] = 1
