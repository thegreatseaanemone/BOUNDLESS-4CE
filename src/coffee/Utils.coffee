window.getVariables = () ->
  return window.state.history[0].variables
window.isnull = (a) ->
  return a == null
window.isundefined = (a) ->
  return a == undefined
window.postDisplay = {}
window.preDisplay = {}

window.getPlayer = () ->
  return getVariables().player

window.jq2html = (a) ->
  return a.prop('outerHTML')

window.clamp = (value, min, max) ->
  return Math.min(Math.max(value, min), max)

window.count = (obj) ->
  return Object.keys(obj).length

window.getClass = (obj) ->
  if typeof obj == 'undefined'
    return 'undefined'
  if obj == null
    return 'null'
  Object::toString.call(obj).match(/^\[object\s(.*)\]$/)[1]

window.pickndrop = (arr) ->
  item = either(arr)
  index = arr.indexOf(item)
  if (index > -1)
    arr.splice(index, 1)
    return item
  return null

window.romanize = (num) ->
  if ! +num
    return NaN
  digits = String(+num).split('')
  key = [
    ''
    'C'
    'CC'
    'CCC'
    'CD'
    'D'
    'DC'
    'DCC'
    'DCCC'
    'CM'
    ''
    'X'
    'XX'
    'XXX'
    'XL'
    'L'
    'LX'
    'LXX'
    'LXXX'
    'XC'
    ''
    'I'
    'II'
    'III'
    'IV'
    'V'
    'VI'
    'VII'
    'VIII'
    'IX'
  ]
  roman = ''
  i = 3
  while i--
    roman = (key[+digits.pop() + i * 10] or '') + roman
  return Array(+digits.join('') + 1).join('M') + roman

###
# Takes a map with the key being the possibility, and the value being the weight in decimal [0,1).
# @example How to use
#   ... = weighted_random {"fuck":0.25, "dicks": 0.75}
###
window.weighted_random = (spec) ->
  i = undefined
  sum = 0
  r = Math.random()
  for i, w of spec
    sum += w
    if r <= sum
      return i
  return either(Object.keys(spec)) # Failover

CHARS_ALPHA_LC = 'abcdefghijklmnopqrstuvwxyz'
CHARS_ALPHA_UC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
CHARS_NUMERIC = '0123456789'

window.CHAR_REDACT='█'

window.CONSIEE_SYMBOL = "\u20B5"  #'₵'

window.random_char = (CHAR_CLASS) ->
  return CHAR_CLASS.charAt(Math.floor(Math.random() * CHAR_CLASS.length))

window.redacted = (nChars) ->
  return repeat_char(CHAR_REDACT, nChars)

window.redacted_sentence = (minWords,maxWords) ->
  words = []
  for _ in [0...random(minWords,maxWords)]
    words.push(redacted(random(1,10)))
  return words.join(' ')

window.repeat_char = (char, nChars) ->
  return Array(nChars+1).join(char)

###*
# Shuffles array in place.
# @param {Array} a items An array containing the items.
###
window.shuffle = (a) ->
  j = undefined
  x = undefined
  i = a.length - 1
  while i > 0
    j = Math.floor(Math.random() * (i + 1))
    x = a[i]
    a[i] = a[j]
    a[j] = x
    i--
  return

# jQuery Extensions

###
# Checks that elements actually exist in this selector.
###
$.fn.exists = ->
  return @length != 0

###
# Checks whether a selector has found an element with the given attribute present.
# @param {String} name Name of the attribute.
###
$.fn.hasAttr = (name) ->
  return @attr(name) != undefined

$.fn.extend disable: (state) ->
  @each ->
    @disabled = state
    return

remove = (array, element) ->
  index = array.indexOf(element)
  if index != -1
    array.splice index, 1
  return

window.escapeJS = (str) ->
  return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');


###
# Given a pct in [0,1], will return true (pct * 100)% of the time.
# @example Usage
#  if(passedRoll(0.8))
#    console.log("80%% of the time, this block is called.");
# @param {float} pct Percent of the time that this roll will pass.
# @return {Boolean} True if random number is below pct.
###
window.passedRoll = (pct) ->
  if pct == 0
    return false
  if pct == 1
    return true
  return Math.random() <= pct

window.LOCALE_STRING_CONSIEE_ARGS =
  maximumFractionDigits: 2
  minimumFractionDigits: 2

window.formatConsiee = (value) ->
  return CONSIEE_SYMBOL + value.toLocaleString(undefined, LOCALE_STRING_CONSIEE_ARGS)

###
TODO
window.ENEMY = null
window.getEnemy: () ->
  if ENEMY == null
    ENEMY = new EnemyCharacter()
  return getPlayer()
###


EAlertType =
  WARNING: 'AddWarning'
  CRITICAL: 'AddCritical'
  GAIN: 'AddGain'
  ALERT: 'AddAlert'
  INFO: 'AddInfo'
  SUCCESS: 'AddSuccess'

###
# Utility for easily adding a message to the alert queue.
###
class AlertUtils
  @AddRaw: (raw_message) ->
    if getVariables().alerts == undefined or getVariables().alerts == 0
      getVariables()["alerts"] = []
    window.getVariables().alerts.push(raw_message)

  @Add: (alert_type, message) ->
    switch alert_type
      when EAlertType.WARNING then @AddWarning message
      when EAlertType.CRITICAL then @AddCritical message
      when EAlertType.GAIN then @AddGain message
      when EAlertType.ALERT then @AddAlert message
      when EAlertType.INFO then @AddInfo message
      when EAlertType.SUCCESS then @AddSuccess message


  #Yes, all these names are nonsensical as fuck.

  @AddWarning: (message) ->
    @AddRaw "[img[icon_alert_critical]]<brightalert>#{message}</brightalert><hr>"

  @AddCritical: (message) ->
    @AddRaw "[img[icon_alert_red]]<deepalert>#{message}</deepalert><hr>"

  @AddGain: (message) ->
    @AddRaw "[img[icon_gain]]#{message}<hr>"

  @AddAlert: (message) ->
    @AddRaw "[img[icon_alert]]#{message}<hr>"

  @AddSuccess: (message) ->
    @AddRaw "<affirmative>#{message}</affirmative><hr>"

  @AddInfo: (message) ->
    @AddAlert message

EMutbarWarningClasses =
  WARNING: 'stat-warn'
  BAD:     'stat-bad'
  VERYBAD: 'stat-very-bad'
AllMutbarWarnClasses=[EMutbarWarningClasses.WARNING, EMutbarWarningClasses.BAD, EMutbarWarningClasses.VERYBAD]

###
Basic garbage collection since Twine breaks fucking everything.
###
class JQHelper
  @Trash: []
  @GC: ->
    for entry in JQHelper.Trash
      [trash, cleanupFunc] = entry
      cleanupFunc($(entry))
    console.log "JQHelper.GC: Cleaned up %d jQuery objects.", JQHelper.Trash.length
    JQHelper.Trash.length = 0

  @_SelectAndPush: (selector, df) ->
    e = $(selector)
    JQHelper.Trash.push [e, df]
    return e


  ###
  Use ONLY for basic jquery stuff.
  ###
  @GetElement: (selector) ->
    return @_SelectAndPush selector, @_DestroyElement
  @_DestroyElement: (e) ->
    if e.exists()
      e.attr("id", null)
      e.remove()

  @GetSpinner: (selector) ->
    return @_SelectAndPush selector, @_DestroySpinner
  @_DestroySpinner: (spinner) ->
    if spinner.exists() and spinner.data('uiSpinner')
      spinner.spinner("destroy")
      spinner.attr("id", null)

  @GetDialog: (selector) ->
    return @_SelectAndPush selector, @_DestroyDialog
  @_DestroyDialog: (dlg) ->
    if dlg.exists() and dlg.data('uiDialog')
      dlg.dialog("destroy")
      dlg.attr("id", null)

  @GetProgressBar: (selector) ->
    return @_SelectAndPush selector, @_DestroyProgressBar
  @_DestroyProgressBar: (prg) ->
    if prg.exists() and prg.hasClass('ui-progressbar')
      prg.progressbar("destroy")
      prg.attr("id", null)
      prg.remove()

JQConfirm = (config) ->
  if !config.title
    config.title='Confirm'
  config.yes_text = if 'yes_text' of config then config.yes_text else 'OK'
  config.no_text = if 'no_text' of config then config.no_text else 'Cancel'
  btns = {}
  btns[config.yes_text] = (ui, e) ->
    if config.yes
      config.yes(ui, e)
    $(this).dialog('close')
  btns[config.no_text] = (ui, e) ->
    if config.no
      config.no(ui, e)
    $(this).dialog('close')
  $('<div></div>')
    .attr('title', config.title)
    .append($('<p></p>').html(config.text))
    .dialog
      resizable: false
      modal: true
      height: "auto"
      close: (ui, e) ->
        $(this).remove()
      buttons: btns

JQAlert = (config) ->
  if !config.title
    config.title='Alert'
  config.ok_text = if 'ok_text' of config then config.ok_text else 'OK'
  btns = {}
  btns[config.ok_text] = (ui, e) ->
    if config.ok
      config.ok(ui, e)
    $(this).dialog('close')
  $('<div></div>')
    .attr('title', config.title)
    .append($('<p></p>').html(config.text))
    .dialog
      resizable: false
      modal: true
      height: "auto"
      close: (ui, e) ->
        $(this).remove()
      buttons: btns
window.deserialize = (data) ->
  if data == null
    return null
  switch typeof data
    when 'Map', 'object', 'Object'
      if '_type' of data
        type_found = window[data['_type']]
        o = new type_found()
        o.deserialize(data)
        return o
      for k,v of data
        data[k] = deserialize v
    when 'Array'
      for i in [0...data.length]
        data[i] = deserialize data[i]
  return data

class MutBar
  constructor: (@ID, @Title, @Icon, @Mutagenic=false) ->
    @Text=''
    @WarnClass=null

  setWarnClass: (cls) ->
    @WarnClass = cls

  buildHTML: () ->
    barbody = $('<div class="sidebar-pip">')
    barbody.attr('id', @ID)
    if @WarnClass != null
      barbody.addClass @WarnClass
    head = $('<span>')
    head.addClass('head')
    if @Mutagenic
      mutagenspan = $('<span class="iconwrap mutagen">')
      mutagenspan.attr('title', 'Mutagen!')
      mutagenspan.append("[img[biohazard]]")
      head.append(mutagenspan)
    iconwrap = $('<span class="iconwrap">')
    iconwrap.attr('title', @Title)
    iconwrap.append("[img[#{@Icon}]]")
    head.append(iconwrap)

    barbody.append(head)

    barvalue = $('<span class="value">')
    barvalue.text(@Text)
    barbody.append(barvalue)
    return barbody

  toHTML: ->
    return jq2html(@buildHTML())

window.dropSpinner = (spinner) ->
  if spinner != null
    spinner.spinner("destroy")
    spinner.attr("id", null)

window.dropTabs = (control) ->
  if control != null
    control.tabs("destroy")
    control.attr("id", null)

window.dropButton = (button) ->
  if button != null
    button.attr("id", null)
    button.remove()

window.dropDialog = (dlg) ->
  if dlg != null
    dlg.dialog('destroy')
    dlg.attr("id", null)

window.andjoin = (entries, joinword='and') ->
  o = ''
  for i in [0...entries.length]
    if i > 0
      if i < (entries.length-1)
        o += ', '
      else
        o += ', '+joinword+' '
    o += entries[i]
  return o

window.orjoin = (entries) ->
  return andjoin(entries, 'or')

###
# Used for setter/getter support.
# @example
#   class MyClass
#      @property 'my_property',
#        get: ->
#          "return value"
#        set: (value) ->
#          # do something with value
###
Function::property = (prop, desc) ->
  Object.defineProperty @prototype, prop, desc

window.getImageBlob = (passage) ->
  # Base64 passage transclusion
  imgPassages = window.tale.lookup('tags', 'Twine.image')
  j = 0
  while j < imgPassages.length
    if imgPassages[j].title == passage
      return imgPassages[j].text
    j++
  return null

window.getPassageByID = (passage) ->
  return window.tale.passages[passage]

window.embedSVG = (passage, where=window.twineCurrentEl) ->
  div = document.createElement('div')
  div.innerHTML = getPassageByID(passage).text
  where.append div.firstChild

###
# Sanity check for numeric assignments.
###
window.assert_sane_number = (value, allow_nan=no, allow_null=no, allow_undefined=no) ->
  err_prefix = "assert_sane_number(value=#{value}, allow_nan=#{allow_nan}, allow_null=#{allow_null}, allow_undefined=#{allow_undefined}"
  if !allow_null and value == null
    console.error "#{err_prefix} - VALUE IS NULL"
    alert "BUG: Assignment to a variable was expected to be a number, but was actually null!\nDeets:\n  #{err_prefix}\nCheck console for stack trace."
    throw new Exception("Assertion failure.")
  if !allow_undefined and value == undefined
    console.error "#{err_prefix} - VALUE IS UNDEFINED"
    alert "BUG: Assignment to a variable was expected to be a number, but was actually undefined!\nDeets:\n  #{err_prefix}\nCheck console for stack trace."
    throw new Exception("Assertion failure.")
  if typeof value != 'number'
    console.error "#{err_prefix} - VALUE IS %s", typeof value
    alert "BUG: Assignment to a variable was expected to be a number, but was actually %s!\nDeets:\n  #{err_prefix}\nCheck console for stack trace.", typeof value
    throw new Exception("Assertion failure.")
  if !allow_nan and isNaN(value)
    console.error "#{err_prefix} - VALUE IS NULL"
    alert "BUG: Assignment to a variable was expected to be a number, but was actually null!\nDeets:\n  #{err_prefix}\nCheck console for stack trace."
    throw new Exception("Assertion failure.")
  return value
