###
# Converts MajorDenizen/MinorDenizens to Denizens object.
###
class SaveMigration_003_AddDenizens extends SaveMigration
  constructor: ->
    super(3, '0.5.0', 'Add Denizens')

  FixPlanet: (planet) ->
    if 'Denizens' of planet
      console.log "[Migration 3] Not converting %s (already converted)", planet.ID
      return planet
    possible_denizens = [planet.MajorDenizen].concat(planet.MinorDenizens)
    console.log "[Migration 3] Converting Planet %s (dz=%s)", planet.ID, possible_denizens
    available_denizens = possible_denizens.slice(0)
    planet.Denizens = {}
    potential=0.5
    for i in [0...possible_denizens.length]
      denid = pickndrop(available_denizens)
      if i < (possible_denizens.length - 1)
        potential /= 2
      planet.Denizens[denid] = {
        _type: "Denizen"
        ID: denid
        Likelihood: potential,
        Flags: EDenizenFlags.CIVILIAN
        Fatness: random(200, 250)
        Population: random(20000,500000)
      }
    delete planet['MajorDenizen']
    delete planet['MinorDenizens']
    return planet

  Upgrade: (savedata) ->
    savedata.variables.planet = @FixPlanet savedata.variables.planet
    for pid of savedata.variables.planets
      savedata.variables.planets[pid] = @FixPlanet savedata.variables.planets[pid]
    return savedata

ALL_MIGRATIONS.push(new SaveMigration_003_AddDenizens())
