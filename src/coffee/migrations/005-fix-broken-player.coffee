###
# Fixes a bug where $player loses _type.
###
class SaveMigration_005_FixBrokenPlayer extends SaveMigration
  constructor: ->
    super(5, '0.5.0', 'Fix broken player')

  Upgrade: (savedata) ->
    savedata.variables.player['_type'] = 'PlayerCharacter'
    if !('_type' of savedata.variables.player.Inventory)
      console.warn '_type of $player.Inventory set to "InventoryController"'
      savedata.variables.player.Inventory['_type'] = 'InventoryController'
    if !('_type' of savedata.variables.player.Chemicals)
      console.warn '_type of $player.Chemicals set to "ChemistryController"'
      savedata.variables.player.Chemicals['_type'] = 'ChemistryController'
    return savedata

ALL_MIGRATIONS.push(new SaveMigration_005_FixBrokenPlayer())
