class SaveMigration_002_OOPPlanets extends SaveMigration
  constructor: ->
    super(2, '0.5.0', 'OOP Planets')

  PreRegisterPlanet: (savedata, planet) ->
    oldID = planet.ID
    if !('planets' of savedata.variables)
      savedata.variables.planets={}
    while true
      planet.RollNewID()
      if !(planet.ID of savedata.variables.planets)
        break
    savedata.variables.planets[planet.ID] = planet
    console.log "[Migration 2] Registered planet %s (was: %s)", planet.ID, oldID

  Upgrade: (savedata) ->
    # Current planet
    curPlanetID = null
    curPlanet = null

    if 'planetClass' of savedata.variables
      cls = GetPlanetClassByName(savedata.variables.planetClass)
      planet = new cls()
      planet.Ambience = savedata.variables.planetAmbience
      planet.Atmosphere = savedata.variables.planetAtmosphere
      planet.Class = savedata.variables.planetClass
      planet.CoreFluid = savedata.variables.planetCoreFluid
      planet.CoreType = savedata.variables.planetCoreType
      planet.Humidity = savedata.variables.planetHumidity
      curPlanetID = planet.ID = savedata.variables.planetID
      planet.MajorDenizen = savedata.variables.majorDenizen
      planet.MinorDenizens = [savedata.variables.minorDenizen1,savedata.variables.minorDenizen2]
      planet.PopulationDensity = savedata.variables.popDensity
      planet.Temperature = savedata.variables.planetTemp
      planet.Type = savedata.variables.planetType
      planet.SurfaceLiquid = savedata.variables.planetFluid
      planet.Name = savedata.variables.planet
      planet.Flags |= EPlanetFlags.VISITED
      curPlanet = planet
      @PreRegisterPlanet(savedata, planet)
      savedata.variables.planet = curPlanet

    @DeleteVar savedata, 'majorDenizen' # majorDenizen
    @DeleteVar savedata, 'minorDenizen1' # minorDenizen1
    @DeleteVar savedata, 'minorDenizen2' # minorDenizen2
    @DeleteVar savedata, 'planetAmbience' # planetAmbience
    @DeleteVar savedata, 'planetAtmosphere' # planetAtmosphere
    @DeleteVar savedata, 'planetClass' # planetClass
    @DeleteVar savedata, 'planetCoreFluid' # planetCoreFluid
    @DeleteVar savedata, 'planetCoreType' # planetCoreType
    @DeleteVar savedata, 'planetHumidity' # planetHumidity
    @DeleteVar savedata, 'planetID' # planetID
    @DeleteVar savedata, 'planetTemp' # planetTemp
    @DeleteVar savedata, 'planetFluid' # planetTemp
    @DeleteVar savedata, 'planetType' # planetType
    @DeleteVar savedata, 'popDensity' # popDensity

    # Homeworld
    homePlanet = null
    homePlanetID = null
    if savedata.variables.hwPlanetID == curPlanetID
      curPlanet.Flags |= EPlanetFlags.HOMEWORLD
      savedata.variables.hwPlanetID = curPlanet.ID
      homePlanet = curPlanet
    else
      # Homeworld
      cls = GetPlanetClassByName(savedata.variables.hwClass)
      if cls == null
        console.error "[Migration 2] Unable to find planet class %s!", savedata.variables.hwClass
      planet = new cls(true)
      planet.Name = savedata.variables.homeWorld
      homePlanetID = planet.ID = savedata.variables.hwPlanetID
      planet.Class = savedata.variables.hwClass
      planet.Type = savedata.variables.hwType
      planet.Temperature = savedata.variables.hwTemp
      planet.SurfaceLiquid = savedata.variables.hwLiquid
      planet.Humidity = savedata.variables.hwHumid
      planet.Atmosphere = savedata.variables.hwAtmos
      planet.PopulationDensity = savedata.variables.hwPop
      planet.CoreType = savedata.variables.hwCore
      planet.CoreFluid = savedata.variables.hwCoreFluid
      planet.Ambience = savedata.variables.hwAmbience
      planet.MajorDenizen = savedata.variables.hwCiv1
      planet.MinorDenizens.push savedata.variables.hwCiv2
      planet.MinorDenizens.push savedata.variables.hwCiv3
      planet.Flags = EPlanetFlags.VISITED|EPlanetFlags.HOMEWORLD
      @PreRegisterPlanet(savedata,planet)
      homePlanet=planet
    savedata.variables.hwPlanetID = planet.ID

    @DeleteVar savedata, 'homeWorld' # Name
    @DeleteVar savedata, 'hwClass' # Class
    @DeleteVar savedata, 'hwType' # Type
    @DeleteVar savedata, 'hwTemp' # Temperature
    @DeleteVar savedata, 'hwHumid' # Humidity
    @DeleteVar savedata, 'hwAtmos' # Atmosphere
    @DeleteVar savedata, 'hwPop' # PopulationDensity
    @DeleteVar savedata, 'hwCore' # CoreType
    @DeleteVar savedata, 'hwCoreFluid' # CoreFluid
    @DeleteVar savedata, 'hwAmbience' # Ambience
    @DeleteVar savedata, 'hwCiv1' # MajorDenizen
    @DeleteVar savedata, 'hwCiv2' # MinorDenizens
    @DeleteVar savedata, 'hwCiv3' # MinorDenizens

    # Moundworld
    if savedata.variables.moundwPlanetID == homePlanetID
      homePlanet.Flags |= EPlanetFlags.HOMEWORLD
      savedata.variables.moundwPlanetID = homePlanet.ID
    else if savedata.variables.moundwPlanetID == curPlanetID
      curPlanet.Flags |= EPlanetFlags.MOUNDWORLD
      savedata.variables.moundwPlanetID = curPlanet.ID
    else
      if 'moundwPlanetID' in savedata.variables and savedata.variables.moundwPlanetID != null and savedata.variables.moundwPlanetID != undefined and savedata.variables.moundwPlanetID != 0
        cls = GetPlanetClassByName(savedata.variables.moundClass)
        if cls == null
          console.error "[Migration 2] Unable to find planet class %s!", savedata.variables.hwClass
        planet = new cls(false)
        planet.Name = savedata.variables.moundPlanet
        planet.ID = savedata.variables.moundwPlanetID
        planet.Class = savedata.variables.moundClass
        planet.Type = savedata.variables.moundwType
        planet.Temperature = savedata.variables.moundwTemp
        planet.Humidity = savedata.variables.moundwHumid
        planet.Atmosphere = savedata.variables.moundwAtmos
        planet.PopulationDensity = savedata.variables.moundwPop
        planet.CoreType = savedata.variables.moundwCore
        planet.LivingType = savedata.variables.moundwLivingType
        planet.Blood = savedata.variables.moundwPlanetBlood
        planet.CoreFluid = savedata.variables.moundwCoreFluid
        planet.Ambience = savedata.variables.moundwAmbience
        planet.MajorDenizen = savedata.variables.moundwCiv1
        planet.MinorDenizens.push savedata.variables.moundwCiv2
        planet.MinorDenizens.push savedata.variables.moundwCiv3
        planet.Flags = EPlanetFlags.VISITED|EPlanetFlags.MOUNDWORLD
        @PreRegisterPlanet(savedata,planet)
        savedata.variables.moundwPlanetID = planet.ID
      else
        console.log "[Migration 2] No mound planet detected, skipping."

    @DeleteVar savedata, 'moundPlanet' # Name
    @DeleteVar savedata, 'moundClass' # Class
    @DeleteVar savedata, 'moundwType' # Type
    @DeleteVar savedata, 'moundwTemp' # Temperature
    @DeleteVar savedata, 'moundwHumid' # Humidity
    @DeleteVar savedata, 'moundwAtmos' # Atmosphere
    @DeleteVar savedata, 'moundwPop' # PopulationDensity
    @DeleteVar savedata, 'moundwCore' # CoreType
    @DeleteVar savedata, 'moundwLivingType' # LivingType
    @DeleteVar savedata, 'moundwPlanetBlood' # Blood
    @DeleteVar savedata, 'moundwCoreFluid' # CoreFluid
    @DeleteVar savedata, 'moundwAmbience' # Ambience
    @DeleteVar savedata, 'moundwCiv1' # MajorDenizen
    @DeleteVar savedata, 'moundwCiv2' # MinorDenizens
    @DeleteVar savedata, 'moundwCiv3' # MinorDenizens

    @DeleteVar savedata, 'homeWorld' # homeWorld
    @DeleteVar savedata, 'hwAmbience' # hwAmbience
    @DeleteVar savedata, 'hwAtmos' # hwAtmos
    @DeleteVar savedata, 'hwCiv1' # hwCiv1
    @DeleteVar savedata, 'hwCiv2' # hwCiv2
    @DeleteVar savedata, 'hwCiv3' # hwCiv3
    @DeleteVar savedata, 'hwClass' # hwClass
    @DeleteVar savedata, 'hwCore' # hwCore
    @DeleteVar savedata, 'hwCoreFluid' # hwCoreFluid
    @DeleteVar savedata, 'hwHumid' # hwHumid
    @DeleteVar savedata, 'hwPop' # hwPop
    @DeleteVar savedata, 'hwTemp' # hwTemp
    @DeleteVar savedata, 'hwType' # hwType
    @DeleteVar savedata, 'majorDenizen' # majorDenizen
    @DeleteVar savedata, 'minorDenizen1' # minorDenizen1
    @DeleteVar savedata, 'minorDenizen2' # minorDenizen2
    @DeleteVar savedata, 'planetAmbience' # planetAmbience
    @DeleteVar savedata, 'planetAtmosphere' # planetAtmosphere
    @DeleteVar savedata, 'planetClass' # planetClass
    @DeleteVar savedata, 'planetCoreFluid' # planetCoreFluid
    @DeleteVar savedata, 'planetCoreType' # planetCoreType
    @DeleteVar savedata, 'planetHumidity' # planetHumidity
    @DeleteVar savedata, 'planetID' # planetID
    @DeleteVar savedata, 'planetTemp' # planetTemp
    @DeleteVar savedata, 'planetType' # planetType
    @DeleteVar savedata, 'popDensity' # popDensity
    return savedata

ALL_MIGRATIONS.push(new SaveMigration_002_OOPPlanets())
