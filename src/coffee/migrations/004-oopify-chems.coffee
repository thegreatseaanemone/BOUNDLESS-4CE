###
# Turns the chemical collection into a ChemicalController.
###
class SaveMigration_004_OOPifyChems extends SaveMigration
  constructor: ->
    super(4, '0.5.0', 'Make Chems OOP')

  Upgrade: (savedata) ->
    savedata.variables.chemicals['_type'] = 'ChemistryController'
    return savedata

ALL_MIGRATIONS.push(new SaveMigration_004_OOPifyChems())
