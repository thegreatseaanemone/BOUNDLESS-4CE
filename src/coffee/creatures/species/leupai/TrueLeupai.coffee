class TrueLeupaiSpecies extends Species
  constructor: ->
    super()
    @Name = 'Leupai'
    @ID = 'leupai'
    @Blurb = "UNKNOWN"
    @Portrait = 'player_leupai'
    @Flags = 0

Species.Register(TrueLeupaiSpecies)
