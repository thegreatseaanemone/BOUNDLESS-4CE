class HeleneSpecies extends Species
  constructor: ->
    super()
    @Name = 'Helene'
    @ID = 'helene'
    @Blurb = "UNKNOWN"
    @Portrait = 'player_helene'
    @Flags = 0

Species.Register(HeleneSpecies)
