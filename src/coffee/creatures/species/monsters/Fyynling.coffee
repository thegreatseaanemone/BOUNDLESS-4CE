class FyynlingSpecies extends Species
  constructor: ->
    super()
    @Name = 'Fyynling'
    @ID = 'fyynling'
    @Blurb = "?!"
    @Portrait = 'player_fyynling'

Species.Register(FyynlingSpecies)
