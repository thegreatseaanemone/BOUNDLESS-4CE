class AladriSpecies extends HumanoidSpecies
  constructor: ->
    super()
    @Name = 'Aladri'
    @ID = 'aladri'
    @Blurb = """
    <span class="cheery bold"><large>Aladri</large></span> are bipedal long-eared fey folk who stalk and protect
    the wildlands wherever they can be found. They can be quite fierce, but some say it's only
    <span class="affirmative bold">to ensure the survival of the things they love.</span>
    """
    @Portrait = 'player_aladri'
    @Flags |= SpeciesFlags.DEFAULTS_UNLOCKED
    @Flags |= SpeciesFlags.HAVE_TEETH
    #@Colors = []

    @Ears.Types = [FaeEar]
    @Eyes.Colors = ["black","indigo","violet","red","magenta","pink","orange","golden","yellow","green","teal","blue","silver","white"]

Species.Register(AladriSpecies)
