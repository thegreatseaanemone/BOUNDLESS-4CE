class PostHumanSpecies extends Species
  constructor: ->
    super()
    @Name = 'Post-Human'
    @ID = 'post-human'
    @Blurb = "<cheery>''<large>Post-humans</large>''</cheery> are a species of bipedal sapient creature with flat faces and rounded ears. They are a relatively frequent discovery throughout time and space, as they tend to get themselves into all sorts of trouble and mess about with everything. <affirmative>''That's what makes them so endearing.''</affirmative>"
    @Portrait = 'player_post-human'
    @Flags = SpeciesFlags.DEFAULTS_UNLOCKED

Species.Register(PostHumanSpecies)
