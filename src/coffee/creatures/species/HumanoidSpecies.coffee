###
# A species with a humanoid body plan.
###
class HumanoidSpecies extends Species
  constructor: ->
    super()

    # $withTeeth
    @Flags |= SpeciesFlags.HAS_TEETH

    # Convenient accessors.
    @Skin  = new SkinSpec
    @Heads = new HeadSpec
    @Ears  = new EarSpec
    @Eyes  = new EyeSpec
    @Arms  = new ArmSpec
    @Legs  = new LegSpec
    @Torso = new TorsoSpec

    @BodyPlan = [
      @Skin
      @Heads
      @Ears
      @Eyes
      @Arms
      @Legs
      @Torso
    ]

    @Torso.Types = [HumanoidTorso] # $playerBody new HumanoidTorso
    @Ears.SetCount 2 # $playerEarNumber
    @Eyes.SetCount 2 # $playerEyeNumber
    @Arms.SetCount 2 # $playerArmNumber
    @Legs.SetCount 2 # $playerLegNumber
