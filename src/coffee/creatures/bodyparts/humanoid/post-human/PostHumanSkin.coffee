class PostHumanSkinBodyPart extends SkinBodyPart
  constructor: ->
    super 'post-human skin'
    @Texture = 'smooth'
    @Color = 'pinkish'

  RollNewPart: ->
    @Color = either('pinkish', 'ivory', 'reddish-pink', 'tanned', 'light brown', 'brown', 'dark brown')

  GetReadoutText: ->
    return "#{@Color} #{@Name}"
