class TorsoBodyPart extends SimpleBodyPart
  constructor: (name) ->
    super name
    @Class = EBodyPartClass.TORSO
  clone: ->
    return super(new TorsoBodyPart(''))
