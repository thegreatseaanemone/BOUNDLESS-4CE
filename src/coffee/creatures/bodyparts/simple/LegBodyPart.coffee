class LegBodyPart extends SimpleBodyPart
  constructor: (name) ->
    super name
    @Class = EBodyPartClass.LEG
  clone: ->
    return super(new LegBodyPart(@Name))
