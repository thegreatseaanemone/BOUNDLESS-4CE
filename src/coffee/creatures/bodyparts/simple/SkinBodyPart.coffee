class SkinBodyPart extends SimpleBodyPart
  constructor: (name) ->
    super name
    @Class = EBodyPartClass.SKIN
    @Texture = ''
  clone: ->
    clone = super(new SkinBodyPart(@Name))
    clone.Texture=@Texture
    return clone

  serialize: ->
    data = super()
    data.Texture = @Texture
    return data

  deserialize: (data) ->
    super(data)
    if 'Texture' of data
      @Texture=data.Texture
