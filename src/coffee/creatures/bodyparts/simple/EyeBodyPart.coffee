class EyeBodyPart extends SimpleBodyPart
  constructor: (name) ->
    super name
    @Class = EBodyPartClass.EYE
  clone: ->
    return super(new EyeBodyPart(''))
