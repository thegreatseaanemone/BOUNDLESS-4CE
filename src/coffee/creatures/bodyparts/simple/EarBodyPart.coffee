class EarBodyPart extends SimpleBodyPart
  constructor: (name) ->
    super name
    @Class = EBodyPartClass.EAR
  clone: ->
    return super(new EarBodyPart(''))
