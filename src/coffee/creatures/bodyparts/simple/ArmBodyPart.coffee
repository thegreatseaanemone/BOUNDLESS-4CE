class ArmBodyPart extends SimpleBodyPart
  constructor: (name) ->
    super name
    @Class = EBodyPartClass.ARM
  clone: ->
    return super(new ArmBodyPart(''))
