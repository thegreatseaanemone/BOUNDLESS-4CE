###
#  An Identity is part of your Mind. It defines *who* you are, what you identify
# as (species, pronouns, etc), and maybe other things eventually.
###

class Identity extends GameObject
  constructor: (@Mind) ->
    ###
    # Your name.
    ###
    @Name = ''

    ###
    # Any pronouns you use.
    ###
    @Pronouns = new Pronouns()

    ###
    # Original Species ID
    ###
    @OriginalSpecies = ''

  clone: ->
    i = new Identity()
    i.Name = @Name
    i.Pronouns = @Pronouns.clone()
    i.OriginalSpecies = @OriginalSpecies
    return i

  serialize: ->
    data = super()
    data.name = @Name
    data.pronouns = @Pronouns.serialize()
    data.original_species = @OriginalSpecies
    return data

  deserialize: (data) ->
    super(data)
    @Name = data.name
    @OriginalSpecies = data.original_species
    @Pronouns.deserialize data.pronouns
