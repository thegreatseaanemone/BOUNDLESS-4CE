# Representation of all player pronouns.
class Pronouns extends GameObject
  @REPLACEMENT_MAPPINGS =
    mister: 'Title'
    title: 'Title'
    mr: 'TitleAbbr'
    titleabbr: 'TitleAbbr'
    they: 'Subject'
    subject: 'Subject'
    theyre: 'SubjectAre'
    subjectare: 'SubjectAre'
    object: 'Object'
    them: 'Object'
    ref: 'Referential'
    referential: 'Referential'
    their: 'Referential'
    dependentpossessive: 'DependentPossessive'
    dpos: 'DependentPossessive'
    theirs: 'DependentPossessive'
    independentpossessive: 'IndependentPossessive'
    ipos: 'IndependentPossessive'
    themself: 'IndependentPossessive'

  @FORMATTERS =
    u: (v) ->
      return v.toUpperCase()
    l: (v) ->
      return v.toLowerCase()
    z: (v) ->
      return zalgo v

  @PROPGEN = [
    'Mister'
    'Mr'
    'They'
    'Theyre'
    'Them'
    'Their'
    'Theirs'
    'Themself'
  ]

  @ALL = {}

  @Register: (PNC) ->
    PNCI = new PNC()
    Pronouns.ALL[PNCI.ID] = PNC

  constructor: ->
    # All these should be added in Title Case.
    @ID = ''
    @Title = '' # Mister
    @TitleAbbr = '' # Mr.
    @Subject = ''
    @SubjectAre = ''
    @Object = ''
    @Referential = ''
    @DependentPossessive = ''
    @IndependentPossessive = ''

  for k in Pronouns.PROPGEN
    lk = k.toLowerCase()
    Object.defineProperty @prototype, k, {get: ((_k, _this) ->
      ->
        _this.Get _k
    )(lk, this)}
    Object.defineProperty @prototype, k.toUpperCase(), {get: ((_k, _this) ->
      ->
        _this.Get _k, 'u'
    )(lk, this)}
    Object.defineProperty @prototype, k.toLowerCase(), {get: ((_k, _this) ->
      ->
        _this.Get _k, 'l'
    )(lk, this)}

  clone: ->
    clone = new Pronouns()
    clone.ID = @ID
    clone.Title = @Title
    clone.TitleAbbr = @TitleAbbr
    clone.Subject = @Subject
    clone.SubjectAre = @SubjectAre
    clone.Object = @Object
    clone.Referential = @Referential
    clone.DependentPossessive = @DependentPossessive
    clone.IndependentPossessive = @IndependentPossessive
    return clone

  serialize: ->
    data = super()
    data['ID'] = @ID
    data['Title'] = @Title
    data['TitleAbbr'] = @TitleAbbr
    data['Subject'] = @Subject
    data['SubjectAre'] = @SubjectAre
    data['Object'] = @Object
    data['Referential'] = @Referential
    data['DependentPossessive'] = @DependentPossessive
    data['IndependentPossessive'] = @IndependentPossessive
    return data

  deserialize: (data) ->
    super(data)
    @ID = data['ID']
    @Title = data['Title']
    @TitleAbbr = data['TitleAbbr']
    @Subject = data['Subject']
    @SubjectAre = data['SubjectAre']
    @Object = data['Object']
    @Referential = data['Referential']
    @DependentPossessive = data['DependentPossessive']
    @IndependentPossessive = data['IndependentPossessive']

  Get: (pid, formatting='') ->
    console.log pid, formatting
    if !(pid of Pronouns.REPLACEMENT_MAPPINGS)
      return null
    val = @[Pronouns.REPLACEMENT_MAPPINGS[pid]]
    if formatting and formatting.length > 0
      for c in formatting
        val = Pronouns.FORMATTERS[c](val)
    return val

  ReplaceIn: (html, prefix='') ->
    if prefix != ''
      prefix = prefix+'\\.'
    # {prefix.attr:formatOpts}
    regex = new RegExp "\\{#{prefix}([a-z]+)(:[^\\}]+)?\\}", "gi"
    console.log regex
    return html.replace regex, (match, pid, formatting, offset, wholestring) =>
      if !(pid of Pronouns.REPLACEMENT_MAPPINGS)
        return '<span class="twineError error">Bad PID '+pid+' in '+match+'!</span>'
      val = @[Pronouns.REPLACEMENT_MAPPINGS[pid]]
      if formatting and formatting.length > 0
        for c in formatting.slice(1)
          val = Pronouns.FORMATTERS[c](val)
      return val
