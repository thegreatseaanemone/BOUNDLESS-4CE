class FemininePronouns extends Pronouns
  constructor: ->
    @ID = 'feminine'
    # Not being presumptive as to marital status.
    @Title = 'Miss'
    @TitleAbbr = 'Ms.'
    @Subject = 'She'
    @SubjectAre = "She's"
    @Object = 'Her'
    @Referential = 'Her'
    @DependentPossessive = 'Hers'
    @IndependentPossessive = 'Herself'

Pronouns.Register FemininePronouns
