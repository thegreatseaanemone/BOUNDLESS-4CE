class MasculinePronouns extends Pronouns
  constructor: ->
    @ID = 'masculine'
    @Title = 'Mister'
    @TitleAbbr = 'Mr.'
    @Subject = 'He'
    @SubjectAre = "He's"
    @Object = 'Him'
    @Referential = 'His'
    @DependentPossessive = 'His'
    @IndependentPossessive = 'Himself'

Pronouns.Register MasculinePronouns
