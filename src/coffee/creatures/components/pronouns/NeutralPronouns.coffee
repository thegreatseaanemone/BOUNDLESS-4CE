class NeutralPronouns extends Pronouns
  constructor: ->
    @ID = 'neutral'
    @Title = 'Individual'
    @TitleAbbr = 'Ind.'
    @Subject = 'They'
    @SubjectAre = "They're"
    @Object = 'Them'
    @Referential = 'Their'
    @DependentPossessive = 'Theirs'
    @IndependentPossessive = 'Themself'

Pronouns.Register NeutralPronouns
