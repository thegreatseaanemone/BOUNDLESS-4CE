###
# Mental state, seperate from physical stuff.
###
class Mind extends GameObject
  constructor: () ->
    @Creature = null
    @Identity = new Identity(@)
    @Stats = new Stats(@)

  serialize: ->
    data = super()
    data.identity = @Identity.serialize()
    data.stats = @Stats.serialize()
    return data

  deserialize: (data) ->
    super(data)
    @Identity.deserialize data.identity
    @Stats.deserialize data.stats

  clone: (clone_creature) ->
    m = new Mind()
    m.Creature=clone_creature
    m.Identity=@Identity.clone(clone_creature)
    m.Identity.Mind = m
    m.Stats = @Stats.clone(clone_creature)
    return m
