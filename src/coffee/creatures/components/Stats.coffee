class Stats extends GameObject
  constructor: (@Creature) ->
    @Strength = new Stat @Creature

  clone: (cloned_creature)->
    s = new Stats cloned_creature
    return s

  serialize: ->
    o = super()
    o.Strength=@Strength.serialize()
    return o

  deserialize: (data) ->
    super(data)
    @Strength.deserialize data.Strength
