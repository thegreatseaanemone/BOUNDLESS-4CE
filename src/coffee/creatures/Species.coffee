# requires: creatures/bodyparts/BodyPart.coffee
# requires: creatures/bodyparts/simple/ArmBodyPart.coffee
# requires: creatures/bodyparts/simple/EarBodyPart.coffee
# requires: creatures/bodyparts/simple/EyeBodyPart.coffee
# requires: creatures/bodyparts/simple/HeadBodyPart.coffee
# requires: creatures/bodyparts/simple/LegBodyPart.coffee
# requires: creatures/bodyparts/simple/SkinBodyPart.coffee
# requires: creatures/bodyparts/simple/TorsoBodyPart.coffee

SpeciesFlags =
  DEFAULTS_UNLOCKED: 1
  HAS_TEETH:         2 # $withTeeth

# This is so weird.  It seems to just be used to determine what areas you can attack...?
# TODO: Completely redo this shit.
EEncounterStructure =
  STANDARD:   0
  HORIZONTAL: 'horizontal'
  VERTICAL:   'vertical'
  SPHERICAL:  'spherical'
  FACELESS:   'faceless'

EKillMode =
  NONE:      0
  DEFENSIVE: "defensive" # Runs away on win condition.
  BITER:     "biter"     # Eats you.
  SLAYER:    "slayer"    # Just kills you.


# Static object, don't serialize.
class BodyPartSpec
  constructor: ->
    @Class = EBodyPartClass.NONE
    # Acceptable types to use for this body part class.
    @Types = []
    # Antennae, horns, etc. (will randomly pick one for each body part)
    @Attachments=[]
    # Possible colors (will randomly pick one for each body part)
    @Colors=[]
    # Locations to attach to (will randomly pick one for each body part)
    @Locations=[]
    # Gain limbs until this cap is reached.
    @Min=1
    # Lose limbs until this cap is reached.
    @Max=Number.MAX_SAFE_INTEGER

  SetCount: (min, max=-1) ->
    if max == -1
      max=min
    @Min=min
    @Max=max

  ###
  # Immediate effects, such as growing swapping out human arms for arka ones, growing fur, etc.
  ###
  ApplyTo: (creature, skip_tf=false) ->
    if @Types.length <= 0
      console.log "#{@constructor.name}.Types == 0"
      return
    typeInstance = null # HACK around a coffeescript bug.
    acceptable_type_names = (typeInstance.name for typeInstance in @Types when typeInstance)
    nBodyParts = 0
    if creature.BodyParts.length > 0
      for bodyPart of creature.GetAllBodyPartsOfClass(@Class)
        if bodyPart.constructor.name in acceptable_type_names
          continue
        nBodyParts++
        chosen_type = either @Types
        newBodyPart = new chosen_type creature
        newBodyPart.TransformFrom bodyPart, skip_tf
        if bodyPart in creature.BodyParts
          creature.RemoveBodyPart bodyPart
        creature.AddBodyPart newBodyPart
    nNewBodyParts=0
    if nBodyParts < @Min
      while nBodyParts < @Min
        chosen_type = either @Types
        if !chosen_type
          console.warn "Could not choose a new BodyPart in #{@constructor.name}!", @Types
          break
        bp = new chosen_type creature
        bp.RollNewPart()
        creature.AddBodyPart(bp)
        nNewBodyParts++
        nBodyParts++


  WorkOn: (creature) ->
    n = creature.CountBodyParts(@Class, skip_tf=false)
    if n > @Max
      # Too many, drop one.
      targets = creature.GetAllBodyPartsOfClass(@Class)
      target = either(targets)
      creature.RemoveBodyPart target
      if creature.IsPC() and !skip_tf
        AlertUtils.AddWarning "You jump in surprise as you watch your #{target.Name} shrivel away!"
    if n < @Min
      # Too few, add one.
      creature.AddBodyPart either(@Types)
      if creature.IsPC() and !skip_tf
        AlertUtils.AddWarning "You watch in horror as you grow a new #{target.Name}!"


class HeadSpec extends BodyPartSpec
  constructor: ->
    super()
    @Class = EBodyPartClass.HEAD
    @Types = [HeadBodyPart]
    @Locations = [EBodyPartLocation.SHOULDERS]
    @Neck = true
    @NeckLength = ['normal']


class EarSpec extends BodyPartSpec
  constructor: ->
    super()
    @Types = [EarBodyPart]
    @Class = EBodyPartClass.EAR
    @Locations = [EBodyPartLocation.HEAD]


class EyeSpec extends BodyPartSpec
  constructor: ->
    super()
    @Types = [EyeBodyPart]
    @Class = EBodyPartClass.EYE
    @Locations = [EBodyPartLocation.HEAD]

class ArmSpec extends BodyPartSpec
  constructor: ->
    super()
    @Types = [ArmBodyPart]
    @Class = EBodyPartClass.ARM
    @Locations = [EBodyPartLocation.TORSO]

class LegSpec extends BodyPartSpec
  constructor: ->
    super()
    @Types = [LegBodyPart]
    @Class = EBodyPartClass.LEG
    @Locations = [EBodyPartLocation.TORSO]

class TorsoSpec extends BodyPartSpec
  constructor: ->
    super()
    @Types = [TorsoBodyPart]
    @Class = EBodyPartClass.TORSO
    @Locations = [""]

class SkinSpec extends BodyPartSpec
  constructor: ->
    super()
    @Types = [SkinBodyPart]
    @Texture = 'TEXTURE'
    @Class = EBodyPartClass.SKIN
    @Locations = [""]

###
# This is basically the part of your DNA that defines what it is to be a member of your species.
#
# Static object, no need to fuck around with serialization
###
class Species
  @ALL_SPECIES: {}

  constructor: ->
    @Name = ''
    @Blurb = ''
    @ID = ''
    @Portrait = ''
    @PossibleSubspecies = []
    @Flags = 0

    ###
    # Name of the race, used for display.
    # @example
    #  class FartDragon extends Creature
    #    constructor: ->
    #      super 'fart dragon' # Sets @Race.
    # @oldcode $encounterRace = 0
    ###
    @Race = ''

    ###
    # This is an odd one.  It's /set/ to -1 in some creatures, but the code
    # /expects/ a /string/!
    #
    # Note: "rare" causes a rare star icon to appear before its name.
    #
    # @oldcode $creatureRarity = 0
    ###
    @Rarity = 0

    ###
    # Moves the creature can do.
    #
    # @fixme Needs a complete rework. It uses magic fucking numbers.
    # @oldcode $moveset
    ###
    @MoveSet = []

    ###
    # Determines how the creature acts after it wins combat.
    #
    # Should probably be moved to OnWon() or something.
    # @type EKillMode
    # @oldcode $killmode
    ###
    @KillMode = EKillMode.NONE

    ###
    # Possible {Item}s presented by GetLoot().
    ###
    @PossibleLoot=[]

    ###
    # Special moves..?
    # @oldcode $encounterSpecial
    ###
    @Specials = []

    ###
    # The image of the creature shown during the encounter.
    # @oldcode $encImg
    ###
    @Image = 0

    ###
    # Possible colors.
    ###
    @Colors = []

    ###
    # Overall body plan.
    # @oldcode $playerBody
    ###
    @BodyShape = ''

    ###
    # @oldcode $playerBody etc
    ###
    @BodyPlan = [
      #@Heads
      #@Ears
      #@Eyes
      #@Arms
      #@Legs
      #@Torso
      # Additional organs go here.
    ]

  @Register: (speciesFunc) ->
    spec = new speciesFunc()
    @ALL_SPECIES[spec.ID]=spec

  @GetByID: (ID) ->
    return @ALL_SPECIES[ID]

  Become: (creature, skip_tf=false) ->
    creature.Species = @
    if @Colors.length > 0
      creature.Color = either @Colors
    for spec in @BodyPlan
      spec.ApplyTo creature, skip_tf

  InitialBodySetup: (creature) ->
    return
