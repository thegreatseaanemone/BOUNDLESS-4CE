These files are made using Inkscape's tracing function.  Any hand-made replacements would be *very* much welcome.

# Current Exceptions
* bg_blank.svg

# Procedure

This is mostly so I can remember WTF I did.  Using Inkscape 0.92 on Windows:

1. Open the PNG from `old/backgrounds/` directly in Inkscape. (Do not import)
2. Use the following settings:
  * Embed
  * DPI From File
  * optimizeSpeed
3. Path > Trace Bitmap (CTRL+ALT+B)
4. Mode Tab:
  * :radio_button: Colors
  * Scans: 64
  * UNCHECK Smooth.
  * CHECK Stack.
  * CHECK Remove Background.
5. Options Tab: (These are all defaults)
  * CHECK Suppress Speckles, Size=2
  * CHECK Smooth Corners, Threshold=1
  * CHECK Optimize Paths, Tolerance=0.20
6. Press OK
7. Save to `src/svg/backgrounds/`.
8. Run `BUILD.py` to build a minified SVG and corresponding .header.yml file.
