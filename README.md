# END OF LIFE
This project is now archived due to a clarification of the terms of approval from the original developer. 

See [this blog post](https://venturega.me/index.php/2018/03/14/boundless-end-of-life/) for more information.

# BOUNDLESS - 4 Chins Edition
**WARNING:** This game does not contain sexual content, *but is intended for adults only*.

This repository houses the source code of the 4CE mod for BOUNDLESS, which is an
adult-oriented role-playing game (RPG) with an emphasis on weight-gain. This mod tries to correct some bugs, design issues, and overall improve quality-of-life (QoL) within
the game.

For more information about BOUNDLESS 4CE, as well as downloads, please visit [the website](http://venturega.me/boundless).

## Legal
[BOUNDLESS](http://boundless-rpg.com) is a creation and copyright of [Revecroix](http://leupai.tumblr.com), who has authorized this mod, but is not currently affiliated with it.

Licensing is currently uncertain, but assumed to be CC-BY-SA-NC, as
Twine is, by its own nature, open-source.

## Compiling
This game requires the Nylon Toolkit for Twine 1.4.2, due to numerous hacks that have had to be done to Twine to support OOP and jQuery UI, in addition to the different file and metadata storage method.

Nylon is still being packaged for FOSS release.  Stay tuned.

## Developing

1. Install [Atom](http://atom.io).
2. Run `apm install https://gitlab.com/VentureGuy/language-sugar-cane.git` to install the sugarcane language.
3. Run `atom /path/to/BOUNDLESS`

The code is organized thusly:

<table>
<tr><th>`src/coffee`</th><td>Contains the sourcecode to OOP objects used in
4CE (currently just machines), as well as save management code and utilities. These are embedded into the HTML outside of Twine.</td></tr>
<tr><th>`src/python`</th><td>Contain Nylon BuildTarget classes and code generators.</td></tr>
<tr><th>`src/tiddler/css`</th><td>CSS stylesheets for BOUNDLESS.</td></tr>
<tr><th>`src/tiddler/images`</th><td>Images for BOUNDLESS.</td></tr>
<tr><th>`src/tiddler/passages`</th><td>\*.tw are Sugarcane passages for BOUNDLESS, while the \*.header.yml files are metadata normally stored inside the .tws, used by Nylon when building the game.</td></tr>
<tr><th>`src/tiddler/scripts`</th><td>Twine-executed Javascript.  Being phased out.</td></tr>
</table>

MRs are welcome.
