{{SpoilerStart}}
{{Note|Generated automatically from NamelessResponses.tw on 2018-02-02 19:16:57.588400.}}
{{AdministreCommand
|Name=fight me
|Desc=...
|Aliases=
* challenge
* fight
* fight me
* i'll kick your ass
* throw down
* throwdown
* u wanna go
* you wanna go
}}
{{AdministreCommand
|Name=is pain necessary?
|Desc=
|Aliases=
* is pain necessary
* is pain necessary?
* is suffering necessary
* is suffering necessary?
}}
{{AdministreCommand
|Name=pain is unnecessary
|Desc=
|Aliases=
* is pain necessary
* is pain necessary?
* is suffering necessary
* is suffering necessary?
* pain is unnecessary
* suffering is unnecessary
}}
{{AdministreCommand
|Name=pronoun
|Desc=Merciful Mode
|Aliases=
* change pronouns
* pronoun
* pronouns
* set pronouns
}}
{{AdministreCommand
|Name=rename
|Desc=change pronouns
|Aliases=
* change name
* name change
* namechange
* rename
}}
{{SpoilerEnd}}
