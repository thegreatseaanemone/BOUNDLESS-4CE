import os
import yaml
import sys
import re
import shutil


ORIG_BASE = os.path.join('projects', 'orig')
PROJ_BASE = os.path.join('projects', 'hacked')
sys.path.append(os.path.join('src', 'python'))
sys.path.append(os.path.join(PROJ_BASE, 'src', 'python'))

from buildtools import log
from twinehack import get_file_list



def load_replacements():
    REPLACEMENTS = []
    with log.info('Loading items.yml...'):
        with open(os.path.join(PROJ_BASE, 'src', 'data', 'items.yml'), 'r') as f:
            for key, item in yaml.load(f).items():
                needle = re.compile(r'\$' + item['ID'] + r' *\+= *([^;>\]]+)')
                repl = '$player.AddItem(' + item['ClassName'] + ', \\g<1>)'
                REPLACEMENTS.append((needle, repl))
                needle = re.compile(r'\$' + item['ID'] + r' *\-= *([^;>\]]+)')
                repl = '$player.RemoveItem(' + item['ClassName'] + ', \\g<1>)'
                REPLACEMENTS.append((needle, repl))
                needle = re.compile(r'\$' + item['ID'] + r'\b')
                repl = '$player.GetItemCount(' + item['ClassName'] + ')'
                REPLACEMENTS.append((needle, repl))
        log.info('Loaded and compiled %d replacements.', len(REPLACEMENTS))
    return REPLACEMENTS
def do_changeover(filename, replacements):
    changes = 0
    filenamenew = filename + '.newinv'
    filenamebak = filename + '.bak'
    if os.path.isfile(filenamenew):
        log.info('rm %s', filenamenew)
        os.remove(filenamenew)
    if os.path.isfile(filenamebak):
        log.info('rm %s', filenamebak)
        os.remove(filenamebak)
    with open(filename, 'r', encoding='utf-8-sig') as inf:
        with open(filenamenew, 'w', encoding='utf-8-sig') as outf:
            '''
            for line in inf:
                for needle, repl in replacements.items():
                    origl = line
                    line = re.sub(needle, repl, line)
                    if line != origl:
                        changes += 1
                outf.write(line)
            '''
            data = inf.read()
            for needle, repl in replacements:
                data, newchanges = needle.subn(repl, data)
                changes += newchanges
            outf.write(data)
    if changes == 0:
        os.remove(filenamenew)
    else:
        log.info('CONVERT %s: %d changes', filename, changes)
        shutil.move(filename, filenamebak)
        shutil.move(filenamenew, filename)
if __name__ == '__main__':
    REPLACEMENTS = load_replacements()
    with log.info('Converting...'):
        for filename in get_file_list(PROJ_BASE, prefix=PROJ_BASE):
            if filename.endswith('.tw'):
                do_changeover(filename, REPLACEMENTS)
